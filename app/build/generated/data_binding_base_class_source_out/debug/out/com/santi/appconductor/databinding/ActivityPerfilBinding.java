// Generated by view binder compiler. Do not edit!
package com.santi.appconductor.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.google.android.material.card.MaterialCardView;
import com.santi.appconductor.R;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityPerfilBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final MaterialCardView card0;

  @NonNull
  public final MaterialCardView card1;

  @NonNull
  public final MaterialCardView card2;

  @NonNull
  public final MaterialCardView card3;

  @NonNull
  public final MaterialCardView card4;

  @NonNull
  public final MaterialCardView card5;

  @NonNull
  public final MaterialCardView card6;

  @NonNull
  public final CircleImageView cvFoto;

  @NonNull
  public final ImageView iconoApellido;

  @NonNull
  public final ImageView iconoAuto;

  @NonNull
  public final ImageView iconoCorreo;

  @NonNull
  public final ImageView iconoDocumento;

  @NonNull
  public final ImageView iconoFoto;

  @NonNull
  public final ImageView iconoNombre;

  @NonNull
  public final ImageView iconoNumero;

  @NonNull
  public final ImageView ircorreo;

  @NonNull
  public final ImageView irdocuemtno;

  @NonNull
  public final ImageView irfoto;

  @NonNull
  public final ImageView irtelefono;

  @NonNull
  public final ImageView irvehiculo;

  @NonNull
  public final TextView itemActualizar;

  @NonNull
  public final TextView itemActualizardocuemtno;

  @NonNull
  public final TextView itemApellido;

  @NonNull
  public final TextView itemCorreo;

  @NonNull
  public final TextView itemFoto;

  @NonNull
  public final TextView itemNombre;

  @NonNull
  public final TextView itemNumero;

  @NonNull
  public final LinearLayout lineDetalle;

  @NonNull
  public final LinearLayout lineHeader;

  @NonNull
  public final ConstraintLayout linearCorreo;

  @NonNull
  public final ConstraintLayout linearDocuemento;

  @NonNull
  public final ConstraintLayout linearFoto;

  @NonNull
  public final ConstraintLayout linearNumero;

  @NonNull
  public final ConstraintLayout linearVehiculo;

  @NonNull
  public final LinearLayout llPersonales;

  @NonNull
  public final RatingBar ratingBar;

  @NonNull
  public final ToolbarAtrasBinding toolbarAtras;

  private ActivityPerfilBinding(@NonNull RelativeLayout rootView, @NonNull MaterialCardView card0,
      @NonNull MaterialCardView card1, @NonNull MaterialCardView card2,
      @NonNull MaterialCardView card3, @NonNull MaterialCardView card4,
      @NonNull MaterialCardView card5, @NonNull MaterialCardView card6,
      @NonNull CircleImageView cvFoto, @NonNull ImageView iconoApellido,
      @NonNull ImageView iconoAuto, @NonNull ImageView iconoCorreo,
      @NonNull ImageView iconoDocumento, @NonNull ImageView iconoFoto,
      @NonNull ImageView iconoNombre, @NonNull ImageView iconoNumero, @NonNull ImageView ircorreo,
      @NonNull ImageView irdocuemtno, @NonNull ImageView irfoto, @NonNull ImageView irtelefono,
      @NonNull ImageView irvehiculo, @NonNull TextView itemActualizar,
      @NonNull TextView itemActualizardocuemtno, @NonNull TextView itemApellido,
      @NonNull TextView itemCorreo, @NonNull TextView itemFoto, @NonNull TextView itemNombre,
      @NonNull TextView itemNumero, @NonNull LinearLayout lineDetalle,
      @NonNull LinearLayout lineHeader, @NonNull ConstraintLayout linearCorreo,
      @NonNull ConstraintLayout linearDocuemento, @NonNull ConstraintLayout linearFoto,
      @NonNull ConstraintLayout linearNumero, @NonNull ConstraintLayout linearVehiculo,
      @NonNull LinearLayout llPersonales, @NonNull RatingBar ratingBar,
      @NonNull ToolbarAtrasBinding toolbarAtras) {
    this.rootView = rootView;
    this.card0 = card0;
    this.card1 = card1;
    this.card2 = card2;
    this.card3 = card3;
    this.card4 = card4;
    this.card5 = card5;
    this.card6 = card6;
    this.cvFoto = cvFoto;
    this.iconoApellido = iconoApellido;
    this.iconoAuto = iconoAuto;
    this.iconoCorreo = iconoCorreo;
    this.iconoDocumento = iconoDocumento;
    this.iconoFoto = iconoFoto;
    this.iconoNombre = iconoNombre;
    this.iconoNumero = iconoNumero;
    this.ircorreo = ircorreo;
    this.irdocuemtno = irdocuemtno;
    this.irfoto = irfoto;
    this.irtelefono = irtelefono;
    this.irvehiculo = irvehiculo;
    this.itemActualizar = itemActualizar;
    this.itemActualizardocuemtno = itemActualizardocuemtno;
    this.itemApellido = itemApellido;
    this.itemCorreo = itemCorreo;
    this.itemFoto = itemFoto;
    this.itemNombre = itemNombre;
    this.itemNumero = itemNumero;
    this.lineDetalle = lineDetalle;
    this.lineHeader = lineHeader;
    this.linearCorreo = linearCorreo;
    this.linearDocuemento = linearDocuemento;
    this.linearFoto = linearFoto;
    this.linearNumero = linearNumero;
    this.linearVehiculo = linearVehiculo;
    this.llPersonales = llPersonales;
    this.ratingBar = ratingBar;
    this.toolbarAtras = toolbarAtras;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityPerfilBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityPerfilBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_perfil, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityPerfilBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.card0;
      MaterialCardView card0 = ViewBindings.findChildViewById(rootView, id);
      if (card0 == null) {
        break missingId;
      }

      id = R.id.card1;
      MaterialCardView card1 = ViewBindings.findChildViewById(rootView, id);
      if (card1 == null) {
        break missingId;
      }

      id = R.id.card2;
      MaterialCardView card2 = ViewBindings.findChildViewById(rootView, id);
      if (card2 == null) {
        break missingId;
      }

      id = R.id.card3;
      MaterialCardView card3 = ViewBindings.findChildViewById(rootView, id);
      if (card3 == null) {
        break missingId;
      }

      id = R.id.card4;
      MaterialCardView card4 = ViewBindings.findChildViewById(rootView, id);
      if (card4 == null) {
        break missingId;
      }

      id = R.id.card5;
      MaterialCardView card5 = ViewBindings.findChildViewById(rootView, id);
      if (card5 == null) {
        break missingId;
      }

      id = R.id.card6;
      MaterialCardView card6 = ViewBindings.findChildViewById(rootView, id);
      if (card6 == null) {
        break missingId;
      }

      id = R.id.cv_foto;
      CircleImageView cvFoto = ViewBindings.findChildViewById(rootView, id);
      if (cvFoto == null) {
        break missingId;
      }

      id = R.id.icono_apellido;
      ImageView iconoApellido = ViewBindings.findChildViewById(rootView, id);
      if (iconoApellido == null) {
        break missingId;
      }

      id = R.id.icono_auto;
      ImageView iconoAuto = ViewBindings.findChildViewById(rootView, id);
      if (iconoAuto == null) {
        break missingId;
      }

      id = R.id.icono_correo;
      ImageView iconoCorreo = ViewBindings.findChildViewById(rootView, id);
      if (iconoCorreo == null) {
        break missingId;
      }

      id = R.id.icono_documento;
      ImageView iconoDocumento = ViewBindings.findChildViewById(rootView, id);
      if (iconoDocumento == null) {
        break missingId;
      }

      id = R.id.icono_foto;
      ImageView iconoFoto = ViewBindings.findChildViewById(rootView, id);
      if (iconoFoto == null) {
        break missingId;
      }

      id = R.id.icono_nombre;
      ImageView iconoNombre = ViewBindings.findChildViewById(rootView, id);
      if (iconoNombre == null) {
        break missingId;
      }

      id = R.id.icono_numero;
      ImageView iconoNumero = ViewBindings.findChildViewById(rootView, id);
      if (iconoNumero == null) {
        break missingId;
      }

      id = R.id.ircorreo;
      ImageView ircorreo = ViewBindings.findChildViewById(rootView, id);
      if (ircorreo == null) {
        break missingId;
      }

      id = R.id.irdocuemtno;
      ImageView irdocuemtno = ViewBindings.findChildViewById(rootView, id);
      if (irdocuemtno == null) {
        break missingId;
      }

      id = R.id.irfoto;
      ImageView irfoto = ViewBindings.findChildViewById(rootView, id);
      if (irfoto == null) {
        break missingId;
      }

      id = R.id.irtelefono;
      ImageView irtelefono = ViewBindings.findChildViewById(rootView, id);
      if (irtelefono == null) {
        break missingId;
      }

      id = R.id.irvehiculo;
      ImageView irvehiculo = ViewBindings.findChildViewById(rootView, id);
      if (irvehiculo == null) {
        break missingId;
      }

      id = R.id.itemActualizar;
      TextView itemActualizar = ViewBindings.findChildViewById(rootView, id);
      if (itemActualizar == null) {
        break missingId;
      }

      id = R.id.itemActualizardocuemtno;
      TextView itemActualizardocuemtno = ViewBindings.findChildViewById(rootView, id);
      if (itemActualizardocuemtno == null) {
        break missingId;
      }

      id = R.id.itemApellido;
      TextView itemApellido = ViewBindings.findChildViewById(rootView, id);
      if (itemApellido == null) {
        break missingId;
      }

      id = R.id.itemCorreo;
      TextView itemCorreo = ViewBindings.findChildViewById(rootView, id);
      if (itemCorreo == null) {
        break missingId;
      }

      id = R.id.itemFoto;
      TextView itemFoto = ViewBindings.findChildViewById(rootView, id);
      if (itemFoto == null) {
        break missingId;
      }

      id = R.id.itemNombre;
      TextView itemNombre = ViewBindings.findChildViewById(rootView, id);
      if (itemNombre == null) {
        break missingId;
      }

      id = R.id.itemNumero;
      TextView itemNumero = ViewBindings.findChildViewById(rootView, id);
      if (itemNumero == null) {
        break missingId;
      }

      id = R.id.lineDetalle;
      LinearLayout lineDetalle = ViewBindings.findChildViewById(rootView, id);
      if (lineDetalle == null) {
        break missingId;
      }

      id = R.id.lineHeader;
      LinearLayout lineHeader = ViewBindings.findChildViewById(rootView, id);
      if (lineHeader == null) {
        break missingId;
      }

      id = R.id.linear_correo;
      ConstraintLayout linearCorreo = ViewBindings.findChildViewById(rootView, id);
      if (linearCorreo == null) {
        break missingId;
      }

      id = R.id.linear_docuemento;
      ConstraintLayout linearDocuemento = ViewBindings.findChildViewById(rootView, id);
      if (linearDocuemento == null) {
        break missingId;
      }

      id = R.id.linear_foto;
      ConstraintLayout linearFoto = ViewBindings.findChildViewById(rootView, id);
      if (linearFoto == null) {
        break missingId;
      }

      id = R.id.linear_numero;
      ConstraintLayout linearNumero = ViewBindings.findChildViewById(rootView, id);
      if (linearNumero == null) {
        break missingId;
      }

      id = R.id.linear_vehiculo;
      ConstraintLayout linearVehiculo = ViewBindings.findChildViewById(rootView, id);
      if (linearVehiculo == null) {
        break missingId;
      }

      id = R.id.llPersonales;
      LinearLayout llPersonales = ViewBindings.findChildViewById(rootView, id);
      if (llPersonales == null) {
        break missingId;
      }

      id = R.id.ratingBar;
      RatingBar ratingBar = ViewBindings.findChildViewById(rootView, id);
      if (ratingBar == null) {
        break missingId;
      }

      id = R.id.toolbar_atras;
      View toolbarAtras = ViewBindings.findChildViewById(rootView, id);
      if (toolbarAtras == null) {
        break missingId;
      }
      ToolbarAtrasBinding binding_toolbarAtras = ToolbarAtrasBinding.bind(toolbarAtras);

      return new ActivityPerfilBinding((RelativeLayout) rootView, card0, card1, card2, card3, card4,
          card5, card6, cvFoto, iconoApellido, iconoAuto, iconoCorreo, iconoDocumento, iconoFoto,
          iconoNombre, iconoNumero, ircorreo, irdocuemtno, irfoto, irtelefono, irvehiculo,
          itemActualizar, itemActualizardocuemtno, itemApellido, itemCorreo, itemFoto, itemNombre,
          itemNumero, lineDetalle, lineHeader, linearCorreo, linearDocuemento, linearFoto,
          linearNumero, linearVehiculo, llPersonales, ratingBar, binding_toolbarAtras);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
