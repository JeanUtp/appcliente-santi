// Generated by view binder compiler. Do not edit!
package com.santi.appconductor.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.santi.appconductor.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityInicioBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final Button btnComenzar;

  @NonNull
  public final ImageView ivLogo2;

  @NonNull
  public final ImageView ivLogocara;

  @NonNull
  public final RelativeLayout rl;

  @NonNull
  public final TextView tv;

  @NonNull
  public final TextView tv2;

  @NonNull
  public final ImageView wave;

  private ActivityInicioBinding(@NonNull RelativeLayout rootView, @NonNull Button btnComenzar,
      @NonNull ImageView ivLogo2, @NonNull ImageView ivLogocara, @NonNull RelativeLayout rl,
      @NonNull TextView tv, @NonNull TextView tv2, @NonNull ImageView wave) {
    this.rootView = rootView;
    this.btnComenzar = btnComenzar;
    this.ivLogo2 = ivLogo2;
    this.ivLogocara = ivLogocara;
    this.rl = rl;
    this.tv = tv;
    this.tv2 = tv2;
    this.wave = wave;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityInicioBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityInicioBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_inicio, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityInicioBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.btnComenzar;
      Button btnComenzar = ViewBindings.findChildViewById(rootView, id);
      if (btnComenzar == null) {
        break missingId;
      }

      id = R.id.ivLogo2;
      ImageView ivLogo2 = ViewBindings.findChildViewById(rootView, id);
      if (ivLogo2 == null) {
        break missingId;
      }

      id = R.id.ivLogocara;
      ImageView ivLogocara = ViewBindings.findChildViewById(rootView, id);
      if (ivLogocara == null) {
        break missingId;
      }

      id = R.id.rl;
      RelativeLayout rl = ViewBindings.findChildViewById(rootView, id);
      if (rl == null) {
        break missingId;
      }

      id = R.id.tv;
      TextView tv = ViewBindings.findChildViewById(rootView, id);
      if (tv == null) {
        break missingId;
      }

      id = R.id.tv2;
      TextView tv2 = ViewBindings.findChildViewById(rootView, id);
      if (tv2 == null) {
        break missingId;
      }

      id = R.id.wave;
      ImageView wave = ViewBindings.findChildViewById(rootView, id);
      if (wave == null) {
        break missingId;
      }

      return new ActivityInicioBinding((RelativeLayout) rootView, btnComenzar, ivLogo2, ivLogocara,
          rl, tv, tv2, wave);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
