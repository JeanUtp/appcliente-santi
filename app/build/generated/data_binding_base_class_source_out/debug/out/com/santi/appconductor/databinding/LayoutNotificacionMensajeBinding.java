// Generated by view binder compiler. Do not edit!
package com.santi.appconductor.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.santi.appconductor.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class LayoutNotificacionMensajeBinding implements ViewBinding {
  @NonNull
  private final LinearLayout rootView;

  @NonNull
  public final Button btnAceptar;

  @NonNull
  public final TextView tvMensaje;

  @NonNull
  public final TextView tvTitulo;

  private LayoutNotificacionMensajeBinding(@NonNull LinearLayout rootView,
      @NonNull Button btnAceptar, @NonNull TextView tvMensaje, @NonNull TextView tvTitulo) {
    this.rootView = rootView;
    this.btnAceptar = btnAceptar;
    this.tvMensaje = tvMensaje;
    this.tvTitulo = tvTitulo;
  }

  @Override
  @NonNull
  public LinearLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static LayoutNotificacionMensajeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static LayoutNotificacionMensajeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.layout_notificacion_mensaje, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static LayoutNotificacionMensajeBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.btnAceptar;
      Button btnAceptar = ViewBindings.findChildViewById(rootView, id);
      if (btnAceptar == null) {
        break missingId;
      }

      id = R.id.tvMensaje;
      TextView tvMensaje = ViewBindings.findChildViewById(rootView, id);
      if (tvMensaje == null) {
        break missingId;
      }

      id = R.id.tvTitulo;
      TextView tvTitulo = ViewBindings.findChildViewById(rootView, id);
      if (tvTitulo == null) {
        break missingId;
      }

      return new LayoutNotificacionMensajeBinding((LinearLayout) rootView, btnAceptar, tvMensaje,
          tvTitulo);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
