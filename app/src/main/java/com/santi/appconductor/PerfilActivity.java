package com.santi.appconductor;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.santi.appconductor.models.ActiveService;
import com.santi.appconductor.models.ChatMessage;
import com.santi.appconductor.models.FirebaseMessage;
import com.santi.appconductor.models.ResponseDriver;
import com.santi.appconductor.models.ResponseDriverBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PerfilActivity extends BaseActivity {

    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Bundle bundle;
    SharedPreferences prefs;
    String cliente;
    ImageView iv_Atras;
    ResponseDriver conductorResponse;
    boolean chatAvailable, available, busy;
    List<ChatMessage> mensajes_chat = new ArrayList<>();
    List<ActiveService> serviciosActivos = new ArrayList<>();
    TextView itemNombre, itemApellido, itemNumero, itemCorreo;
    ImageView ivFoto, irfoto,irtelefono,ircorreo,irvehiculo, irDocumento;
    ConstraintLayout linear_foto,linear_numero,linear_correo,linear_vehiculo,linear_documento;
    RatingBar ratingBar;
    AlertDialog adDisplayService;
    TextView tool_titulo_atras;

    int serv_id = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Mi Perfil");
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        bundle = getIntent().getExtras();
        iv_Atras = findViewById(R.id.iv_Atras);
        iv_Atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //if (bundle != null) {
            //conductorResponse = (ResponseDriver) bundle.getSerializable("conductor");
        //}
        Type listType1 = new TypeToken<ArrayList<ActiveService>>(){}.getType();

        Gson gson = new Gson();
        String serviciosActivosString = getIntent().getStringExtra("serviciosActivos");
        serviciosActivos = gson.fromJson(serviciosActivosString, listType1);

        cliente = getIntent().getStringExtra("cliente");
        chatAvailable = getIntent().getBooleanExtra("chatAvailable", false);
        available = getIntent().getBooleanExtra("available", false);
        busy = getIntent().getBooleanExtra("busy", false);
        serv_id = getIntent().getIntExtra("serv_id", -1);
        itemNombre = findViewById(R.id.itemNombre);
        itemApellido = findViewById(R.id.itemApellido);
        itemNumero = findViewById(R.id.itemNumero);
        itemCorreo = findViewById(R.id.itemCorreo);
        ivFoto = findViewById(R.id.cv_foto);
        ratingBar = findViewById(R.id.ratingBar);
        irfoto = findViewById(R.id.irfoto);
        irtelefono = findViewById(R.id.irtelefono);
        ircorreo = findViewById(R.id.ircorreo);
        irvehiculo = findViewById(R.id.irvehiculo);
        linear_foto = findViewById(R.id.linear_foto);
        linear_numero = findViewById(R.id.linear_numero);
        linear_correo = findViewById(R.id.linear_correo);
        linear_vehiculo = findViewById(R.id.linear_vehiculo);
        irDocumento = findViewById(R.id.irdocuemtno);
        linear_documento = findViewById(R.id.linear_docuemento);

        cargarDatosConductor();
        irfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas actualizar tu foto de perfil?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, EditarFotoPerfilActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

        linear_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas actualizar tu foto de perfil?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, EditarFotoPerfilActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });

        irtelefono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas cambiar tu número de celular?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, CambiarNumeroActivity.class);
                        intent.putExtra("tel", conductorResponse.getCond_NroTelefono());
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        linear_numero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas cambiar tu número de celular?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, CambiarNumeroActivity.class);
                        intent.putExtra("tel", conductorResponse.getCond_NroTelefono());
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        ircorreo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas actualizar tu correo electrónico?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, CambiarEmailActivity.class);
                        intent.putExtra("cor", conductorResponse.getCond_Correo());
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        linear_correo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas actualizar tu correo electrónico?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, CambiarEmailActivity.class);
                        intent.putExtra("cor", conductorResponse.getCond_Correo());
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        irvehiculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas actualizar los datos de tu vehículo?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, EditarVehiculoActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        linear_vehiculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas actualizar los datos de tu vehículo?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, EditarVehiculoActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        irDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas actualizar tus documentos?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, ListaDocumentosPerfilActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });


        linear_documento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertadd = new AlertDialog.Builder(PerfilActivity.this);
                LayoutInflater factory = LayoutInflater.from(PerfilActivity.this);
                final View view = factory.inflate(R.layout.dialog, null);
                TextView texto = view.findViewById(R.id.texto);
                Button btnAceptar = view.findViewById(R.id.btnAceptar);
                Button btnRechazar = view.findViewById(R.id.btnRechazar);
                texto.setText("¿Estás seguro que deseas actualizar tus documentos?");
                alertadd.setView(view);
                final AlertDialog dialog = alertadd.create();
                btnAceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(PerfilActivity.this, ListaDocumentosPerfilActivity.class);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });
                btnRechazar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });

        if (chatAvailable) {
            Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
            String mensajesChatString = getIntent().getStringExtra("mensajesChat");
            mensajes_chat = gson.fromJson(mensajesChatString, listType);
        }
    }

    public void cargarDatosConductor() {
        //DisplayProgressDialog(getString(R.string.loading));
        //pDialog.show();
        Log.i("mensaje","cargarDatosConductor");
        Call<ResponseDriverBody> callSeleccionarCliente = conductorService.getSeleccionarConductor(prefs.getInt(Constants.pais_id,0), prefs.getInt(Constants.primary_id,0));
        callSeleccionarCliente.enqueue(new Callback<ResponseDriverBody>() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onResponse(Call<ResponseDriverBody> call, Response<ResponseDriverBody> response) {
                //if (pDialog != null && pDialog.isShowing()) {
                  //  pDialog.dismiss();
                //}
                Log.i("mensaje","response: "+response);
                if (response.isSuccessful()) {
                    if (response.body()!=null) {
                        if (response.body().isEstado()) {
                            bundle = new Bundle();
                            conductorResponse = response.body().getValor();
                            bundle.putSerializable("conductor", conductorResponse);

                            if(conductorResponse == null){
                                itemNombre.setText(" ");
                                itemApellido.setText(" ");
                                itemNumero.setText(" ");
                                itemCorreo.setText(" ");
                                ratingBar.setRating(0);
                            }else{
                                itemNombre.setText(conductorResponse.getCond_Nombres());
                                itemApellido.setText(conductorResponse.getCond_ApellidoPaterno() + " " +conductorResponse.getCond_ApellidoMaterno());
                                itemNumero.setText(conductorResponse.getCond_NroTelefono());
                                itemCorreo.setText(conductorResponse.getCond_Correo());
                                ratingBar.setRating(conductorResponse.getCond_CalificacionPromedio());
                            }

                            if (conductorResponse.getCond_Foto() != null) {
                                Glide.with(ivFoto)
                                        .load(conductorResponse.getCond_Foto())
                                        .apply(RequestOptions.skipMemoryCacheOf(true))
                                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                                        .fitCenter()
                                        .into(ivFoto);
                            }
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }

                    } else {
                        adDisplayService = displayMessage(getString(R.string.driver_data_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.driver_data_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseDriverBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.i("mensaje","onFailure: "+t);
                adDisplayService = displayMessage(getString(R.string.driver_data_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkMessages();
        cargarDatosConductor();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        if (!mensajes_chat.isEmpty()) {
            Gson gson = new Gson();
            String mensajesChatString = gson.toJson(mensajes_chat);
            intent.putExtra("mensajesChat", mensajesChatString);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_OK);
        }
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (available) {
            LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver3), new IntentFilter("MyData"));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (available) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver3);
        }
    }

    private final BroadcastReceiver mMessageReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkMessages();
        }
    };

    private void checkMessages() {
        if (adDisplayService == null || !adDisplayService.isShowing()) {
            List<FirebaseMessage> pending_messages;
            String pendingmessages = prefs.getString("pending_messages", "");
            if (!pendingmessages.isEmpty() && pendingmessages.length() > 2) {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<FirebaseMessage>>(){}.getType();
                pending_messages = gson.fromJson(pendingmessages, listType);
                FirebaseMessage fbMessage = pending_messages.get(0);
                pending_messages.remove(0);

                pendingmessages = gson.toJson(pending_messages);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("pending_messages", pendingmessages);
                editor.apply();

                processMessage(fbMessage);
            }
        }
    }

    private void processMessage(FirebaseMessage mensaje) {
        try {
            final JSONObject obj = new JSONObject(mensaje.getMessage());
            if (obj.has("Tipo")) {
                switch (obj.getInt("Tipo")) {
                    case 1: {
                        /*if (!busy) {*/
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Moneda"),new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(RESULT_FIRST_USER, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        /*}*/
                        break;
                    }
                    case 3: {
                        if (obj.getInt("Eser_Id") == 6 && !serviciosActivos.isEmpty() && obj.getInt("Serv_Id") == serviciosActivos.get(0).getServ_id()) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                        break;
                    }
                    case 4: {
                        adDisplayService = displayNotificationChat(String.format(getString(R.string.new_message_expression), serviciosActivos.get(0).getCliente().split(" ")[0], obj.getString("Chat_Mensaje")), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        ChatMessage msg = new ChatMessage(obj.getString("Chat_Emisor"), obj.getString("Chat_Mensaje"), obj.getString("Chat_Fecha"));
                        mensajes_chat.add(msg);
                        break;
                    }
                    case 5: {
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayScheduledService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(2, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        break;
                    }
                    case 6: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_canceled_by_customer), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("NombreCompleto")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        break;
                    }
                    case 7: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_reminder), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("Cliente")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        break;
                    }
                    case 9: {
                        /*if (!busy) {*/
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayServiceExtended(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"),obj.getString("Moneda"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(RESULT_FIRST_USER, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        /*}*/
                        break;
                    }
                    default: {
                        checkMessages();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            checkMessages();
            e.printStackTrace();
        }
    }

}