package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseResumenTotalBody implements Serializable {
    private boolean Estado;
    private ResponseResumenTotal Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseResumenTotalBody() {

    }

    public ResponseResumenTotalBody(boolean estado, ResponseResumenTotal valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public ResponseResumenTotal getValor() {
        return Valor;
    }

    public void setValor(ResponseResumenTotal valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}