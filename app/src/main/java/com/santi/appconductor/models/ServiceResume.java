package com.santi.appconductor.models;

import java.io.Serializable;

public class ServiceResume implements Serializable {
    private int TotalServicios;
    private int Fpag_Id;
    private String FormaPago;
    private double Monto;

    public ServiceResume() {
    }

    public ServiceResume(int totalServicios, int fpag_Id, String formaPago, double monto) {
        TotalServicios = totalServicios;
        Fpag_Id = fpag_Id;
        FormaPago = formaPago;
        Monto = monto;
    }

    public int getTotalServicios() {
        return TotalServicios;
    }

    public void setTotalServicios(int totalServicios) {
        TotalServicios = totalServicios;
    }

    public int getFpag_Id() {
        return Fpag_Id;
    }

    public void setFpag_Id(int fpag_Id) {
        Fpag_Id = fpag_Id;
    }

    public String getFormaPago() {
        return FormaPago;
    }

    public void setFormaPago(String formaPago) {
        FormaPago = formaPago;
    }

    public double getMonto() {
        return Monto;
    }

    public void setMonto(double monto) {
        Monto = monto;
    }
}
