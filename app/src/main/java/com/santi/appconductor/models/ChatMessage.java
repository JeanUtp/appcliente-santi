package com.santi.appconductor.models;

import java.io.Serializable;

public class ChatMessage implements Serializable {
    private String Chat_Emisor;
    private String Chat_Mensaje;
    private String Chat_Fecha;

    public ChatMessage() {
    }

    public ChatMessage(String chat_Emisor, String chat_Mensaje, String chat_Fecha) {
        Chat_Emisor = chat_Emisor;
        Chat_Mensaje = chat_Mensaje;
        Chat_Fecha = chat_Fecha;
    }

    public String getChat_Emisor() {
        return Chat_Emisor;
    }

    public void setChat_Emisor(String chat_Emisor) {
        Chat_Emisor = chat_Emisor;
    }

    public String getChat_Mensaje() {
        return Chat_Mensaje;
    }

    public void setChat_Mensaje(String chat_Mensaje) {
        Chat_Mensaje = chat_Mensaje;
    }

    public String getChat_Fecha() {
        return Chat_Fecha;
    }

    public void setChat_Fecha(String chat_Fecha) {
        Chat_Fecha = chat_Fecha;
    }
}
