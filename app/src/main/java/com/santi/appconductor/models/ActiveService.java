package com.santi.appconductor.models;

public class ActiveService {
    private int sol_id, serv_id, clie_id, fpag_id;
    double monto;
    private String cliente, origen, destino, coordOrigen, coordDestino, telfCliente,fotoCliente;
    private boolean extendido = false;
    private int timpodeViaje;

    public ActiveService() {
    }

    public ActiveService(int sol_id, int serv_id, int clie_id, int fpag_id, double monto, String cliente, String origen, String destino, String coordOrigen, String coordDestino, String telfCliente, String fotoCliente, boolean extendido, int timpodeViaje) {
        this.sol_id = sol_id;
        this.serv_id = serv_id;
        this.clie_id = clie_id;
        this.fpag_id = fpag_id;
        this.monto = monto;
        this.cliente = cliente;
        this.origen = origen;
        this.destino = destino;
        this.coordOrigen = coordOrigen;
        this.coordDestino = coordDestino;
        this.telfCliente = telfCliente;
        this.fotoCliente = fotoCliente;
        this.extendido = extendido;
        this.timpodeViaje = timpodeViaje;
    }

    public int getSol_id() {
        return sol_id;
    }

    public void setSol_id(int sol_id) {
        this.sol_id = sol_id;
    }

    public int getServ_id() {
        return serv_id;
    }

    public void setServ_id(int serv_id) {
        this.serv_id = serv_id;
    }

    public int getClie_id() {
        return clie_id;
    }

    public void setClie_id(int clie_id) {
        this.clie_id = clie_id;
    }

    public int getFpag_id() {
        return fpag_id;
    }

    public void setFpag_id(int fpag_id) {
        this.fpag_id = fpag_id;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public String getCoordOrigen() {
        return coordOrigen;
    }

    public void setCoordOrigen(String coordOrigen) {
        this.coordOrigen = coordOrigen;
    }

    public String getCoordDestino() {
        return coordDestino;
    }

    public void setCoordDestino(String coordDestino) {
        this.coordDestino = coordDestino;
    }

    public String getTelfCliente() {
        return telfCliente;
    }

    public void setTelfCliente(String telfCliente) {
        this.telfCliente = telfCliente;
    }

    public String getFotoCliente() {
        return fotoCliente;
    }

    public void setFotoCliente(String fotoCliente) {
        this.fotoCliente = fotoCliente;
    }

    public boolean isExtendido() {
        return extendido;
    }

    public void setExtendido(boolean extendido) {
        this.extendido = extendido;
    }

    public int getTimpodeViaje() {
        return timpodeViaje;
    }

    public void setTimpodeViaje(int timpodeViaje) {
        this.timpodeViaje = timpodeViaje;
    }
}
