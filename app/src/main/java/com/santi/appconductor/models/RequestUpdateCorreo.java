package com.santi.appconductor.models;

public class RequestUpdateCorreo {

    private int Pais_Id;
    private int Cond_Id;
    private String Cond_Correo;

    public RequestUpdateCorreo() {}

    public RequestUpdateCorreo(int pais_Id, int cond_Id, String cond_Correo) {
        Pais_Id = pais_Id;
        Cond_Id = cond_Id;
        Cond_Correo = cond_Correo;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public String getCond_Correo() {
        return Cond_Correo;
    }

    public void setCond_Correo(String cond_Correo) {
        Cond_Correo = cond_Correo;
    }
}
