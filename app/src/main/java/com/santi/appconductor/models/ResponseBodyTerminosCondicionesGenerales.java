package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseBodyTerminosCondicionesGenerales implements Serializable {
    private boolean Estado;
    private String Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseBodyTerminosCondicionesGenerales() {

    }

    public ResponseBodyTerminosCondicionesGenerales(boolean estado, String valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String valor) {
        Valor = valor;
    }

    public String getMessage() {
        return Mensaje;
    }

    public void setMessage(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}
