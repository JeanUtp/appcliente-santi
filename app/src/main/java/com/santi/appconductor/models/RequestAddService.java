package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestAddService implements Serializable {
    private int Soli_Id, Cond_Id;

    public RequestAddService() {
    }

    public RequestAddService(int soli_Id, int cond_Id) {
        Soli_Id = soli_Id;
        Cond_Id = cond_Id;
    }

    public int getSoli_Id() {
        return Soli_Id;
    }

    public void setSoli_Id(int soli_Id) {
        Soli_Id = soli_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }
}
