package com.santi.appconductor.models;

import java.io.Serializable;

public class Travel implements Serializable {

    private int Soli_Id;
    private int Serv_Id;
    private int Clie_Id;
    private int Cond_Id;
    private int Fpag_Id;
    private String Fpag_Nombre;
    private String NombreCompleto;
    private String Foto;
    private double Serv_Precio;
    private int Tser_Id;
    private String Tser_Nombre;
    private double Tser_Comision;
    private double MontoCalculado;
    private String Soli_OrigenCoordenada;
    private String Soli_OrigenDireccion;
    private String Soli_DestinoCoordenada;
    private String Soli_DestinoDireccion;
    private boolean Soli_Anulado;
    private String Mes;
    private int NumeroMes;
    private String Soli_FechaRegistro;
    private String Soli_HoraRegistro;
    private float Calificacion;
    private int Vehi_Id;
    private String Vehi_NroPlaca;
    private int Mode_Id;
    private String Mode_Nombre;
    private int Pais_Id;
    private String Pais_Nombre;
    private int Mone_Id;
    private String Mone_Abreviatura;
    private String Mone_Simbolo;
    private int AnuladoPor;

    public Travel() {}

    public Travel(int soli_Id, int serv_Id, int clie_Id, int cond_Id, int fpag_Id, String fpag_Nombre, String nombreCompleto, String foto, double serv_Precio, int tser_Id, String tser_Nombre, double tser_Comision, double montoCalculado, String soli_OrigenCoordenada, String soli_OrigenDireccion, String soli_DestinoCoordenada, String soli_DestinoDireccion, boolean soli_Anulado, String mes, int numeroMes, String soli_FechaRegistro, String soli_HoraRegistro, float calificacion, int vehi_Id, String vehi_NroPlaca, int mode_Id, String mode_Nombre, int pais_Id, String pais_Nombre, int mone_Id, String mone_Abreviatura, String mone_Simbolo, int anuladoPor) {
        Soli_Id = soli_Id;
        Serv_Id = serv_Id;
        Clie_Id = clie_Id;
        Cond_Id = cond_Id;
        Fpag_Id = fpag_Id;
        Fpag_Nombre = fpag_Nombre;
        NombreCompleto = nombreCompleto;
        Foto = foto;
        Serv_Precio = serv_Precio;
        Tser_Id = tser_Id;
        Tser_Nombre = tser_Nombre;
        Tser_Comision = tser_Comision;
        MontoCalculado = montoCalculado;
        Soli_OrigenCoordenada = soli_OrigenCoordenada;
        Soli_OrigenDireccion = soli_OrigenDireccion;
        Soli_DestinoCoordenada = soli_DestinoCoordenada;
        Soli_DestinoDireccion = soli_DestinoDireccion;
        Soli_Anulado = soli_Anulado;
        Mes = mes;
        NumeroMes = numeroMes;
        Soli_FechaRegistro = soli_FechaRegistro;
        Soli_HoraRegistro = soli_HoraRegistro;
        Calificacion = calificacion;
        Vehi_Id = vehi_Id;
        Vehi_NroPlaca = vehi_NroPlaca;
        Mode_Id = mode_Id;
        Mode_Nombre = mode_Nombre;
        Pais_Id = pais_Id;
        Pais_Nombre = pais_Nombre;
        Mone_Id = mone_Id;
        Mone_Abreviatura = mone_Abreviatura;
        Mone_Simbolo = mone_Simbolo;
        AnuladoPor = anuladoPor;
    }

    public int getSoli_Id() {
        return Soli_Id;
    }

    public void setSoli_Id(int soli_Id) {
        Soli_Id = soli_Id;
    }

    public int getServ_Id() {
        return Serv_Id;
    }

    public void setServ_Id(int serv_Id) {
        Serv_Id = serv_Id;
    }

    public int getClie_Id() {
        return Clie_Id;
    }

    public void setClie_Id(int clie_Id) {
        Clie_Id = clie_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getFpag_Id() {
        return Fpag_Id;
    }

    public void setFpag_Id(int fpag_Id) {
        Fpag_Id = fpag_Id;
    }

    public String getFpag_Nombre() {
        return Fpag_Nombre;
    }

    public void setFpag_Nombre(String fpag_Nombre) {
        Fpag_Nombre = fpag_Nombre;
    }

    public String getNombreCompleto() {
        return NombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        NombreCompleto = nombreCompleto;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public double getServ_Precio() {
        return Serv_Precio;
    }

    public void setServ_Precio(double serv_Precio) {
        Serv_Precio = serv_Precio;
    }

    public double getTser_Comision() {
        return Tser_Comision;
    }

    public void setTser_Comision(double tser_Comision) {
        Tser_Comision = tser_Comision;
    }

    public double getMontoCalculado() {
        return MontoCalculado;
    }

    public void setMontoCalculado(double montoCalculado) {
        MontoCalculado = montoCalculado;
    }

    public String getSoli_OrigenCoordenada() {
        return Soli_OrigenCoordenada;
    }

    public void setSoli_OrigenCoordenada(String soli_OrigenCoordenada) {
        Soli_OrigenCoordenada = soli_OrigenCoordenada;
    }

    public String getSoli_OrigenDireccion() {
        return Soli_OrigenDireccion;
    }

    public void setSoli_OrigenDireccion(String soli_OrigenDireccion) {
        Soli_OrigenDireccion = soli_OrigenDireccion;
    }

    public String getSoli_DestinoCoordenada() {
        return Soli_DestinoCoordenada;
    }

    public void setSoli_DestinoCoordenada(String soli_DestinoCoordenada) {
        Soli_DestinoCoordenada = soli_DestinoCoordenada;
    }

    public String getSoli_DestinoDireccion() {
        return Soli_DestinoDireccion;
    }

    public void setSoli_DestinoDireccion(String soli_DestinoDireccion) {
        Soli_DestinoDireccion = soli_DestinoDireccion;
    }

    public boolean isSoli_Anulado() {
        return Soli_Anulado;
    }

    public void setSoli_Anulado(boolean soli_Anulado) {
        Soli_Anulado = soli_Anulado;
    }

    public String getMes() {
        return Mes;
    }

    public void setMes(String mes) {
        Mes = mes;
    }

    public int getNumeroMes() {
        return NumeroMes;
    }

    public void setNumeroMes(int numeroMes) {
        NumeroMes = numeroMes;
    }

    public String getSoli_FechaRegistro() {
        return Soli_FechaRegistro;
    }

    public void setSoli_FechaRegistro(String soli_FechaRegistro) {
        Soli_FechaRegistro = soli_FechaRegistro;
    }

    public String getSoli_HoraRegistro() {
        return Soli_HoraRegistro;
    }

    public void setSoli_HoraRegistro(String soli_HoraRegistro) {
        Soli_HoraRegistro = soli_HoraRegistro;
    }

    public float getCalificacion() {
        return Calificacion;
    }

    public void setCalificacion(float calificacion) {
        Calificacion = calificacion;
    }

    public int getVehi_Id() {
        return Vehi_Id;
    }

    public void setVehi_Id(int vehi_Id) {
        Vehi_Id = vehi_Id;
    }

    public String getVehi_NroPlaca() {
        return Vehi_NroPlaca;
    }

    public void setVehi_NroPlaca(String vehi_NroPlaca) {
        Vehi_NroPlaca = vehi_NroPlaca;
    }

    public int getMode_Id() {
        return Mode_Id;
    }

    public void setMode_Id(int mode_Id) {
        Mode_Id = mode_Id;
    }

    public String getMode_Nombre() {
        return Mode_Nombre;
    }

    public void setMode_Nombre(String mode_Nombre) {
        Mode_Nombre = mode_Nombre;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public String getPais_Nombre() {
        return Pais_Nombre;
    }

    public void setPais_Nombre(String pais_Nombre) {
        Pais_Nombre = pais_Nombre;
    }

    public int getMone_Id() {
        return Mone_Id;
    }

    public void setMone_Id(int mone_Id) {
        Mone_Id = mone_Id;
    }

    public String getMone_Abreviatura() {
        return Mone_Abreviatura;
    }

    public void setMone_Abreviatura(String mone_Abreviatura) {
        Mone_Abreviatura = mone_Abreviatura;
    }

    public String getMone_Simbolo() {
        return Mone_Simbolo;
    }

    public void setMone_Simbolo(String mone_Simbolo) {
        Mone_Simbolo = mone_Simbolo;
    }

    public int getTser_Id() {
        return Tser_Id;
    }

    public void setTser_Id(int tser_Id) {
        Tser_Id = tser_Id;
    }

    public String getTser_Nombre() {
        return Tser_Nombre;
    }

    public void setTser_Nombre(String tser_Nombre) {
        Tser_Nombre = tser_Nombre;
    }

    public int getAnuladoPor() {
        return AnuladoPor;
    }

    public void setAnuladoPor(int anuladoPor) {
        AnuladoPor = anuladoPor;
    }
}
