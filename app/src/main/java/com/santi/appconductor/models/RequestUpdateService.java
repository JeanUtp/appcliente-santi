package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestUpdateService implements Serializable {
    private int Serv_Id, Clie_Id, Cond_Id, Eser_Id;

    public RequestUpdateService() {
    }

    public RequestUpdateService(int serv_Id, int clie_Id, int cond_Id, int eser_Id) {
        Serv_Id = serv_Id;
        Clie_Id = clie_Id;
        Cond_Id = cond_Id;
        Eser_Id = eser_Id;
    }

    public int getServ_Id() {
        return Serv_Id;
    }

    public void setServ_Id(int serv_Id) {
        Serv_Id = serv_Id;
    }

    public int getClie_Id() {
        return Clie_Id;
    }

    public void setClie_Id(int clie_Id) {
        Clie_Id = clie_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getEser_Id() {
        return Eser_Id;
    }

    public void setEser_Id(int eser_Id) {
        Eser_Id = eser_Id;
    }
}
