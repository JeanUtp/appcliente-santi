package com.santi.appconductor.models;

import java.io.Serializable;

public class CalificacionClientePost implements Serializable {
    private Integer Serv_Id;
    private Integer Clie_Id;
    private float Serv_CalificacionCliente;
    private String Serv_CalificacionComentario;

    public CalificacionClientePost() {}

    public CalificacionClientePost(Integer serv_Id, Integer clie_Id, float serv_CalificacionCliente, String serv_CalificacionComentario) {
        Serv_Id = serv_Id;
        Clie_Id = clie_Id;
        Serv_CalificacionCliente = serv_CalificacionCliente;
        Serv_CalificacionComentario = serv_CalificacionComentario;
    }

    public Integer getServ_Id() {
        return Serv_Id;
    }

    public void setServ_Id(Integer serv_Id) {
        Serv_Id = serv_Id;
    }

    public Integer getClie_Id() {
        return Clie_Id;
    }

    public void setClie_Id(Integer clie_Id) {
        Clie_Id = clie_Id;
    }

    public float getServ_CalificacionCliente() {
        return Serv_CalificacionCliente;
    }

    public void setServ_CalificacionCliente(float serv_CalificacionCliente) {
        Serv_CalificacionCliente = serv_CalificacionCliente;
    }

    public String getServ_CalificacionComentario() {
        return Serv_CalificacionComentario;
    }

    public void setServ_CalificacionComentario(String serv_CalificacionComentario) {
        Serv_CalificacionComentario = serv_CalificacionComentario;
    }
}
