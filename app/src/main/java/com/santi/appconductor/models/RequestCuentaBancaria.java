package com.santi.appconductor.models;

public class RequestCuentaBancaria {

    private int Cond_Id;
    private int Banc_Id;
    private String Cban_Numero;
    private String Cban_CCI;
    private String Cban_Nombre;
    private String Cban_NroDocumento;

    public RequestCuentaBancaria() {}

    public RequestCuentaBancaria(int cond_Id, int banc_Id, String cban_Numero, String cban_CCI, String cban_Nombre, String cban_NroDocumento) {
        Cond_Id = cond_Id;
        Banc_Id = banc_Id;
        Cban_Numero = cban_Numero;
        Cban_CCI = cban_CCI;
        Cban_Nombre = cban_Nombre;
        Cban_NroDocumento = cban_NroDocumento;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getBanc_Id() {
        return Banc_Id;
    }

    public void setBanc_Id(int banc_Id) {
        Banc_Id = banc_Id;
    }

    public String getCban_Numero() {
        return Cban_Numero;
    }

    public void setCban_Numero(String cban_Numero) {
        Cban_Numero = cban_Numero;
    }

    public String getCban_CCI() {
        return Cban_CCI;
    }

    public void setCban_CCI(String cban_CCI) {
        Cban_CCI = cban_CCI;
    }

    public String getCban_Nombre() {
        return Cban_Nombre;
    }

    public void setCban_Nombre(String cban_Nombre) {
        Cban_Nombre = cban_Nombre;
    }

    public String getCban_NroDocumento() {
        return Cban_NroDocumento;
    }

    public void setCban_NroDocumento(String cban_NroDocumento) {
        Cban_NroDocumento = cban_NroDocumento;
    }
}
