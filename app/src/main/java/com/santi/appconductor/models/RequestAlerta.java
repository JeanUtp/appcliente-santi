package com.santi.appconductor.models;

import com.google.android.gms.maps.model.LatLng;

public class RequestAlerta {

    private int Cond_Id;
    private int Clie_Id;
    private String Serv_Id;
    private String  Aler_Coordenada;

    public RequestAlerta() {}

    public RequestAlerta(int cond_Id, int clie_Id, String serv_Id, String aler_Coordenada) {
        Cond_Id = cond_Id;
        Clie_Id = clie_Id;
        Serv_Id = serv_Id;
        Aler_Coordenada = aler_Coordenada;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getClie_Id() {
        return Clie_Id;
    }

    public void setClie_Id(int clie_Id) {
        Clie_Id = clie_Id;
    }

    public String getServ_Id() {
        return Serv_Id;
    }

    public void setServ_Id(String serv_Id) {
        Serv_Id = serv_Id;
    }

    public String getAler_Coordenada() {
        return Aler_Coordenada;
    }

    public void setAler_Coordenada(String aler_Coordenada) {
        Aler_Coordenada = aler_Coordenada;
    }
}
