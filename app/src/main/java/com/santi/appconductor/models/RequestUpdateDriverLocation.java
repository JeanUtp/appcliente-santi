package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestUpdateDriverLocation implements Serializable {
    private int Cond_Id;
    private String Ucon_Coordenada;

    public RequestUpdateDriverLocation() {
    }

    public RequestUpdateDriverLocation(int cond_Id, String ucon_Coordenada) {
        Cond_Id = cond_Id;
        Ucon_Coordenada = ucon_Coordenada;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public String getUcon_Coordenada() {
        return Ucon_Coordenada;
    }

    public void setUcon_Coordenada(String ucon_Coordenada) {
        Ucon_Coordenada = ucon_Coordenada;
    }
}
