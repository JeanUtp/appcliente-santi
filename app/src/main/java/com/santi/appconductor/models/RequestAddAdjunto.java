package com.santi.appconductor.models;

public class RequestAddAdjunto {

    private int Adju_Id;
    private int Cond_Id;
    private int Tadj_Id;
    private int Eadj_Id;
    private String Adju_NombreArchivo1;
    private String Adju_NombreArchivo2;

    public RequestAddAdjunto() {}

    public RequestAddAdjunto(int adju_Id, int cond_Id, int tadj_Id, int eadj_Id, String adju_NombreArchivo1, String adju_NombreArchivo2) {
        Adju_Id = adju_Id;
        Cond_Id = cond_Id;
        Tadj_Id = tadj_Id;
        Eadj_Id = eadj_Id;
        Adju_NombreArchivo1 = adju_NombreArchivo1;
        Adju_NombreArchivo2 = adju_NombreArchivo2;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getTadj_Id() {
        return Tadj_Id;
    }

    public void setTadj_Id(int tadj_Id) {
        Tadj_Id = tadj_Id;
    }

    public String getAdju_NombreArchivo1() {
        return Adju_NombreArchivo1;
    }

    public void setAdju_NombreArchivo1(String adju_NombreArchivo1) {
        Adju_NombreArchivo1 = adju_NombreArchivo1;
    }

    public String getAdju_NombreArchivo2() {
        return Adju_NombreArchivo2;
    }

    public void setAdju_NombreArchivo2(String adju_NombreArchivo2) {
        Adju_NombreArchivo2 = adju_NombreArchivo2;
    }

    public int getAdju_Id() {
        return Adju_Id;
    }

    public void setAdju_Id(int adju_Id) {
        Adju_Id = adju_Id;
    }

    public int getEadj_Id() {
        return Eadj_Id;
    }

    public void setEadj_Id(int eadj_Id) {
        Eadj_Id = eadj_Id;
    }
}
