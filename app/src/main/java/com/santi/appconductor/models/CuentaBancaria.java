package com.santi.appconductor.models;

public class CuentaBancaria {
    private int Cban_Id;
    private int Cond_Id;
    private int Banc_Id;
    private String Banc_Nombre;
    private String Cban_Numero;
    private String Cban_CCI;
    private String Cban_Nombre;
    private String Cban_NroDocumento;

    public CuentaBancaria(int cban_Id, int cond_Id, int banc_Id, String banc_Nombre, String cban_Numero, String cban_CCI, String cban_Nombre, String cban_NroDocumento) {
        Cban_Id = cban_Id;
        Cond_Id = cond_Id;
        Banc_Id = banc_Id;
        Banc_Nombre = banc_Nombre;
        Cban_Numero = cban_Numero;
        Cban_CCI = cban_CCI;
        Cban_Nombre = cban_Nombre;
        Cban_NroDocumento = cban_NroDocumento;
    }

    public int getCban_Id() {
        return Cban_Id;
    }

    public void setCban_Id(int cban_Id) {
        Cban_Id = cban_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getBanc_Id() {
        return Banc_Id;
    }

    public void setBanc_Id(int banc_Id) {
        Banc_Id = banc_Id;
    }

    public String getBanc_Nombre() {
        return Banc_Nombre;
    }

    public void setBanc_Nombre(String banc_Nombre) {
        Banc_Nombre = banc_Nombre;
    }

    public String getCban_Numero() {
        return Cban_Numero;
    }

    public void setCban_Numero(String cban_Numero) {
        Cban_Numero = cban_Numero;
    }

    public String getCban_CCI() {
        return Cban_CCI;
    }

    public void setCban_CCI(String cban_CCI) {
        Cban_CCI = cban_CCI;
    }

    public String getCban_Nombre() {
        return Cban_Nombre;
    }

    public void setCban_Nombre(String cban_Nombre) {
        Cban_Nombre = cban_Nombre;
    }

    public String getCban_NroDocumento() {
        return Cban_NroDocumento;
    }

    public void setCban_NroDocumento(String cban_NroDocumento) {
        Cban_NroDocumento = cban_NroDocumento;
    }
}
