package com.santi.appconductor.models;

public class RequestAbono {

    private int Pais_Id;
    private int Cond_Id;
    private double Abon_Monto;
    private String Abon_NombreArchivo;

    public RequestAbono() {}

    public RequestAbono(int pais_Id, int cond_Id, double abon_Monto, String abon_NombreArchivo) {
        Pais_Id = pais_Id;
        Cond_Id = cond_Id;
        Abon_Monto = abon_Monto;
        Abon_NombreArchivo = abon_NombreArchivo;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public double getAbon_Monto() {
        return Abon_Monto;
    }

    public void setAbon_Monto(double abon_Monto) {
        Abon_Monto = abon_Monto;
    }

    public String getAbon_NombreArchivo() {
        return Abon_NombreArchivo;
    }

    public void setAbon_NombreArchivo(String abon_NombreArchivo) {
        Abon_NombreArchivo = abon_NombreArchivo;
    }
}
