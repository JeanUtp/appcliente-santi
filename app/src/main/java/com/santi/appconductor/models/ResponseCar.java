package com.santi.appconductor.models;

public class ResponseCar {
    RespondeVehiculo Vehiculo;
    private String Marc_Nombre;
    private String Mode_Nombre;

    public ResponseCar() {}

    public ResponseCar(RespondeVehiculo vehiculo, String marc_Nombre, String mode_Nombre) {
        Vehiculo = vehiculo;
        Marc_Nombre = marc_Nombre;
        Mode_Nombre = mode_Nombre;
    }

    public RespondeVehiculo getVehiculo() {
        return Vehiculo;
    }

    public void setVehiculo(RespondeVehiculo vehiculo) {
        Vehiculo = vehiculo;
    }

    public String getMarc_Nombre() {
        return Marc_Nombre;
    }

    public void setMarc_Nombre(String marc_Nombre) {
        Marc_Nombre = marc_Nombre;
    }

    public String getMode_Nombre() {
        return Mode_Nombre;
    }

    public void setMode_Nombre(String mode_Nombre) {
        Mode_Nombre = mode_Nombre;
    }
}
