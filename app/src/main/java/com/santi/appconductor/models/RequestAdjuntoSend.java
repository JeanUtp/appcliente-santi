package com.santi.appconductor.models;

public class RequestAdjuntoSend {

    private int Cond_Id;
    private int Tadj_Id;
    private String Adju_NombreArchivo1;
    private String Adju_NombreArchivo2;

    public RequestAdjuntoSend() {}

    public RequestAdjuntoSend(int cond_Id, int tadj_Id, String adju_NombreArchivo1, String adju_NombreArchivo2) {
        Cond_Id = cond_Id;
        Tadj_Id = tadj_Id;
        Adju_NombreArchivo1 = adju_NombreArchivo1;
        Adju_NombreArchivo2 = adju_NombreArchivo2;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getTadj_Id() {
        return Tadj_Id;
    }

    public void setTadj_Id(int tadj_Id) {
        Tadj_Id = tadj_Id;
    }

    public String getAdju_NombreArchivo1() {
        return Adju_NombreArchivo1;
    }

    public void setAdju_NombreArchivo1(String adju_NombreArchivo1) {
        Adju_NombreArchivo1 = adju_NombreArchivo1;
    }

    public String getAdju_NombreArchivo2() {
        return Adju_NombreArchivo2;
    }

    public void setAdju_NombreArchivo2(String adju_NombreArchivo2) {
        Adju_NombreArchivo2 = adju_NombreArchivo2;
    }
}
