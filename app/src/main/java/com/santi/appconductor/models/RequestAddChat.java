package com.santi.appconductor.models;

public class RequestAddChat {
    private int Serv_Id;
    private String Chat_Emisor = "C";
    private String Chat_Mensaje;

    public RequestAddChat() {
    }

    public RequestAddChat(int serv_Id, String chat_Emisor, String chat_Mensaje) {
        Serv_Id = serv_Id;
        Chat_Emisor = chat_Emisor;
        Chat_Mensaje = chat_Mensaje;
    }

    public int getServ_Id() {
        return Serv_Id;
    }

    public void setServ_Id(int serv_Id) {
        Serv_Id = serv_Id;
    }

    public String getChat_Emisor() {
        return Chat_Emisor;
    }

    public void setChat_Emisor(String chat_Emisor) {
        Chat_Emisor = chat_Emisor;
    }

    public String getChat_Mensaje() {
        return Chat_Mensaje;
    }

    public void setChat_Mensaje(String chat_Mensaje) {
        Chat_Mensaje = chat_Mensaje;
    }
}
