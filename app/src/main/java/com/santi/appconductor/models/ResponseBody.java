package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseBody implements Serializable {
    private boolean Estado;
    private Object Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseBody() {

    }

    public ResponseBody(boolean estado, Object valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public Object getValor() {
        return Valor;
    }

    public void setValor(Object valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}
