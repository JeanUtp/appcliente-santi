package com.santi.appconductor.models;

import java.io.Serializable;
import java.util.List;

public class ResponseAdjuntoBody implements Serializable  {
    private boolean Estado;
    public List<RequestAdjunto> Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseAdjuntoBody(boolean Estado, List<RequestAdjunto> Valor, String Mensaje, Integer CodError){
        this.Estado = Estado;
        this.Valor = Valor;
        this.Mensaje = Mensaje;
        this.CodError = CodError;
    }

    public ResponseAdjuntoBody() { }

    public List<RequestAdjunto> getValor() { return Valor; }

    public boolean isEstado() { return Estado; }

    public String getMensaje() { return Mensaje; }

    public Integer getCodError() { return CodError; }

}
