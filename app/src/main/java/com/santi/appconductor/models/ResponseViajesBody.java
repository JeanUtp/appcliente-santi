package com.santi.appconductor.models;

import java.io.Serializable;
import java.util.List;

public class ResponseViajesBody implements Serializable {
    private boolean Estado;
    private List<Travel> Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseViajesBody() {

    }

    public ResponseViajesBody(boolean estado, List<Travel> valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public List<Travel> getValor() {
        return Valor;
    }

    public void setValor(List<Travel> valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}
