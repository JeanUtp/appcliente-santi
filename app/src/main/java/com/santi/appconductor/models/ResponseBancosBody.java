package com.santi.appconductor.models;

import java.util.List;

public class ResponseBancosBody {

    private boolean Estado;
    public List<Bancos> Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseBancosBody(boolean Estado, List<Bancos> Valor, String Mensaje, Integer CodError){
        this.Estado = Estado;
        this.Valor = Valor;
        this.Mensaje = Mensaje;
        this.CodError = CodError;
    }

    public ResponseBancosBody() { }

    public List<Bancos> getValor() { return Valor; }

    public boolean isEstado() { return Estado; }

    public String getMensaje() { return Mensaje; }

    public Integer getCodError() { return CodError; }

}
