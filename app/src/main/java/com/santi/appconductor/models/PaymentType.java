package com.santi.appconductor.models;

import java.io.Serializable;

public class PaymentType implements Serializable {
    private int Fpag_Id;
    private String Fpag_Nombre;

    public PaymentType() {
    }

    public PaymentType(int fpag_Id, String fpag_Nombre) {
        Fpag_Id = fpag_Id;
        Fpag_Nombre = fpag_Nombre;
    }

    public int getFpag_Id() {
        return Fpag_Id;
    }

    public void setFpag_Id(int fpag_Id) {
        Fpag_Id = fpag_Id;
    }

    public String getFpag_Nombre() {
        return Fpag_Nombre;
    }

    public void setFpag_Nombre(String fpag_Nombre) {
        Fpag_Nombre = fpag_Nombre;
    }
}
