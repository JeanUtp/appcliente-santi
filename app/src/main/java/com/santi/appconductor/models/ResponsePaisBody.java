package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponsePaisBody implements Serializable {
    private boolean Estado;
    private ResponsePais Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponsePaisBody() {

    }

    public ResponsePaisBody(boolean estado, ResponsePais valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public ResponsePais getValor() {
        return Valor;
    }

    public void setValor(ResponsePais valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}
