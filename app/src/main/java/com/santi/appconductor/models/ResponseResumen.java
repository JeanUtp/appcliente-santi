package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseResumen implements Serializable {

    private int Abon_Id;
    private  int Pais_Id;
    private String Pais_Nombre;
    private int Cond_Id;
    private String NombreCompleto;
    private String Cond_NroTelefono;
    private int Eabo_Id;
    private String Eabo_Nombre;
    private String Mone_Nombre;
    private String Mone_Simbolo;
    private double Abon_Monto;
    private double Abon_Saldo;
    private String Abon_Flag;
    private String Abon_NombreArchivo;
    private String Abon_Observacion;
    private String Eabo_FechaCreacion;
    private String Eabo_HoraCreacion;

    public ResponseResumen() {}

    public ResponseResumen(int abon_Id, int pais_Id, String pais_Nombre, int cond_Id, String nombreCompleto, String cond_NroTelefono, int eabo_Id, String eabo_Nombre, String mone_Nombre, String mone_Simbolo, double abon_Monto, double abon_Saldo, String abon_Flag, String abon_NombreArchivo, String abon_Observacion, String eabo_FechaCreacion, String eabo_HoraCreacion) {
        Abon_Id = abon_Id;
        Pais_Id = pais_Id;
        Pais_Nombre = pais_Nombre;
        Cond_Id = cond_Id;
        NombreCompleto = nombreCompleto;
        Cond_NroTelefono = cond_NroTelefono;
        Eabo_Id = eabo_Id;
        Eabo_Nombre = eabo_Nombre;
        Mone_Nombre = mone_Nombre;
        Mone_Simbolo = mone_Simbolo;
        Abon_Monto = abon_Monto;
        Abon_Saldo = abon_Saldo;
        Abon_Flag = abon_Flag;
        Abon_NombreArchivo = abon_NombreArchivo;
        Abon_Observacion = abon_Observacion;
        Eabo_FechaCreacion = eabo_FechaCreacion;
        Eabo_HoraCreacion = eabo_HoraCreacion;
    }

    public int getAbon_Id() {
        return Abon_Id;
    }

    public void setAbon_Id(int abon_Id) {
        Abon_Id = abon_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getEabo_Id() {
        return Eabo_Id;
    }

    public void setEabo_Id(int eabo_Id) {
        Eabo_Id = eabo_Id;
    }

    public String getEabo_Nombre() {
        return Eabo_Nombre;
    }

    public void setEabo_Nombre(String eabo_Nombre) {
        Eabo_Nombre = eabo_Nombre;
    }

    public String getMone_Simbolo() {
        return Mone_Simbolo;
    }

    public void setMone_Simbolo(String mone_Simbolo) {
        Mone_Simbolo = mone_Simbolo;
    }

    public double getAbon_Monto() {
        return Abon_Monto;
    }

    public void setAbon_Monto(double abon_Monto) {
        Abon_Monto = abon_Monto;
    }

    public double getAbon_Saldo() {
        return Abon_Saldo;
    }

    public void setAbon_Saldo(double abon_Saldo) {
        Abon_Saldo = abon_Saldo;
    }

    public String getAbon_Flag() {
        return Abon_Flag;
    }

    public void setAbon_Flag(String abon_Flag) {
        Abon_Flag = abon_Flag;
    }

    public String getEabo_FechaCreacion() {
        return Eabo_FechaCreacion;
    }

    public void setEabo_FechaCreacion(String eabo_FechaCreacion) {
        Eabo_FechaCreacion = eabo_FechaCreacion;
    }

    public String getEabo_HoraCreacion() {
        return Eabo_HoraCreacion;
    }

    public void setEabo_HoraCreacion(String eabo_HoraCreacion) {
        Eabo_HoraCreacion = eabo_HoraCreacion;
    }

    public String getAbon_Observacion() {
        return Abon_Observacion;
    }

    public void setAbon_Observacion(String abon_Observacion) {
        Abon_Observacion = abon_Observacion;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public String getPais_Nombre() {
        return Pais_Nombre;
    }

    public void setPais_Nombre(String pais_Nombre) {
        Pais_Nombre = pais_Nombre;
    }

    public String getNombreCompleto() {
        return NombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        NombreCompleto = nombreCompleto;
    }

    public String getCond_NroTelefono() {
        return Cond_NroTelefono;
    }

    public void setCond_NroTelefono(String cond_NroTelefono) {
        Cond_NroTelefono = cond_NroTelefono;
    }

    public String getMone_Nombre() {
        return Mone_Nombre;
    }

    public void setMone_Nombre(String mone_Nombre) {
        Mone_Nombre = mone_Nombre;
    }

    public String getAbon_NombreArchivo() {
        return Abon_NombreArchivo;
    }

    public void setAbon_NombreArchivo(String abon_NombreArchivo) {
        Abon_NombreArchivo = abon_NombreArchivo;
    }
}
