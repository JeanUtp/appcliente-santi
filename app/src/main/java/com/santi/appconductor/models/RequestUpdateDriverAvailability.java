package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestUpdateDriverAvailability implements Serializable {
    private int Cond_Id;
    private boolean Cond_Disponibilidad;

    public RequestUpdateDriverAvailability() {

    }

    public RequestUpdateDriverAvailability(int cond_Id, boolean cond_Disponibilidad) {
        Cond_Id = cond_Id;
        Cond_Disponibilidad = cond_Disponibilidad;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public boolean isCond_Disponibilidad() {
        return Cond_Disponibilidad;
    }

    public void setCond_Disponibilidad(boolean cond_Disponibilidad) {
        Cond_Disponibilidad = cond_Disponibilidad;
    }
}
