package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseActiveServiceBody implements Serializable {
    private boolean Estado;
    private ResponseActiveService Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseActiveServiceBody() {
    }

    public ResponseActiveServiceBody(boolean estado, ResponseActiveService valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public ResponseActiveService getValor() {
        return Valor;
    }

    public void setValor(ResponseActiveService valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}

