package com.santi.appconductor.models;

public class TiempoDeViajeImput {

    private String CoordenadaOrigen;
    private String CoordenadaDestino;

    public TiempoDeViajeImput() {}

    public TiempoDeViajeImput(String coordenadaOrigen, String coordenadaDestino) {
        CoordenadaOrigen = coordenadaOrigen;
        CoordenadaDestino = coordenadaDestino;
    }

    public String getCoordenadaOrigen() {
        return CoordenadaOrigen;
    }

    public void setCoordenadaOrigen(String coordenadaOrigen) {
        CoordenadaOrigen = coordenadaOrigen;
    }

    public String getCoordenadaDestino() {
        return CoordenadaDestino;
    }

    public void setCoordenadaDestino(String coordenadaDestino) {
        CoordenadaDestino = coordenadaDestino;
    }
}
