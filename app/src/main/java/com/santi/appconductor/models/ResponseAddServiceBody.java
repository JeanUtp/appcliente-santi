package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseAddServiceBody implements Serializable {
    private boolean Estado;
    private ResponseAddService Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseAddServiceBody(boolean estado, ResponseAddService valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public ResponseAddServiceBody() {
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public ResponseAddService getValor() {
        return Valor;
    }

    public void setValor(ResponseAddService valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}

