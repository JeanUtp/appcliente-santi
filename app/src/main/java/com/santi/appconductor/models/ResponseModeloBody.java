package com.santi.appconductor.models;

import java.util.List;

public class ResponseModeloBody {
    private boolean Estado;
    public List<RequestModelo> Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseModeloBody(boolean Estado, List<RequestModelo> Valor, String Mensaje, Integer CodError){
        this.Estado = Estado;
        this.Valor = Valor;
        this.Mensaje = Mensaje;
        this.CodError = CodError;
    }

    public ResponseModeloBody() { }

    public List<RequestModelo> getValor() { return Valor; }

    public boolean isEstado() { return Estado; }

    public String getMensaje() { return Mensaje; }

    public Integer getCodError() { return CodError; }

}
