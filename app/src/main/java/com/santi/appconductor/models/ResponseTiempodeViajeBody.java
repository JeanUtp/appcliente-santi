package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseTiempodeViajeBody implements Serializable {
    private boolean Estado;
    private TiempodeViaje Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseTiempodeViajeBody() {}

    public ResponseTiempodeViajeBody(boolean estado, TiempodeViaje valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public TiempodeViaje getValor() {
        return Valor;
    }

    public void setValor(TiempodeViaje valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}
