package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestUpdateFirebaseToken implements Serializable {
    private int Clie_Id;
    private int Cond_Id;
    private String TokenFirebase;
    private String idDispositivo;

    public RequestUpdateFirebaseToken() {
        Clie_Id = 0;
    }

    public RequestUpdateFirebaseToken(int clie_Id, int cond_Id, String tokenFirebase, String idDispositivo) {
        Clie_Id = clie_Id;
        Cond_Id = cond_Id;
        TokenFirebase = tokenFirebase;
        this.idDispositivo = idDispositivo;
    }

    public int getClie_Id() {
        return Clie_Id;
    }

    public void setClie_Id(int clie_Id) {
        Clie_Id = clie_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public String getTokenFirebase() {
        return TokenFirebase;
    }

    public void setTokenFirebase(String tokenFirebase) {
        TokenFirebase = tokenFirebase;
    }

    public String getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(String idDispositivo) {
        this.idDispositivo = idDispositivo;
    }
}
