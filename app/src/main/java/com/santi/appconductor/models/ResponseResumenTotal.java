package com.santi.appconductor.models;

public  class ResponseResumenTotal {

    private String Descripcion;
    private String Mone_Simbolo;
    private double Total;
    private int  Retraso;
    private int Estado;
    private String EnlaceCuentas;

    public ResponseResumenTotal() {}

    public ResponseResumenTotal(String descripcion, String mone_Simbolo, double total, int retraso, int estado, String enlaceCuentas) {
        Descripcion = descripcion;
        Mone_Simbolo = mone_Simbolo;
        Total = total;
        Retraso = retraso;
        Estado = estado;
        EnlaceCuentas = enlaceCuentas;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getMone_Simbolo() {
        return Mone_Simbolo;
    }

    public void setMone_Simbolo(String mone_Simbolo) {
        Mone_Simbolo = mone_Simbolo;
    }

    public double getTotal() {
        return Total;
    }

    public void setTotal(double total) {
        Total = total;
    }

    public int getRetraso() {
        return Retraso;
    }

    public void setRetraso(int retraso) {
        Retraso = retraso;
    }

    public int getEstado() {
        return Estado;
    }

    public void setEstado(int estado) {
        Estado = estado;
    }

    public String getEnlaceCuentas() {
        return EnlaceCuentas;
    }

    public void setEnlaceCuentas(String enlaceCuentas) {
        EnlaceCuentas = enlaceCuentas;
    }
}