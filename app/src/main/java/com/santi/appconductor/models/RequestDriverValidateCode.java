package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestDriverValidateCode implements Serializable {
    private String NroTelefono;
    private String CodigoVerificacion;
    private String TokenFirebase;
    private String IdDispositivo;
    private int Pais_Id;

    public RequestDriverValidateCode() {
    }

    public RequestDriverValidateCode(String nroTelefono, String codigoVerificacion, String tokenFirebase, String idDispositivo, int pais_Id) {
        NroTelefono = nroTelefono;
        CodigoVerificacion = codigoVerificacion;
        TokenFirebase = tokenFirebase;
        IdDispositivo = idDispositivo;
        Pais_Id = pais_Id;
    }

    public String getNroTelefono() {
        return NroTelefono;
    }

    public void setNroTelefono(String nroTelefono) {
        NroTelefono = nroTelefono;
    }

    public String getCodigoVerificacion() {
        return CodigoVerificacion;
    }

    public void setCodigoVerificacion(String codigoVerificacion) {
        CodigoVerificacion = codigoVerificacion;
    }

    public String getTokenFirebase() {
        return TokenFirebase;
    }

    public void setTokenFirebase(String tokenFirebase) {
        TokenFirebase = tokenFirebase;
    }

    public String getIdDispositivo() {
        return IdDispositivo;
    }

    public void setIdDispositivo(String idDispositivo) {
        IdDispositivo = idDispositivo;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }
}
