package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseAddService implements Serializable {
    private int Serv_Id;
    private int Clie_Id;
    private int Fpag_Id;
    private double Precio;
    private String Clie_NroTelefono;
    private String Soli_OrigenCoordenada;
    private String Soli_DestinoCoordenada;

    public ResponseAddService() {
    }

    public ResponseAddService(int serv_Id, int clie_Id, int fpag_Id, double precio, String clie_NroTelefono, String soli_OrigenCoordenada, String soli_DestinoCoordenada) {
        Serv_Id = serv_Id;
        Clie_Id = clie_Id;
        Fpag_Id = fpag_Id;
        Precio = precio;
        Clie_NroTelefono = clie_NroTelefono;
        Soli_OrigenCoordenada = soli_OrigenCoordenada;
        Soli_DestinoCoordenada = soli_DestinoCoordenada;
    }

    public int getServ_Id() {
        return Serv_Id;
    }

    public void setServ_Id(int serv_Id) {
        Serv_Id = serv_Id;
    }

    public int getClie_Id() {
        return Clie_Id;
    }

    public void setClie_Id(int clie_Id) {
        Clie_Id = clie_Id;
    }

    public int getFpag_Id() {
        return Fpag_Id;
    }

    public void setFpag_Id(int fpag_Id) {
        Fpag_Id = fpag_Id;
    }

    public String getClie_NroTelefono() {
        return Clie_NroTelefono;
    }

    public void setClie_NroTelefono(String clie_NroTelefono) {
        Clie_NroTelefono = clie_NroTelefono;
    }

    public String getSoli_OrigenCoordenada() {
        return Soli_OrigenCoordenada;
    }

    public void setSoli_OrigenCoordenada(String soli_OrigenCoordenada) {
        Soli_OrigenCoordenada = soli_OrigenCoordenada;
    }

    public String getSoli_DestinoCoordenada() {
        return Soli_DestinoCoordenada;
    }

    public void setSoli_DestinoCoordenada(String soli_DestinoCoordenada) {
        Soli_DestinoCoordenada = soli_DestinoCoordenada;
    }

    public double getPrecio() {
        return Precio;
    }

    public void setPrecio(double precio) {
        Precio = precio;
    }
}
