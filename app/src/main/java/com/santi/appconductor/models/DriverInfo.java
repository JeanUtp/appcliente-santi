package com.santi.appconductor.models;

public class DriverInfo {

    private int Cond_Id;
    private String Cond_Nombres;
    private String Cond_ApellidoPaterno;
    private String Cond_ApellidoMaterno;
    private String Cond_NroTelefono;
    private int Tdoc_Id;
    private int Pais_Id;
    private String Cond_NroDocumento;
    private String Cond_Correo;
    private String Cond_TokenFirebase;
    private String Cond_IdDispositivo;

    public DriverInfo() {}

    public DriverInfo(int cond_Id, String cond_Nombres, String cond_ApellidoPaterno, String cond_ApellidoMaterno, String cond_NroTelefono, int tdoc_Id, int pais_Id, String cond_NroDocumento, String cond_Correo, String cond_TokenFirebase, String cond_IdDispositivo) {
        Cond_Id = cond_Id;
        Cond_Nombres = cond_Nombres;
        Cond_ApellidoPaterno = cond_ApellidoPaterno;
        Cond_ApellidoMaterno = cond_ApellidoMaterno;
        Cond_NroTelefono = cond_NroTelefono;
        Tdoc_Id = tdoc_Id;
        Pais_Id = pais_Id;
        Cond_NroDocumento = cond_NroDocumento;
        Cond_Correo = cond_Correo;
        Cond_TokenFirebase = cond_TokenFirebase;
        Cond_IdDispositivo = cond_IdDispositivo;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public String getCond_Nombres() {
        return Cond_Nombres;
    }

    public void setCond_Nombres(String cond_Nombres) {
        Cond_Nombres = cond_Nombres;
    }

    public String getCond_ApellidoPaterno() {
        return Cond_ApellidoPaterno;
    }

    public void setCond_ApellidoPaterno(String cond_ApellidoPaterno) {
        Cond_ApellidoPaterno = cond_ApellidoPaterno;
    }

    public String getCond_ApellidoMaterno() {
        return Cond_ApellidoMaterno;
    }

    public void setCond_ApellidoMaterno(String cond_ApellidoMaterno) {
        Cond_ApellidoMaterno = cond_ApellidoMaterno;
    }

    public String getCond_NroTelefono() {
        return Cond_NroTelefono;
    }

    public void setCond_NroTelefono(String cond_NroTelefono) {
        Cond_NroTelefono = cond_NroTelefono;
    }

    public int getTdoc_Id() {
        return Tdoc_Id;
    }

    public void setTdoc_Id(int tdoc_Id) {
        Tdoc_Id = tdoc_Id;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public String getCond_NroDocumento() {
        return Cond_NroDocumento;
    }

    public void setCond_NroDocumento(String cond_NroDocumento) {
        Cond_NroDocumento = cond_NroDocumento;
    }

    public String getCond_Correo() {
        return Cond_Correo;
    }

    public void setCond_Correo(String cond_Correo) {
        Cond_Correo = cond_Correo;
    }

    public String getCond_TokenFirebase() {
        return Cond_TokenFirebase;
    }

    public void setCond_TokenFirebase(String cond_TokenFirebase) {
        Cond_TokenFirebase = cond_TokenFirebase;
    }

    public String getCond_IdDispositivo() {
        return Cond_IdDispositivo;
    }

    public void setCond_IdDispositivo(String cond_IdDispositivo) {
        Cond_IdDispositivo = cond_IdDispositivo;
    }
}
