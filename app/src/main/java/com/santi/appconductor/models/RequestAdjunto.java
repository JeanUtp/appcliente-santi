package com.santi.appconductor.models;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.io.Serializable;

public class RequestAdjunto implements Serializable {

    private int Tadj_Id;
    private int Pais_Id;
    private String Tadj_Nombre;
    private String Tadj_Descripcion;
    private int Tadj_NroImagenes;
    private boolean Tadj_Opcional;
    private boolean Tadj_AplicaBloqueo;
    private int Adju_Id;
    private String Adju_NombreArchivo1;
    private String Adju_NombreArchivo2;
    private String Adju_Observacion;
    private int Econ_Id;
    private int Eadj_Id;
    private String Eadj_Nombre;
    private String Adju_FechaVencimiento;

    public RequestAdjunto() { }

    public RequestAdjunto(int tadj_Id, int pais_Id, String tadj_Nombre, String tadj_Descripcion, int tadj_NroImagenes, boolean tadj_Opcional, boolean tadj_AplicaBloqueo, int adju_Id, String adju_NombreArchivo1, String adju_NombreArchivo2, String adju_Observacion, int econ_Id, int eadj_Id, String eadj_Nombre, String adju_FechaVencimiento) {
        Tadj_Id = tadj_Id;
        Pais_Id = pais_Id;
        Tadj_Nombre = tadj_Nombre;
        Tadj_Descripcion = tadj_Descripcion;
        Tadj_NroImagenes = tadj_NroImagenes;
        Tadj_Opcional = tadj_Opcional;
        Tadj_AplicaBloqueo = tadj_AplicaBloqueo;
        Adju_Id = adju_Id;
        Adju_NombreArchivo1 = adju_NombreArchivo1;
        Adju_NombreArchivo2 = adju_NombreArchivo2;
        Adju_Observacion = adju_Observacion;
        Econ_Id = econ_Id;
        Eadj_Id = eadj_Id;
        Eadj_Nombre = eadj_Nombre;
        Adju_FechaVencimiento = adju_FechaVencimiento;
    }

    public int getTadj_Id() {
        return Tadj_Id;
    }

    public void setTadj_Id(int tadj_Id) {
        Tadj_Id = tadj_Id;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public String getTadj_Nombre() {
        return Tadj_Nombre;
    }

    public void setTadj_Nombre(String tadj_Nombre) {
        Tadj_Nombre = tadj_Nombre;
    }

    public String getTadj_Descripcion() {
        return Tadj_Descripcion;
    }

    public void setTadj_Descripcion(String tadj_Descripcion) {
        Tadj_Descripcion = tadj_Descripcion;
    }

    public int getTadj_NroImagenes() {
        return Tadj_NroImagenes;
    }

    public void setTadj_NroImagenes(int tadj_NroImagenes) {
        Tadj_NroImagenes = tadj_NroImagenes;
    }

    public boolean isTadj_Opcional() {
        return Tadj_Opcional;
    }

    public void setTadj_Opcional(boolean tadj_Opcional) {
        Tadj_Opcional = tadj_Opcional;
    }

    public boolean isTadj_AplicaBloqueo() {
        return Tadj_AplicaBloqueo;
    }

    public void setTadj_AplicaBloqueo(boolean tadj_AplicaBloqueo) {
        Tadj_AplicaBloqueo = tadj_AplicaBloqueo;
    }

    public int getAdju_Id() {
        return Adju_Id;
    }

    public void setAdju_Id(int adju_Id) {
        Adju_Id = adju_Id;
    }

    public String getAdju_NombreArchivo1() {
        return Adju_NombreArchivo1;
    }

    public void setAdju_NombreArchivo1(String adju_NombreArchivo1) {
        Adju_NombreArchivo1 = adju_NombreArchivo1;
    }

    public String getAdju_NombreArchivo2() {
        return Adju_NombreArchivo2;
    }

    public void setAdju_NombreArchivo2(String adju_NombreArchivo2) {
        Adju_NombreArchivo2 = adju_NombreArchivo2;
    }

    public String getAdju_Observacion() {
        return Adju_Observacion;
    }

    public void setAdju_Observacion(String adju_Observacion) {
        Adju_Observacion = adju_Observacion;
    }

    public int getEcon_Id() {
        return Econ_Id;
    }

    public void setEcon_Id(int econ_Id) {
        Econ_Id = econ_Id;
    }

    public int getEadj_Id() {
        return Eadj_Id;
    }

    public void setEadj_Id(int eadj_Id) {
        Eadj_Id = eadj_Id;
    }

    public String getEadj_Nombre() {
        return Eadj_Nombre;
    }

    public void setEadj_Nombre(String eadj_Nombre) {
        Eadj_Nombre = eadj_Nombre;
    }

    public String getAdju_FechaVencimiento() {
        return Adju_FechaVencimiento;
    }

    public void setAdju_FechaVencimiento(String adju_FechaVencimiento) {
        Adju_FechaVencimiento = adju_FechaVencimiento;
    }
}
