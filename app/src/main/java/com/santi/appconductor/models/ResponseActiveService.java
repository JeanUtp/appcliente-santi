package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseActiveService implements Serializable {
    private int Serv_Id;
    private String NombreCompleto;
    private String NroTelefono;
    private int Eser_Id;
    private int Soli_Id;
    private int Clie_Id;
    private String Foto;
    private String Soli_OrigenCoordenada;
    private String Soli_OrigenDireccion;
    private String Soli_DestinoCoordenada;
    private String Soli_DestinoDireccion;
    private int Tser_Id;
    private double Soli_Precio;
    private int Fpag_Id;
    private int TiempoDeViaje;

    public ResponseActiveService() {}

    public ResponseActiveService(int serv_Id, String nombreCompleto, String nroTelefono, int eser_Id, int soli_Id, int clie_Id, String foto, String soli_OrigenCoordenada, String soli_OrigenDireccion, String soli_DestinoCoordenada, String soli_DestinoDireccion, int tser_Id, double soli_Precio, int fpag_Id, int tiempoDeViaje) {
        Serv_Id = serv_Id;
        NombreCompleto = nombreCompleto;
        NroTelefono = nroTelefono;
        Eser_Id = eser_Id;
        Soli_Id = soli_Id;
        Clie_Id = clie_Id;
        Foto = foto;
        Soli_OrigenCoordenada = soli_OrigenCoordenada;
        Soli_OrigenDireccion = soli_OrigenDireccion;
        Soli_DestinoCoordenada = soli_DestinoCoordenada;
        Soli_DestinoDireccion = soli_DestinoDireccion;
        Tser_Id = tser_Id;
        Soli_Precio = soli_Precio;
        Fpag_Id = fpag_Id;
        TiempoDeViaje = tiempoDeViaje;
    }

    public int getServ_Id() {
        return Serv_Id;
    }

    public void setServ_Id(int serv_Id) {
        Serv_Id = serv_Id;
    }

    public String getNombreCompleto() {
        return NombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        NombreCompleto = nombreCompleto;
    }

    public String getNroTelefono() {
        return NroTelefono;
    }

    public void setNroTelefono(String nroTelefono) {
        NroTelefono = nroTelefono;
    }

    public int getEser_Id() {
        return Eser_Id;
    }

    public void setEser_Id(int eser_Id) {
        Eser_Id = eser_Id;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public int getSoli_Id() {
        return Soli_Id;
    }

    public void setSoli_Id(int soli_Id) {
        Soli_Id = soli_Id;
    }

    public int getClie_Id() {
        return Clie_Id;
    }

    public void setClie_Id(int clie_Id) {
        Clie_Id = clie_Id;
    }

    public String getSoli_OrigenCoordenada() {
        return Soli_OrigenCoordenada;
    }

    public void setSoli_OrigenCoordenada(String soli_OrigenCoordenada) {
        Soli_OrigenCoordenada = soli_OrigenCoordenada;
    }

    public String getSoli_OrigenDireccion() {
        return Soli_OrigenDireccion;
    }

    public void setSoli_OrigenDireccion(String soli_OrigenDireccion) {
        Soli_OrigenDireccion = soli_OrigenDireccion;
    }

    public String getSoli_DestinoCoordenada() {
        return Soli_DestinoCoordenada;
    }

    public void setSoli_DestinoCoordenada(String soli_DestinoCoordenada) {
        Soli_DestinoCoordenada = soli_DestinoCoordenada;
    }

    public String getSoli_DestinoDireccion() {
        return Soli_DestinoDireccion;
    }

    public void setSoli_DestinoDireccion(String soli_DestinoDireccion) {
        Soli_DestinoDireccion = soli_DestinoDireccion;
    }

    public int getTser_Id() {
        return Tser_Id;
    }

    public void setTser_Id(int tser_Id) {
        Tser_Id = tser_Id;
    }

    public double getSoli_Precio() {
        return Soli_Precio;
    }

    public void setSoli_Precio(double soli_Precio) {
        Soli_Precio = soli_Precio;
    }

    public int getFpag_Id() {
        return Fpag_Id;
    }

    public void setFpag_Id(int fpag_Id) {
        Fpag_Id = fpag_Id;
    }

    public int getTiempoDeViaje() {
        return TiempoDeViaje;
    }

    public void setTiempoDeViaje(int tiempoDeViaje) {
        TiempoDeViaje = tiempoDeViaje;
    }
}
