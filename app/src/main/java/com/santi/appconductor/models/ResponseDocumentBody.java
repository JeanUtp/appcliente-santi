package com.santi.appconductor.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ResponseDocumentBody{
    private boolean Estado;
    public List<RequestDocument> Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseDocumentBody(boolean Estado, List<RequestDocument> Valor, String Mensaje, Integer CodError){
        this.Estado = Estado;
        this.Valor = Valor;
        this.Mensaje = Mensaje;
        this.CodError = CodError;
    }

    public ResponseDocumentBody() { }

    public List<RequestDocument> getValor() { return Valor; }

    public boolean isEstado() { return Estado; }

    public String getMensaje() { return Mensaje; }

    public Integer getCodError() { return CodError; }

}
