package com.santi.appconductor.models;

public class ResponseActualizarCar {
    RespondeActualizarVehiculo Vehiculo;
    private String Marc_Nombre;
    private String Mode_Nombre;

    public ResponseActualizarCar() {}

    public ResponseActualizarCar(RespondeActualizarVehiculo vehiculo, String marc_Nombre, String mode_Nombre) {
        Vehiculo = vehiculo;
        Marc_Nombre = marc_Nombre;
        Mode_Nombre = mode_Nombre;
    }

    public RespondeActualizarVehiculo getVehiculo() {
        return Vehiculo;
    }

    public void setVehiculo(RespondeActualizarVehiculo vehiculo) {
        Vehiculo = vehiculo;
    }

    public String getMarc_Nombre() {
        return Marc_Nombre;
    }

    public void setMarc_Nombre(String marc_Nombre) {
        Marc_Nombre = marc_Nombre;
    }

    public String getMode_Nombre() {
        return Mode_Nombre;
    }

    public void setMode_Nombre(String mode_Nombre) {
        Mode_Nombre = mode_Nombre;
    }
}
