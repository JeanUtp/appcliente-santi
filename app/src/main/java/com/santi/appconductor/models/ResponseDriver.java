package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseDriver implements Serializable {
    private int Cond_Id;
    private String Cond_Nombres;
    private String NombreCompleto;
    private String Cond_ApellidoPaterno;
    private String Cond_ApellidoMaterno;
    private String Cond_NroTelefono;
    private int Tdoc_Id;
    private int Pais_Id;
    private String Cond_NroDocumento;
    private String Cond_Correo;
    private String Cond_Foto;
    private int Econ_Id;
    private int Etap_Id;
    private boolean Cond_Disponible;
    private String Cond_TokenFirebase;
    private String Cond_IdDispositivo;
    private int Cond_UsuarioCreacion;
    private int Cond_UsuarioEdicion;
    private float Cond_CalificacionPromedio;


    //private boolean Cond_Activo;
    //private String Tdoc_Nombre;
    //private String Activo;
    //private String Disponible;
    //private int PageSize;
    //private int PageNumber;
    //private int PageCount;
    //private int RowNumber;
    //private int RowCount;
    //private String Filter;
    //private String Sorting;
    //private int PageIndex;


    public ResponseDriver() {}

    public ResponseDriver(int cond_Id, String cond_Nombres, String nombreCompleto, String cond_ApellidoPaterno, String cond_ApellidoMaterno, String cond_NroTelefono, int tdoc_Id, int pais_Id, String cond_NroDocumento, String cond_Correo, String cond_Foto, int econ_Id, int etap_Id, boolean cond_Disponible, String cond_TokenFirebase, String cond_IdDispositivo, int cond_UsuarioCreacion, int cond_UsuarioEdicion, float cond_CalificacionPromedio) {
        Cond_Id = cond_Id;
        Cond_Nombres = cond_Nombres;
        NombreCompleto = nombreCompleto;
        Cond_ApellidoPaterno = cond_ApellidoPaterno;
        Cond_ApellidoMaterno = cond_ApellidoMaterno;
        Cond_NroTelefono = cond_NroTelefono;
        Tdoc_Id = tdoc_Id;
        Pais_Id = pais_Id;
        Cond_NroDocumento = cond_NroDocumento;
        Cond_Correo = cond_Correo;
        Cond_Foto = cond_Foto;
        Econ_Id = econ_Id;
        Etap_Id = etap_Id;
        Cond_Disponible = cond_Disponible;
        Cond_TokenFirebase = cond_TokenFirebase;
        Cond_IdDispositivo = cond_IdDispositivo;
        Cond_UsuarioCreacion = cond_UsuarioCreacion;
        Cond_UsuarioEdicion = cond_UsuarioEdicion;
        Cond_CalificacionPromedio = cond_CalificacionPromedio;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public String getCond_Nombres() {
        return Cond_Nombres;
    }

    public void setCond_Nombres(String cond_Nombres) {
        Cond_Nombres = cond_Nombres;
    }

    public String getNombreCompleto() {
        return NombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        NombreCompleto = nombreCompleto;
    }

    public String getCond_ApellidoPaterno() {
        return Cond_ApellidoPaterno;
    }

    public void setCond_ApellidoPaterno(String cond_ApellidoPaterno) {
        Cond_ApellidoPaterno = cond_ApellidoPaterno;
    }

    public String getCond_ApellidoMaterno() {
        return Cond_ApellidoMaterno;
    }

    public void setCond_ApellidoMaterno(String cond_ApellidoMaterno) {
        Cond_ApellidoMaterno = cond_ApellidoMaterno;
    }

    public String getCond_NroTelefono() {
        return Cond_NroTelefono;
    }

    public void setCond_NroTelefono(String cond_NroTelefono) {
        Cond_NroTelefono = cond_NroTelefono;
    }

    public int getTdoc_Id() {
        return Tdoc_Id;
    }

    public void setTdoc_Id(int tdoc_Id) {
        Tdoc_Id = tdoc_Id;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public String getCond_NroDocumento() {
        return Cond_NroDocumento;
    }

    public void setCond_NroDocumento(String cond_NroDocumento) {
        Cond_NroDocumento = cond_NroDocumento;
    }

    public String getCond_Correo() {
        return Cond_Correo;
    }

    public void setCond_Correo(String cond_Correo) {
        Cond_Correo = cond_Correo;
    }

    public String getCond_Foto() {
        return Cond_Foto;
    }

    public void setCond_Foto(String cond_Foto) {
        Cond_Foto = cond_Foto;
    }

    public int getEcon_Id() {
        return Econ_Id;
    }

    public void setEcon_Id(int econ_Id) {
        Econ_Id = econ_Id;
    }

    public int getEtap_Id() {
        return Etap_Id;
    }

    public void setEtap_Id(int etap_Id) {
        Etap_Id = etap_Id;
    }

    public boolean isCond_Disponible() {
        return Cond_Disponible;
    }

    public void setCond_Disponible(boolean cond_Disponible) {
        Cond_Disponible = cond_Disponible;
    }

    public String getCond_TokenFirebase() {
        return Cond_TokenFirebase;
    }

    public void setCond_TokenFirebase(String cond_TokenFirebase) {
        Cond_TokenFirebase = cond_TokenFirebase;
    }

    public String getCond_IdDispositivo() {
        return Cond_IdDispositivo;
    }

    public void setCond_IdDispositivo(String cond_IdDispositivo) {
        Cond_IdDispositivo = cond_IdDispositivo;
    }

    public int getCond_UsuarioCreacion() {
        return Cond_UsuarioCreacion;
    }

    public void setCond_UsuarioCreacion(int cond_UsuarioCreacion) {
        Cond_UsuarioCreacion = cond_UsuarioCreacion;
    }

    public int getCond_UsuarioEdicion() {
        return Cond_UsuarioEdicion;
    }

    public void setCond_UsuarioEdicion(int cond_UsuarioEdicion) {
        Cond_UsuarioEdicion = cond_UsuarioEdicion;
    }

    public float getCond_CalificacionPromedio() {
        return Cond_CalificacionPromedio;
    }

    public void setCond_CalificacionPromedio(float cond_CalificacionPromedio) {
        Cond_CalificacionPromedio = cond_CalificacionPromedio;
    }
}
