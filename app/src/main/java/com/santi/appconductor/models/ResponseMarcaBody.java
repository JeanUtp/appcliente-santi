package com.santi.appconductor.models;

import java.util.List;

public class ResponseMarcaBody {
    private boolean Estado;
    public List<RequestMarca> Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseMarcaBody(boolean Estado, List<RequestMarca> Valor, String Mensaje, Integer CodError){
        this.Estado = Estado;
        this.Valor = Valor;
        this.Mensaje = Mensaje;
        this.CodError = CodError;
    }

    public ResponseMarcaBody() { }

    public List<RequestMarca> getValor() { return Valor; }

    public boolean isEstado() { return Estado; }

    public String getMensaje() { return Mensaje; }

    public Integer getCodError() { return CodError; }

}
