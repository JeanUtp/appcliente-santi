package com.santi.appconductor.models;

public class ResponseObtenerVehiculo {

    private int Vehi_Id;
    private String Vehi_NroPlaca;
    private int Cond_Id;
    private String Vehi_Foto1;
    private String Vehi_Foto2;
    private String Vehi_Foto1Base64;
    private String Vehi_Foto2Base64;
    private String Vehi_AnioFab;
    private int Marc_Id;
    private String Marc_Nombre;
    private int Mode_Id;
    private String Mode_Nombre;
    private int Tser_Id;
    private String Tser_Nombre;

    public ResponseObtenerVehiculo() {}

    public ResponseObtenerVehiculo(int vehi_Id, String vehi_NroPlaca, int cond_Id, String vehi_Foto1, String vehi_Foto2, String vehi_Foto1Base64, String vehi_Foto2Base64, String vehi_AnioFab, int marc_Id, String marc_Nombre, int mode_Id, String mode_Nombre, int tser_Id, String tser_Nombre) {
        Vehi_Id = vehi_Id;
        Vehi_NroPlaca = vehi_NroPlaca;
        Cond_Id = cond_Id;
        Vehi_Foto1 = vehi_Foto1;
        Vehi_Foto2 = vehi_Foto2;
        Vehi_Foto1Base64 = vehi_Foto1Base64;
        Vehi_Foto2Base64 = vehi_Foto2Base64;
        Vehi_AnioFab = vehi_AnioFab;
        Marc_Id = marc_Id;
        Marc_Nombre = marc_Nombre;
        Mode_Id = mode_Id;
        Mode_Nombre = mode_Nombre;
        Tser_Id = tser_Id;
        Tser_Nombre = tser_Nombre;
    }

    public int getVehi_Id() {
        return Vehi_Id;
    }

    public void setVehi_Id(int vehi_Id) {
        Vehi_Id = vehi_Id;
    }

    public String getVehi_NroPlaca() {
        return Vehi_NroPlaca;
    }

    public void setVehi_NroPlaca(String vehi_NroPlaca) {
        Vehi_NroPlaca = vehi_NroPlaca;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public String getVehi_Foto1() {
        return Vehi_Foto1;
    }

    public void setVehi_Foto1(String vehi_Foto1) {
        Vehi_Foto1 = vehi_Foto1;
    }

    public String getVehi_Foto2() {
        return Vehi_Foto2;
    }

    public void setVehi_Foto2(String vehi_Foto2) {
        Vehi_Foto2 = vehi_Foto2;
    }

    public String getVehi_Foto1Base64() {
        return Vehi_Foto1Base64;
    }

    public void setVehi_Foto1Base64(String vehi_Foto1Base64) {
        Vehi_Foto1Base64 = vehi_Foto1Base64;
    }

    public String getVehi_Foto2Base64() {
        return Vehi_Foto2Base64;
    }

    public void setVehi_Foto2Base64(String vehi_Foto2Base64) {
        Vehi_Foto2Base64 = vehi_Foto2Base64;
    }

    public String getVehi_AnioFab() {
        return Vehi_AnioFab;
    }

    public void setVehi_AnioFab(String vehi_AnioFab) {
        Vehi_AnioFab = vehi_AnioFab;
    }

    public int getMarc_Id() {
        return Marc_Id;
    }

    public void setMarc_Id(int marc_Id) {
        Marc_Id = marc_Id;
    }

    public String getMarc_Nombre() {
        return Marc_Nombre;
    }

    public void setMarc_Nombre(String marc_Nombre) {
        Marc_Nombre = marc_Nombre;
    }

    public int getMode_Id() {
        return Mode_Id;
    }

    public void setMode_Id(int mode_Id) {
        Mode_Id = mode_Id;
    }

    public String getMode_Nombre() {
        return Mode_Nombre;
    }

    public void setMode_Nombre(String mode_Nombre) {
        Mode_Nombre = mode_Nombre;
    }

    public int getTser_Id() {
        return Tser_Id;
    }

    public void setTser_Id(int tser_Id) {
        Tser_Id = tser_Id;
    }

    public String getTser_Nombre() {
        return Tser_Nombre;
    }

    public void setTser_Nombre(String tser_Nombre) {
        Tser_Nombre = tser_Nombre;
    }
}
