package com.santi.appconductor.models;

public class RequestUpdateFotPerfil {

    private int Pais_Id;
    private int Cond_Id;
    private String Cond_Foto;

    public RequestUpdateFotPerfil() {}

    public RequestUpdateFotPerfil(int pais_Id, int cond_Id, String cond_Foto) {
        Pais_Id = pais_Id;
        Cond_Id = cond_Id;
        Cond_Foto = cond_Foto;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public String getCond_Foto() {
        return Cond_Foto;
    }

    public void setCond_Foto(String cond_Foto) {
        Cond_Foto = cond_Foto;
    }
}
