package com.santi.appconductor.models;

public class RequestMarca {

    private int Marc_Id;
    private String Marc_Nombre;


    public RequestMarca() {}

    public RequestMarca(int marc_Id, String marc_Nombre) {
        Marc_Id = marc_Id;
        Marc_Nombre = marc_Nombre;
    }

    public int getMarc_Id() {
        return Marc_Id;
    }

    public void setMarc_Id(int marc_Id) {
        Marc_Id = marc_Id;
    }

    public String getMarc_Nombre() {
        return Marc_Nombre;
    }

    public void setMarc_Nombre(String marc_Nombre) {
        Marc_Nombre = marc_Nombre;
    }
}
