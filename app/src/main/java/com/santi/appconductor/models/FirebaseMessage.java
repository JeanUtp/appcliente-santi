package com.santi.appconductor.models;

import java.util.Date;

public class FirebaseMessage {
    private String message;
    private Date receivedOn;

    public FirebaseMessage(String message, Date receivedOn) {
        this.message = message;
        this.receivedOn = receivedOn;
    }

    public FirebaseMessage() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getReceivedOn() {
        return receivedOn;
    }

    public void setReceivedOn(Date receivedOn) {
        this.receivedOn = receivedOn;
    }
}
