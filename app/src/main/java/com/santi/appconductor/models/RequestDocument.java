package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestDocument{

    private int Tdoc_Id;
    private String Tdoc_Nombre;

    public RequestDocument(int tdoc_Id, String tdoc_Nombre) {
        Tdoc_Id = tdoc_Id;
        Tdoc_Nombre = tdoc_Nombre;
    }

    public int getTdoc_Id() {
        return Tdoc_Id;
    }

    public void setTdoc_Id(int tdoc_Id) {
        Tdoc_Id = tdoc_Id;
    }

    public String getTdoc_Nombre() {
        return Tdoc_Nombre;
    }

    public void setTdoc_Nombre(String tdoc_Nombre) {
        Tdoc_Nombre = tdoc_Nombre;
    }

}
