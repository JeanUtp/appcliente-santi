package com.santi.appconductor.models;

public class RespondeVehiculo {

    private int marc_id;
    private int Mode_Id;
    private int Cond_Id;
    private int Vehi_AnioFab;
    private String Vehi_NroPlaca;

    public RespondeVehiculo() {}

    public RespondeVehiculo(int marc_id, int mode_Id, int cond_Id, int vehi_AnioFab, String vehi_NroPlaca) {
        this.marc_id = marc_id;
        Mode_Id = mode_Id;
        Cond_Id = cond_Id;
        Vehi_AnioFab = vehi_AnioFab;
        Vehi_NroPlaca = vehi_NroPlaca;
    }

    public int getMarc_id() {
        return marc_id;
    }

    public void setMarc_id(int marc_id) {
        this.marc_id = marc_id;
    }

    public int getMode_Id() {
        return Mode_Id;
    }

    public void setMode_Id(int mode_Id) {
        Mode_Id = mode_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getVehi_AnioFab() {
        return Vehi_AnioFab;
    }

    public void setVehi_AnioFab(int vehi_AnioFab) {
        Vehi_AnioFab = vehi_AnioFab;
    }

    public String getVehi_NroPlaca() {
        return Vehi_NroPlaca;
    }

    public void setVehi_NroPlaca(String vehi_NroPlaca) {
        Vehi_NroPlaca = vehi_NroPlaca;
    }
}
