package com.santi.appconductor.models;

public class ResponsePais {
   private int Pais_Id;
   private String Pais_Nombre;
   private String Pais_CountryCode;
   private int Mone_Id;
   private String Pais_Icono;
   private String Pais_PrefijoTelefono;
   private boolean Pais_Activo;
   private int Pais_CaracterTelefono;
   private String Mone_Abreviatura;
   private String Mone_Simbolo;

    public ResponsePais() {}

    public ResponsePais(int pais_Id, String pais_Nombre, String pais_CountryCode, int mone_Id, String pais_Icono, String pais_PrefijoTelefono, boolean pais_Activo, int pais_CaracterTelefono, String mone_Abreviatura, String mone_Simbolo) {
        Pais_Id = pais_Id;
        Pais_Nombre = pais_Nombre;
        Pais_CountryCode = pais_CountryCode;
        Mone_Id = mone_Id;
        Pais_Icono = pais_Icono;
        Pais_PrefijoTelefono = pais_PrefijoTelefono;
        Pais_Activo = pais_Activo;
        Pais_CaracterTelefono = pais_CaracterTelefono;
        Mone_Abreviatura = mone_Abreviatura;
        Mone_Simbolo = mone_Simbolo;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public String getPais_Nombre() {
        return Pais_Nombre;
    }

    public void setPais_Nombre(String pais_Nombre) {
        Pais_Nombre = pais_Nombre;
    }

    public String getPais_CountryCode() {
        return Pais_CountryCode;
    }

    public void setPais_CountryCode(String pais_CountryCode) {
        Pais_CountryCode = pais_CountryCode;
    }

    public int getMone_Id() {
        return Mone_Id;
    }

    public void setMone_Id(int mone_Id) {
        Mone_Id = mone_Id;
    }

    public String getPais_Icono() {
        return Pais_Icono;
    }

    public void setPais_Icono(String pais_Icono) {
        Pais_Icono = pais_Icono;
    }

    public String getPais_PrefijoTelefono() {
        return Pais_PrefijoTelefono;
    }

    public void setPais_PrefijoTelefono(String pais_PrefijoTelefono) {
        Pais_PrefijoTelefono = pais_PrefijoTelefono;
    }

    public boolean isPais_Activo() {
        return Pais_Activo;
    }

    public void setPais_Activo(boolean pais_Activo) {
        Pais_Activo = pais_Activo;
    }

    public int getPais_CaracterTelefono() {
        return Pais_CaracterTelefono;
    }

    public void setPais_CaracterTelefono(int pais_CaracterTelefono) {
        Pais_CaracterTelefono = pais_CaracterTelefono;
    }

    public String getMone_Abreviatura() {
        return Mone_Abreviatura;
    }

    public void setMone_Abreviatura(String mone_Abreviatura) {
        Mone_Abreviatura = mone_Abreviatura;
    }

    public String getMone_Simbolo() {
        return Mone_Simbolo;
    }

    public void setMone_Simbolo(String mone_Simbolo) {
        Mone_Simbolo = mone_Simbolo;
    }
}
