package com.santi.appconductor.models;

public class RequestUpdateNumber {

    private int Pais_Id;
    private int Cond_Id;
    private String Cond_NroTelefono;

    public RequestUpdateNumber() {}

    public RequestUpdateNumber(int pais_Id, int cond_Id, String cond_NroTelefono) {
        Pais_Id = pais_Id;
        Cond_Id = cond_Id;
        Cond_NroTelefono = cond_NroTelefono;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public String getCond_NroTelefono() {
        return Cond_NroTelefono;
    }

    public void setCond_NroTelefono(String cond_NroTelefono) {
        Cond_NroTelefono = cond_NroTelefono;
    }
}
