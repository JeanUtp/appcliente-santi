package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestCancelService implements Serializable {
    private int Serv_Id;
    private int Clie_Id;
    private int Cond_Id;
    private String Serv_Comentario;

    public RequestCancelService() {
    }

    public RequestCancelService(int serv_Id, int clie_Id, int cond_Id, String serv_Comentario) {
        Serv_Id = serv_Id;
        Clie_Id = clie_Id;
        Cond_Id = cond_Id;
        Serv_Comentario = serv_Comentario;
    }

    public int getServ_Id() {
        return Serv_Id;
    }

    public void setServ_Id(int serv_Id) {
        Serv_Id = serv_Id;
    }

    public int getClie_Id() {
        return Clie_Id;
    }

    public void setClie_Id(int clie_Id) {
        Clie_Id = clie_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public String getServ_Comentario() {
        return Serv_Comentario;
    }

    public void setServ_Comentario(String serv_Comentario) {
        Serv_Comentario = serv_Comentario;
    }
}
