package com.santi.appconductor.models;

import java.io.Serializable;

public class TiempodeViaje implements Serializable {

    private int Tiempo;

    public TiempodeViaje() {}

    public TiempodeViaje(int tiempo) {
        Tiempo = tiempo;
    }

    public int getTiempo() {
        return Tiempo;
    }

    public void setTiempo(int tiempo) {
        Tiempo = tiempo;
    }
}
