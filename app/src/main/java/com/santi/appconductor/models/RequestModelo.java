package com.santi.appconductor.models;

public class RequestModelo {

    private int Mode_Id;
    private String Mode_Nombre;
    private int Marc_Id;
    private String Marc_Nombre;

    public int getMode_Id() {
        return Mode_Id;
    }

    public void setMode_Id(int mode_Id) {
        Mode_Id = mode_Id;
    }

    public String getMode_Nombre() {
        return Mode_Nombre;
    }

    public void setMode_Nombre(String mode_Nombre) {
        Mode_Nombre = mode_Nombre;
    }

    public int getMarc_Id() {
        return Marc_Id;
    }

    public void setMarc_Id(int marc_Id) {
        Marc_Id = marc_Id;
    }

    public String getMarc_Nombre() {
        return Marc_Nombre;
    }

    public void setMarc_Nombre(String marc_Nombre) {
        Marc_Nombre = marc_Nombre;
    }
}
