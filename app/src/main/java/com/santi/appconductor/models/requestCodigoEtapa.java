package com.santi.appconductor.models;

import java.io.Serializable;

public class requestCodigoEtapa implements Serializable {
    private int Etap_Id;
    private String Descripcion;

    public requestCodigoEtapa() {}

    public requestCodigoEtapa(int etap_Id, String descripcion) {
        Etap_Id = etap_Id;
        Descripcion = descripcion;
    }

    public int getEtap_Id() {
        return Etap_Id;
    }

    public void setEtap_Id(int etap_Id) {
        Etap_Id = etap_Id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }
}
