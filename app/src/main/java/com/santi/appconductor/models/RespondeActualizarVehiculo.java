package com.santi.appconductor.models;

public class RespondeActualizarVehiculo {

    private int Mode_Id;
    private int Cond_Id;
    private int Vehi_AnioFab;
    private String Vehi_NroPlaca;
    private String Vehi_Foto1;
    private String Vehi_Foto2;

    public RespondeActualizarVehiculo() {}

    public RespondeActualizarVehiculo(int mode_Id, int cond_Id, int vehi_AnioFab, String vehi_NroPlaca, String vehi_Foto1, String vehi_Foto2) {
        Mode_Id = mode_Id;
        Cond_Id = cond_Id;
        Vehi_AnioFab = vehi_AnioFab;
        Vehi_NroPlaca = vehi_NroPlaca;
        Vehi_Foto1 = vehi_Foto1;
        Vehi_Foto2 = vehi_Foto2;
    }

    public int getMode_Id() {
        return Mode_Id;
    }

    public void setMode_Id(int mode_Id) {
        Mode_Id = mode_Id;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public int getVehi_AnioFab() {
        return Vehi_AnioFab;
    }

    public void setVehi_AnioFab(int vehi_AnioFab) {
        Vehi_AnioFab = vehi_AnioFab;
    }

    public String getVehi_NroPlaca() {
        return Vehi_NroPlaca;
    }

    public void setVehi_NroPlaca(String vehi_NroPlaca) {
        Vehi_NroPlaca = vehi_NroPlaca;
    }

    public String getVehi_Foto1() {
        return Vehi_Foto1;
    }

    public void setVehi_Foto1(String vehi_Foto1) {
        Vehi_Foto1 = vehi_Foto1;
    }

    public String getVehi_Foto2() {
        return Vehi_Foto2;
    }

    public void setVehi_Foto2(String vehi_Foto2) {
        Vehi_Foto2 = vehi_Foto2;
    }
}
