package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestEndService implements Serializable {
    private int Serv_Id;
    private int Fpag_Id;
    private double Serv_Precio;

    public RequestEndService() {
    }

    public RequestEndService(int serv_Id, int fpag_Id, double serv_Precio) {
        Serv_Id = serv_Id;
        Fpag_Id = fpag_Id;
        Serv_Precio = serv_Precio;
    }

    public int getServ_Id() {
        return Serv_Id;
    }

    public void setServ_Id(int serv_Id) {
        Serv_Id = serv_Id;
    }

    public int getFpag_Id() {
        return Fpag_Id;
    }

    public void setFpag_Id(int fpag_Id) {
        Fpag_Id = fpag_Id;
    }

    public double getServ_Precio() {
        return Serv_Precio;
    }

    public void setServ_Precio(double serv_Precio) {
        Serv_Precio = serv_Precio;
    }
}
