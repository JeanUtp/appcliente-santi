package com.santi.appconductor.models;

import java.io.Serializable;

public class RequestCodigo implements Serializable {
    private int Cond_Id;
    private int Econ_Id;
    private requestCodigoEtapa Etapa;

    public RequestCodigo() {}

    public RequestCodigo(int cond_Id, int econ_Id, requestCodigoEtapa etapa) {
        Cond_Id = cond_Id;
        Econ_Id = econ_Id;
        Etapa = etapa;
    }

    public int getCond_Id() {
        return Cond_Id;
    }

    public void setCond_Id(int cond_Id) {
        Cond_Id = cond_Id;
    }

    public requestCodigoEtapa getEtapa() {
        return Etapa;
    }

    public void setEtapa(requestCodigoEtapa etapa) {
        Etapa = etapa;
    }

    public int getEcon_Id() {
        return Econ_Id;
    }

    public void setEcon_Id(int econ_Id) {
        Econ_Id = econ_Id;
    }
}
