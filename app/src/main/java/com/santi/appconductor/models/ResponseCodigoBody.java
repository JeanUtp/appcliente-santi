package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseCodigoBody implements Serializable {
    private boolean Estado;
    private RequestCodigo Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseCodigoBody() {}

    public ResponseCodigoBody(boolean estado, RequestCodigo valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public RequestCodigo getValor() {
        return Valor;
    }

    public void setValor(RequestCodigo valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}
