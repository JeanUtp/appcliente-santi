package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseDriverBody implements Serializable {
    private boolean Estado;
    private ResponseDriver Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseDriverBody() {

    }

    public ResponseDriverBody(boolean estado, ResponseDriver valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public ResponseDriver getValor() {
        return Valor;
    }

    public void setValor(ResponseDriver valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}
