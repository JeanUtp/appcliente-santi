package com.santi.appconductor.models;

import java.io.Serializable;

public class ResponseCuentaBancariaBody implements Serializable {
    private boolean Estado;
    private CuentaBancaria Valor;
    private String Mensaje;
    private Integer CodError;

    public ResponseCuentaBancariaBody() {

    }

    public ResponseCuentaBancariaBody(boolean estado, CuentaBancaria valor, String mensaje, Integer codError) {
        Estado = estado;
        Valor = valor;
        Mensaje = mensaje;
        CodError = codError;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean estado) {
        Estado = estado;
    }

    public CuentaBancaria getValor() {
        return Valor;
    }

    public void setValor(CuentaBancaria valor) {
        Valor = valor;
    }

    public String getMensaje() {
        return Mensaje;
    }

    public void setMensaje(String mensaje) {
        Mensaje = mensaje;
    }

    public Integer getCodError() {
        return CodError;
    }

    public void setCodError(Integer codError) {
        CodError = codError;
    }
}
