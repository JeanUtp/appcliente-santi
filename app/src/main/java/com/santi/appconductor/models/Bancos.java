package com.santi.appconductor.models;

public class Bancos {

    private int Banc_Id;
    private String Banc_Nombre;
    private int Pais_Id;

    public Bancos() {}

    public Bancos(int banc_Id, String banc_Nombre, int pais_Id) {
        Banc_Id = banc_Id;
        Banc_Nombre = banc_Nombre;
        Pais_Id = pais_Id;
    }

    public int getBanc_Id() {
        return Banc_Id;
    }

    public void setBanc_Id(int banc_Id) {
        Banc_Id = banc_Id;
    }

    public String getBanc_Nombre() {
        return Banc_Nombre;
    }

    public void setBanc_Nombre(String banc_Nombre) {
        Banc_Nombre = banc_Nombre;
    }

    public int getPais_Id() {
        return Pais_Id;
    }

    public void setPais_Id(int pais_Id) {
        Pais_Id = pais_Id;
    }
}
