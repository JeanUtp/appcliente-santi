package com.santi.appconductor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.button.MaterialButton;

public class InicioActivity extends AppCompatActivity {

    Bundle bundle;
    String token_fb,pais_Id,pais_Prefijo,pais_code;
    Button btnComenzar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        bundle = getIntent().getExtras();
        token_fb = getIntent().getStringExtra("token_fb");
        pais_Id = getIntent().getStringExtra("pais_Id");
        pais_Prefijo = getIntent().getStringExtra("pais_Prefijo");
        pais_code = getIntent().getStringExtra("pais_code");
        btnComenzar = findViewById(R.id.btnComenzar);
        btnComenzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(InicioActivity.this, RegistroNumeroActivity.class);
                intent.putExtra("token_fb", token_fb);
                intent.putExtra("pais_Id", pais_Id);
                intent.putExtra("pais_Prefijo", pais_Prefijo);
                intent.putExtra("pais_code", pais_code);
                System.out.println("token_fb: " + token_fb);
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() { }
}