package com.santi.appconductor.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.santi.appconductor.R;
import com.santi.appconductor.models.Travel;
import com.santi.appconductor.utils.CustomTypefaceSpan;
import com.santi.appconductor.utils.Utilities;

import java.util.List;


public class TravelAdapter extends RecyclerView.Adapter<TravelAdapter.MyViewHolder> {
    private final Context mContext;
    private final List<Travel> viajes;
    private OnClickEvent onClickEvent;

    Utilities util = new Utilities();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvFecha, tvPrecio, tvDireccion;
        public ImageButton ibDetalle;

        public MyViewHolder(View view) {
            super(view);
            tvFecha = view.findViewById(R.id.tvFecha);
            tvPrecio = view.findViewById(R.id.tvPrecio);
            tvDireccion = view.findViewById(R.id.tvDireccion);
            ibDetalle = view.findViewById(R.id.ibDetalle);
        }

        public Context getMyContext() {
            return mContext;
        }
    }

    public interface OnClickEvent {
        void onClick(Travel v, int position, View view);
    }

    public void setOnClickEvent(OnClickEvent onClickEvent) {
        this.onClickEvent = onClickEvent;
    }

    public TravelAdapter(Context mContext, List<Travel> Viajes) {
        this.mContext = mContext;
        this.viajes = Viajes;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_viaje, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Travel v = viajes.get(position);
        holder.setIsRecyclable(false);
        holder.tvFecha.setText(String.format(holder.getMyContext().getString(R.string.date_time_expression), v.getSoli_FechaRegistro(), v.getSoli_HoraRegistro()));
        holder.tvPrecio.setText(String.format(holder.getMyContext().getString(R.string.amount_expression), util.formatearNumero(v.getServ_Precio())));

        Typeface tf = Typeface.createFromAsset(holder.getMyContext().getAssets(), "font/opensans_regular.ttf");
        Typeface tfb = Typeface.createFromAsset(holder.getMyContext().getAssets(), "font/opensans_bold.ttf");

        SpannableStringBuilder SS = new SpannableStringBuilder(String.format(holder.getMyContext().getString(R.string.address_expression), v.getSoli_DestinoDireccion()));
        SS.setSpan(new CustomTypefaceSpan("", tfb), 0, 10, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        SS.setSpan (new CustomTypefaceSpan("", tf), 10, SS.length()-1, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        holder.tvDireccion.setText(SS);

        //holder.tvDireccion.setText(String.format(holder.getMyContext().getString(R.string.address_expression), v.getSoli_DestinoDireccion()));

        holder.ibDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onClickEvent != null) {
                    onClickEvent.onClick(viajes.get(position), position, view);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return viajes.size();
    }
}