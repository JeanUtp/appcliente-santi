package com.santi.appconductor.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.santi.appconductor.R;
import com.santi.appconductor.interfaces.SimpleRow;
import com.santi.appconductor.models.RequestAdjunto;
import com.santi.appconductor.models.Travel;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

public class ViajesAdapter extends RecyclerView.Adapter<ViajesAdapter.EventosUViewHolder> {

    List<Travel> listaEventosu;
    private SimpleRow eventos;
    private boolean p = false;
    public ViajesAdapter(List<Travel> listaEventosu) { this.listaEventosu = listaEventosu; }

    @NonNull
    @Override
    public EventosUViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mis_viajes, parent, false);
        return new EventosUViewHolder(view, eventos);
    }

    @Override
    public void onBindViewHolder(@NonNull EventosUViewHolder holder, int position) {
        if(position > 0 && listaEventosu.get(position -1 ).getNumeroMes() == listaEventosu.get(position).getNumeroMes()){
            p = true;
        } else {
            p = false;
        }
        holder.bind(listaEventosu.get(position));
    }

    @Override
    public int getItemCount() {
        return listaEventosu.size();
    }

    public void setEventos(SimpleRow eventos) { this.eventos = eventos; }

    public void setData(List<Travel> listaEventosu) {
        this.listaEventosu.clear();
        this.listaEventosu.addAll(listaEventosu);
        notifyDataSetChanged();
    }

    class EventosUViewHolder extends RecyclerView.ViewHolder {

        private TextView nTextView1 = itemView.findViewById(R.id.item_direccion_salida);
        private TextView nTextView2 = itemView.findViewById(R.id.item_direccion_destino);
        private TextView nTextView3 = itemView.findViewById(R.id.item_costo_ganancia);
        private TextView nTextView4 = itemView.findViewById(R.id.item_costo_precio);
        private TextView nTextView5 = itemView.findViewById(R.id.tvFechaItem);
        private TextView titleTextView = itemView.findViewById(R.id.tv_mes);
        private View container = itemView.findViewById(R.id.container_mis_viajes);


        EventosUViewHolder(@NonNull View itemView,final SimpleRow onAcciones) {
            super(itemView);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    onAcciones.onClic(position);
                }
            });
        }

        void bind(Travel listaEventosu) {
            titleTextView.setText(listaEventosu.getMes());
            if (p) {
                titleTextView.setVisibility(View.GONE);
                titleTextView.setText(listaEventosu.getMes());
            } else {
                titleTextView.setVisibility(View.VISIBLE);
            }
            nTextView1.setText(listaEventosu.getSoli_OrigenDireccion());
            nTextView2.setText(listaEventosu.getSoli_DestinoDireccion());
            Double d = listaEventosu.getMontoCalculado();
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
            otherSymbols.setDecimalSeparator('.');
            DecimalFormat mf = new DecimalFormat("0.00",otherSymbols);
            mf.setMinimumFractionDigits(2);
            String s = mf.format(d);
            nTextView3.setText(listaEventosu.getMone_Simbolo() +" "+s);

            Double dd = listaEventosu.getServ_Precio();
            DecimalFormatSymbols otherSymbolss = new DecimalFormatSymbols(Locale.US);
            otherSymbolss.setDecimalSeparator('.');
            DecimalFormat mff = new DecimalFormat("0.00",otherSymbolss);
            mff.setMinimumFractionDigits(2);
            String ss = mff.format(dd);
            nTextView4.setText(listaEventosu.getMone_Simbolo() +" " +ss);

            nTextView5.setText(listaEventosu.getSoli_FechaRegistro() + " " +listaEventosu.getSoli_HoraRegistro());
        }

    }


}
