package com.santi.appconductor.adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.santi.appconductor.R;
import com.santi.appconductor.interfaces.SimpleRow;
import com.santi.appconductor.models.ResponseResumen;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import retrofit2.http.GET;

public class ResumenAdapter extends RecyclerView.Adapter<ResumenAdapter.EventosUViewHolder> {

    List<ResponseResumen> listaEventosu;
    private SimpleRow eventos;
    public ResumenAdapter(List<ResponseResumen> listaEventosu) { this.listaEventosu = listaEventosu; }

    @NonNull
    @Override
    public EventosUViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_resumen, parent, false);
        return new EventosUViewHolder(view, eventos);
    }

    @Override
    public void onBindViewHolder(@NonNull EventosUViewHolder holder, int position) {
        holder.bind(listaEventosu.get(position));
    }

    @Override
    public int getItemCount() {
        return listaEventosu.size();
    }

    public void setEventos(SimpleRow eventos) { this.eventos = eventos; }

    public void setData(List<ResponseResumen> listaEventosu) {
        this.listaEventosu.clear();
        this.listaEventosu.addAll(listaEventosu);
        notifyDataSetChanged();
    }

    class EventosUViewHolder extends RecyclerView.ViewHolder {

        private TextView tvEstado = itemView.findViewById(R.id.tvEstado);
        private TextView item_costo_ganancia = itemView.findViewById(R.id.item_costo_ganancia);
        private TextView item_costo_precio = itemView.findViewById(R.id.item_costo_precio);
        private TextView tvFechaItem = itemView.findViewById(R.id.tvFechaItem);
        private TextView tvHoraItem = itemView.findViewById(R.id.tvHoraItem);
        private TextView tvNombre = itemView.findViewById(R.id.tvNombre);
        private ImageView info = itemView.findViewById(R.id.info);
        private View indicator_actividad_status = itemView.findViewById(R.id.indicator_actividad_status);
        EventosUViewHolder(@NonNull View itemView, final SimpleRow onAcciones) {
            super(itemView);
            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    onAcciones.onClic(position);
                }
            });
        }

        void bind(ResponseResumen listaEventosu) {
            tvEstado.setText(listaEventosu.getEabo_Nombre());
            tvNombre.setText(listaEventosu.getAbon_Flag());

            Double d = listaEventosu.getAbon_Monto();
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
            otherSymbols.setDecimalSeparator('.');
            DecimalFormat mf = new DecimalFormat("0.00",otherSymbols);
            mf.setMinimumFractionDigits(2);
            String s = mf.format(d);
            item_costo_ganancia.setText(listaEventosu.getMone_Simbolo() +""+ s);

            Double dd = listaEventosu.getAbon_Saldo();
            DecimalFormatSymbols otherSymbolss = new DecimalFormatSymbols(Locale.US);
            otherSymbols.setDecimalSeparator('.');
            DecimalFormat mff = new DecimalFormat("0.00",otherSymbolss);
            mff.setMinimumFractionDigits(2);
            String ss = mf.format(dd);
            item_costo_precio.setText(listaEventosu.getMone_Simbolo() +""+ ss);
            tvFechaItem.setText(listaEventosu.getEabo_FechaCreacion());
            tvHoraItem.setText(listaEventosu.getEabo_HoraCreacion());
            if(listaEventosu.getEabo_Nombre().equals("Rechazado")){
                info.setVisibility(View.VISIBLE);
            }else{
                info.setVisibility(View.GONE);
            }
        }

    }


}
