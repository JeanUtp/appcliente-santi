package com.santi.appconductor.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.santi.appconductor.R;
import com.santi.appconductor.models.ChatMessage;

import java.util.List;

public class ChatMessageAdapter extends RecyclerView.Adapter<ChatMessageAdapter.MyViewHolder> {
    private final List<ChatMessage> mensajes_chat;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvMensajeU, tvMensajeC;
        public RelativeLayout rlBgU, rlBgC;

        public MyViewHolder(View view) {
            super(view);
            tvMensajeU = view.findViewById(R.id.tvMensajeU);
            tvMensajeC = view.findViewById(R.id.tvMensajeC);
            rlBgU = view.findViewById(R.id.rlBgU);
            rlBgC = view.findViewById(R.id.rlBgC);
        }

    }

    public ChatMessageAdapter(List<ChatMessage> mensajeChats) {
        this.mensajes_chat = mensajeChats;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_mensaje_chat, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ChatMessage mc = mensajes_chat.get(position);
        holder.setIsRecyclable(false);
        if (mc.getChat_Emisor().equals("U")) {
            holder.rlBgU.setVisibility(View.VISIBLE);
            holder.tvMensajeU.setText(mc.getChat_Mensaje());
        } else if (mc.getChat_Emisor().equals("C")) {
            holder.rlBgC.setVisibility(View.VISIBLE);
            holder.tvMensajeC.setText(mc.getChat_Mensaje());
        }
    }

    @Override
    public int getItemCount() {
        return mensajes_chat.size();
    }
}