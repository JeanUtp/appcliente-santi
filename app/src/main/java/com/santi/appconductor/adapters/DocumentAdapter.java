package com.santi.appconductor.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.santi.appconductor.R;
import com.santi.appconductor.interfaces.SimpleRow;
import com.santi.appconductor.models.RequestAdjunto;
import com.santi.appconductor.models.RequestDocument;

import java.util.List;

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.EventosUViewHolder> {

    List<RequestAdjunto> listaEventosu;
    private SimpleRow eventos;
    public DocumentAdapter(List<RequestAdjunto> listaEventosu) { this.listaEventosu = listaEventosu; }

    @NonNull
    @Override
    public EventosUViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lista_documentos, parent, false);
        return new EventosUViewHolder(view, eventos);
    }

    @Override
    public void onBindViewHolder(@NonNull EventosUViewHolder holder, int position) {
        holder.bind(listaEventosu.get(position));
    }

    @Override
    public int getItemCount() {
        return listaEventosu.size();
    }

    public void setEventos(SimpleRow eventos) { this.eventos = eventos; }

    public void setData(List<RequestAdjunto> listaEventosu) {
        this.listaEventosu.clear();
        this.listaEventosu.addAll(listaEventosu);
        notifyDataSetChanged();
    }

    class EventosUViewHolder extends RecyclerView.ViewHolder {

        private TextView nTextView1 = itemView.findViewById(R.id.item_nombre_doc);
        private ImageView img_ir = itemView.findViewById(R.id.ic_icono_next);
        private ImageView icono_documentos = itemView.findViewById(R.id.icono_documentos);
        private View container = itemView.findViewById(R.id.container_doc);


        EventosUViewHolder(@NonNull View itemView,final SimpleRow onAcciones) {
            super(itemView);
            img_ir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    onAcciones.onClic(position);
                }
            });

            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    onAcciones.onClic(position);
                }
            });

        }

        void bind(RequestAdjunto listaEventosu) {
                nTextView1.setText(listaEventosu.getTadj_Nombre());
                int Econ_id,Adj_id;
                Econ_id = listaEventosu.getEcon_Id();
                Adj_id = listaEventosu.getAdju_Id();

                if(listaEventosu.getEadj_Id() == 0){
                    icono_documentos.setImageResource(R.drawable.ic_documentos);
                } else if (listaEventosu.getEadj_Id() == 1){
                    icono_documentos.setImageResource(R.drawable.cargado);
                }else if (listaEventosu.getEadj_Id() == 2){
                    icono_documentos.setImageResource(R.drawable.ic_comprobado);
                }else if (listaEventosu.getEadj_Id() == 3){
                    icono_documentos.setImageResource(R.drawable.ic_cancelar);
                }else if (listaEventosu.getEadj_Id() == 4){
                    icono_documentos.setImageResource(R.drawable.alerta);
                }else if (listaEventosu.getEadj_Id() == 5){
                    icono_documentos.setImageResource(R.drawable.vencido);
                }

                /*switch (listaEventosu.getEcon_Id()){
                    //Poatulante o en espera (por defecto)
                    case 0:
                        icono_documentos.setImageResource(R.drawable.ic_documentos);
                        break;
                    //Aprovado (Verde)
                    case 1:
                        icono_documentos.setImageResource(R.drawable.ic_comprobado);
                        break;
                    //Observado (rojo)
                    case 2:
                        icono_documentos.setImageResource(R.drawable.ic_cancelar);
                        break;
                }*/

                /*switch (Adj_id){
                //Poatulante o en espera (por defecto)
                case 0:
                    icono_documentos.setImageResource(R.drawable.ic_documentos);
                    break;
                //Cargado (Verde)
                case 1:
                    icono_documentos.setImageResource(R.drawable.cargado);
                    break;
                //Aprovado (Verde)
                case 2:
                    icono_documentos.setImageResource(R.drawable.ic_comprobado);
                    break;
                //Rechazado (rojo)
                case 3:
                    icono_documentos.setImageResource(R.drawable.ic_cancelar);
                    break;
                //Vencido (Amarillo)
                case 4:
                    icono_documentos.setImageResource(R.drawable.alerta);
                    break;
            }*/

        }

    }


}
