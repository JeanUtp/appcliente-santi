package com.santi.appconductor;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.santi.appconductor.R;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroNumeroActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Button btnSiguiente;
    ImageView ivBandera;
    TextView tvprefijo;
    EditText etCelular;
    SharedPreferences prefs;
    Bundle bundle;
    int caracteres;
    String token_fb,pais_Id,pais_Prefijo,pais_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_numero);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        bundle = getIntent().getExtras();
        caracteres = prefs.getInt(Constants.caracteres,0);
        System.out.println("caracteres: " +caracteres);
        token_fb = getIntent().getStringExtra("token_fb");
        pais_Id = getIntent().getStringExtra("pais_Id");
        pais_Prefijo = getIntent().getStringExtra("pais_Prefijo");
        pais_code = getIntent().getStringExtra("pais_code");
        etCelular = findViewById(R.id.etCelular);
        etCelular.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        tvprefijo = findViewById(R.id.tvprefijo);
        ivBandera = findViewById(R.id.ivBandera);
        btnSiguiente = findViewById(R.id.btnSiguiente);
        Glide.with(ivBandera)
                .load(prefs.getString(Constants.icpais,""))
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .into(ivBandera);

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyb();
                if (etCelular.getText().toString().length() == caracteres) {
                    validarNumero();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.phone_required), Toast.LENGTH_SHORT).show();
                }
            }
        });

        DisplayProgressDialog(getString(R.string.validating));
        tvprefijo.setText(prefs.getString(Constants.prefij,"0"));
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvprefijo.setText(prefs.getString(Constants.prefij,""));
        Glide.with(ivBandera)
                .load(prefs.getString(Constants.icpais,""))
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .into(ivBandera);
    }

    public void validarNumero() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        System.out.println(prefs.getInt(Constants.pais_id,0));
        String id = String.valueOf(prefs.getInt(Constants.pais_id,0));
        Log.i("getValidaTelefono", etCelular.getText().toString() + " / " +id);
        Call<ResponseBody> callValidarNumeroCelular = conductorService.getValidaTelefonoConductor(etCelular.getText().toString(), id);
        callValidarNumeroCelular.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
             System.out.println(response.body());
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.i("mensaje","responseResumen: "+response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        if (response.body().isEstado()) {
                            System.out.println("phone_number: " + etCelular.getText().toString() + " - " + "code: " + response.body().getValor().toString() + "token_fb: " + token_fb);
                            Intent intent = new Intent(RegistroNumeroActivity.this, RegistroCodigoVerificacionActivity.class);
                            intent.putExtra("phone_number", etCelular.getText().toString());
                            intent.putExtra("code", response.body().getValor().toString());
                            intent.putExtra("token_fb", token_fb);
                            intent.putExtra("pais_Id", pais_Id);
                            intent.putExtra("num_telefono", etCelular.getText().toString());
                            startActivity(intent);
                            finish();
                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.phone_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.phone_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.phone_error));
            }
        });
    }

}