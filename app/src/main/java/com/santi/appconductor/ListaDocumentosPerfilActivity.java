package com.santi.appconductor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.santi.appconductor.adapters.DocumentAdapter;
import com.santi.appconductor.interfaces.SimpleRow;
import com.santi.appconductor.models.RequestAdjunto;
import com.santi.appconductor.models.ResponseAdjuntoBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaDocumentosPerfilActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Bundle bundle;
    Button terminar;
    SharedPreferences prefs;
    private ResponseAdjuntoBody responseAdjuntoBody;
    private List<RequestAdjunto> requestAdjuntos = new ArrayList();
    TextView tool_titulo_atras;
    ImageView iv_Close;
    RecyclerView recy_lista_docs;
    DocumentAdapter documentAdapter;
    String pais_Id, cond_id;
    int cond, pas;
    private List<RequestAdjunto> documentList = new ArrayList<>();
    ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_documentos_perfil);
        setupUI(getWindow().getDecorView().getRootView());
        iv_Close = findViewById(R.id.iv_Close);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Documentos Adjuntos");

        pais_Id = getIntent().getStringExtra("pais_Id");
        cond_id = getIntent().getStringExtra("cond_id");
        System.out.println("cond_id qui: " + cond_id);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        bundle = getIntent().getExtras();
        recy_lista_docs = findViewById(R.id.recy_lista_docs);
        listarDocumentos();
    }

    private void listarDocumentos() {
        cond = prefs.getInt(Constants.primary_id, 0);
        pas = prefs.getInt(Constants.pais_id, 0);
        Call<ResponseAdjuntoBody> callValidarCodigoVerificacion = conductorService.getListarAdjuntos(pas,cond);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseAdjuntoBody>() {
            @Override
            public void onResponse(Call<ResponseAdjuntoBody> call, Response<ResponseAdjuntoBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            responseAdjuntoBody = response.body();
                            documentList = response.body().getValor();
                            documentAdapter = new DocumentAdapter(response.body().getValor());
                            documentAdapter.setEventos(eventos());
                            LinearLayoutManager linearLayoutManager=new LinearLayoutManager(ListaDocumentosPerfilActivity.this);
                            recy_lista_docs.setLayoutManager(linearLayoutManager);
                            recy_lista_docs.setHasFixedSize(true);
                            recy_lista_docs.setAdapter(documentAdapter);
                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseAdjuntoBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        listarDocumentos();
    }

    private SimpleRow eventos() {
        SimpleRow s = new SimpleRow() {
            @Override
            public void onClic(int position) {
                RequestAdjunto d = documentList.get(position);
                Intent intent = new Intent(ListaDocumentosPerfilActivity.this, InformacionDocumentosActivity.class);
                intent.putExtra("documento", d);
                startActivity(intent);
            }
        };
        return s;
    }

}