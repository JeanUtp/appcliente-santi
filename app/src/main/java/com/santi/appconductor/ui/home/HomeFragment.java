package com.santi.appconductor.ui.home;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Camera;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.santi.appconductor.MenuActivity;
import com.santi.appconductor.R;
import com.santi.appconductor.models.ActiveService;
import com.santi.appconductor.models.RequestAlerta;
import com.santi.appconductor.models.ResponseActiveService;
import com.santi.appconductor.models.ResponseActiveServiceBody;
import com.santi.appconductor.models.ResponseTiempodeViajeBody;
import com.santi.appconductor.models.TiempoDeViajeImput;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment implements HomeFragmentViews, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    LinearLayout llOpciones, llCall, llChat, llArrived, llStart, llCancel;
    TextView txtcliente,txtorigen,txtdestino,txttiempoviaje,txtprecio, txtcliente1,txtorigen1,txtdestino1,txttiempoviaje1,tittiempoviaje,tittiempoviaje1;
    LinearLayout btnFinish;
    Button btnterminar;

    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    double latDest, longDest;
    Marker mCurrLocationMarker;
    Marker mDestinationMarker;
    Marker mRecojoMarker;
    Polyline route = null;
    String ubicacionDestino1,ubicacionDestino2;
    String precio;
    ImageView ivMyLocation, ivOpenIn;

    SharedPreferences prefs;

    boolean map_ready = false;
    String titleDest, addressDest, pending_title = null;
    double pending_lat;
    double pending_lng;
    String pending_address = null;
    boolean pending_show_options = false;
    boolean shouldZoom = true;
    boolean inRoute = false;
    Timer tUpdateLocation;
    MenuActivity ma;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("pruebaError","onCreateView: "+container);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        prefs = getContext().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);

        btnFinish = root.findViewById(R.id.btnFinish);
        btnterminar = root.findViewById(R.id.btnterminar);
        btnterminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuActivity ma = (MenuActivity) getActivity();
                if (ma != null) {
                    ma.confirmTravelEnded();
                }
            }
        });

        ma = (MenuActivity) getActivity();

        llOpciones = root.findViewById(R.id.llOpciones);

        llCall = root.findViewById(R.id.llCall);
        llChat = root.findViewById(R.id.llChat);
        llArrived = root.findViewById(R.id.llArrived);
        llStart = root.findViewById(R.id.llStart);
        llCancel = root.findViewById(R.id.llCancel);

        txtcliente = root.findViewById(R.id.txtcliente);
        txtorigen = root.findViewById(R.id.txtorigen);
        txtdestino = root.findViewById(R.id.txtdestino);
        txttiempoviaje = root.findViewById(R.id.txttiempoviaje);
        txtprecio = root.findViewById(R.id.txtprecio);

        txtcliente1 = root.findViewById(R.id.txtcliente1);
        txtorigen1 = root.findViewById(R.id.txtorigen1);
        txtdestino1 = root.findViewById(R.id.txtdestino1);
        txttiempoviaje1 = root.findViewById(R.id.txttiempoviaje1);
        tittiempoviaje = root.findViewById(R.id.tittiempoviaje);
        tittiempoviaje1 = root.findViewById(R.id.tittiempoviaje1);
        txtcliente.setText(prefs.getString(Constants.client,""));
        txtorigen.setText(prefs.getString(Constants.orig,""));
        txtdestino.setText(prefs.getString(Constants.desti,""));

        llCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ma != null) {
                    ma.callClient();
                }
            }
        });

        llChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ma != null) {
                    ma.gotoChat();
                }
            }
        });

        llArrived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ma != null) {
                    ma.confirmDriverArrived();
                }
            }
        });

        llStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ma != null) {
                    ma.confirmDriverLeaving();
                }
            }
        });

        llCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ma != null) {
                    ma.confirmCancelService();
                }
            }
        });

        mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map); ;//root.findViewById(R.id.map);

        if (mapFrag != null) {
            Log.d("pruebaError","getMapAsync<-");

            mapFrag.getMapAsync(this);
        }

        ivMyLocation = root.findViewById(R.id.ivMyLocation);
        ivMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMyLocation();
            }
        });

        ivOpenIn = root.findViewById(R.id.ivOpenIn);
        ivOpenIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDestination();
            }
        });

        return root;
    }

    private void openDestination() {
        String uri = "";
        Intent intent;
        try {

            uri = String.format(Locale.ENGLISH,"geo:" + latDest + "," + longDest + "?q=" + latDest + "," + longDest +" ("+ addressDest + ")");
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            try {
                Intent unrestrictedIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(unrestrictedIntent);
            } catch (ActivityNotFoundException innerEx) {
                Toast.makeText(getContext(), getString(R.string.no_map_apps), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap=googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(false);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                ivMyLocation.setVisibility(View.VISIBLE);
            } else {
                checkLocationPermission();
            }
        }
        else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(false);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
            ivMyLocation.setVisibility(View.VISIBLE);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(100);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location) {
        try {
            Log.d("LOCATION_DETECTED_GPS", "location detected(H): " + location.getLatitude() + "," + location.getLongitude());
            mLastLocation = location;
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

            SharedPreferences.Editor editor = prefs.edit();
            String ubicacion = location.getLatitude() + "," + location.getLongitude();
            Log.d("Location","location updated on storage");
            editor.putString(Constants.last_location, ubicacion);
            editor.apply();

            if (mCurrLocationMarker != null) {
                Log.d("LOCATION_DETECTED_GPS", "location updated (H)");
                //mCurrLocationMarker.remove();
                mCurrLocationMarker.setPosition(latLng);
            } else {
                Log.d("LOCATION_DETECTED_GPS", "location created - first time");
                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_sedan);
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(getString(R.string.current_location));
                markerOptions.icon(icon);
                mCurrLocationMarker = mGoogleMap.addMarker(markerOptions);
            }

            map_ready = true;
            if (pending_address != null && pending_title != null) {
                addDestination(pending_title, pending_lat, pending_lng, pending_address, pending_show_options);
            } else {
                if (inRoute) {
                    if (mDestinationMarker != null) {
                        mDestinationMarker.remove();
                    }

                    if (mRecojoMarker != null) {
                        mRecojoMarker.remove();
                    }

                    BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_destination);
                    LatLng latLngDest = new LatLng(latDest, longDest);
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLngDest);
                    markerOptions.title(titleDest);
                    markerOptions.snippet(String.format(getString(R.string.marker_snippet_format), addressDest));
                    markerOptions.icon(icon);
                    mDestinationMarker = mGoogleMap.addMarker(markerOptions);

                    ivOpenIn.setVisibility(View.VISIBLE);

                    mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        @Override
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(Marker marker) {

                            LinearLayout info = new LinearLayout(getContext());
                            info.setOrientation(LinearLayout.VERTICAL);

                            TextView title = new TextView(getContext());
                            title.setTextColor(Color.BLACK);
                            title.setGravity(Gravity.CENTER);
                            title.setTypeface(null, Typeface.BOLD);
                            title.setText(marker.getTitle());

                            TextView snippet = new TextView(getContext());
                            snippet.setTextColor(Color.GRAY);
                            snippet.setText(marker.getSnippet());

                            info.addView(title);
                            info.addView(snippet);

                            return info;
                        }
                    });

                    LatLng origin = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    LatLng dest = new LatLng(latDest, longDest);

                    if (route == null) {
                        String url = getDirectionsUrl(origin, dest);

                        DownloadTask downloadTask = new DownloadTask();

                        downloadTask.execute(url);
                    }
                } else {
                    if (route != null) {
                        route.remove();
                    }
                    if (mDestinationMarker != null) {
                        mDestinationMarker.remove();
                    }

                    if (mRecojoMarker != null) {
                        mRecojoMarker.remove();
                    }
                }
            }

            if (shouldZoom) {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(16));
                shouldZoom = false;
            }

            if(prefs.getString(Constants.destino_solicitud_menu,"0").length() > 1){

                solicitudViaje("titleDestino", -11.939229, -77.043171, "AddressR",
                        "titleRecojo",-11.945559, -77.091429, "AddressR");
            }
        } catch (Exception e) {
            Log.e("Error", "error in onLocationChanged due to: " + e.getMessage());
        }
    }

    public void getMyLocation() {
        try {
            if (!inRoute) {
                shouldZoom = true;
            }

            String ubicacion = prefs.getString(Constants.last_location, "");
            if (!ubicacion.isEmpty()) {
                LatLng latLng = new LatLng(Double.parseDouble(ubicacion.split(",")[0]), Double.parseDouble(ubicacion.split(",")[1]));
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16);
                mGoogleMap.animateCamera(cameraUpdate);
            }
        } catch (Exception e) {
            Log.e("Error", "Error in getMyLocation due to: " + e.getMessage());
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                new AlertDialog.Builder(getContext())
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION );
                            }
                        })
                        .create()
                        .show();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION );
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    } else {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.must_grant_permission), Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    public void showOptions() {
        llOpciones.setVisibility(View.VISIBLE);
        btnFinish.setVisibility(View.GONE);
    }

    public void hideOptions() {
        llCall.setVisibility(View.VISIBLE);
        llChat.setVisibility(View.VISIBLE);
        llArrived.setVisibility(View.VISIBLE);
        llStart.setVisibility(View.GONE);
        llOpciones.setVisibility(View.GONE);
    }

    public void driverArrived() {
        llArrived.setVisibility(View.GONE);
        llStart.setVisibility(View.VISIBLE);
    }

    public void driverLeaving(String title, double lat, double lng, String address) {
        removeDestination();
        addDestination(title, lat, lng, address, false);
        hideOptions();
        btnFinish.setVisibility(View.VISIBLE);
    }

    public void travelEnded() {
        removeDestination();
        btnFinish.setVisibility(View.GONE);
        ivOpenIn.setVisibility(View.GONE);
    }

    public void addCard(String nombre, String origen, String destino, int tiempo) {
        System.out.println("VA addcard");
        txtcliente.setText(nombre);
        txtorigen.setText(origen);
        txtdestino.setText(destino);
        txttiempoviaje.setText(String.valueOf(tiempo)+" min");

        txtcliente1.setText(nombre);
        txtorigen1.setText(origen);
        txtdestino1.setText(destino);
        txttiempoviaje1.setText(String.valueOf(tiempo)+" min");
        ScheduleUpdateTime(Constants.short_locate_interval);
    }

    public void ScheduleUpdateTime(long time) {
        System.out.println("VA timer");
        tUpdateLocation = new Timer();
        tUpdateLocation.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                actualizarTime();
            }
        }, 0, time);
    }

    public void actualizarTime(){
        System.out.println("VA actualizarcard");
        ma.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                System.out.println("VA run");
                Call<ResponseActiveServiceBody> callVerificarServicioActivo0 = conductorService.getSeleccionarServicioActivo(prefs.getInt(Constants.primary_id,0));
                callVerificarServicioActivo0.enqueue(new Callback<ResponseActiveServiceBody>() {
                    @Override
                    public void onResponse(Call<ResponseActiveServiceBody> call, Response<ResponseActiveServiceBody> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                if (response.body().isEstado()) {
                                    ResponseActiveService sr = response.body().getValor();
                                    ubicacionDestino1 = sr.getSoli_OrigenCoordenada();
                                    ubicacionDestino2 = sr.getSoli_DestinoCoordenada();
                                    precio = String.valueOf(sr.getSoli_Precio());
                                    txtprecio.setText("I/."+precio);

                                    int eser_id = sr.getEser_Id();
                                    System.out.println("eser_id: " + eser_id);
                                    if (eser_id == 2) { //Esperando que cliente aborde
                                        System.out.println("VA 2");
                                        final String ubicacionOrigen = prefs.getString(Constants.last_location, "");
                                        TiempoDeViajeImput tiempoDeViajeImput = new TiempoDeViajeImput();
                                        tiempoDeViajeImput.setCoordenadaOrigen(ubicacionOrigen);
                                        tiempoDeViajeImput.setCoordenadaDestino(ubicacionDestino1);
                                        System.out.println("Origen1: "+ubicacionOrigen);
                                        System.out.println("Destino1: "+ubicacionDestino1);
                                        Call<ResponseTiempodeViajeBody> callVerificarServicioActivo1 = conductorService.getObtenerTiempoo(tiempoDeViajeImput);
                                        callVerificarServicioActivo1.enqueue(new Callback<ResponseTiempodeViajeBody>() {
                                            @Override
                                            public void onResponse(Call<ResponseTiempodeViajeBody> call, Response<ResponseTiempodeViajeBody> response) {
                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        if (response.body().isEstado()) {
                                                            System.out.println("Tiempo: "+ response.body().getValor().getTiempo());
                                                            String tiem = String.valueOf(response.body().getValor().getTiempo());
                                                            txttiempoviaje.setText(tiem+" min");
                                                            txttiempoviaje1.setText(tiem+" min");
                                                            tittiempoviaje.setText("Tiempo de recojo: ");
                                                            tittiempoviaje1.setText("Tiempo de recojo: ");
                                                        }
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<ResponseTiempodeViajeBody> call, Throwable t) {
                                            }
                                        });

                                    }else if (eser_id == 3) { //Esperando que cliente aborde
                                        System.out.println("VA 3");
                                        final String ubicacionOrigen = prefs.getString(Constants.last_location, "");
                                        TiempoDeViajeImput tiempoDeViajeImput = new TiempoDeViajeImput();
                                        tiempoDeViajeImput.setCoordenadaOrigen(ubicacionOrigen);
                                        tiempoDeViajeImput.setCoordenadaDestino(ubicacionDestino1);
                                        System.out.println("Origen1: "+ubicacionOrigen);
                                        System.out.println("Destino1: "+ubicacionDestino1);
                                        Call<ResponseTiempodeViajeBody> callVerificarServicioActivo1 = conductorService.getObtenerTiempoo(tiempoDeViajeImput);
                                        callVerificarServicioActivo1.enqueue(new Callback<ResponseTiempodeViajeBody>() {
                                            @Override
                                            public void onResponse(Call<ResponseTiempodeViajeBody> call, Response<ResponseTiempodeViajeBody> response) {
                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        if (response.body().isEstado()) {
                                                            System.out.println("Tiempo: "+ response.body().getValor().getTiempo());
                                                            String tiem = String.valueOf(response.body().getValor().getTiempo());
                                                            txttiempoviaje.setText(tiem+" min");
                                                            txttiempoviaje1.setText(tiem+" min");
                                                            tittiempoviaje.setText("Tiempo de recojo: ");
                                                            tittiempoviaje1.setText("Tiempo de recojo: ");
                                                        }
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<ResponseTiempodeViajeBody> call, Throwable t) {
                                            }
                                        });

                                    } else if (eser_id == 4) { //En viaje con cliente
                                        System.out.println("VA 4");
                                        final String ubicacionOrigen = prefs.getString(Constants.last_location, "");
                                        TiempoDeViajeImput tiempoDeViajeImput = new TiempoDeViajeImput();
                                        tiempoDeViajeImput.setCoordenadaOrigen(ubicacionOrigen);
                                        tiempoDeViajeImput.setCoordenadaDestino(ubicacionDestino2);
                                        System.out.println("Origen2: "+ubicacionOrigen);
                                        System.out.println("Destino2: "+ubicacionDestino2);
                                        Call<ResponseTiempodeViajeBody> callVerificarServicioActivo2 = conductorService.getObtenerTiempoo(tiempoDeViajeImput);
                                        callVerificarServicioActivo2.enqueue(new Callback<ResponseTiempodeViajeBody>() {
                                            @Override
                                            public void onResponse(Call<ResponseTiempodeViajeBody> call, Response<ResponseTiempodeViajeBody> response) {
                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        if (response.body().isEstado()) {
                                                            System.out.println("Tiempo: "+ response.body().getValor().getTiempo());
                                                            String tiem = String.valueOf(response.body().getValor().getTiempo());
                                                            txttiempoviaje.setText(tiem+" min");
                                                            txttiempoviaje1.setText(tiem+" min");
                                                            tittiempoviaje.setText("Tiempo de viaje: ");
                                                            tittiempoviaje1.setText("Tiempo de viaje: ");
                                                        }
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<ResponseTiempodeViajeBody> call, Throwable t) {
                                            }
                                        });
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseActiveServiceBody> call, Throwable t) {
                    }
                });
            }
        });
    }

    public void stopUpdateTime() {
        if (tUpdateLocation != null) {
            tUpdateLocation.cancel();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(Constants.time, "0");
            editor.apply();
        }
    }

    public void addDestination(String title, double lat, double lng, String address, boolean showOptions) {
        try {
            if (map_ready) {
                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_destination);

                LatLng latLng = new LatLng(lat, lng);
                titleDest = title;
                latDest = lat;
                longDest = lng;
                addressDest = address;
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(title);
                markerOptions.snippet(String.format(getString(R.string.marker_snippet_format), address));
                markerOptions.icon(icon);
                if (mDestinationMarker != null) {
                    mDestinationMarker.remove();
                }
                mDestinationMarker = mGoogleMap.addMarker(markerOptions);

                ivOpenIn.setVisibility(View.VISIBLE);

                mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {

                        LinearLayout info = new LinearLayout(getContext());
                        info.setOrientation(LinearLayout.VERTICAL);

                        TextView title = new TextView(getContext());
                        title.setTextColor(Color.BLACK);
                        title.setGravity(Gravity.CENTER);
                        title.setTypeface(null, Typeface.BOLD);
                        title.setText(marker.getTitle());

                        TextView snippet = new TextView(getContext());
                        snippet.setTextColor(Color.GRAY);
                        snippet.setText(marker.getSnippet());

                        info.addView(title);
                        info.addView(snippet);

                        return info;
                    }
                });

                LatLng origin = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                LatLng dest = new LatLng(lat, lng);

                String url = getDirectionsUrl(origin, dest);

                DownloadTask downloadTask = new DownloadTask();

                downloadTask.execute(url);

                inRoute = true;

                if (showOptions) {
                    showOptions();
                }

                pending_title = null;
                pending_address = null;
                pending_show_options = false;
            } else {
                pending_title = title;
                pending_lat = lat;
                pending_lng = lng;
                pending_address = address;
                pending_show_options = showOptions;
            }
        } catch (Exception e) {
            Log.e("Error", "error adding destination due to: " + e.getMessage());
        }
    }

    public void solicitudViaje(String titleDestino, double latDestino, double lngDestino, String addressDestino,
                               String titleRecojo, double latRecojo, double lngRecojo, String addressRecojo) {
        try {
            if (map_ready) {

                /* DESTINO */
                BitmapDescriptor iconDestino = BitmapDescriptorFactory.fromResource(R.drawable.ic_destination);

                LatLng latLngDestino = new LatLng(latDestino, lngDestino);
                MarkerOptions markerOptionsDestino = new MarkerOptions();
                markerOptionsDestino.position(latLngDestino);
                markerOptionsDestino.title(titleDestino);
                markerOptionsDestino.snippet(String.format(getString(R.string.marker_snippet_format), addressDestino));
                markerOptionsDestino.icon(iconDestino);

                /* RECOJO */
                BitmapDescriptor iconRecojo = BitmapDescriptorFactory.fromResource(R.drawable.ic_destination);

                LatLng latLngRecojo = new LatLng(latRecojo, lngRecojo);
                MarkerOptions markerOptionsRecojo = new MarkerOptions();
                markerOptionsRecojo.position(latLngRecojo);
                markerOptionsRecojo.title(titleRecojo);
                markerOptionsRecojo.snippet(String.format(getString(R.string.marker_snippet_format), addressRecojo));
                markerOptionsRecojo.icon(iconRecojo);

                if (mDestinationMarker != null) {
                    mDestinationMarker.remove();
                }
                if (mRecojoMarker != null) {
                    mRecojoMarker.remove();
                }
                mDestinationMarker = mGoogleMap.addMarker(markerOptionsDestino);
                mRecojoMarker = mGoogleMap.addMarker(markerOptionsRecojo);

                ivOpenIn.setVisibility(View.VISIBLE);

                mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                    @Override
                    public View getInfoWindow(Marker arg0) {
                        return null;
                    }

                    @Override
                    public View getInfoContents(Marker marker) {

                        LinearLayout info = new LinearLayout(getContext());
                        info.setOrientation(LinearLayout.VERTICAL);

                        TextView title = new TextView(getContext());
                        title.setTextColor(Color.BLACK);
                        title.setGravity(Gravity.CENTER);
                        title.setTypeface(null, Typeface.BOLD);
                        title.setText(marker.getTitle());

                        TextView snippet = new TextView(getContext());
                        snippet.setTextColor(Color.GRAY);
                        snippet.setText(marker.getSnippet());

                        info.addView(title);
                        info.addView(snippet);

                        return info;
                    }
                });

                inRoute = true;

                //Este metodo solo mueve la cámara hacia solo una coordenada
                //getMyLocation();
            }
        } catch (Exception e) {
            Log.e("Error", "error adding destination due to: " + e.getMessage());
        }
    }

    public Location getmLastLocation() {
        return mLastLocation;
    }

    public void removeDestination() {
        inRoute = false;
        if (route != null)
            route.remove();
        if (mDestinationMarker != null)
            mDestinationMarker.remove();

        getMyLocation();
    }

    public void removeSolicitudViaje() {
        inRoute = false;
        if (route != null)
            route.remove();
        if (mDestinationMarker != null)
            mDestinationMarker.remove();
        if (mRecojoMarker != null)
            mRecojoMarker.remove();
        getMyLocation();
    }

    public void cancelTravel() {
        removeDestination();
        hideOptions();
        btnFinish.setVisibility(View.GONE);
        ivOpenIn.setVisibility(View.GONE);
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest) {
        // Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

        // Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

        /*// Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;*/

        String sensor = "sensor=true";
        String mode = "mode=driving";
        String key = "key=" + Constants.g_api_key; //+getResources().getString(R.string.google_maps_key);
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&" + key;

        // Output format
        String output = "json";

        // Building the url to the web service
        return "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while(( line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("URL dl error", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(13);
                lineOptions.color(Color.parseColor("#47b2e4"));
            }

            // Drawing polyline in the Google Map for the i-th route
            if(lineOptions != null) {
                if (route != null) {
                    route.remove();
                }
                route = mGoogleMap.addPolyline(lineOptions);
                ArrayList<LatLng> lista = new ArrayList<>();
                lista.add(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
                lista.add(new LatLng(latDest, longDest));
                //zoomRoute(mGoogleMap, lista);
            }
            else {
                Log.d("onPostExecute","without Polylines drawn");
            }
        }
    }

    public class DirectionsJSONParser {

        /** Receives a JSONObject and returns a list of lists containing latitude and longitude */
        public List<List<HashMap<String,String>>> parse(JSONObject jObject) {

            List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String,String>>>() ;
            JSONArray jRoutes = null;
            JSONArray jLegs = null;
            JSONArray jSteps = null;

            try {

                jRoutes = jObject.getJSONArray("routes");

                /** Traversing all routes */
                for (int i = 0; i < jRoutes.length(); i++) {
                    jLegs = ( (JSONObject)jRoutes.get(i)).getJSONArray("legs");
                    List path = new ArrayList<HashMap<String, String>>();

                    /** Traversing all legs */
                    for (int j = 0; j < jLegs.length(); j++) {
                        jSteps = ( (JSONObject)jLegs.get(j)).getJSONArray("steps");

                        /** Traversing all steps */
                        for (int k = 0; k < jSteps.length(); k++) {
                            String polyline = "";
                            polyline = (String)((JSONObject)((JSONObject)jSteps.get(k)).get("polyline")).get("points");
                            List<LatLng> list = decodePoly(polyline);

                            /** Traversing all points */
                            for (int l = 0; l < list.size(); l++) {
                                HashMap<String, String> hm = new HashMap<String, String>();
                                hm.put("lat", Double.toString(((LatLng)list.get(l)).latitude) );
                                hm.put("lng", Double.toString(((LatLng)list.get(l)).longitude) );
                                path.add(hm);
                            }
                        }
                        routes.add(path);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
            }

            return routes;
        }

        private List<LatLng> decodePoly(String encoded) {

            List<LatLng> poly = new ArrayList<LatLng>();
            int index = 0, len = encoded.length();
            int lat = 0, lng = 0;

            while (index < len) {
                int b, shift = 0, result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lat += dlat;

                shift = 0;
                result = 0;
                do {
                    b = encoded.charAt(index++) - 63;
                    result |= (b & 0x1f) << shift;
                    shift += 5;
                } while (b >= 0x20);
                int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
                lng += dlng;

                LatLng p = new LatLng((((double) lat / 1E5)),
                        (((double) lng / 1E5)));
                poly.add(p);
            }

            return poly;
        }
    }

    public void zoomRoute(GoogleMap googleMap, List<LatLng> lstLatLngRoute) {

        if (googleMap == null || lstLatLngRoute == null || lstLatLngRoute.isEmpty()) return;

        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : lstLatLngRoute)
            boundsBuilder.include(latLngPoint);

        int routePadding = 100;
        LatLngBounds latLngBounds = boundsBuilder.build();

        googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, routePadding));
    }
}
