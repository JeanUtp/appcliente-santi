package com.santi.appconductor;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RechazadoActivity extends BaseActivity {

    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Button btnSalir;
    TextView tv2;
    String numeroayuda, correo;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rechazado);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        tv2 = findViewById(R.id.tv2);
        btnSalir = findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.apply();
                Intent intent = new Intent(RechazadoActivity.this, SplashScreenActivity.class);
                startActivity(intent);
                finish();
            }
        });

        consultarCorreoAyuda();
    }

    private void consultarCorreoAyuda() {
        //DisplayProgressDialog(getString(R.string.loading));
        //pDialog.show();
        Call<ResponseBody> callServicio = conductorService.getSeleccionarTelefonoAyudaConductor();
        callServicio.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                //if (pDialog != null && pDialog.isShowing()) {
                    //pDialog.dismiss();
                //}
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            numeroayuda = response.body().getValor().toString();
                            consultarCorAyuda();
                        } else {
                            Toast.makeText(RechazadoActivity.this, response.body().getMensaje(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(RechazadoActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RechazadoActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //if (pDialog != null && pDialog.isShowing()) {
                    //pDialog.dismiss();
                //}
                Toast.makeText(RechazadoActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void consultarCorAyuda() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        Call<ResponseBody> callServicio = conductorService.getSeleccionarCorreoAyudaConductor();
        callServicio.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            correo = response.body().getValor().toString();
                            tv2.setText("Su solicitud fue rechazada por nuestro equipo. Para mayor información comunicarse con la central al "+ numeroayuda +" o escribir a "+correo+".\n\n- Equipo Santi.");
                        } else {
                            Toast.makeText(RechazadoActivity.this, response.body().getMensaje(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(RechazadoActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(RechazadoActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Toast.makeText(RechazadoActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public void onBackPressed() { }
}