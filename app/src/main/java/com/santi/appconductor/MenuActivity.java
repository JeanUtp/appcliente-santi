package com.santi.appconductor;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.santi.appconductor.models.ActiveService;
import com.santi.appconductor.models.CalificacionClientePost;
import com.santi.appconductor.models.ChatMessage;
import com.santi.appconductor.models.FirebaseMessage;
import com.santi.appconductor.models.PaymentType;
import com.santi.appconductor.models.RequestAddService;
import com.santi.appconductor.models.RequestAlerta;
import com.santi.appconductor.models.RequestCancelService;
import com.santi.appconductor.models.RequestEndService;
import com.santi.appconductor.models.RequestUpdateDriverLocation;
import com.santi.appconductor.models.RequestUpdateFirebaseToken;
import com.santi.appconductor.models.RequestUpdateService;
import com.santi.appconductor.models.ResponseActiveService;
import com.santi.appconductor.models.ResponseActiveServiceBody;
import com.santi.appconductor.models.ResponseAddService;
import com.santi.appconductor.models.ResponseAddServiceBody;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseDriver;
import com.santi.appconductor.models.ResponseDriverBody;
import com.santi.appconductor.models.ResponseTiempodeViajeBody;
import com.santi.appconductor.models.TiempoDeViajeImput;
import com.santi.appconductor.services.AliveFirebase;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.services.LocationService;
import com.santi.appconductor.ui.home.HomeFragment;
import com.santi.appconductor.utils.Constants;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.santi.appconductor.utils.Constants.monesym;

public class MenuActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    SharedPreferences prefs;
    private AppBarConfiguration mAppBarConfiguration;
    ImageView ivMenu,ivBotonPanico,ivFotoPerfil;
    LinearLayout llProfile, llMyTravels, llResume, llCallCentral,llCuentaBancaria,llSolicitudViajeMenu;
    TextView txtrecojoMenu,txttiempoMenu,txtclienteMenu,txtorigenMenu,txtdestinoMenu, txtprecioMenu;
    TextView tx_apellidos,tx_nombres,vermas;
    LinearLayout deuda;
    Bundle bundle;
    Button btnHelp, btnRechazarMenu, btnAceptarMenu;;
    Switch btnDisponible;
    SharedPreferences.Editor editor;
    DrawerLayout drawer;
    ResponseDriver conductorResponse = new ResponseDriver();
    List<ActiveService> serviciosActivos = new ArrayList<>();
    List<PaymentType> formas_pago = new ArrayList<>();
    List<ChatMessage> mensajes_chat = new ArrayList<>();
    List<String> pending_messages = new ArrayList<>();
    boolean available, busy = false, chatAvailable, chatOpen = false,dispo;
    int sol_id = -1;
    int serv_id = -1;
    int clie_id = -1;
    String cliente, origen, destino, coordOrigen, coordDestino, telfCliente = "",fotocliente,clienteok;
    double monto = 12.00d;
    int fpag_id = -1;
    Boolean valor;
    boolean askedforGPS = false;
    boolean paused = false;
    int tiempoviaje;
    int timetravel;
    private boolean servactive = false;
    Timer tLocation;
    Timer tUpdateLocation;
    Timer tFirebase;
    ProgressDialog pDialogg;
    AlertDialog adDisplayService;
    AlertDialog adDQ;

    SettingsClient mSettingsClient;
    LocationSettingsRequest mLocationSettingsRequest;
    private static final int REQUEST_CHECK_SETTINGS = 214;
    private static final int OPEN_CHAT = 1000;
    private static final int MY_TRAVELS = 2000;
    private static final int RESUME = 3000;
    private static final int PROFILE = 4000;
    private static final int BANC = 5000;

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver), new IntentFilter("MyData"));
        getApplicationContext().registerReceiver(mGpsSwitchStateReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        getApplicationContext().unregisterReceiver(mGpsSwitchStateReceiver);

    }

    public void processMessage(FirebaseMessage mensaje) {
        System.out.println("Entro" + " / " + available);
        if (true) {
            try {
                final JSONObject obj = new JSONObject(Objects.requireNonNull(mensaje.getMessage()));
                if (obj.has("Tipo")) {
                    if (adDQ != null && adDQ.isShowing()) {
                        adDQ.dismiss();
                        serviciosActivos.remove(0);
                    }

                    System.out.println("Tipo: "+obj.getInt("Tipo"));
                    switch (obj.getInt("Tipo")) {

                        case 1:{
                            /*if (!busy) {*/
                            if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                                llSolicitudViajeMenu.setVisibility(View.VISIBLE);
                                playSound();

                                System.out.println("Tipo: "+obj.getInt("Tipo"));
                                sol_id = obj.getInt("Soli_Id");
                                cliente = obj.getString("Cliente");
                                System.out.println("1" + cliente);
                                origen = obj.getString("Origen");
                                destino = obj.getString("Destino");

                                editor = prefs.edit();
                                editor.putString(Constants.destino_solicitud_menu, "Destino: "+destino);
                                editor.apply();

                                fillTextTravel(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo")))
                                        ,String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje")))
                                        ,obj.getString("Cliente")
                                        ,obj.getString("Origen")
                                        ,obj.getString("Destino")
                                        ,obj.getString("Moneda")+obj.getDouble("Precio"));
                                /*

                                System.out.println("Tipo: "+obj.getInt("Tipo"));
                                sol_id = obj.getInt("Soli_Id");
                                cliente = obj.getString("Cliente");
                                System.out.println("1" + cliente);
                                origen = obj.getString("Origen");
                                destino = obj.getString("Destino");

                                Log.d("pruebaError", String.valueOf(getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().size()));


                                adDisplayService = displayService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Moneda"),
                                        new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("pruebaError1", String.valueOf(getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().size()));
                                        insertarServicio();

                                    }
                                }, new Runnable() {
                                    @Override
                                    public void run() {
                                        Log.d("pruebaError2", String.valueOf(getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().size()));

                                        checkMessages();
                                    }
                                });*/
                                expireServiceSolicitud((mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                    @Override
                                    public void run() {

                                        checkMessages();
                                    }
                                });


                            } else {
                                checkMessages();
                            }
                            /*}*/
                            break;
                        }
                        case 3: {

                            if (obj.getInt("Eser_Id") == 6 && !serviciosActivos.isEmpty() && obj.getInt("Serv_Id") == serviciosActivos.get(0).getServ_id()) {
                                adDisplayService = displayNotification(getString(R.string.service_canceled_by_customer) , new Runnable() {
                                    @Override
                                    public void run() {
                                        System.out.println("Aqui 1 cliente");
                                        checkMessages();
                                    }
                                });
                                cancelTravel();
                            }
                            break;
                        }
                        case 4: {
                            if (chatAvailable) {
                                ChatMessage msg = new ChatMessage(obj.getString("Chat_Emisor"), obj.getString("Chat_Mensaje"), obj.getString("Chat_Fecha"));
                                mensajes_chat.add(msg);
                                Gson gson = new Gson();
                                String mensajesChatString = gson.toJson(msg);
                                System.out.println("01: " + mensajesChatString);
                                if (chatOpen) {
                                    Bundle b = new Bundle();
                                    b.putSerializable("mensaje", msg);
                                    Intent i = new Intent(MenuActivity.this, ChatActivity.class);
                                    i.putExtras(b);
                                    startActivity(i);
                                } else {
                                    adDisplayService = displayNotificationChat(String.format(getString(R.string.new_message_expression), serviciosActivos.get(0).getCliente().split(" ")[0], obj.getString("Chat_Mensaje")), new Runnable() {
                                        @Override
                                        public void run() {
                                            checkMessages();
                                        }
                                    });
                                }
                            } else {
                                checkMessages();
                            }
                            break;
                        }
                        case 5: {
                            if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                                sol_id = obj.getInt("Soli_Id");
                                cliente = obj.getString("Cliente");
                                System.out.println("2" + cliente);
                                origen = obj.getString("Origen");
                                destino = obj.getString("Destino");
                                adDisplayService = displayScheduledService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), new Runnable() {
                                    @Override
                                    public void run() {
                                        addScheduledService();
                                    }
                                }, new Runnable() {
                                    @Override
                                    public void run() {
                                        checkMessages();
                                    }
                                });
                                expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                    @Override
                                    public void run() {
                                        checkMessages();
                                    }
                                });
                            } else {
                                checkMessages();
                            }
                            break;
                        }
                        case 6: {
                            adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_canceled_by_customer), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("NombreCompleto")) , new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            break;
                        }
                        case 7: {
                            adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_reminder), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("Cliente")) , new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            break;
                        }
                        case 9: {
                            /*if (!busy) {*/
                            if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                                sol_id = obj.getInt("Soli_Id");
                                cliente = obj.getString("Cliente");
                                System.out.println("3" + cliente);
                                origen = obj.getString("Origen");
                                destino = obj.getString("Destino");
                                adDisplayService = displayServiceExtended(
                                        String.format(getString(
                                                R.string.time_expression),
                                                String.valueOf(obj.getInt("TiempoDeRecojo"))),
                                                String.format(getString(R.string.time_expression),
                                                String.valueOf(obj.getInt("TiempoDeViaje"))),
                                                obj.getString("Cliente"),
                                                obj.getString("Origen"),
                                                obj.getString("Destino"),
                                                obj.getDouble("Precio"),
                                                obj.getString("Moneda"),
                                        new Runnable() {
                                    @Override
                                    public void run() {
                                        insertarServicio();
                                    }
                                }, new Runnable() {
                                    @Override
                                    public void run() {
                                        checkMessages();
                                    }
                                });
                                expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                    @Override
                                    public void run() {
                                        checkMessages();
                                    }
                                });
                            } else {
                                checkMessages();
                            }
                            /*}*/
                            break;
                        }
                        default: {
                            checkMessages();
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                checkMessages();
            }
        }
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            checkMessages();
        }
    };

    private void checkMessages() {
        if (adDisplayService == null || !adDisplayService.isShowing()) {
            List<FirebaseMessage> pending_messages = new ArrayList<>();
            String pendingmessages = prefs.getString("pending_messages", "");
            if (!pendingmessages.isEmpty() && pendingmessages.length() > 2) {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<FirebaseMessage>>(){}.getType();
                pending_messages = gson.fromJson(pendingmessages, listType);
                FirebaseMessage fbMessage = pending_messages.get(0);
                pending_messages.remove(0);

                pendingmessages = gson.toJson(pending_messages);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("pending_messages", pendingmessages);
                editor.apply();

                Log.d("fbMessage:",fbMessage.getMessage());
                processMessage(fbMessage);
            }
        }
    }

    private BroadcastReceiver mGpsSwitchStateReceiver = new BroadcastReceiver() {
        /*@Override
         public void onReceive(Context context, Intent intent) {

             if (intent.getAction().matches("android.location.PROVIDERS_CHANGED")) {
                 // Make an action or refresh an already managed state.
                 if (!isLocationEnabled(getApplicationContext())) {
                     Toast.makeText(getApplicationContext(), "Debe habilitar GPS", Toast.LENGTH_SHORT).show();
                 }
             }
         }*/
        @Override
        public void onReceive(Context context, Intent intent)
        {
            final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) ) {
                if (!isLocationEnabled(getApplicationContext())) {
                    if (!askedforGPS) {
                        askedforGPS = true;
                        displayGPS();
                    }
                }
            }
        }
    };

    private void verificarServicioActivo() {
        System.out.println("Verificar servicio activo");
        Call<ResponseActiveServiceBody> callVerificarServicioActivo = conductorService.getSeleccionarServicioActivo(prefs.getInt(Constants.primary_id,0));
        callVerificarServicioActivo.enqueue(new Callback<ResponseActiveServiceBody>() {
            @Override
            public void onResponse(Call<ResponseActiveServiceBody> call, Response<ResponseActiveServiceBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            Log.d("GPS","servicio activo!");
                            ResponseActiveService sr = response.body().getValor();

                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString(Constants.client, sr.getNombreCompleto());
                            editor.putString(Constants.orig, sr.getSoli_OrigenDireccion());
                            editor.putString(Constants.desti, sr.getSoli_DestinoDireccion());
                            editor.apply();

                            sol_id = sr.getSoli_Id();
                            cliente = sr.getNombreCompleto();
                            tiempoviaje = sr.getTiempoDeViaje();
                            System.out.println("4" + cliente);
                            fotocliente = sr.getFoto();
                            origen = sr.getSoli_OrigenDireccion();
                            destino = sr.getSoli_DestinoDireccion();
                            serv_id = sr.getServ_Id();
                            clie_id = sr.getClie_Id();
                            monto = sr.getSoli_Precio();
                            coordOrigen = sr.getSoli_OrigenCoordenada();
                            coordDestino = sr.getSoli_DestinoCoordenada();
                            fpag_id = sr.getFpag_Id();
                            int eser_id = sr.getEser_Id();
                            telfCliente = sr.getNroTelefono();
                            ivBotonPanico.setVisibility(View.VISIBLE);
                            servactive = true;
                            if (eser_id == 2) { //En busca de cliente
                                double lat = Double.parseDouble(coordOrigen.split(",")[0]);
                                double lng = Double.parseDouble(coordOrigen.split(",")[1]);
                                ActiveService ns = new ActiveService();
                                ns.setSol_id(sol_id);
                                ns.setCliente(cliente);
                                ns.setTimpodeViaje(tiempoviaje);
                                System.out.println("5" + cliente);
                                ns.setFotoCliente(fotocliente);
                                ns.setOrigen(origen);
                                ns.setDestino(destino);
                                ns.setServ_id(serv_id);
                                ns.setClie_id(clie_id);
                                ns.setMonto(monto);
                                ns.setFpag_id(fpag_id);
                                ns.setCoordOrigen(coordOrigen);
                                ns.setCoordDestino(coordDestino);
                                ns.setTelfCliente(telfCliente);
                                serviciosActivos.add(ns);
                                servactive = true;
                                System.out.println("add1");
                                addDestination(cliente, lat, lng, origen,origen,destino,tiempoviaje);
                                System.out.println("addDestination1");
                                //checkMessages();
                                /*if (serviciosActivos.size() == 1) {
                                    //mensajes_chat.clear();
                                }*/
                            } else if (eser_id == 3) { //Esperando que cliente aborde
                                double lat = Double.parseDouble(coordOrigen.split(",")[0]);
                                double lng = Double.parseDouble(coordOrigen.split(",")[1]);
                                ActiveService ns = new ActiveService();
                                ns.setSol_id(sol_id);
                                ns.setCliente(cliente);
                                ns.setTimpodeViaje(tiempoviaje);
                                System.out.println("6" + cliente);
                                ns.setFotoCliente(fotocliente);
                                ns.setOrigen(origen);
                                ns.setDestino(destino);
                                ns.setServ_id(serv_id);
                                ns.setClie_id(clie_id);
                                ns.setMonto(monto);
                                ns.setFpag_id(fpag_id);
                                ns.setCoordOrigen(coordOrigen);
                                ns.setCoordDestino(coordDestino);
                                serviciosActivos.add(ns);
                                addDestination(cliente, lat, lng, origen,origen,destino,tiempoviaje);
                                servactive = true;
                                System.out.println("addDestination2");
                                driverArrived();
                            } else if (eser_id == 4) { //En viaje con cliente
                                double lat = Double.parseDouble(coordOrigen.split(",")[0]);
                                double lng = Double.parseDouble(coordOrigen.split(",")[1]);
                                ActiveService ns = new ActiveService();
                                ns.setSol_id(sol_id);
                                ns.setCliente(cliente);
                                ns.setTimpodeViaje(tiempoviaje);
                                System.out.println("7" + cliente);
                                ns.setFotoCliente(fotocliente);
                                ns.setOrigen(origen);
                                ns.setDestino(destino);
                                ns.setServ_id(serv_id);
                                ns.setClie_id(clie_id);
                                ns.setMonto(monto);
                                ns.setFpag_id(fpag_id);
                                ns.setCoordOrigen(coordOrigen);
                                ns.setCoordDestino(coordDestino);
                                serviciosActivos.add(ns);
                                addDestination(cliente, lat, lng, origen,origen,destino,tiempoviaje);
                                btnDisponible.setVisibility(View.GONE);
                                driverLeaving();
                            }
                            stopScheduleReportGPS();
                            ScheduleReportGPS(Constants.short_report_interval);
                           //ScheduleUpdateTime(Constants.short_locate_interval);
                        }else{
                            servactive = false;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseActiveServiceBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }
        });
    }

    private void obtenertimetravel(){
        System.out.println("Obtener tiempo de servicio");
        final String ubicacionOrigen = prefs.getString(Constants.last_location, "");
        final String ubicacionDestino = coordDestino;
        System.out.println("Origen: " + ubicacionOrigen);
        System.out.println("Destino: " + ubicacionDestino);
        final TiempoDeViajeImput tiempoDeViajeImput = new TiempoDeViajeImput();

        tiempoDeViajeImput.setCoordenadaOrigen(ubicacionOrigen);
        tiempoDeViajeImput.setCoordenadaDestino(ubicacionDestino);

        Call<ResponseTiempodeViajeBody> callVerificarServicioActivo = conductorService.getObtenerTiempoo(tiempoDeViajeImput);
        callVerificarServicioActivo.enqueue(new Callback<ResponseTiempodeViajeBody>() {
            @Override
            public void onResponse(Call<ResponseTiempodeViajeBody> call, Response<ResponseTiempodeViajeBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            System.out.println("Tiempo: "+ response.body().getValor().getTiempo());
                            double lat2 = Double.parseDouble(coordOrigen.split(",")[0]);
                            double lng2 = Double.parseDouble(coordOrigen.split(",")[1]);
                            tiempoviaje = response.body().getValor().getTiempo();
                            System.out.println("cliente: "+cliente + " lat2: "+lat2+" lang2: "+lng2+" origen: "+origen+" destino: "+destino+" tiempoviaje: "+tiempoviaje);
                            addDestination(cliente, lat2, lng2, origen,origen,destino,tiempoviaje);
                            System.out.println("addDestination3");
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString(Constants.time, String.valueOf(tiempoviaje));
                            editor.apply();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseTiempodeViajeBody> call, Throwable t) {
            }
        });
    }

    private void insertarServicio() {
        HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
        if (frag.getmLastLocation() != null) {

            DisplayProgressDialog(getString(R.string.updating));
            pDialog.show();

            RequestAddService request = new RequestAddService();
            request.setSoli_Id(sol_id);
            request.setCond_Id(prefs.getInt(Constants.primary_id,0));

            Call<ResponseAddServiceBody> callInsertarServicio = conductorService.postInsertarServicio(request);
            callInsertarServicio.enqueue(new Callback<ResponseAddServiceBody>() {
                @Override
                public void onResponse(Call<ResponseAddServiceBody> call, Response<ResponseAddServiceBody> response) {
                    if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().isEstado()) {
                                ResponseAddService isResponse = response.body().getValor();
                                /*serv_id = isResponse.getServ_Id();
                                clie_id = isResponse.getClie_Id();
                                monto = isResponse.getPrecio();
                                fpag_id = isResponse.getFpag_Id();
                                coordOrigen = isResponse.getSoli_OrigenCoordenada();
                                coordDestino = isResponse.getSoli_DestinoCoordenada();
                                double lat = Double.parseDouble(coordOrigen.split(",")[0]);
                                double lng = Double.parseDouble(coordOrigen.split(",")[1]);
                                telfCliente = isResponse.getClie_NroTelefono();*/
                                ActiveService ns = new ActiveService();
                                ns.setSol_id(sol_id);
                                ns.setCliente(cliente);
                                ns.setTimpodeViaje(tiempoviaje);
                                clienteok = cliente;
                                System.out.println("8" + cliente);
                                ns.setFotoCliente(fotocliente);
                                ns.setOrigen(origen);
                                ns.setDestino(destino);
                                ns.setTelfCliente(isResponse.getClie_NroTelefono());
                                ns.setServ_id(isResponse.getServ_Id());
                                ns.setClie_id(isResponse.getClie_Id());
                                ns.setMonto(isResponse.getPrecio());
                                ns.setFpag_id(isResponse.getFpag_Id());
                                ns.setCoordOrigen(isResponse.getSoli_OrigenCoordenada());
                                ns.setCoordDestino(isResponse.getSoli_DestinoCoordenada());
                                if (serviciosActivos.isEmpty()) {
                                    serv_id = isResponse.getServ_Id();
                                    clie_id = isResponse.getClie_Id();
                                    monto = isResponse.getPrecio();
                                    fpag_id = isResponse.getFpag_Id();
                                    coordOrigen = isResponse.getSoli_OrigenCoordenada();
                                    coordDestino = isResponse.getSoli_DestinoCoordenada();
                                    //double lat = Double.parseDouble(coordOrigen.split(",")[0]);
                                    //double lng = Double.parseDouble(coordOrigen.split(",")[1]);
                                    telfCliente = isResponse.getClie_NroTelefono();
                                    mensajes_chat.clear();
                                    addDestination(cliente,  Double.parseDouble(isResponse.getSoli_OrigenCoordenada().split(",")[0]), Double.parseDouble(isResponse.getSoli_OrigenCoordenada().split(",")[1]), origen,origen,destino,tiempoviaje);
                                    //ScheduleUpdateTime(Constants.short_locate_interval);
                                    System.out.println("addDestination4");
                                    checkMessages();
                                    stopScheduleReportGPS();
                                    ScheduleReportGPS(Constants.short_report_interval);
                                } else {
                                    adDisplayService = displayMessage(getString(R.string.new_pending_service_accepted), new Runnable() {
                                        @Override
                                        public void run() {
                                            checkMessages();
                                        }
                                    });
                                    if (clie_id == serviciosActivos.get(0).getClie_id()) {
                                        ns.setExtendido(true);
                                    }
                                }
                                serviciosActivos.add(ns);
                            } else {
                                adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                    @Override
                                    public void run() {
                                        checkMessages();
                                    }
                                });
                            }
                        } else {
                            adDisplayService = displayMessage(getString(R.string.accept_service_error), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        checkMessages();
                    }
                }

                @Override
                public void onFailure(Call<ResponseAddServiceBody> call, Throwable t) {
                    if (pDialog != null && pDialog.isShowing()) {
                        pDialog.dismiss();
                    }
                    adDisplayService = displayMessage(getString(R.string.accept_service_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            });
        } else {
            adDisplayService = displayMessage(getString(R.string.cant_accept_service_due_to_gps), new Runnable() {
                @Override
                public void run() {
                    checkMessages();
                }
            });
        }
    }

    private void addScheduledService() {
        DisplayProgressDialog(getString(R.string.updating));
        pDialog.show();

        RequestAddService request = new RequestAddService();
        request.setSoli_Id(sol_id);
        request.setCond_Id(prefs.getInt(Constants.primary_id,0));

        Call<ResponseAddServiceBody> callInsertarServicio = conductorService.postInsertarServicio(request);
        callInsertarServicio.enqueue(new Callback<ResponseAddServiceBody>() {
            @Override
            public void onResponse(Call<ResponseAddServiceBody> call, Response<ResponseAddServiceBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            adDisplayService = displayMessage(getString(R.string.accepted_scheduled_service), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.accept_service_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    checkMessages();
                }
            }

            @Override
            public void onFailure(Call<ResponseAddServiceBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.accept_service_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    private void actualizarServicio(final int estado) {
        DisplayProgressDialog(getString(R.string.updating));
        pDialog.show();

        RequestUpdateService request = new RequestUpdateService();
        request.setServ_Id(serviciosActivos.get(0).getServ_id());
        request.setClie_Id(0);
        request.setCond_Id(prefs.getInt(Constants.primary_id,0));
        request.setEser_Id(estado);

        Call<ResponseBody> callActualizarServicio = conductorService.putActualizarServicio(request);
        callActualizarServicio.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            //int valor = (int)response.body().getValor();
                            switch (estado) {
                                case 3: {
                                    if (serviciosActivos.get(0).isExtendido()) {
                                        double lat = Double.parseDouble(serviciosActivos.get(0).getCoordOrigen().split(",")[0]);
                                        double lng = Double.parseDouble(serviciosActivos.get(0).getCoordOrigen().split(",")[1]);
                                        addDestination(serviciosActivos.get(0).getCliente(), lat, lng, serviciosActivos.get(0).getOrigen(),serviciosActivos.get(0).getOrigen(),serviciosActivos.get(0).getDestino(), serviciosActivos.get(0).getTimpodeViaje());
                                        System.out.println("addDestination5");
                                    }
                                    driverArrived();
                                    break;
                                }
                                case 4: {
                                    driverLeaving();
                                    break;
                                }
                            }
                            checkMessages();
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.update_service_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.update_service_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.update_service_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    private void listarFormasPago() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();

        Call<ResponseBody> callListarFormasPago = conductorService.getListarFormaPago();
        callListarFormasPago.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            List<PaymentType> temp = (List<PaymentType>)response.body().getValor();

                            formas_pago.clear();

                            for (int x = 0; x < temp.size(); x++) {
                                Object getrow = temp.get(x);
                                LinkedTreeMap<Object,Object> t = (LinkedTreeMap) getrow;
                                Double tid = (Double) t.get("Fpag_Id");
                                int id = tid.intValue();
                                String nombre = t.get("Fpag_Nombre").toString();
                                /*Gson gson = new Gson();
                                String jsString = temp.get(x).toString();
                                fp = gson.fromJson(jsString, FormaPago.class);*/
                                PaymentType fp = new PaymentType(id, nombre);
                                formas_pago.add(fp);
                            }
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.loading_payment_types_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.loading_payment_types_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
                cargarDatosConductor();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.loading_payment_types_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
                cargarDatosConductor();
            }
        });
    }

    private void anularServicio(String comentario) {
        DisplayProgressDialog(getString(R.string.canceling));
        pDialog.show();

        RequestCancelService request = new RequestCancelService();
        request.setServ_Id(serviciosActivos.get(0).getServ_id());
        request.setClie_Id(0);
        request.setCond_Id(prefs.getInt(Constants.primary_id,0));
        request.setServ_Comentario(comentario);

        Call<ResponseBody> callAnularServicio = conductorService.putAnularServicio(request);
        callAnularServicio.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            cancelTravel();
                            editor = prefs.edit();
                            editor.remove(Constants.CHAT_MENSAJES);
                            editor.apply();
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.cancel_service_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.cancel_service_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.cancel_service_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    private void finalizarServicio(int fpag_id) {
        DisplayProgressDialog(getString(R.string.finishing));
        pDialog.show();

        RequestEndService request = new RequestEndService();
        request.setServ_Id(serviciosActivos.get(0).getServ_id());
        request.setFpag_Id(fpag_id);
        request.setServ_Precio(serviciosActivos.get(0).getMonto());

        Call<ResponseBody> callService = conductorService.putFinalizarServicio(request);
        callService.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            travelEnded();
                            editor = prefs.edit();
                            editor.remove(Constants.CHAT_MENSAJES);
                            editor.apply();
                            checkMessages();
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.finish_service_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.finish_service_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.finish_service_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    private void calificarCliente(Integer Serv_Id, Integer Clie_Id, float calificacion) {
        System.out.println("aqui val");
        DisplayProgressDialog(getString(R.string.qualifying));
        pDialog.show();
        CalificacionClientePost calificacionClientePost = new CalificacionClientePost();
        calificacionClientePost.setClie_Id(Clie_Id);
        calificacionClientePost.setServ_Id(Serv_Id);
        calificacionClientePost.setServ_CalificacionCliente(calificacion);
        calificacionClientePost.setServ_CalificacionComentario("");
        Call<ResponseBody> callService = conductorService.putCalificarCliente(calificacionClientePost);
        callService.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i("mensaje","responseResumen: "+response);
                System.out.println("mensaje: " + response.body().getMensaje());
                System.out.println("Estado: " + response.body().isEstado());
                System.out.println("Valor: " + response.body().getValor());
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (!response.body().isEstado()) {
                            System.out.println("mensajew: " + response.body().getMensaje());
                            System.out.println("Calificar usuario ok");
                            editor = prefs.edit();
                            editor.remove(Constants.CHAT_MENSAJES);
                            editor.apply();
                        }
                    } else {
                        Log.d("Calificar usuario", "No se pudo enviar la calificación");
                    }
                } else {
                    Log.d("Calificar usuario", "No se pudo enviar la calificación");
                }
                serviciosActivos.remove(0);
                siguienteServicio();
                checkMessages();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.d("Calificar usuario", "No se pudo enviar la calificación");
                serviciosActivos.remove(0);
                siguienteServicio();
                checkMessages();
            }
        });
    }

    private void consultarTelfAyuda() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();

        Call<ResponseBody> callServicio = conductorService.getSeleccionarTelefonoAyudaConductor();
        callServicio.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            String numero = response.body().getValor().toString();
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:" + numero));
                            startActivity(callIntent);
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.call_help_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.call_help_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.call_help_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    private void consultarTiempoExpiracion() {
        Call<ResponseBody> callServicio = conductorService.getSeleccionarTiempoAceptarSolicitud();
        callServicio.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            long exp = Long.parseLong(response.body().getValor().toString()) * 1000 ;
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putLong("ExpiracionSolicitud", exp);
                            editor.apply();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) { }
        });
    }

    private void fillTextTravel(String recojo,String tiempo,String cliente,String origen,String destino,String precio){
        txtrecojoMenu.setText(recojo);
        txttiempoMenu.setText(tiempo);
        txtclienteMenu.setText(cliente);
        txtorigenMenu.setText(origen);
        txtdestinoMenu.setText(destino);
        txtprecioMenu.setText(precio);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        txtrecojoMenu = findViewById(R.id.txtrecojoMenu);
        txttiempoMenu = findViewById(R.id.txttiempoMenu);
        txtclienteMenu = findViewById(R.id.txtclienteMenu);
        txtorigenMenu = findViewById(R.id.txtorigenMenu);
        txtdestinoMenu = findViewById(R.id.txtdestinoMenu);
        txtprecioMenu = findViewById(R.id.txtprecioMenu);
        btnRechazarMenu = findViewById(R.id.btnRechazarMenu);
        btnAceptarMenu = findViewById(R.id.btnAceptarMenu);

        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
//        fab.setVisibility(View.VISIBLE);
        ivMenu = findViewById(R.id.ivMenu);
        btnHelp = findViewById(R.id.btnHelp);
        ivBotonPanico = findViewById(R.id.ivBotonPanico);
        deuda = findViewById(R.id.deuda);
        vermas = findViewById(R.id.vermas);
        if(prefs.getString(Constants.estado_con,"").equals("7")){
            deuda.setVisibility(View.VISIBLE);
            vermas.setText(Html.fromHtml("<u>Ver más</u>"));
            deuda.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!serviciosActivos.isEmpty()) {
                        cliente = serviciosActivos.get(0).getCliente();
                        System.out.println("9" + cliente);
                        serv_id = serviciosActivos.get(0).getServ_id();
                    }
                    Intent i = new Intent(MenuActivity.this, ResumenActivity.class);
                    Gson gson = new Gson();
                    String serviciosActivosString = gson.toJson(serviciosActivos);
                    i.putExtra("serviciosActivos", serviciosActivosString);
                    i.putExtra("cliente", cliente);
                    System.out.println("10" + cliente);
                    i.putExtra("chatAvailable", chatAvailable);
                    i.putExtra("available", available);
                    i.putExtra("busy", busy);
                    i.putExtra("serv_id", serv_id);
                    if (chatAvailable) {
                        String mensajesChatString = gson.toJson(mensajes_chat);
                        i.putExtra("mensajesChat", mensajesChatString);
                    }
                    startActivityForResult(i, RESUME);
                }
            });
        }else{
            deuda.setVisibility(View.GONE);
        }
        ivBotonPanico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
                builder.setTitle("ALERTA");
                builder.setMessage("¿Desea reportar un incidente?");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pDialogg = new ProgressDialog(MenuActivity.this);
                        pDialogg.setMessage(getString(R.string.loading));
                        pDialogg.setCancelable(false);
                        pDialogg.setIndeterminate(false);
                        pDialogg.show();
                        final String ubicacion = prefs.getString(Constants.last_location, "");
                        String[] latlong =  ubicacion.split(",");
                        double latitude = Double.parseDouble(latlong[0]);
                        double longitude = Double.parseDouble(latlong[1]);
                        LatLng location = new LatLng(latitude, longitude);
                        if (!serviciosActivos.isEmpty()) {
                            clie_id = serviciosActivos.get(0).getClie_id();
                            serv_id = serviciosActivos.get(0).getServ_id();
                        }
                        RequestAlerta requestAlerta = new RequestAlerta();
                        requestAlerta.setCond_Id(prefs.getInt(Constants.primary_id,0));
                        requestAlerta.setClie_Id(0);
                        requestAlerta.setServ_Id(String.valueOf(serv_id));
                        requestAlerta.setAler_Coordenada(String.valueOf(latitude)+String.valueOf(longitude));

                        System.out.println("primary_id"+ (prefs.getInt(Constants.primary_id,0)) + "clie_id: " + clie_id + " serv_id: " + serv_id + " location: " + location + " latitude: "+ latitude + " longitude: " + longitude);
                        Call<ResponseBody> callValidarCodigoVerificacion = conductorService.postRegistrarAlerta(requestAlerta);
                        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                Log.i("mensaje","response: "+response);
                                pDialogg.dismiss();
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if (response.body().isEstado()) {

                                            AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
                                            builder.setTitle("Alerta Activada");
                                            builder.setMessage("Se registró una alerta  y se notificará a la central.");
                                            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                            AlertDialog dialog = builder.create();
                                            dialog.show();

                                        } else {
                                            displayMessageSingle(response.body().getMensaje());
                                        }
                                    } else {
                                        displayMessageSingle(getString(R.string.code_error));
                                    }
                                } else {
                                    displayMessageSingle(getString(R.string.code_error));
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                if (pDialog != null && pDialog.isShowing()) {
                                    pDialog.dismiss();
                                }
                                displayMessageSingle(getString(R.string.code_error));
                            }
                        });
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        if (getIntent().hasExtra("token_fb") && !getIntent().getStringExtra("token_fb").isEmpty()) {
            String token_fb = getIntent().getStringExtra("token_fb");
            updateTokenFirebase(token_fb);
        }

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_home, R.id.nav_perfil).setDrawerLayout(drawer).build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        llSolicitudViajeMenu = findViewById(R.id.llSolicitudViajeMenu);
        //ivCloseDrawer = navigationView.getHeaderView(0).findViewById(R.id.ivCloseDrawer);

        llProfile = navigationView.getHeaderView(0).findViewById(R.id.llProfile);
        tx_nombres = navigationView.getHeaderView(0).findViewById(R.id.tx_nombres);
        tx_apellidos = navigationView.getHeaderView(0).findViewById(R.id.tx_apellidos);
        ivFotoPerfil = navigationView.getHeaderView(0).findViewById(R.id.ivFoto);
        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!serviciosActivos.isEmpty()) {
                    cliente = serviciosActivos.get(0).getCliente();
                    System.out.println("11" + cliente);
                    serv_id = serviciosActivos.get(0).getServ_id();
                }

                if (bundle != null) {
                    Intent i = new Intent(MenuActivity.this, PerfilActivity.class);
                    i.putExtras(bundle);
                    Gson gson = new Gson();
                    String serviciosActivosString = gson.toJson(serviciosActivos);
                    i.putExtra("serviciosActivos", serviciosActivosString);
                    i.putExtra("cliente", cliente);
                    System.out.println("12" + cliente);
                    i.putExtra("chatAvailable", chatAvailable);
                    i.putExtra("available", available);
                    i.putExtra("busy", busy);
                    i.putExtra("serv_id", serv_id);
                    if (chatAvailable) {
                        String mensajesChatString = gson.toJson(mensajes_chat);
                        i.putExtra("mensajesChat", mensajesChatString);
                    }
                    startActivityForResult(i, PROFILE);
                } else {
                    adDisplayService = displayMessage(getString(R.string.driver_data_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }
        });

        llMyTravels = navigationView.getHeaderView(0).findViewById(R.id.llMyTravels);
        llMyTravels.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!serviciosActivos.isEmpty()) {
                    cliente = serviciosActivos.get(0).getCliente();
                    System.out.println("13" + cliente);
                    serv_id = serviciosActivos.get(0).getServ_id();
                }
                Intent i = new Intent(MenuActivity.this, ViajesActivity.class);
                Gson gson = new Gson();
                String serviciosActivosString = gson.toJson(serviciosActivos);
                i.putExtra("serviciosActivos", serviciosActivosString);
                i.putExtra("cliente", cliente);
                System.out.println("14" + cliente);
                i.putExtra("chatAvailable", chatAvailable);
                i.putExtra("available", available);
                i.putExtra("busy", busy);
                i.putExtra("serv_id", serv_id);
                if (chatAvailable) {
                    String mensajesChatString = gson.toJson(mensajes_chat);
                    i.putExtra("mensajesChat", mensajesChatString);
                }
                startActivityForResult(i, MY_TRAVELS);
            }
        });

        llResume = navigationView.getHeaderView(0).findViewById(R.id.llResume);
        llResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!serviciosActivos.isEmpty()) {
                    cliente = serviciosActivos.get(0).getCliente();
                    System.out.println("15" + cliente);
                    serv_id = serviciosActivos.get(0).getServ_id();
                }
                Intent i = new Intent(MenuActivity.this, ResumenActivity.class);
                Gson gson = new Gson();
                String serviciosActivosString = gson.toJson(serviciosActivos);
                i.putExtra("serviciosActivos", serviciosActivosString);
                i.putExtra("cliente", cliente);
                System.out.println("16" + cliente);
                i.putExtra("chatAvailable", chatAvailable);
                i.putExtra("available", available);
                i.putExtra("busy", busy);
                i.putExtra("serv_id", serv_id);
                if (chatAvailable) {
                    String mensajesChatString = gson.toJson(mensajes_chat);
                    i.putExtra("mensajesChat", mensajesChatString);
                }
                startActivityForResult(i, RESUME);
            }
        });

        llCuentaBancaria = navigationView.getHeaderView(0).findViewById(R.id.llCuentaBancaria);
        llCuentaBancaria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!serviciosActivos.isEmpty()) {
                    cliente = serviciosActivos.get(0).getCliente();
                    System.out.println("17" + cliente);
                    serv_id = serviciosActivos.get(0).getServ_id();
                }
                Intent i = new Intent(MenuActivity.this, CuentaBancariaActivity.class);
                Gson gson = new Gson();
                String serviciosActivosString = gson.toJson(serviciosActivos);
                i.putExtra("serviciosActivos", serviciosActivosString);
                i.putExtra("cliente", cliente);
                System.out.println("18" + cliente);
                i.putExtra("chatAvailable", chatAvailable);
                i.putExtra("available", available);
                i.putExtra("busy", busy);
                i.putExtra("serv_id", serv_id);
                i.putExtra("conductor", conductorResponse);
                if (chatAvailable) {
                    String mensajesChatString = gson.toJson(mensajes_chat);
                    i.putExtra("mensajesChat", mensajesChatString);
                }
                startActivityForResult(i, BANC);
            }
        });

        llCallCentral = navigationView.getHeaderView(0).findViewById(R.id.llCallCentral);
        llCallCentral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callHelp();
            }
        });

        btnHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callHelp();
            }
        });
        btnAceptarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertarServicio();
                llSolicitudViajeMenu.setVisibility(View.GONE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constants.destino_solicitud_menu, "0");
                editor.apply();

                HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
                frag.removeSolicitudViaje();
            }
        });
        btnRechazarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                checkMessages();
                llSolicitudViajeMenu.setVisibility(View.GONE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constants.destino_solicitud_menu, "0");
                editor.apply();

                HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
                frag.removeSolicitudViaje();
            }
        });
        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.open();
            }
        });
        /*ivCloseDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.close();
            }
        });*/

        drawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                cargarDatosConductor();
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        btnDisponible = findViewById(R.id.btnDisponible);


        btnDisponible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){

                if (btnDisponible.isChecked()) {
                    System.out.println("!Activado: " + !available);
                    cambiarEstadoConductor(!available);
                    System.out.println("servicio0");
                }else{
                    System.out.println("Activado: " + available);
                    cambiarEstadoConductor(!available);
                    System.out.println("servicio1");
                }
            }
        });


        btnDisponible.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (btnDisponible.isChecked()) {
                    Toast.makeText(getApplicationContext(), "Activado", Toast.LENGTH_SHORT).show();
                    cambiarEstadoConductor(!available);
                }
                return true;
            }
        });

        if (!isLocationEnabled(this)) {
            askedforGPS = true;
            displayGPS();
        }

        if (adDisplayService == null || !adDisplayService.isShowing()) {
            listarFormasPago();
        }
        consultarTiempoExpiracion();
        verificarServicioActivo();
        //ScheduleFirebase();

        if (!serviciosActivos.isEmpty()){
            ivBotonPanico.setVisibility(View.VISIBLE);

            /*ivBotonPanico.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final String ubicacion = prefs.getString(Constants.last_location, "");
                    String[] latlong =  ubicacion.split(",");
                    double latitude = Double.parseDouble(latlong[0]);
                    double longitude = Double.parseDouble(latlong[1]);
                    LatLng location = new LatLng(latitude, longitude);

                    if (!serviciosActivos.isEmpty()) {
                        clie_id = serviciosActivos.get(0).getClie_id();
                        serv_id = serviciosActivos.get(0).getServ_id();
                    }

                    RequestAlerta requestAlerta = new RequestAlerta();
                    requestAlerta.setCond_Id(prefs.getInt(Constants.primary_id,0));
                    requestAlerta.setClie_Id(clie_id);
                    requestAlerta.setServ_Id(serv_id);
                    requestAlerta.setAler_Coordenada(location);
                    Call<ResponseBody> callValidarCodigoVerificacion = conductorService.postRegistrarAlerta(requestAlerta);
                    callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            if (response.isSuccessful()) {
                                if (response.body() != null) {
                                    if (response.body().isEstado()) {

                                        AlertDialog.Builder builder = new AlertDialog.Builder(MenuActivity.this);
                                        builder.setTitle("Alerta Activada");
                                        builder.setMessage("Se registró una alerta  y se notificará a la central");
                                        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                        AlertDialog dialog = builder.create();
                                        dialog.show();

                                    } else {
                                        displayMessageSingle(response.body().getMensaje());
                                    }
                                } else {
                                    displayMessageSingle(getString(R.string.code_error));
                                }
                            } else {
                                displayMessageSingle(getString(R.string.code_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            if (pDialog != null && pDialog.isShowing()) {
                                pDialog.dismiss();
                            }
                            displayMessageSingle(getString(R.string.code_error));
                        }
                    });
                }
            });*/

        } else{
            //GONE
            ivBotonPanico.setVisibility(View.VISIBLE);
        }

        //displayQualification();
        startService(new Intent(getBaseContext(), AliveFirebase.class));
        checkMessages();


    }

    public void updateTokenFirebase(String token) {
        RequestUpdateFirebaseToken request = new RequestUpdateFirebaseToken();
        request.setCond_Id(prefs.getInt(Constants.primary_id,0));
        request.setTokenFirebase(token);
        request.setIdDispositivo(getDeviceId(this));

        Call<ResponseBody> callActualizarTokenFirebase = conductorService.putActualizarTokenFirebase(request);
        callActualizarTokenFirebase.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            Log.d("Firebase", "Token actualizado en DB");
                        } else {
                            Log.d("Firebase", "Error actualizando token en DB");
                        }
                    } else {
                        Log.d("Firebase", "Error actualizando token en DB");
                    }
                } else {
                    Log.d("Firebase", "Error actualizando token en DB");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Firebase", "Error actualizando token en DB");
            }
        });
    }

    public void cargarDatosConductor() {
        DisplayProgressDialog(getString(R.string.loading));
        //pDialog.show();
        Log.i("mensaje","cargarDatosConductor");
        Call<ResponseDriverBody> callSeleccionarCliente = conductorService.getSeleccionarConductor(prefs.getInt(Constants.pais_id,0), prefs.getInt(Constants.primary_id,0));
        callSeleccionarCliente.enqueue(new Callback<ResponseDriverBody>() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onResponse(Call<ResponseDriverBody> call, Response<ResponseDriverBody> response) {
                /*if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }*/
                Log.i("mensaje","response: "+response);
                if (response.isSuccessful()) {
                    if (response.body()!=null) {
                        if (response.body().isEstado()) {
                            bundle = new Bundle();
                            conductorResponse = response.body().getValor();
                            bundle.putSerializable("conductor", conductorResponse);
                            String string = conductorResponse.getCond_Nombres();
                            String[] parts = string.split(" ");
                            if(parts.length >= 2){
                                if(parts[0].isEmpty()){
                                    tx_nombres.setText(conductorResponse.getCond_Nombres());
                                }else{
                                    if(parts[1].isEmpty()){
                                        tx_nombres.setText(conductorResponse.getCond_Nombres());
                                    }else{
                                        tx_nombres.setText(parts[0] + " "+parts[1]);
                                    }
                                }

                            }else{
                                tx_nombres.setText(conductorResponse.getCond_Nombres());
                            }
                            tx_apellidos.setText(conductorResponse.getCond_ApellidoPaterno() +" "+conductorResponse.getCond_ApellidoMaterno());
                          //tx_nombres_apellidos.setText(String.format(getString(R.string.name_format), conductorResponse.getCond_Nombres(), conductorResponse.getCond_ApellidoPaterno(), conductorResponse.getCond_ApellidoMaterno()));
                            available = conductorResponse.isCond_Disponible();
                            System.out.println("available guardado: " + available);
                            if (available == false) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    btnDisponible.setChecked(false);
                                    available = false;
                                }
                                btnDisponible.setChecked(false);
                                btnDisponible.setText(getString(R.string.set_available));
                                //stopScheduleReportGPS();
                                ScheduleReportGPS(Constants.Long_report_interval_Disable);
                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    btnDisponible.setChecked(true);
                                    available = true;
                                }
                                btnDisponible.setChecked(true);
                                btnDisponible.setText(getString(R.string.set_not_available));
                                ScheduleReportGPS(Constants.long_report_interval);
                            }

                            if (conductorResponse.getCond_Foto() != null) {
                                Glide.with(ivFotoPerfil)
                                        .load(conductorResponse.getCond_Foto())
                                        .apply(RequestOptions.skipMemoryCacheOf(true))
                                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                                        .fitCenter()
                                        .into(ivFotoPerfil);

                            }
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }

                    } else {
                        adDisplayService = displayMessage(getString(R.string.driver_data_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.driver_data_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseDriverBody> call, Throwable t) {
                /*if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }*/
                Log.i("mensaje","onFailure: "+t);
                adDisplayService = displayMessage(getString(R.string.driver_data_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    public void cambiarEstadoConductor(final boolean nuevo_estado) {
        System.out.println("Aqui3 en e servicio");
        DisplayProgressDialog(getString(R.string.updating));
        pDialog.show();

        Call<ResponseBody> callActualizarDisponibilidadConductor = conductorService.putActualizarDisponibilidadConductor(prefs.getInt(Constants.primary_id,0), nuevo_estado);
        callActualizarDisponibilidadConductor.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            available = nuevo_estado;
                            System.out.println("available nuevo: " + available);
                            if (available == false) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    btnDisponible.setChecked(false);
                                    btnDisponible.setText(getString(R.string.set_available));
                                    available = false;
                                }
                                btnDisponible.setChecked(false);
                                btnDisponible.setText(getString(R.string.set_available));
                                //stopScheduleReportGPS();
                                ScheduleReportGPS(Constants.Long_report_interval_Disable);

                            } else {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    btnDisponible.setChecked(true);
                                    available = true;
                                }
                                btnDisponible.setChecked(true);
                                btnDisponible.setText(getString(R.string.set_not_available));
                                ScheduleReportGPS(Constants.long_report_interval);
                            }
                        } else {
                            btnDisponible.setChecked(false);
                            btnDisponible.setText(getString(R.string.set_available));
                            available = false;
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.available_change_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.available_change_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.available_change_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isLocationEnabled(this)) {
            if (!askedforGPS) {
                askedforGPS = true;
                displayGPS();
            }
        }
        if (paused) {
            checkMessages();
            paused = false;
            stopService(new Intent(getBaseContext(), LocationService.class));
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onPause() {
        super.onPause();
        paused = true;
        startForegroundService(new Intent(getBaseContext(), LocationService.class));

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        /*if (getIntent().hasExtra("message")) {
            processMessage(getIntent().getStringExtra("message"));
        }*/
        //checkMessages();
    }

    public static Boolean isLocationEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
// This is new method provided in API 28
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return lm.isLocationEnabled();
        } else {
// This is Deprecated in API 28
            int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            return  (mode != Settings.Secure.LOCATION_MODE_OFF);

        }
    }

    public void addDestination(String title, double lat, double lng, String address,String origen, String destino, int tiempo) {
        busy = true;
        chatAvailable = true;
        //mensajes_chat.clear();
        System.out.println("02: ");
        btnDisponible.setVisibility(View.GONE);
        ivBotonPanico.setVisibility(View.VISIBLE);
        final String ubicacionOrigen = prefs.getString(Constants.last_location, "");
        final String ubicacionDestino = coordDestino;
        HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
        System.out.println("addDestinations");
        frag.addDestination(title, lat, lng, address, true);
        frag.addCard(title, origen, destino, tiempo);
    }

    public void cancelTravel() {
        //serviciosActivos.remove(0);
        serviciosActivos.clear();
        if (serviciosActivos.isEmpty()) {
            busy = false;
            chatAvailable = false;
            btnDisponible.setVisibility(View.VISIBLE);
            //GONE
            ivBotonPanico.setVisibility(View.VISIBLE);
            stopScheduleReportGPS();
            //stopScheduleUpdateTime();
            HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
            frag.stopUpdateTime();
            ScheduleReportGPS(Constants.long_report_interval);
        } else {
            siguienteServicio();
        }
        sol_id = -1;
        serv_id = -1;
        clie_id = -1;
        telfCliente = "";
        mensajes_chat.clear();
        System.out.println("03: ");
        btnHelp.setVisibility(View.GONE);
        HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
        frag.cancelTravel();
        //changeTimeReportGPS(Constants.INTERVALO_LIBRE);
    }

    private void siguienteServicio() {
        if (!serviciosActivos.isEmpty()) {
            serv_id = serviciosActivos.get(0).getServ_id();
            clie_id = serviciosActivos.get(0).getClie_id();
            monto = serviciosActivos.get(0).getMonto();
            fpag_id = serviciosActivos.get(0).getFpag_id();
            coordOrigen = serviciosActivos.get(0).getCoordOrigen();
            coordDestino = serviciosActivos.get(0).getCoordDestino();
            double lat = Double.parseDouble(coordOrigen.split(",")[0]);
            double lng = Double.parseDouble(coordOrigen.split(",")[1]);
            telfCliente = serviciosActivos.get(0).getTelfCliente();
            cliente = serviciosActivos.get(0).getCliente();
            System.out.println("19" + cliente);

            if (serviciosActivos.get(0).isExtendido()) {
                actualizarServicio(3);
            } else {
                //double lat = Double.parseDouble(serviciosActivos.get(0).getCoordOrigen().split(",")[0]);
                //double lng = Double.parseDouble(serviciosActivos.get(0).getCoordOrigen().split(",")[1]);
                addDestination(serviciosActivos.get(0).getCliente(), lat, lng, serviciosActivos.get(0).getOrigen(),serviciosActivos.get(0).getOrigen(),serviciosActivos.get(0).getDestino(),serviciosActivos.get(0).getTimpodeViaje());
                System.out.println("addDestination6");
            }
        }
    }

    public void driverArrived() {
        ivBotonPanico.setVisibility(View.VISIBLE);
        HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
        frag.driverArrived();
    }

    public void driverLeaving() {
        //btnHelp.setVisibility(View.VISIBLE);
        ivBotonPanico.setVisibility(View.VISIBLE);
        HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
        double lat = Double.parseDouble(serviciosActivos.get(0).getCoordDestino().split(",")[0]);
        double lng = Double.parseDouble(serviciosActivos.get(0).getCoordDestino().split(",")[1]);
        frag.driverLeaving("", lat, lng, serviciosActivos.get(0).getDestino());
        //changeTimeReportGPS(Constants.INTERVALO_OCUPADO);
    }

    public void travelEnded() {
        if (serviciosActivos.size() == 1) {
            busy = false;
            chatAvailable = false;
            btnDisponible.setVisibility(View.VISIBLE);
            //GONE
            ivBotonPanico.setVisibility(View.VISIBLE);
            stopScheduleReportGPS();
            //stopScheduleUpdateTime();
            HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
            frag.stopUpdateTime();
            ScheduleReportGPS(Constants.long_report_interval);
        }
        sol_id = -1;
        serv_id = -1;
        clie_id = -1;
        telfCliente = "";
        mensajes_chat.clear();
        System.out.println("04: ");
        btnHelp.setVisibility(View.GONE);
        //GONE
        ivBotonPanico.setVisibility(View.VISIBLE);
        HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
        frag.travelEnded();
        displayQualification();

        //changeTimeReportGPS(Constants.INTERVALO_LIBRE);
        //adDQ = displayQualification(serviciosActivos.get(0).getCliente(), new Runnable() {
        //     @Override
        //     public void run() {
        //calificarCliente(false);
        //      }
        //   }, new Runnable() {
        //       @Override
        //       public void run() {
        //           //calificarCliente(true);
        //      }
        //  },
        //   new Runnable() {
        //    @Override
        //   public void run() {
        //        serviciosActivos.remove(0);
        //        siguienteServicio();
        //     }
        //  });
    }

    public void displayQualification() {
        AlertDialog.Builder mBuilder3 = new AlertDialog.Builder(MenuActivity.this);
        View viewCalificarServicio = LayoutInflater.from(MenuActivity.this).inflate(R.layout.layout_calificacion, null);
        mBuilder3.setView(viewCalificarServicio).setCancelable(false);
        final AlertDialog dialogCalificarServicio = mBuilder3.create();

        TextView tvTitulo = viewCalificarServicio.findViewById(R.id.tvTitulo);
        TextView tvOmitir = viewCalificarServicio.findViewById(R.id.tvOmitir);
        final RatingBar ratingBar = viewCalificarServicio.findViewById(R.id.ratingBar);
        //final EditText etSugerencia = viewCalificarServicio.findViewById(R.id.etSugerencia);
        Button button = viewCalificarServicio.findViewById(R.id.btnAceptarcal);
        tvTitulo.setText(String.format(getString(R.string.qualification_title), clienteok));
        System.out.println("20" + clienteok);
        System.out.println("20.1" + cliente);
        System.out.println("20.2"+ serviciosActivos.get(0).getCliente());

        SpannableString ssUnico = new SpannableString(tvOmitir.getText().toString());
        StyleSpan boldUnico = new StyleSpan(android.graphics.Typeface.BOLD);
        ClickableSpan clickableSpan1Unico = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {

            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLACK);
                ds.setUnderlineText(true);
            }
        };
        ssUnico.setSpan(boldUnico, 0, tvOmitir.getText().toString().length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        ssUnico.setSpan(clickableSpan1Unico, 0, tvOmitir.getText().toString().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvOmitir.setText(ssUnico);
        tvOmitir.setMovementMethod(LinkMovementMethod.getInstance());
        Objects.requireNonNull(dialogCalificarServicio.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_calificacion));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float calificacion = 0;
                System.out.println("Enviar: " + serviciosActivos.get(0).getServ_id() + serviciosActivos.get(0).getClie_id() + ratingBar.getRating());
                System.out.println("Calificacion:" + ratingBar.getRating());
                if (ratingBar.getRating() > 0) {
                    calificacion = ratingBar.getRating();
                }else{
                    calificacion = 0;
                }
                calificarCliente(serviciosActivos.get(0).getServ_id(), serviciosActivos.get(0).getClie_id(), calificacion);
                dialogCalificarServicio.dismiss();
            }
        });

        tvOmitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviciosActivos.remove(0);
                siguienteServicio();
                dialogCalificarServicio.dismiss();
            }
        });

        dialogCalificarServicio.show();
    }

    public void confirmDriverArrived() {
        actualizarServicio(3);
    }

    public void confirmDriverLeaving() {
        actualizarServicio(4);
    }

    public void confirmTravelEnded() {
        displayFinish();
    }

    public void confirmCancelService() {
        displayCancel();
    }

    public void callClient() {
        System.out.println("1");
        System.out.println("Pref: " + prefs.getString(Constants.prefij, "")  + " Telf: " + telfCliente);
        Intent intentApp = getPackageManager().getLaunchIntentForPackage("com.whatsapp");
        //if (intentApp != null) {
        System.out.println("2");
        PackageManager packageManager = getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);
        String numeroTelefono = prefs.getString(Constants.prefij, "")+telfCliente;
        try {
            System.out.println("3");
            String url = "https://api.whatsapp.com/send?phone=" +numeroTelefono+ "&text=" + URLEncoder.encode("", "UTF-8");
            System.out.println(url);
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            startActivity(i);
            System.out.println("3.5");
        } catch (Exception e) {
            System.out.println("4");
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp"));
            startActivity(intent);
            e.printStackTrace();
        }

        //if (checkCallPhonePermission() && telfCliente != null && !telfCliente.isEmpty()) {
        //    Intent callIntent = new Intent(Intent.ACTION_CALL);
        //    callIntent.setData(Uri.parse("tel:" + telfCliente));
        //    startActivity(callIntent);
        //}
    }

    public void callHelp() {
        if (checkCallPhonePermission()) {
            consultarTelfAyuda();
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CALL_PHONE) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                adDisplayService = displayMessage(getString(R.string.must_grant_permission), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callClient();
            }
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 98;
    private boolean checkCallPhonePermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {

                // Should we show an explanation?
                if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CALL_PHONE)) {

                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                    new AlertDialog.Builder(this)
                            .setTitle(getString(R.string.call_phone_permission_required))
                            .setMessage(getString(R.string.call_phone_permission_explanation))
                            .setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    ActivityCompat.requestPermissions(MenuActivity.this,
                                            new String[]{Manifest.permission.CALL_PHONE},
                                            MY_PERMISSIONS_REQUEST_CALL_PHONE);
                                }
                            })
                            .create()
                            .show();


                } else {
                    // No explanation needed, we can request the permission.
                    ActivityCompat.requestPermissions(this,
                            new String[]{Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_CALL_PHONE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void displayFinish() {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_finish_service, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        final TextView tvTitulo = dialoglayout.findViewById(R.id.tvTitulo);

        final TextView tvTituloFormaPago = dialoglayout.findViewById(R.id.tvTituloFormaPago);

        final TextView tvFormaPago = dialoglayout.findViewById(R.id.tvFormaPago);

        /*final Spinner spFormaPago = dialoglayout.findViewById(R.id.spFormaPago);
        spFormaPago.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //booking_state = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> aFormaPago = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner_item, new ArrayList<String>()) {

            @Override
            public boolean isEnabled(int position) {
                // TODO Auto-generated method stub
                *//*if (position == 0) {
                    return false;
                }*//*
                return true;
            }



            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getView(position, convertView, parent);
                //view.setTypeface(typeface);
                return view;
            }



            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View mView = super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    //mTextView.setBackground(getResources().getDrawable(R.drawable.background_test));
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    mTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                }

                return mTextView;
            }
        };*/

        for (PaymentType fp : formas_pago) {
            if (fp.getFpag_Id() == serviciosActivos.get(0).getFpag_id()) {
                tvFormaPago.setText(fp.getFpag_Nombre());
                break;
            }
            //aFormaPago.add(fp.getFpag_Nombre());
        }

        /*spFormaPago.setAdapter(aFormaPago);*/

        final TextView tvTotal = dialoglayout.findViewById(R.id.tvTotal);
        tvTotal.setText(prefs.getString(monesym, "")+" "+String.format(util.formatearNumero(serviciosActivos.get(0).getMonto())));

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        final Button btnConfirmar = dialoglayout.findViewById(R.id.btnConfirmar);

        final EditText etComentarios = dialoglayout.findViewById(R.id.etComentarios);

        final CheckBox cbCancelar = dialoglayout.findViewById(R.id.cbCancelar);
        cbCancelar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etComentarios.setVisibility(View.VISIBLE);
                    btnConfirmar.setText(getString(R.string.cancel_service));
                    tvTotal.setText(prefs.getString(monesym, "")+" "+String.format(util.formatearNumero(0.00d)));
                    tvTituloFormaPago.setVisibility(View.GONE);
                    tvFormaPago.setVisibility(View.GONE);
                    /*spFormaPago.setVisibility(View.GONE);*/
                    /*tvTituloTotal.setVisibility(View.GONE);
                    tvTotal.setVisibility(View.GONE);*/
                    tvTitulo.setText(getString(R.string.confirm_cancel_service));
                } else {
                    etComentarios.setVisibility(View.GONE);
                    btnConfirmar.setText(getString(R.string.confirm));
                    tvTotal.setText(prefs.getString(monesym, "")+" "+String.format(util.formatearNumero(monto)));
                    tvTituloFormaPago.setVisibility(View.VISIBLE);
                    tvFormaPago.setVisibility(View.VISIBLE);
                    /*spFormaPago.setVisibility(View.VISIBLE);*/
                    /*tvTituloTotal.setVisibility(View.VISIBLE);
                    tvTotal.setVisibility(View.VISIBLE);*/
                    tvTitulo.setText(getString(R.string.confirm_payment_type));
                }
            }
        });

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyb();
                if (cbCancelar.isChecked()) {
                    if (etComentarios.getText().toString().isEmpty()) {
                        etComentarios.setError(getString(R.string.comment_required));
                        etComentarios.requestFocus();
                    } else {
                        alert.dismiss();
                        anularServicio(etComentarios.getText().toString());
                    }
                } else {
                    alert.dismiss();
                    finalizarServicio(serviciosActivos.get(0).getFpag_id());
                }
            }
        });

        Button btnCancelar = dialoglayout.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void displayCancel() {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_finish_service, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        final TextView tvTitulo = dialoglayout.findViewById(R.id.tvTitulo);

        final TextView tvTituloFormaPago = dialoglayout.findViewById(R.id.tvTituloFormaPago);

        final TextView tvFormaPago = dialoglayout.findViewById(R.id.tvFormaPago);
        //final Spinner spFormaPago = dialoglayout.findViewById(R.id.spFormaPago);
       /* spFormaPago.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //booking_state = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

       /* ArrayAdapter<String> aFormaPago = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner_item, new ArrayList<String>()) {

            @Override
            public boolean isEnabled(int position) {
                return true;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getView(position, convertView, parent);
                return view;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View mView = super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;


                mTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                return mTextView;
            }
        };

        for (PaymentType fp : formas_pago) {
            aFormaPago.add(fp.getFpag_Nombre());
        }

        spFormaPago.setAdapter(aFormaPago);*/

        final TextView tvTotal = dialoglayout.findViewById(R.id.tvTotal);
        tvTotal.setText(prefs.getString(monesym, "")+" "+String.format(util.formatearNumero(monto)));

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        final Button btnConfirmar = dialoglayout.findViewById(R.id.btnConfirmar);

        final EditText etComentarios = dialoglayout.findViewById(R.id.etComentarios);

        final CheckBox cbCancelar = dialoglayout.findViewById(R.id.cbCancelar);
        cbCancelar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etComentarios.setVisibility(View.VISIBLE);
                    btnConfirmar.setText(getString(R.string.cancel_service));
                    tvTotal.setText(prefs.getString(monesym, "")+" "+String.format(util.formatearNumero(0.00d)));
                    tvTituloFormaPago.setVisibility(View.GONE);
                    /*spFormaPago.setVisibility(View.GONE);*/
                    tvFormaPago.setVisibility(View.GONE);
                    /*tvTituloTotal.setVisibility(View.GONE);
                    tvTotal.setVisibility(View.GONE);*/
                    tvTitulo.setText(getString(R.string.confirm_cancel_service));
                } else {
                    etComentarios.setVisibility(View.GONE);
                    btnConfirmar.setText(getString(R.string.confirm));
                    tvTotal.setText(prefs.getString(monesym, "")+" "+String.format(util.formatearNumero(monto)));
                    tvTituloFormaPago.setVisibility(View.VISIBLE);
                    tvFormaPago.setVisibility(View.VISIBLE);
                    /*spFormaPago.setVisibility(View.VISIBLE);*/
                    /*tvTituloTotal.setVisibility(View.VISIBLE);
                    tvTotal.setVisibility(View.VISIBLE);*/
                    tvTitulo.setText(getString(R.string.confirm_payment_type));
                }
            }
        });

        cbCancelar.setChecked(true);
        cbCancelar.setEnabled(false);

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etComentarios.getText().toString().isEmpty()) {
                    etComentarios.setError(getString(R.string.comment_required));
                    etComentarios.requestFocus();
                } else {
                    alert.dismiss();
                    anularServicio(etComentarios.getText().toString());
                }
            }
        });

        Button btnCancelar = dialoglayout.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }

    private void turnGPSOn() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY));
        builder.setAlwaysShow(true);
        mLocationSettingsRequest = builder.build();

        mSettingsClient = LocationServices.getSettingsClient(MenuActivity.this);

        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        //Success Perform Task Here
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(MenuActivity.this, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.e("GPS","Unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e("GPS","Location settings are inadequate, and cannot be fixed here. Fix in Settings.");
                        }
                    }
                })
                .addOnCanceledListener(new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        Log.e("GPS","checkLocationSettings -> onCanceled");
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("Entro al onresulto");

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    //Success Perform Task Here
                    askedforGPS = false;
                    break;
                case Activity.RESULT_CANCELED:
                    Log.e("GPS","User denied to access location");
                    //openGpsEnableSetting();
                    displayGPS();
                    break;
            }
        } else if (requestCode == OPEN_CHAT) {
            if (resultCode == RESULT_OK) {
                String mensajesChatString = data.getStringExtra("mensajesChat");
                Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
                Gson gson = new Gson();
                mensajes_chat = gson.fromJson(mensajesChatString, listType);
                System.out.println("05: " + mensajesChatString);
            } else if (resultCode == RESULT_CANCELED) {
                displayNotification(getString(R.string.service_canceled_by_customer) , new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Aqui 2 cliente");
                        checkMessages();
                    }
                });
                cancelTravel();
            } else if (resultCode == RESULT_FIRST_USER) {
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                insertarServicio();
            } else { //2
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                addScheduledService();
            }
            chatOpen = false;
        } else if (requestCode == MY_TRAVELS) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.hasExtra("mensajesChat")) {
                    String mensajesChatString = data.getStringExtra("mensajesChat");
                    Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
                    Gson gson = new Gson();
                    mensajes_chat = gson.fromJson(mensajesChatString, listType);
                    System.out.println("06: " + mensajesChatString);
                }
            } else if (resultCode == RESULT_CANCELED) {
                drawer.close();
                displayNotification(getString(R.string.service_canceled_by_customer) , new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Aqui 3 cliente");
                        checkMessages();
                    }
                });
                cancelTravel();
            } else if (resultCode == RESULT_FIRST_USER) {
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                insertarServicio();
            } else if (resultCode == 2) { //2 - Viaje programado aceptado
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                addScheduledService();
            } else if (resultCode == 4) { //4 - Viaje programado iniciado
                drawer.close();
                verificarServicioActivo();
            }
        } else if (requestCode == RESUME) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.hasExtra("mensajesChat")) {
                    String mensajesChatString = data.getStringExtra("mensajesChat");
                    Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
                    Gson gson = new Gson();
                    mensajes_chat = gson.fromJson(mensajesChatString, listType);
                    System.out.println("07: " + mensajesChatString);
                }
            } else if (resultCode == RESULT_CANCELED) {
                drawer.close();
                displayNotification(getString(R.string.service_canceled_by_customer) , new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Aqui 4 cliente");
                        checkMessages();
                    }
                });
                cancelTravel();
            } else if (resultCode == RESULT_FIRST_USER) {
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                insertarServicio();
            } else { //2
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                addScheduledService();
            }
        } else if (requestCode == PROFILE) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.hasExtra("mensajesChat")) {
                    String mensajesChatString = data.getStringExtra("mensajesChat");
                    Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
                    Gson gson = new Gson();
                    mensajes_chat = gson.fromJson(mensajesChatString, listType);
                    System.out.println("08: " + mensajesChatString);
                }
            } else if (resultCode == RESULT_CANCELED) {
                drawer.close();
                displayNotification(getString(R.string.service_canceled_by_customer) , new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Aqui 5 cliente");
                        checkMessages();
                    }
                });
                cancelTravel();
            } else if (resultCode == RESULT_FIRST_USER) {
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                insertarServicio();
            } else { //2
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                addScheduledService();
            }
        }else if (requestCode == BANC) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.hasExtra("mensajesChat")) {
                    String mensajesChatString = data.getStringExtra("mensajesChat");
                    Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
                    Gson gson = new Gson();
                    mensajes_chat = gson.fromJson(mensajesChatString, listType);
                    System.out.println("09: " + mensajesChatString);
                }
            } else if (resultCode == RESULT_CANCELED) {
                drawer.close();
                displayNotification(getString(R.string.service_canceled_by_customer) , new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Aqui 6 cliente");
                        checkMessages();
                    }
                });
                cancelTravel();
            } else if (resultCode == RESULT_FIRST_USER) {
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                insertarServicio();
            } else { //2
                drawer.close();
                sol_id = data.getIntExtra("Soli_Id", -1);
                cliente = data.getStringExtra("Cliente");
                origen = data.getStringExtra("Origen");
                destino = data.getStringExtra("Destino");
                addScheduledService();
            }
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void displayGPS() {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_gps, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnHabilitar = dialoglayout.findViewById(R.id.btnHabilitar);
        btnHabilitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                turnGPSOn();
            }
        });
    }

    public void ScheduleFirebase() {
        tFirebase = new Timer();
        tFirebase.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                getApplicationContext().sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
                getApplicationContext().sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));
            }
        }, 0, 500);
    }

    public void ScheduleReportGPS(long time) {
        tLocation = new Timer();
        Log.d("GPS: ","Tiempo: "+time);

        tLocation.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                actualizarUbicacion();
                Log.d("GPS: ","Tiempo(Met): "+time);

                if(servactive == true){
                    //obtenertiempo();
                }
            }
        }, 0, time);
        //put here time 1000 milliseconds=1 ;
        //        ctime.set(Calendar.MILLISECOND, 0);
        //
        //        if (alarmManager == null && pi == null) {
        //            Intent intentAlarm = new Intent(this, LocationReporter.class);
        //            pi = PendingIntent.getSesecond
        /*final Calendar ctime = Calendar.getInstance();
        ctime.set(Calendar.MINUTE, 0);
        ctime.set(Calendar.SECOND, 0)rvice(this, 0, intentAlarm, PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.RTC, ctime.getTime().getTime(), time , pi); // Millisec * Second * Minute
        } else {
            stopScheduleReportGPS();
            alarmManager.setRepeating(AlarmManager.RTC, ctime.getTime().getTime(), time , pi); // Millisec * Second * Minute
        }*/
        /*if (alarmManager == null && pi == null) {
            Intent intentAlarm = new Intent(this, LocationReporter.class);
            pi = PendingIntent.getBroadcast(this, 0, intentAlarm, 0);
            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), time , pi); // Millisec * Second * Minute
        } else {
            stopScheduleReportGPS();
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), time , pi); // Millisec * Second * Minute
        }*/
    }

    public void stopScheduleReportGPS() {
        if (tLocation != null) {
            tLocation.cancel();
        }
    }

    public void ScheduleUpdateTime(long time) {
        tUpdateLocation = new Timer();
        tUpdateLocation.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                actualizarUbicacion();
                if(servactive == true){
                    //System.out.println("ScheduleUpdateTime");
                    //obtenertimetravel();
                }
            }
        }, 0, time);
    }

    public void stopScheduleUpdateTime() {
        if (tUpdateLocation != null) {
            tUpdateLocation.cancel();
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(Constants.time, "0");
            editor.apply();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //alarmManager.cancel(pi);
        if (tLocation != null) {
            tLocation.cancel();
        }
        stopService(new Intent(getBaseContext(), AliveFirebase.class));
        stopService(new Intent(getBaseContext(), LocationService.class));
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pending_messages", "");
        editor.apply();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isOpen()) {
            drawer.close();
        } else {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
            View viewMensaje = LayoutInflater.from(this).inflate(R.layout.layout_dialog_salir, null);
            mBuilder.setView(viewMensaje).setCancelable(false);
            final AlertDialog dialogNotificacion = mBuilder.create();
            Button btnSi = viewMensaje.findViewById(R.id.btnSi);
            btnSi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //MenuActivity.super.onBackPressed();
                    //moveTaskToBack(true); finish();
                    ExitActivity.exitApplication(MenuActivity.this);
                    dialogNotificacion.dismiss();
                }
            });

            Button btnNo = viewMensaje.findViewById(R.id.btnNo);
            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogNotificacion.dismiss();
                }
            });

            dialogNotificacion.show();
        }
    }

    public void gotoChat() {
        Intent intent = new Intent(MenuActivity.this, ChatActivity.class);
        Gson gson = new Gson();
        String serviciosActivosString = gson.toJson(serviciosActivos);
        intent.putExtra("serviciosActivos", serviciosActivosString);
        //if (serv_id < 0) {
        intent.putExtra("serv_id", serviciosActivos.get(0).getServ_id());
        //}

        intent.putExtra("cliente", serviciosActivos.get(0).getCliente());
        intent.putExtra("busy", busy);
        String mensajesChatString = gson.toJson(mensajes_chat);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Constants.CHAT_MENSAJES, mensajesChatString);
        editor.apply();
        System.out.println("main 2 " + mensajesChatString);
        intent.putExtra("mensajesChat", mensajesChatString);
        startActivityForResult(intent, OPEN_CHAT);
        chatOpen = true;
    }

    private void actualizarUbicacion() {
        RequestUpdateDriverLocation request = new RequestUpdateDriverLocation();
        request.setCond_Id(prefs.getInt(Constants.primary_id,0));
        final String ubicacion = prefs.getString(Constants.last_location, "");
        request.setUcon_Coordenada(ubicacion);

        if (!ubicacion.isEmpty()) {
            Call<ResponseBody> callService = conductorService.putActualizarUbicacionConductor(request);
            callService.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().isEstado()) {
                                Log.d("GPS", "Ubicación reportada con éxito (M)" + ubicacion);
                            } else {
                                Log.d("GPS", response.body().getMensaje());
                            }
                        } else {
                            Log.d("GPS", "Error reportando Ubicación");
                        }
                    } else {
                        Log.d("GPS", "Error reportando Ubicación");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("GPS", "Error reportando Ubicación");
                }
            });
        } else {
            Log.d("GPS", "No hay Ubicación para reportar");
        }
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {

        String deviceId;

        deviceId = Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return deviceId;
    }

    public void expireServiceSolicitud(long exp, final Runnable r) {
        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        long expDef = prefs.getLong("ExpiracionSolicitud",20000);
        new CountDownTimer(exp + expDef, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                llSolicitudViajeMenu.setVisibility(View.GONE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constants.destino_solicitud_menu, "0");
                editor.apply();

                HomeFragment frag = (HomeFragment)getSupportFragmentManager().getFragments().get(0).getChildFragmentManager().getFragments().get(0);
                frag.removeSolicitudViaje();
            }
        }.start();
    }
}