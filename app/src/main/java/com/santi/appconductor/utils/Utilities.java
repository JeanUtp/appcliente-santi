package com.santi.appconductor.utils;

import android.app.Activity;
import android.content.res.Resources;
import android.util.TypedValue;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Utilities {
    public String formatearNumero(Object obj) {
        Locale currentLocale = Locale.getDefault();
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(currentLocale);
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');
        double check = (double) obj;
        String pattern = "0,000.00";
        if (check < 1000) {
            pattern = "0.00";
        }
        DecimalFormat df = new DecimalFormat(pattern, otherSymbols);
        return df.format(obj);
    }

    public int dpToPx(Activity a, int dp) {
        Resources r = a.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
