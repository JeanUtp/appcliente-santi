package com.santi.appconductor.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.Toast;

import com.google.android.gms.common.stats.ConnectionTracker;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class MostrarFotoTask extends AsyncTask<String, Void, Bitmap> {

    private OnMostrarFotoTaskCompleted listener;

    public MostrarFotoTask(OnMostrarFotoTaskCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(params[0]), null, options);
            options.inSampleSize = Constants.calculateInSampleSize(options, 122, 120);
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeStream(new FileInputStream(params[0]), null, options);
        } catch (FileNotFoundException | NullPointerException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        listener.onMostrarFotoTaskCompleted(bitmap);
    }

    public interface OnMostrarFotoTaskCompleted {
        void onMostrarFotoTaskCompleted(Bitmap bitmap);
    }
}
