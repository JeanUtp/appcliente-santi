package com.santi.appconductor.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Constants {
    public static final String endpoint_apis = "http://54.235.172.23/SANTI_API/"; //DEV
    //public static final String endpoint_apis = "http://apis.santi-app.net/"; //PROD
    public static final int splash_timeout = 1000;
    public static final String share_tag = "SantiDriverPreferences";
    public static final String primary_id = "idConductor";
    public static final String estado_panta = "estadopanta";
    public static final String pais_id = "idpais";
    public static final String estado_con = "estado_con";
    public static final String token = "token";
    public static final String ID_CEL = "idcel";
    public static final String num_CEL = "idcel";
    public static final String prefij = "prefij";
    public static final String icpais = "icpais";
    public static final String CHAT_MENSAJES = "chat_mensajes";
    public static final String caracteres = "caracteres";
    public static final String monesym = "monesym";
    public static final String client = "client";
    public static final String orig = "orig";
    public static final String desti = "desti";
    public static final String time = "0";
    public static final String last_location = "lastLocation";
    public static final long long_report_interval = 20000;//Disponible -> 20 seconds
    public static final long Long_report_interval_Disable = 60000; //No disponible -> 60 seconds
    public static final long short_report_interval = 5000; //En servicio -> 5 seconds
    public static final long short_locate_interval = 9000; //En servicio -> 9 seconds
    public static final String g_api_key = "AIzaSyAW62lFMPaya0zxvjfDNkXSu16e5HTGoRo"; //DEV
    //public static final String g_api_key = "AIzaSyAQVMPfCmjvZ7t4XRdCxxA8Sufz87FP2tU"; //PROD
    public static final String ACEPTA_TERMINOS_CONDICIONES = "acepta_terminos_condiciones";
    public static final String ACEPTA_TERMINOS_CONDICIONES_GENERALES = "acepta_terminos_condiciones_generales";
    public static final int CAMARA = 1;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 132;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 98;
    private static final String RUTA = "/santiappconductor";

    public static final String destino_solicitud_menu = "destinoSolicitudMenu";

    public static byte[] resizeImage(Context ctx, byte[] input, int widthNew, int heightNew) {// int resId

        Bitmap BitmapOrg = BitmapFactory.decodeByteArray(input , 0, input.length);
        int width = BitmapOrg.getWidth();
        int height = BitmapOrg.getHeight();
        System.out.println("Scala que se original " + width + " - "+ height);
        /*if(width > 4000 || height > 3000){
            width = BitmapOrg.getWidth()/2;
            height = BitmapOrg.getHeight()/2;
            System.out.println("Scala reducida a la mitad " + width + " - "+ height);
        }else{
            width = BitmapOrg.getWidth();
            height = BitmapOrg.getHeight();
            System.out.println("Scala original " + width + " - "+ height);
        }*/

        /*int newWidth = widthNew;
        int newHeight = heightNew;

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        float scaleWidth2 = ((float) width) / ((float) newWidth);
        float scaleHeight2 = ((float) height) / ((float)newHeight);

        float scaleWidth3 = ((float) width);
        float scaleHeight3 = ((float) height);

        System.out.println("Scala que se original " + width + " - "+ height);
        System.out.println("Scala que se manda1 " + scaleWidth + " - "+scaleHeight);
        System.out.println("Scala que se inversa2 " + scaleWidth2 + " - "+ scaleHeight2);
        System.out.println("Scala que se manda orig3 " + scaleWidth3 + " - "+ scaleHeight3);*/

        Matrix matrix = new Matrix();
        matrix.postScale(1, 1);
        Bitmap resizedBitmap = Bitmap.createBitmap(BitmapOrg, 0, 0, width, height, matrix, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        resizedBitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static String compressImage(Context context, String uri ) {

        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(uri, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 1016x812
        //float maxHeight = 1616.0f;
        //float maxWidth = 1112.0f;
        float maxHeight = 950.0f;
        float maxWidth = 1000.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow Android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[20 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(uri, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 1.0f;
        float middleY = actualHeight / 1.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 1, middleY - bmp.getHeight() / 1, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(uri);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = uri;
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

}