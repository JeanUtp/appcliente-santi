package com.santi.appconductor.utils;

import java.util.ArrayList;
import java.util.List;

public final class Countries {

  public static final List<Country> COUNTRIES = new ArrayList<>();
  static {

      COUNTRIES.add(new Country ("ar", "Argentina", 54));
      COUNTRIES.add(new Country ("bo", "Bolivia", 591));
      COUNTRIES.add(new Country ("cl", "Chile", 56));
      COUNTRIES.add(new Country ("co", "Colombia", 57));
      COUNTRIES.add(new Country ("cr", "Costa Rica", 506));
      COUNTRIES.add(new Country ("cu", "Cuba", 53));
      COUNTRIES.add(new Country ("do", "República Dominicana", 1));
      COUNTRIES.add(new Country ("ec", "Ecuador", 593));
      COUNTRIES.add(new Country ("sv", "El Salvador", 503));
      COUNTRIES.add(new Country ("es", "España", 34));
      COUNTRIES.add(new Country ("us", "Estados unidos", 1));
      COUNTRIES.add(new Country ("ph", "Filipinas", 63));
      COUNTRIES.add(new Country ("gq", "Guinea Ecuatorial", 240));
      COUNTRIES.add(new Country ("gt", "Guatemala", 502));
      COUNTRIES.add(new Country ("hn", "Honduras", 504));
      COUNTRIES.add(new Country ("mx", "México", 52));
      COUNTRIES.add(new Country ("ni", "Nicaragua", 505));
      COUNTRIES.add(new Country ("pa", "Panamá", 507));
      COUNTRIES.add(new Country ("py", "Paraguay", 595));
      COUNTRIES.add(new Country ("pe", "Perú", 51));
      COUNTRIES.add(new Country ("pr", "Puerto Rico", 1));
      COUNTRIES.add(new Country ("uy", "Uruguay", 598));
      COUNTRIES.add(new Country ("ve", "Venezuela", 58));
      COUNTRIES.add(new Country ("pe", "Perú", 51));
  }

}
