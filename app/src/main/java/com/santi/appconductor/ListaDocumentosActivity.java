package com.santi.appconductor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.santi.appconductor.R;
import com.santi.appconductor.adapters.DocumentAdapter;
import com.santi.appconductor.interfaces.SimpleRow;
import com.santi.appconductor.models.RequestAdjunto;
import com.santi.appconductor.models.RequestDocument;
import com.santi.appconductor.models.RequestMarca;
import com.santi.appconductor.models.ResponseAdjuntoBody;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseDocumentBody;
import com.santi.appconductor.models.ResponseMarcaBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaDocumentosActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Bundle bundle;
    Button terminar;
    SharedPreferences prefs;
    private ResponseAdjuntoBody responseAdjuntoBody;
    private List<RequestAdjunto> requestAdjuntos = new ArrayList();
    TextView tool_titulo;
    RecyclerView recy_lista_docs;
    DocumentAdapter documentAdapter;
    String pais_Id, cond_id;
    int cond, pas;
    private List<RequestAdjunto> documentList = new ArrayList<>();
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_documentos);
        setupUI(getWindow().getDecorView().getRootView());
        pais_Id = getIntent().getStringExtra("pais_Id");
        cond_id = getIntent().getStringExtra("cond_id");
        System.out.println("cond_id qui: " + cond_id);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        cond = prefs.getInt(Constants.primary_id, 0);
        pas = prefs.getInt(Constants.pais_id, 0);
        bundle = getIntent().getExtras();
        tool_titulo = findViewById(R.id.tool_titulo);
        recy_lista_docs = findViewById(R.id.recy_lista_docs);
        tool_titulo.setText(R.string.paso3);
        tool_titulo.setVisibility(View.VISIBLE);
        terminar = findViewById(R.id.terminar);
        terminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ListaDocumentosActivity.this);
                builder.setTitle("¿Estás seguro de terminar el registro?");
                builder.setMessage("Debes adjuntar todos los documentos requeridos");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        validarLista();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        listarDocumentos();
    }

    private void listarDocumentos() {
        Call<ResponseAdjuntoBody> callValidarCodigoVerificacion = conductorService.getListarAdjuntos(pas,cond);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseAdjuntoBody>() {
            @Override
            public void onResponse(Call<ResponseAdjuntoBody> call, Response<ResponseAdjuntoBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            responseAdjuntoBody = response.body();
                            documentList = response.body().getValor();
                            //System.out.println("Adju_Id:" + response.body().Valor.get(2).getAdju_Id());
                            documentAdapter = new DocumentAdapter(response.body().getValor());
                            documentAdapter.setEventos(eventos());
                            LinearLayoutManager linearLayoutManager=new LinearLayoutManager(ListaDocumentosActivity.this);
                            recy_lista_docs.setLayoutManager(linearLayoutManager);
                            recy_lista_docs.setHasFixedSize(true);
                            recy_lista_docs.setAdapter(documentAdapter);
                            if (responseAdjuntoBody != null) {
                                for (int i = 0; i < responseAdjuntoBody.getValor().size(); i++) {
                                    Log.d("aca sub", String.valueOf(responseAdjuntoBody.getValor().get(i).getAdju_Id()));
                                    requestAdjuntos.add(responseAdjuntoBody.getValor().get(i));
                                    if(responseAdjuntoBody.getValor().get(i).isTadj_Opcional()==false){
                                        if(responseAdjuntoBody.getValor().get(i).getAdju_Id() != 0){
                                            terminar.setEnabled(true);
                                            terminar.setBackgroundResource(R.drawable.button_primary_2);
                                        }else{
                                            terminar.setEnabled(false);
                                            terminar.setBackgroundResource(R.drawable.button_primary_2_grey);
                                        }
                                    }
                                }
                            }
                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseAdjuntoBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //checkMessages();
        listarDocumentos();
    }

    private SimpleRow eventos() {
        SimpleRow s = new SimpleRow() {
            @Override
            public void onClic(int position) {
                RequestAdjunto d = documentList.get(position);
                System.out.println("Tadj_Id: " + d.getTadj_Id());
                System.out.println("Eadj_Id: " + d.getAdju_Id());
                System.out.println("Adju_Id: " + d.getAdju_Id());
                System.out.println("Nombre: " + d.getTadj_Nombre());
                Intent intent = new Intent(ListaDocumentosActivity.this, InformacionDocumentosActivity.class);
                intent.putExtra("documento", d);
                startActivity(intent);
            }
        };
        return s;
    }

    private void validarLista() {
        if (responseAdjuntoBody != null) {
            for (int i = 0; i < responseAdjuntoBody.getValor().size(); i++) {
                if(responseAdjuntoBody.getValor().get(i).isTadj_Opcional()==false){
                    if(responseAdjuntoBody.getValor().get(i).getAdju_Id() != 0){
                        irEspera();
                    }else{
                        Toast.makeText(ListaDocumentosActivity.this, "Debes adjuntar todos los documentos requeridos", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    private void irEspera(){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Constants.primary_id, cond);
        editor.apply();
        Intent intent = new Intent(ListaDocumentosActivity.this, EsperaActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed (){}
}