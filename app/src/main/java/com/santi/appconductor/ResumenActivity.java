package com.santi.appconductor;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.santi.appconductor.adapters.ResumenAdapter;
import com.santi.appconductor.interfaces.SimpleRow;
import com.santi.appconductor.models.ActiveService;
import com.santi.appconductor.models.ChatMessage;
import com.santi.appconductor.models.FirebaseMessage;
import com.santi.appconductor.models.RequestAdjunto;
import com.santi.appconductor.models.ResponseAdjuntoBody;
import com.santi.appconductor.models.ResponseDocumentBody;
import com.santi.appconductor.models.ResponseResumen;
import com.santi.appconductor.models.ResponseResumenBody;
import com.santi.appconductor.models.ResponseResumenTotalBody;
import com.santi.appconductor.models.Travel;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResumenActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    ImageView iv_Atras;
    TextView tvMonto, tvtitulo,tvDias,tvNoPastpays;
    RecyclerView recy_resumen;
    LinearLayout relalayout;
    ResumenAdapter resumenAdapter;
    SharedPreferences prefs;
    ProgressDialog pDialogg;
    private ResponseResumenBody responseResumenBody;
    private List<ResponseResumen> resumen = new ArrayList<>();
    boolean chatAvailable, available, busy;
    List<Travel> viajes = new ArrayList<>();
    List<ChatMessage> mensajes_chat = new ArrayList<>();
    List<ActiveService> serviciosActivos = new ArrayList<>();
    AlertDialog adDisplayService;
    TextView tool_titulo_atras,vercuenta;
    FloatingActionButton fab;
    String link;
    private List<ResponseResumen> responseResumen = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumen);
        setupUI(getWindow().getDecorView().getRootView());
        iv_Atras = findViewById(R.id.iv_Atras);
        iv_Atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Resumen de pagos");
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        tvtitulo = findViewById(R.id.tvtitulo);
        tvNoPastpays = findViewById(R.id.tvNoPastpays);
        relalayout = findViewById(R.id.relalayout);
        tvMonto = findViewById(R.id.tvMonto);
        tvDias = findViewById(R.id.tvDias);
        recy_resumen = findViewById(R.id.recy_resumen);
        vercuenta = findViewById(R.id.vercuenta);
        vercuenta.setText(Html.fromHtml("<u>Ver cuentas bancarias de Santi</u>"));
        vercuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("link " + link);
                Uri uriUrl = Uri.parse(link);
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });
        fab = findViewById(R.id.fab);
        obtenerTotal();
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ResumenActivity.this, AbonoActivity.class);
                startActivity(intent);
            }
        });

        listarResumen();
    }

    private void obtenerTotal(){
        System.out.println("Aqui ire" + prefs.getInt(Constants.primary_id,0));
        Call<ResponseResumenTotalBody> callServiciosResumen = conductorService.getObtenerTotal(prefs.getInt(Constants.primary_id,0));
        callServiciosResumen.enqueue(new Callback<ResponseResumenTotalBody>() {
            @Override
            public void onResponse(Call<ResponseResumenTotalBody> call, Response<ResponseResumenTotalBody> response) {
                Log.i("mensajess","responseResumen: "+response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            tvtitulo.setText(response.body().getValor().getDescripcion());
                            tvMonto.setText(response.body().getValor().getMone_Simbolo()+" "+String.valueOf(response.body().getValor().getTotal()));
                            tvDias.setText(String.valueOf(response.body().getValor().getRetraso()));
                            link = response.body().getValor().getEnlaceCuentas();
                            //1 santi te debe
                            //2 le debes a santi
                            if(response.body().getValor().getTotal() == 0.0){
                                fab.setEnabled(false);
                            }else{
                                fab.setEnabled(true);
                            }

                            if(response.body().getValor().getEstado() == 1){
                                fab.setEnabled(false);
                            }else{
                                fab.setEnabled(true);
                            }

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseResumenTotalBody> call, Throwable t) {
                System.out.println("error");
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    private void listarResumen() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        pDialogg = new ProgressDialog(this);
        pDialogg.setMessage(getString(R.string.loading));
        pDialogg.setCancelable(false);
        pDialogg.setIndeterminate(false);
        pDialogg.show();
        System.out.println("IDCOND:" + prefs.getInt(Constants.primary_id,0));
        int id = prefs.getInt(Constants.primary_id,0);
        Call<ResponseResumenBody> callServiciosResumen = conductorService.getListarResumen(id,0);
        callServiciosResumen.enqueue(new Callback<ResponseResumenBody>() {
            @Override
            public void onResponse(Call<ResponseResumenBody> call, Response<ResponseResumenBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                pDialogg.dismiss();
                Log.i("mensaje","responseResumen: "+response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            if (response.body().getValor().isEmpty()) {
                                tvNoPastpays.setVisibility(View.VISIBLE);
                                tvNoPastpays.setText(getString(R.string.no_past_pay));
                                relalayout.setVisibility(View.GONE);
                            }else {
                                responseResumenBody = response.body();
                                resumen = response.body().getValor();
                                resumenAdapter = new ResumenAdapter(response.body().getValor());
                                resumenAdapter.setEventos(eventos());
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ResumenActivity.this);
                                recy_resumen.setLayoutManager(linearLayoutManager);
                                recy_resumen.setHasFixedSize(true);
                                recy_resumen.setAdapter(resumenAdapter);
                            }
                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseResumenBody> call, Throwable t) {
                System.out.println("error");
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    private SimpleRow eventos() {
        SimpleRow s = new SimpleRow() {
            @Override
            public void onClic(int position) {
                ResponseResumen r = resumen.get(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(ResumenActivity.this);
                builder.setTitle("Abono Rechazado");
                System.out.println(r.getAbon_Observacion());
                builder.setMessage(resumen.get(position).getAbon_Observacion());
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

            }
        };
        return s;
    }

    @Override
    protected void onResume() {
        super.onResume();
        listarResumenOnre();
        obtenerTotal();
        checkMessages();
    }


    private void listarResumenOnre() {
        //DisplayProgressDialog(getString(R.string.loading));
        //pDialog.show();
        //pDialogg = new ProgressDialog(this);
        //pDialogg.setMessage(getString(R.string.loading));
        //pDialogg.setCancelable(false);
        //pDialogg.setIndeterminate(false);
        //pDialogg.show();
        System.out.println("IDCOND:" + prefs.getInt(Constants.primary_id,0));
        int id = prefs.getInt(Constants.primary_id,0);
        Call<ResponseResumenBody> callServiciosResumen = conductorService.getListarResumen(id,0);
        callServiciosResumen.enqueue(new Callback<ResponseResumenBody>() {
            @Override
            public void onResponse(Call<ResponseResumenBody> call, Response<ResponseResumenBody> response) {
                //if (pDialog != null && pDialog.isShowing()) {
                //  pDialog.dismiss();
                //}
                //pDialogg.dismiss();
                Log.i("mensaje","responseResumen: "+response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            if (response.body().getValor().isEmpty()) {
                                tvNoPastpays.setVisibility(View.VISIBLE);
                                tvNoPastpays.setText(getString(R.string.no_past_pay));
                                relalayout.setVisibility(View.GONE);
                            }else {
                                responseResumenBody = response.body();
                                resumen = response.body().getValor();
                                resumenAdapter = new ResumenAdapter(response.body().getValor());
                                resumenAdapter.setEventos(eventos());
                                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ResumenActivity.this);
                                recy_resumen.setLayoutManager(linearLayoutManager);
                                recy_resumen.setHasFixedSize(true);
                                recy_resumen.setAdapter(resumenAdapter);
                            }
                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseResumenBody> call, Throwable t) {
                System.out.println("error");
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        if (!mensajes_chat.isEmpty()) {
            Gson gson = new Gson();
            String mensajesChatString = gson.toJson(mensajes_chat);
            intent.putExtra("mensajesChat", mensajesChatString);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_OK);
        }
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (available) {
            LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver3), new IntentFilter("MyData"));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (available) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver3);
        }
    }

    private final BroadcastReceiver mMessageReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkMessages();
        }
    };

    private void checkMessages() {
        if (adDisplayService == null || !adDisplayService.isShowing()) {
            List<FirebaseMessage> pending_messages;
            String pendingmessages = prefs.getString("pending_messages", "");
            if (!pendingmessages.isEmpty() && pendingmessages.length() > 2) {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<FirebaseMessage>>(){}.getType();
                pending_messages = gson.fromJson(pendingmessages, listType);
                FirebaseMessage fbMessage = pending_messages.get(0);
                pending_messages.remove(0);

                pendingmessages = gson.toJson(pending_messages);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("pending_messages", pendingmessages);
                editor.apply();

                processMessage(fbMessage);
            }
        }
    }

    private void processMessage(FirebaseMessage mensaje) {
        try {
            final JSONObject obj = new JSONObject(mensaje.getMessage());
            if (obj.has("Tipo")) {
                switch (obj.getInt("Tipo")) {
                    case 1: {
                        /*if (!busy) {*/
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"),obj.getString("Moneda"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(RESULT_FIRST_USER, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        /*}*/
                        break;
                    }
                    case 3: {
                        if (obj.getInt("Eser_Id") == 6 && !serviciosActivos.isEmpty() && obj.getInt("Serv_Id") == serviciosActivos.get(0).getServ_id()) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                        break;
                    }
                    case 4: {
                        adDisplayService = displayNotificationChat(String.format(getString(R.string.new_message_expression), serviciosActivos.get(0).getCliente().split(" ")[0], obj.getString("Chat_Mensaje")), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        ChatMessage msg = new ChatMessage(obj.getString("Chat_Emisor"), obj.getString("Chat_Mensaje"), obj.getString("Chat_Fecha"));
                        mensajes_chat.add(msg);
                        break;
                    }
                    case 5: {
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayScheduledService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(2, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        break;
                    }
                    case 6: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_canceled_by_customer), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("NombreCompleto")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        break;
                    }
                    case 7: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_reminder), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("Cliente")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        break;
                    }
                    case 9: {
                        /*if (!busy) {*/
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayServiceExtended(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Moneda"),new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(RESULT_FIRST_USER, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        /*}*/
                        break;
                    }
                    default: {
                        checkMessages();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            checkMessages();
            e.printStackTrace();
        }
    }
}