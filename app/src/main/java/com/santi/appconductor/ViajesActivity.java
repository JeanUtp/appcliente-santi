package com.santi.appconductor;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.santi.appconductor.adapters.DocumentAdapter;
import com.santi.appconductor.adapters.TravelAdapter;
import com.santi.appconductor.adapters.ViajesAdapter;
import com.santi.appconductor.interfaces.SimpleRow;
import com.santi.appconductor.models.ActiveService;
import com.santi.appconductor.models.ChatMessage;
import com.santi.appconductor.models.FirebaseMessage;
import com.santi.appconductor.models.RequestAdjunto;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseViajesBody;
import com.santi.appconductor.models.Travel;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;
import com.santi.appconductor.utils.Utilities;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViajesActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    ImageView iv_Atras;
    TextView tool_titulo_atras, tvNoPastTravels;
    LinearLayout layoytrelative;
    SharedPreferences prefs;
    String cliente;
    boolean chatAvailable, available, busy;
    List<Travel> viajes = new ArrayList<>();
    List<ChatMessage> mensajes_chat = new ArrayList<>();
    List<ActiveService> serviciosActivos = new ArrayList<>();
    AlertDialog adDisplayService;
    Spinner spFilter;
    RecyclerView rvViajes;
    ViajesAdapter viajesAdapter;
    Utilities util = new Utilities();
    int estado_transicion;
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    Calendar c = Calendar.getInstance();
    boolean fromScheduled = true;

    int serv_id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viajes);
        setupUI(getWindow().getDecorView().getRootView());
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        iv_Atras = findViewById(R.id.iv_Atras);
        iv_Atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constants.estado_panta, "");
                editor.apply();
                onBackPressed();
            }
        });
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Mis viajes");
        tvNoPastTravels = findViewById(R.id.tvNoPastTravels);
        layoytrelative = findViewById(R.id.layoytrelative);
        cliente = getIntent().getStringExtra("cliente");
        chatAvailable = getIntent().getBooleanExtra("chatAvailable", false);
        available = getIntent().getBooleanExtra("available", false);
        busy = getIntent().getBooleanExtra("busy", false);
        serv_id = getIntent().getIntExtra("serv_id", serv_id);

        Type listType1 = new TypeToken<ArrayList<ActiveService>>(){}.getType();

        Gson gson = new Gson();
        String serviciosActivosString = getIntent().getStringExtra("serviciosActivos");
        serviciosActivos = gson.fromJson(serviciosActivosString, listType1);

        if (chatAvailable) {
            Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
            String mensajesChatString = getIntent().getStringExtra("mensajesChat");
            mensajes_chat = gson.fromJson(mensajesChatString, listType);
        }

        spFilter = findViewById(R.id.spFilter);
        rvViajes = findViewById(R.id.rvViajes);

        spFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0: {
                        c = Calendar.getInstance();
                        String today = sdf.format(new Date(c.getTimeInMillis()));
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(Constants.estado_panta, "0");
                        editor.apply();
                        System.out.println("0");
                        consultarViajesPasados(today, today);
                        break;
                    }
                    case 1: {
                        c = Calendar.getInstance();
                        Date d = new Date(c.getTimeInMillis());
                        String lastDay = sdf.format(d);
                        c.add(Calendar.DATE, -7);
                        d = new Date(c.getTimeInMillis());
                        String firstDay = sdf.format(d);
                        Log.d("Fechas", firstDay + " " + lastDay);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(Constants.estado_panta, "1");
                        editor.apply();
                        System.out.println("1");
                        consultarViajesPasados(firstDay, lastDay);
                        break;
                    }
                    case 2: {
                        c = Calendar.getInstance();
                        Date d = new Date(c.getTimeInMillis());
                        String lastDay = sdf.format(d);
                        c.add(Calendar.DATE, -30);
                        d = new Date(c.getTimeInMillis());
                        String firstDay = sdf.format(d);
                        Log.d("Fechas", firstDay + " " + lastDay);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(Constants.estado_panta, "2");
                        editor.apply();
                        System.out.println("2");
                        consultarViajesPasados(firstDay, lastDay);
                        break;
                    }
                    case 3: {
                        c = Calendar.getInstance();
                        Date dd = new Date(c.getTimeInMillis());
                        String lastDay = sdf.format(dd);
                        c.add(Calendar.MONTH, -12);
                        dd = new Date(c.getTimeInMillis());
                        String firstDay = sdf.format(dd);
                        Log.d("Fechas", firstDay + " " + lastDay);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString(Constants.estado_panta, "3");
                        editor.apply();
                        System.out.println("3");
                        consultarViajesPasados(firstDay, lastDay);
                        break;
                    }
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        final ArrayAdapter<String> aaFilters = new ArrayAdapter<String>(getApplicationContext(), R.layout.my_spinner_item, new ArrayList<String>()) {
            @Override
            public boolean isEnabled(int position) {
                // TODO Auto-generated method stub
                /*if (position == 0) {
                    return false;
                }*/
                return true;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                TextView view = (TextView) super.getView(position, convertView, parent);
                //view.setTypeface(typeface);
                view.setTextColor(Color.RED);
                return view;
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                // TODO Auto-generated method stub
                View mView = super.getDropDownView(position, convertView, parent);
                TextView mTextView = (TextView) mView;


                mTextView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                //mTextView.setTypeface(typeface);
                /*if (position == 0) {
                    mTextView.setTextColor(Color.GRAY);
                } else {
                    mTextView.setTextColor(Color.BLACK);
                }*/
                return mTextView;
            }
        };
        aaFilters.add("Hoy");
        aaFilters.add("Esta Semana");
        aaFilters.add("Este Mes");
        aaFilters.add("Todos");
        spFilter.setAdapter(aaFilters);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkMessages();
        if(prefs.getString(Constants.estado_panta,"") != ""){
            System.out.println("Estado_Pantalla: " + prefs.getString(Constants.estado_panta,""));
            estado_transicion = Integer.parseInt(prefs.getString(Constants.estado_panta,""));
            switch (estado_transicion) {
                case 0: {
                    c = Calendar.getInstance();
                    String today = sdf.format(new Date(c.getTimeInMillis()));
                    consultarViajesPasadosOn(today, today);
                    break;
                }
                case 1: {
                    c = Calendar.getInstance();
                    Date d = new Date(c.getTimeInMillis());
                    String lastDay = sdf.format(d);
                    c.add(Calendar.DATE, -7);
                    d = new Date(c.getTimeInMillis());
                    String firstDay = sdf.format(d);
                    Log.d("Fechas", firstDay + " " + lastDay);
                    consultarViajesPasadosOnResum(firstDay, lastDay);
                    break;
                }
                case 2: {
                    c = Calendar.getInstance();
                    Date d = new Date(c.getTimeInMillis());
                    String lastDay = sdf.format(d);
                    c.add(Calendar.DATE, -30);
                    d = new Date(c.getTimeInMillis());
                    String firstDay = sdf.format(d);
                    Log.d("Fechas", firstDay + " " + lastDay);
                    consultarViajesPasadosOnResum(firstDay, lastDay);
                    break;
                }
                case 3: {
                    c = Calendar.getInstance();
                    Date dd = new Date(c.getTimeInMillis());
                    String lastDay = sdf.format(dd);
                    c.add(Calendar.MONTH, -12);
                    dd = new Date(c.getTimeInMillis());
                    String firstDay = sdf.format(dd);
                    Log.d("Fechas", firstDay + " " + lastDay);
                    consultarViajesPasadosOnResum(firstDay, lastDay);
                    break;
                }
                default:
                    break;
            }
        }else{
            System.out.println("Else");
        }

    }

    private void consultarViajesPasados(String fecInicio, String fecFin) {
        System.out.println("me estan consultando felipe");
        System.out.println("id: " + prefs.getInt(Constants.primary_id,0) + "inicio:" + fecInicio + "Fin: " + fecFin);
        fromScheduled = false;
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();

        Call<ResponseViajesBody> callServiciosRealizados = conductorService.getListarServiciosRealizados(prefs.getInt(Constants.primary_id,0), fecInicio, fecFin);
        callServiciosRealizados.enqueue(new Callback<ResponseViajesBody>() {
            @Override
            public void onResponse(Call<ResponseViajesBody> call, Response<ResponseViajesBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            if (response.body().getValor().isEmpty()) {
                                tvNoPastTravels.setVisibility(View.VISIBLE);
                                tvNoPastTravels.setText(getString(R.string.no_past_travels));
                                layoytrelative.setVisibility(View.GONE);
                                checkMessages();
                            } else {
                                tvNoPastTravels.setVisibility(View.GONE);
                                layoytrelative.setVisibility(View.VISIBLE);
                                viajes = response.body().getValor();
                                viajesAdapter = new ViajesAdapter(response.body().getValor());
                                viajesAdapter.setEventos(eventos());
                                LinearLayoutManager linearLayoutManager=new LinearLayoutManager(ViajesActivity.this);
                                rvViajes.setLayoutManager(linearLayoutManager);
                                rvViajes.setHasFixedSize(true);
                                rvViajes.setAdapter(viajesAdapter);
                            }
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseViajesBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    private void consultarViajesPasadosOnResum(String fecInicio, String fecFin) {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        System.out.println("me estan consultando felipe");
        System.out.println("id: " + prefs.getInt(Constants.primary_id,0) + "inicio:" + fecInicio + "Fin: " + fecFin);
        fromScheduled = false;
        System.out.println("cONSULTANDO ONRESUME");
        Call<ResponseViajesBody> callServiciosRealizados = conductorService.getListarServiciosRealizados(prefs.getInt(Constants.primary_id,0), fecInicio, fecFin);
        callServiciosRealizados.enqueue(new Callback<ResponseViajesBody>() {
            @Override
            public void onResponse(Call<ResponseViajesBody> call, Response<ResponseViajesBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            if(response.body().getValor().isEmpty()) {
                                tvNoPastTravels.setVisibility(View.VISIBLE);
                                tvNoPastTravels.setText(getString(R.string.no_past_travels));
                                layoytrelative.setVisibility(View.GONE);
                                checkMessages();
                            } else {
                                tvNoPastTravels.setVisibility(View.GONE);
                                layoytrelative.setVisibility(View.VISIBLE);
                                viajes = response.body().getValor();
                                viajesAdapter = new ViajesAdapter(response.body().getValor());
                                viajesAdapter.setEventos(eventos());
                                LinearLayoutManager linearLayoutManager=new LinearLayoutManager(ViajesActivity.this);
                                rvViajes.setLayoutManager(linearLayoutManager);
                                rvViajes.setHasFixedSize(true);
                                rvViajes.setAdapter(viajesAdapter);
                            }
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseViajesBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    private void consultarViajesPasadosOn(String fecInicio, String fecFin) {
        System.out.println("me estan consultando felipe");
        System.out.println("id: " + prefs.getInt(Constants.primary_id,0) + "inicio:" + fecInicio + "Fin: " + fecFin);
        fromScheduled = false;
        System.out.println("cONSULTANDO ONRESUME");
        Call<ResponseViajesBody> callServiciosRealizados = conductorService.getListarServiciosRealizados(prefs.getInt(Constants.primary_id,0), fecInicio, fecFin);
        callServiciosRealizados.enqueue(new Callback<ResponseViajesBody>() {
            @Override
            public void onResponse(Call<ResponseViajesBody> call, Response<ResponseViajesBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            if(response.body().getValor().isEmpty()) {
                                tvNoPastTravels.setVisibility(View.VISIBLE);
                                tvNoPastTravels.setText(getString(R.string.no_past_travels));
                                layoytrelative.setVisibility(View.GONE);
                                checkMessages();
                            } else {
                                tvNoPastTravels.setVisibility(View.GONE);
                                layoytrelative.setVisibility(View.VISIBLE);
                                viajes = response.body().getValor();
                                viajesAdapter = new ViajesAdapter(response.body().getValor());
                                viajesAdapter.setEventos(eventos());
                                LinearLayoutManager linearLayoutManager=new LinearLayoutManager(ViajesActivity.this);
                                rvViajes.setLayoutManager(linearLayoutManager);
                                rvViajes.setHasFixedSize(true);
                                rvViajes.setAdapter(viajesAdapter);
                            }
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseViajesBody> call, Throwable t) {
                adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    private void consultarViajesPasadosOnre(String fecInicio, String fecFin) {
        System.out.println("me estan consultando felipe");
        System.out.println("id: " + prefs.getInt(Constants.primary_id,0) + "inicio:" + fecInicio + "Fin: " + fecFin);
        fromScheduled = false;
        Call<ResponseViajesBody> callServiciosRealizados = conductorService.getListarServiciosRealizados(prefs.getInt(Constants.primary_id,0), fecInicio, fecFin);
        callServiciosRealizados.enqueue(new Callback<ResponseViajesBody>() {
            @Override
            public void onResponse(Call<ResponseViajesBody> call, Response<ResponseViajesBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            if (response.body().getValor().isEmpty()) {
                                tvNoPastTravels.setVisibility(View.VISIBLE);
                                tvNoPastTravels.setText(getString(R.string.no_past_travels));
                                layoytrelative.setVisibility(View.GONE);
                                checkMessages();
                            } else {
                                tvNoPastTravels.setVisibility(View.GONE);
                                layoytrelative.setVisibility(View.VISIBLE);
                                viajes = response.body().getValor();
                                viajesAdapter = new ViajesAdapter(response.body().getValor());
                                viajesAdapter.setEventos(eventos());
                                LinearLayoutManager linearLayoutManager=new LinearLayoutManager(ViajesActivity.this);
                                rvViajes.setLayoutManager(linearLayoutManager);
                                rvViajes.setHasFixedSize(true);
                                rvViajes.setAdapter(viajesAdapter);
                            }
                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseViajesBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.past_travels_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    private SimpleRow eventos() {
        SimpleRow s = new SimpleRow() {
            @Override
            public void onClic(int position) {
                Travel t = viajes.get(position);
                Intent intent = new Intent(ViajesActivity.this, DetalleViajeActivity.class);
                intent.putExtra("travel", t);
                startActivity(intent);
            }
        };
        return s;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        if (!mensajes_chat.isEmpty()) {
            Gson gson = new Gson();
            String mensajesChatString = gson.toJson(mensajes_chat);
            intent.putExtra("mensajesChat", mensajesChatString);
            setResult(RESULT_OK, intent);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString(Constants.estado_panta, "");
            editor.apply();
        } else {
            setResult(RESULT_OK);
        }
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (available) {
            LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver3), new IntentFilter("MyData"));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (available) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver3);
        }
    }

    private final BroadcastReceiver mMessageReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkMessages();
        }
    };

    private void checkMessages() {
        if (adDisplayService == null || !adDisplayService.isShowing()) {
            List<FirebaseMessage> pending_messages = new ArrayList<>();
            String pendingmessages = prefs.getString("pending_messages", "");
            if (!pendingmessages.isEmpty() && pendingmessages.length() > 2) {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<FirebaseMessage>>(){}.getType();
                pending_messages = gson.fromJson(pendingmessages, listType);
                FirebaseMessage fbMessage = pending_messages.get(0);
                pending_messages.remove(0);

                pendingmessages = gson.toJson(pending_messages);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("pending_messages", pendingmessages);
                editor.apply();

                processMessage(fbMessage);
            }
        }
    }

    private void processMessage(FirebaseMessage mensaje) {
        try {
            final JSONObject obj = new JSONObject(mensaje.getMessage());
            if (obj.has("Tipo")) {
                switch (obj.getInt("Tipo")) {
                    case 1: {
                        /*if (!busy) {*/
                            if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                                adDisplayService = displayService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Moneda"),new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent i = new Intent();
                                        try {
                                            i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                            i.putExtra("Cliente", obj.getString("Cliente"));
                                            i.putExtra("Origen", obj.getString("Origen"));
                                            i.putExtra("Destino", obj.getString("Destino"));
                                            setResult(RESULT_FIRST_USER, i);
                                            finish();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                }, new Runnable() {
                                    @Override
                                    public void run() {
                                        checkMessages();
                                    }
                                });
                                expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                    @Override
                                    public void run() {
                                        checkMessages();
                                    }
                                });
                            } else {
                                checkMessages();
                            }
                        /*}*/
                        break;
                    }
                    case 3: {
                        if (obj.getInt("Eser_Id") == 6 && !serviciosActivos.isEmpty() && obj.getInt("Serv_Id") == serviciosActivos.get(0).getServ_id()) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                        break;
                    }
                    case 4: {
                        adDisplayService = displayNotificationChat(String.format(getString(R.string.new_message_expression), serviciosActivos.get(0).getCliente().split(" ")[0], obj.getString("Chat_Mensaje")), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        ChatMessage msg = new ChatMessage(obj.getString("Chat_Emisor"), obj.getString("Chat_Mensaje"), obj.getString("Chat_Fecha"));
                        mensajes_chat.add(msg);
                        break;
                    }
                    case 5: {
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayScheduledService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(2, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        break;
                    }
                    case 6: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_canceled_by_customer), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("NombreCompleto")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                                if (spFilter.getVisibility() == View.GONE) {
                                    //consultarViajesPendientes();
                                }
                            }
                        });
                        break;
                    }
                    case 7: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_reminder), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("Cliente")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        break;
                    }
                    case 9: {
                        /*if (!busy) {*/
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayServiceExtended(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Moneda"),new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(RESULT_FIRST_USER, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        break;
                    }
                    default: {
                        checkMessages();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            checkMessages();
            e.printStackTrace();
        }
    }

    public static class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private final int spanCount;
        private final int spacing;
        private final boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                if (data != null && data.hasExtra("mensajesChat")) {
                    String mensajesChatString = data.getStringExtra("mensajesChat");
                    Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
                    Gson gson = new Gson();
                    mensajes_chat = gson.fromJson(mensajesChatString, listType);
                }
                if (!fromScheduled) {
                    switch (spFilter.getSelectedItemPosition()) {
                        case 0: {
                            c = Calendar.getInstance();
                            String today = sdf.format(new Date(c.getTimeInMillis()));
                            consultarViajesPasados(today, today);
                            break;
                        }
                        case 1: {
                            c = Calendar.getInstance();
                            Date d = new Date(c.getTimeInMillis());
                            String lastDay = sdf.format(d);
                            c.add(Calendar.DATE, -7);
                            d = new Date(c.getTimeInMillis());
                            String firstDay = sdf.format(d);
                            Log.d("Fechas", firstDay + " " + lastDay);
                            consultarViajesPasados(firstDay, lastDay);
                            break;
                        }
                        case 2: {
                            c = Calendar.getInstance();
                            Date d = new Date(c.getTimeInMillis());
                            String lastDay = sdf.format(d);
                            c.add(Calendar.DATE, -30);
                            d = new Date(c.getTimeInMillis());
                            String firstDay = sdf.format(d);
                            Log.d("Fechas", firstDay + " " + lastDay);
                            consultarViajesPasados(firstDay, lastDay);
                            break;
                        }
                        case 3: {
                            c = Calendar.getInstance();
                            Date dd = new Date(c.getTimeInMillis());
                            String lastDay = sdf.format(dd);
                            c.add(Calendar.MONTH, -12);
                            dd = new Date(c.getTimeInMillis());
                            String firstDay = sdf.format(dd);
                            Log.d("Fechas", firstDay + " " + lastDay);
                            consultarViajesPasados(firstDay, lastDay);
                            break;
                        }
                        default:
                            break;
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                setResult(RESULT_CANCELED);
                finish();
            } else if (resultCode == RESULT_FIRST_USER) {
                Intent i = new Intent();
                i.putExtra("Soli_Id", data.getIntExtra("Soli_Id", -1));
                i.putExtra("Cliente", data.getStringExtra("Cliente"));
                i.putExtra("Origen", data.getStringExtra("Origen"));
                i.putExtra("Destino", data.getStringExtra("Destino"));
                setResult(RESULT_FIRST_USER, i);
                finish();
            } else if (resultCode == 2) { //2 - Viaje programado
                Intent i = new Intent();
                i.putExtra("Soli_Id", data.getIntExtra("Soli_Id", -1));
                i.putExtra("Cliente", data.getStringExtra("Cliente"));
                i.putExtra("Origen", data.getStringExtra("Origen"));
                i.putExtra("Destino", data.getStringExtra("Destino"));
                setResult(2, i);
                finish();
            } else if (resultCode == 3) { //3 - Viaje programado cancelado
                //consultarViajesPendientes();
            } else if (resultCode == 4) { //4 - Viaje programado iniciado
                setResult(4);
                finish();
            }
        }
    }

}
