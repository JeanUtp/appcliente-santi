package com.santi.appconductor;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.santi.appconductor.models.RequestDriverValidateCode;
import com.santi.appconductor.models.RequestUpdateNumber;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseCodigoBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CambiarNumeroCodigoActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    EditText etCodigo1,etCodigo2,etCodigo3,etCodigo4;
    TextView tvIndicacion, tvContador, tvReenviar;
    Button btnValidar;
    Bundle bundle;
    ImageView iv_Close;
    SharedPreferences prefs;
    String token_fb,pais_Id,num_telefono, cond_id;
    TextView tool_titulo_atras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_numero_codigo);

        iv_Close = findViewById(R.id.iv_Close);
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tool_titulo_atras.setText("Código de validación");
        setupUI(getWindow().getDecorView().getRootView());
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        bundle = getIntent().getExtras();
        token_fb = prefs.getString(Constants.token,"");
        pais_Id = getIntent().getStringExtra("pais_Id");
        num_telefono = getIntent().getStringExtra("num_telefono");
        System.out.println("pais_Id: " + pais_Id);
        etCodigo1 = findViewById(R.id.etCodigo1);
        etCodigo2 = findViewById(R.id.etCodigo2);
        etCodigo3 = findViewById(R.id.etCodigo3);
        etCodigo4 = findViewById(R.id.etCodigo4);
        tvIndicacion = findViewById(R.id.tvIndicacion);
        tvContador = findViewById(R.id.tvContador);
        tvReenviar = findViewById(R.id.tvReenviar);

        btnValidar = findViewById(R.id.btnValidar);
        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyb();
                if (etCodigo1.getText().toString().equals("") && etCodigo2.getText().toString().equals("") && etCodigo3.getText().toString().equals("") & etCodigo4.getText().toString().equals("")) {
                    displayMessageSingle(getString(R.string.code_required));
                } else {
                    validarCodigoVerificacion();
                }
            }
        });

        tvIndicacion.setText(String.format(getString(R.string.enter_code), bundle.getString("phone_number")));
        DisplayProgressDialog(getString(R.string.validating));
        tvReenviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarNumero();
            }
        });
        runCounter();

        etCodigo1.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        etCodigo1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                etCodigo2.requestFocus();
            }
        });
        etCodigo2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                etCodigo3.requestFocus();
            }
        });
        etCodigo3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                etCodigo4.requestFocus();
            }
        });
    }

    public void validarCodigoVerificacion() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        final String celular = bundle.getString("phone_number");
        String codigo = etCodigo1.getText().toString()+etCodigo2.getText().toString()+etCodigo3.getText().toString()+etCodigo4.getText().toString();
        System.out.println("Codigo: " + codigo + "pais_Id: " + bundle.getString("pais_Id"));

        RequestDriverValidateCode request = new RequestDriverValidateCode();
        request.setNroTelefono(celular);
        request.setCodigoVerificacion(codigo);
        request.setTokenFirebase(token_fb);
        System.out.println("token_fb: " + token_fb);
        request.setIdDispositivo(getDeviceId(this));
        System.out.println("token_fb: " + token_fb);
        request.setPais_Id(Integer.parseInt(pais_Id));

        Call<ResponseCodigoBody> callValidarCodigoVerificacion = conductorService.postValidarCodigoVerificacion(request);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseCodigoBody>() {
            @Override
            public void onResponse(Call<ResponseCodigoBody> call, Response<ResponseCodigoBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.i("mensaje validarcod","response: "+response);
                System.out.println("Aqui 1");
                if (response.isSuccessful()) {
                    System.out.println("Aqui 2");
                    if (response.body() != null) {
                        System.out.println("Aqui 3");
                        if (response.body().isEstado()) {
                            actualizarTelefono();
                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseCodigoBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    public void runCounter() {
        tvContador.setVisibility(View.VISIBLE);
        CountDownTimer countDownTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                String cifra = String.format(Locale.getDefault(), "00:%d", millisUntilFinished / 1000L);
                if (cifra.length() < 5) {
                    cifra = "00:0" + cifra.substring(3);
                }
                tvContador.setText(String.format(getString(R.string.resend_countdown), cifra));
            }

            public void onFinish() {
                tvReenviar.setVisibility(View.VISIBLE);
                tvContador.setVisibility(View.GONE);
            }
        }.start();
    }

    public void validarNumero() {
        pDialog.show();
        Call<ResponseBody> callValidarNumeroCelular = conductorService.getValidaTelefonoConductor(bundle.getString("phone_number"),bundle.getString("pais_Id"));
        callValidarNumeroCelular.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.i("mensaje validarnum","response: "+response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            displayMessageSingle(getString(R.string.code_sent));
                            tvReenviar.setVisibility(View.GONE);
                            runCounter();
                        } else {
                            displayMessageSingle(getString(R.string.resend_error));
                        }
                    } else {
                        displayMessageSingle(getString(R.string.resend_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.resend_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.resend_error));
            }
        });
    }

    private void actualizarTelefono() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        RequestUpdateNumber requestUpdateNumber = new RequestUpdateNumber();
        requestUpdateNumber.setCond_Id(prefs.getInt(Constants.primary_id,0));
        requestUpdateNumber.setPais_Id(prefs.getInt(Constants.pais_id,0));
        requestUpdateNumber.setCond_NroTelefono(num_telefono);

        Call<ResponseBody> callValidarCodigoVerificacion = conductorService.actualizarNumero(requestUpdateNumber);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.i("mensajeactualizar","response: "+response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            //retorna atras
                            AlertDialog.Builder builder = new AlertDialog.Builder(CambiarNumeroCodigoActivity.this);
                            builder.setTitle(R.string.doc_titulo);
                            builder.setMessage("El número de teléfono se actualizó con éxito.");
                            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onBackPressed();
                                    finish();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {
        String deviceId;
        deviceId = Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return deviceId;
    }
}