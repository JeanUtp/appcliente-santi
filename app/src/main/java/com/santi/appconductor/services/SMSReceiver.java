package com.santi.appconductor.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;

public class SMSReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Telephony.Sms.Intents.SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            for (SmsMessage smsMessage : Telephony.Sms.Intents.getMessagesFromIntent(intent)) {
                String message = smsMessage.getMessageBody();
                if (message.toLowerCase().contains("bertha")) {
                    String token = message.replace(".","").substring(message.length() - 5);
                    Intent iToken = new Intent("broadCastToken");
                    iToken.putExtra("received_token", token);
                    context.sendBroadcast(iToken);
                }
            }
        }
    }
}



