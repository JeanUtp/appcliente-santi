package com.santi.appconductor.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.santi.appconductor.models.RequestUpdateDriverLocation;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.utils.Constants;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationReporter extends BroadcastReceiver {

    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    SharedPreferences prefs;

    @Override
    public void onReceive(Context context, Intent intent) {
        prefs =  context.getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        Log.d("GPS", "Reportar ubicación");
        actualizarUbicacion();
    }

    private void actualizarUbicacion() {
        RequestUpdateDriverLocation request = new RequestUpdateDriverLocation();
        request.setCond_Id(prefs.getInt(Constants.primary_id,0));
        final String ubicacion = prefs.getString(Constants.last_location, "");
        request.setUcon_Coordenada(ubicacion);

        if (!Objects.requireNonNull(ubicacion).isEmpty()) {
            Call<ResponseBody> callService = conductorService.putActualizarUbicacionConductor(request);
            callService.enqueue(new Callback<ResponseBody>() {
                @SuppressWarnings("NullableProblems")
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().isEstado()) {
                                Log.d("GPS", "Ubicación reportada con éxito(R) " + ubicacion);
                            } else {
                                Log.d("GPS", response.body().getMensaje());
                            }
                        } else {
                            Log.d("GPS", "Error reportando Ubicación");
                        }
                    } else {
                        Log.d("GPS", "Error reportando Ubicación");
                    }
                }

                @SuppressWarnings("NullableProblems")
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("GPS", "Error reportando Ubicación");
                }
            });
        } else {
            Log.d("GPS", "No hay Ubicación para reportar");
        }
    }

}
