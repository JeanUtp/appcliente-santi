package com.santi.appconductor.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Person;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ShortcutInfo;
import android.graphics.drawable.Icon;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.santi.appconductor.MenuActivity;
import com.santi.appconductor.R;

import java.util.Collections;

public class GestorNotificaciones {
    private final String CHANNELS = "Algoritmo";
    private final int REQUEST_CONTENT = 1;
    private final int REQUEST_BUBBLE = 2;
    private final int NOTIFICATION_ID = 0;
    Context _context;
    private NotificationManager notificationManager;
    public GestorNotificaciones(Context context)
    {
        _context = context;
        notificationManager = _context.getSystemService(NotificationManager.class);
        setUpNotificationChannels();
    }

    private void setUpNotificationChannels() {
        if (notificationManager.getNotificationChannel(CHANNELS) == null) {
            Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + _context.getPackageName() + "/" + R.raw.misc237);
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationChannel channel = new NotificationChannel(
                    CHANNELS,
                    "Canal de Nueva Solicitud",
                    NotificationManager.IMPORTANCE_HIGH
            );
            channel.setDescription("Nueva notificacion");
            channel.setImportance(NotificationManager.IMPORTANCE_HIGH);
            channel.setSound(sound, attributes);
            notificationManager.createNotificationChannel(channel);
        }
    }
    private Person crearPersona(Icon icon) {
        return new Person.Builder()
                .setName("Nueva Solicitud de Viaje")
                .setIcon(icon)
                .setBot(true)
                .setImportant(true)
                .build();
    }
    private PendingIntent crearIntent(int requestCode) {
        return PendingIntent.getActivity(
                _context,
                requestCode,
                new Intent(_context, MenuActivity.class), PendingIntent.FLAG_UPDATE_CURRENT

        );
    }
    private Notification.BubbleMetadata crearBubbleMetadata(Icon icon)  {
        return new Notification.BubbleMetadata.Builder(crearIntent(REQUEST_BUBBLE), icon)
                .setDesiredHeight(600)
                .setAutoExpandBubble(false)
                //.setSuppressNotification(true)
                .build();
    }



    private Notification.Builder crearNotification(String mensaje, Icon icon , Person persona) {

        String shortcutId = "Id1";
        ShortcutInfo shortcut =
                new ShortcutInfo.Builder(_context, shortcutId)
                        .setCategories(Collections.singleton("com.example.category.IMG_SHARE_TARGET"))
                        .setIntent(new Intent(Intent.ACTION_DEFAULT))
                        .setLongLived(true)
                        .setPerson(persona)
                        .setShortLabel(persona.getName())
                        .build();

        return new Notification.Builder(_context, CHANNELS)
                .setContentText(mensaje)
                .setLargeIcon(icon)
                .setSmallIcon(icon)
                .setCategory(Notification.CATEGORY_CALL)
                .setStyle(
                        new Notification.MessagingStyle(persona)
                                .setGroupConversation(false)
                                .addMessage(mensaje, System.currentTimeMillis(), persona)
                )
                .addPerson(persona)
                .setShowWhen(true)
                .addAction(new Notification.Action.Builder(Icon.createWithResource(_context, R.drawable.background_button_dialogs), "Ver", crearIntent(REQUEST_BUBBLE)).build())
                .setContentIntent(crearIntent(REQUEST_CONTENT))
                .setAutoCancel(true)
                .setFullScreenIntent(crearIntent(REQUEST_BUBBLE), true)
                .setShortcutId(shortcutId);

    }
    public void mostrarNotification(String mensaje) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Icon icon = Icon.createWithResource(_context, R.mipmap.ic_launcher_santinew3_round);
            Person persona = crearPersona(icon);
            Notification.Builder notification = crearNotification(mensaje, icon, persona);
            Notification.BubbleMetadata bubbleMetaData = crearBubbleMetadata(icon);
            notification.setBubbleMetadata(bubbleMetaData);
            notificationManager.notify(NOTIFICATION_ID, notification.build());
        }

    }
}
