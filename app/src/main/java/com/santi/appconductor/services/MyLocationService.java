package com.santi.appconductor.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.santi.appconductor.models.RequestUpdateDriverLocation;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.utils.Constants;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyLocationService extends Service {

    private static final String TAG = "MyLocationService";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 10;
    private static final float LOCATION_DISTANCE = 1f;
    SharedPreferences prefs;
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            SharedPreferences.Editor editor = prefs.edit();
            String ubicacion = location.getLatitude() + "," + location.getLongitude();
            Log.d("Location","location updated on storage");
            editor.putString(Constants.last_location, ubicacion);
            editor.apply();
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    /*
    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    */

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {

        Log.e(TAG, "onCreate");

        prefs =  getApplicationContext().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);

        initializeLocationManager();



        Log.d("location", "servicio iniciado");

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    LOCATION_INTERVAL,
                    LOCATION_DISTANCE,


                    mLocationListeners[0]
            );
        } catch (SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }

        /*try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    LOCATION_INTERVAL,
                    LOCATION_DISTANCE,
                    mLocationListeners[1]
            );
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }*/
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listener, ignore", ex);
                }
            }
        }
        Log.d("location", "servicio destruido");
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager - LOCATION_INTERVAL: "+ LOCATION_INTERVAL + " LOCATION_DISTANCE: " + LOCATION_DISTANCE);
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private void actualizarUbicacion() {

        RequestUpdateDriverLocation request = new RequestUpdateDriverLocation();
        request.setCond_Id(prefs.getInt(Constants.primary_id,0));
        final String ubicacion = prefs.getString(Constants.last_location, "");
        request.setUcon_Coordenada(ubicacion);

        if (!Objects.requireNonNull(ubicacion).isEmpty()) {
            Call<ResponseBody> callService = conductorService.putActualizarUbicacionConductor(request);
            callService.enqueue(new Callback<ResponseBody>() {
                @SuppressWarnings("NullableProblems")
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            if (response.body().isEstado()) {
                                Log.d("GPS", "Ubicación reportada con éxito (S) " + ubicacion);
                            } else {
                                Log.d("GPS", response.body().getMensaje());
                            }
                        } else {
                            Log.d("GPS", "Error reportando Ubicación");
                        }
                    } else {
                        Log.d("GPS", "Error reportando Ubicación");
                    }
                }

                @SuppressWarnings("NullableProblems")
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("GPS", "Error reportando Ubicación");
                }
            });
        } else {
            Log.d("GPS", "No hay Ubicación para reportar");
        }
    }

}
