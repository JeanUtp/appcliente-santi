package com.santi.appconductor.services;

import com.santi.appconductor.models.CalificacionClientePost;
import com.santi.appconductor.models.DriverInfo;
import com.santi.appconductor.models.RequestAbono;
import com.santi.appconductor.models.RequestAddAdjunto;
import com.santi.appconductor.models.RequestAddChat;
import com.santi.appconductor.models.RequestAddService;
import com.santi.appconductor.models.RequestAlerta;
import com.santi.appconductor.models.RequestCancelService;
import com.santi.appconductor.models.RequestCuentaBancaria;
import com.santi.appconductor.models.RequestDriverValidateCode;
import com.santi.appconductor.models.RequestEndService;
import com.santi.appconductor.models.RequestUpdateCorreo;
import com.santi.appconductor.models.RequestUpdateDriverLocation;
import com.santi.appconductor.models.RequestUpdateFirebaseToken;
import com.santi.appconductor.models.RequestUpdateFotPerfil;
import com.santi.appconductor.models.RequestUpdateNumber;
import com.santi.appconductor.models.RequestUpdateService;
import com.santi.appconductor.models.ResponseActiveServiceBody;
import com.santi.appconductor.models.ResponseActualizarCar;
import com.santi.appconductor.models.ResponseAddServiceBody;
import com.santi.appconductor.models.ResponseAdjuntoBody;
import com.santi.appconductor.models.ResponseBancosBody;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseBodyTerminosCondicionesGenerales;
import com.santi.appconductor.models.ResponseCar;
import com.santi.appconductor.models.ResponseCodigoBody;
import com.santi.appconductor.models.ResponseCuentaBancariaBody;
import com.santi.appconductor.models.ResponseDocumentBody;
import com.santi.appconductor.models.ResponseDriverBody;
import com.santi.appconductor.models.ResponseMarcaBody;
import com.santi.appconductor.models.ResponseModeloBody;
import com.santi.appconductor.models.ResponsePaisBody;
import com.santi.appconductor.models.ResponseResumenBody;
import com.santi.appconductor.models.ResponseResumenTotalBody;
import com.santi.appconductor.models.ResponseTiempodeViajeBody;
import com.santi.appconductor.models.ResponseVehiculoBody;
import com.santi.appconductor.models.ResponseViajesBody;
import com.santi.appconductor.models.TiempoDeViajeImput;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface DriverService {

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ValidarPais/{ContryCode}")
    Call<ResponsePaisBody> getValidaPais(@Path("ContryCode") String ContryCode);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarCuentaBancaria/{Cond_Id}")
    Call<ResponseCuentaBancariaBody> getCuentaBancaria(@Path("Cond_Id") int Cond_Id);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ValidaTelefonoConductor/{NroTelefono}/{idPais}")
    Call<ResponseBody> getValidaTelefonoConductor(@Path("NroTelefono") String NroTelefono,
                                                  @Path("idPais") String idPais);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("ValidarCodigoVerificacionConductor")
    Call<ResponseCodigoBody> postValidarCodigoVerificacion(@Body RequestDriverValidateCode request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ListarTipoDocumento")
    Call<ResponseDocumentBody> getListarDocumento();

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("InsertarConductor")
    Call<ResponseBody> postRegistrarDatosConductor(@Body DriverInfo request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ListarMarcas")
    Call<ResponseMarcaBody> getListarMarca();

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ListarModelos/{Marc_Id}")
    Call<ResponseModeloBody> getListarModelos(@Path("Marc_Id") Integer Marc_Id);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("InsertarVehiculo")
    Call<ResponseBody> postRegistrarDatosVehiculo(@Body ResponseCar request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ListarTipoAdjunto/{Pais_Id}/{Cond_Id}")
    Call<ResponseAdjuntoBody> getListarAdjuntos(@Path("Pais_Id") Integer Pais_Id,
                                                @Path("Cond_Id") Integer Cond_Id);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("InsertarAdjuntos")
    Call<ResponseBody> postRegistrarAdjuntos(@Body RequestAddAdjunto request);


    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarConductor/{PaisId}/{ConductorId}")
    Call<ResponseDriverBody> getSeleccionarConductor(@Path("PaisId") Integer PaisId,
                                                     @Path("ConductorId") Integer ConductorId);


    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("InsertarAlerta")
    Call<ResponseBody> postRegistrarAlerta(@Body RequestAlerta request);


    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("ActualizarDisponibilidadConductor/{ConductorId}/{Estado}")
    Call<ResponseBody> putActualizarDisponibilidadConductor(@Path("ConductorId") int ConductorId,
                                                            @Path("Estado") boolean Estado);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ListarResumenRecaudacion/{Cond_Id}/{Eabo_Id}")
    Call<ResponseResumenBody> getListarResumen(@Path("Cond_Id") Integer Cond_Id,
                                               @Path("Eabo_Id") Integer Eabo_Id);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarResumenTotalRecaudacion/{Cond_Id}")
    Call<ResponseResumenTotalBody> getObtenerTotal(@Path("Cond_Id") Integer Cond_Id);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("InsertarAbono")
    Call<ResponseBody> postRegistrarAbono(@Body RequestAbono request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("ActualizarFotoConductor")
    Call<ResponseBody> actualizarFotoPefil(@Body RequestUpdateFotPerfil request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("ActualizarVehiculo")
    Call<ResponseBody> putActualizarDatosVehiculo(@Body ResponseActualizarCar request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("ActualizarCorreoConductor")
    Call<ResponseBody> actualizarcorrreo(@Body RequestUpdateCorreo request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("ActualizarNroTelefonoConductor")
    Call<ResponseBody> actualizarNumero(@Body RequestUpdateNumber request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ListarBancoPorPais/{Pais_Id}")
    Call<ResponseBancosBody> getListarBancos(@Path("Pais_Id") int Pais_Id);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("InsertarCuentaBancaria")
    Call<ResponseBody> postRegistrCuenta(@Body RequestCuentaBancaria request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("InsertarServicio")
    Call<ResponseAddServiceBody> postInsertarServicio(@Body RequestAddService request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("ActualizarEstadoServicio")
    Call<ResponseBody> putActualizarServicio(@Body RequestUpdateService request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ListarFormaPago")
    Call<ResponseBody> getListarFormaPago();

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("AnularServicio")
    Call<ResponseBody> putAnularServicio(@Body RequestCancelService request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("FinalizarServicio")
    Call<ResponseBody> putFinalizarServicio(@Body RequestEndService request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("ActualizarUbicacionConductor")
    Call<ResponseBody> putActualizarUbicacionConductor(@Body RequestUpdateDriverLocation request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("CalificarCliente")
    Call<ResponseBody> putCalificarCliente(@Body CalificacionClientePost calificacionClientePost);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("InsertarChat")
    Call<ResponseBody> postInsertarChat(@Body RequestAddChat request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarResumenServicios/{CondId}/{FechaIni}/{FechaFin}")
    Call<ResponseBody> getSeleccionarResumenServicios(@Path("CondId") int CondId,
                                                      @Path("FechaIni") String FechaIni,
                                                      @Path("FechaFin") String FechaFin);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("ListarMisViajes/0/{ConductorId}/{FecInicio}/{FecFin}")
    Call<ResponseViajesBody> getListarServiciosRealizados(@Path("ConductorId") int ConductorId,
                                                          @Path("FecInicio") String FecInicio,
                                                          @Path("FecFin") String FecFin);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarTelefonoAyudaConductor")
    Call<ResponseBody> getSeleccionarTelefonoAyudaConductor();

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarCorreoAyudaConductor")
    Call<ResponseBody> getSeleccionarCorreoAyudaConductor();

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarServicioActivo/0/{ConductorId}")
    Call<ResponseActiveServiceBody> getSeleccionarServicioActivo(@Path("ConductorId") int ConductorId);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarTiempoAceptarSolicitud")
    Call<ResponseBody> getSeleccionarTiempoAceptarSolicitud();


    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @PUT("ActualizarTokenFirebase")
    Call<ResponseBody> putActualizarTokenFirebase(@Body RequestUpdateFirebaseToken request);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarVehiculo/{ConductorId}")
    Call<ResponseVehiculoBody> getSeleccionarVehiculo(@Path("ConductorId") int ConductorId);

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @GET("SeleccionarTerminosYCondicionesConductor")
    Call<ResponseBodyTerminosCondicionesGenerales> SeleccionarTerminosYCondicionesGenerales();

    @Headers({"Content-Type:application/json", "authorization:8GG9lGJgp+aAC2rU+eCQyb17rtQkhQz+TY2vVLabk24="})
    @POST("TiempoDeViaje")
    Call<ResponseTiempodeViajeBody> getObtenerTiempoo(@Body TiempoDeViajeImput tiempoDeViajeImput);
}
