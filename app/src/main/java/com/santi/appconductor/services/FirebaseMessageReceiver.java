package com.santi.appconductor.services;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.santi.appconductor.models.FirebaseMessage;
import com.santi.appconductor.models.RequestUpdateFirebaseToken;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.utils.Constants;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirebaseMessageReceiver extends FirebaseMessagingService {

    private LocalBroadcastManager broadcaster;
    private SharedPreferences prefs;
    private GestorNotificaciones gestorNotificationes;
    @Override
    public void onCreate() {
        broadcaster = LocalBroadcastManager.getInstance(this);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("Firebase", "Message received");
        Intent intent = new Intent("MyData");

        String message;
        try {
            message = (String) remoteMessage.getData().values().toArray()[0];

            Log.d("Firebase cntn", message);

            FirebaseMessage fbMsg = new FirebaseMessage();
            fbMsg.setMessage(message);
            fbMsg.setReceivedOn(Calendar.getInstance().getTime());

            List<FirebaseMessage> pending_messages = new ArrayList<>();
            String pendingmessages = prefs.getString("pending_messages", "");
            Gson gson = new Gson();

            if (!Objects.requireNonNull(pendingmessages).isEmpty() && pendingmessages.length() > 2) {
                Type listType = new TypeToken<ArrayList<FirebaseMessage>>(){}.getType();
                pending_messages = gson.fromJson(pendingmessages, listType);
            }
            pending_messages.add(fbMsg);

            pendingmessages = gson.toJson(pending_messages);

            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("pending_messages", pendingmessages);
            editor.apply();

            List<FirebaseMessage> listpending_messages = new ArrayList<>();
            String getpendingmessages = prefs.getString("pending_messages", "");
            if (!getpendingmessages.isEmpty() && getpendingmessages.length() > 2) {
                Gson gson2 = new Gson();
                Type listType = new TypeToken<ArrayList<FirebaseMessage>>(){}.getType();
                listpending_messages = gson2.fromJson(getpendingmessages, listType);
                FirebaseMessage fbMessage = listpending_messages.get(0);
                listpending_messages.remove(0);

                final JSONObject obj = new JSONObject(Objects.requireNonNull(fbMessage.getMessage()));

                if (obj.has("Tipo")){

                    switch (obj.getInt("Tipo"))
                    {
                        case 1:
                            gestorNotificationes = new GestorNotificaciones(getApplication());
                            gestorNotificationes.mostrarNotification("Cliente: " + obj.getString("Cliente") + "\n"
                            +"Origen: "+obj.getString("Origen") + "\n"
                            +"Destino: "+obj.getString("Destino") + "\n"
                            +"Tiempo de Recojo: "+obj.getString("TiempoDeRecojo") + "\n"
                                    +"Tiempo de Viaje: "+obj.getString("TiempoDeViaje") + "\n"
                                    +"Precio: "+obj.getString("Precio") + "\n"
                                    +"Moneda: "+obj.getString("Moneda") + "\n");
                    }

                }

            }


            broadcaster.sendBroadcast(intent);
        } catch (Exception e) { //Not data message or doesn't have proper message

        }

    }


    @SuppressWarnings("NullableProblems")
    @Override
    public void onNewToken(String token) {
        Log.d("Firebase", "New Token: " + token);
        if (prefs.getInt(Constants.primary_id,0) > 0) {
            updateTokenFirebase(token);
        }
    }

    public void updateTokenFirebase(String token) {
        RequestUpdateFirebaseToken request = new RequestUpdateFirebaseToken();
        request.setCond_Id(prefs.getInt(Constants.primary_id,0));
        request.setTokenFirebase(token);
        request.setIdDispositivo(getDeviceId(this));

        DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);

        Call<ResponseBody> callActualizarTokenFirebase = conductorService.putActualizarTokenFirebase(request);
        callActualizarTokenFirebase.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            Log.d("Firebase", "Token actualizado en DB desde FR");
                        } else {
                            Log.d("Firebase", "Error actualizando token en DB desde FR");
                        }
                    } else {
                        Log.d("Firebase", "Error actualizando token en DB desde FR");
                    }
                } else {
                    Log.d("Firebase", "Error actualizando token en DB desde FR");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Firebase", "Error actualizando token en DB");
            }
        });
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {

        String deviceId;

        deviceId = Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return deviceId;
    }
}
