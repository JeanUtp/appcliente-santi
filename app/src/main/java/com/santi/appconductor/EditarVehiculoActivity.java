package com.santi.appconductor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.textfield.TextInputLayout;
import com.santi.appconductor.models.ActiveService;
import com.santi.appconductor.models.RequestMarca;
import com.santi.appconductor.models.RequestModelo;
import com.santi.appconductor.models.RespondeActualizarVehiculo;
import com.santi.appconductor.models.RespondeVehiculo;
import com.santi.appconductor.models.ResponseActiveService;
import com.santi.appconductor.models.ResponseActiveServiceBody;
import com.santi.appconductor.models.ResponseActualizarCar;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseCar;
import com.santi.appconductor.models.ResponseMarcaBody;
import com.santi.appconductor.models.ResponseModeloBody;
import com.santi.appconductor.models.ResponseVehiculoBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;
import com.santi.appconductor.utils.PermisionChecker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.santi.appconductor.utils.Constants.CAMARA;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;
import static com.santi.appconductor.utils.Constants.compressImage;

public class EditarVehiculoActivity extends BaseActivity implements View.OnClickListener{
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Button btnGuardar,btnEditar;
    TextView tool_titulo;
    EditText etPlaca,etAnio,etMarca,etModelo,etservicio;
    Context context;
    LinearLayout lyMarca, lyModelo;
    TextInputLayout tvModelo,tvMarca;
    SharedPreferences prefs;
    AutoCompleteTextView spAnioFabricacion,spMarca,spModelo;
    private ResponseMarcaBody responseMarcaBody;
    private List<RequestMarca> requestMarcaList = new ArrayList();
    private ResponseModeloBody responseModeloBody;
    private List<RequestModelo> requestModeloList = new ArrayList();
    ImageView iv_Close;
    //variables imagenes
    boolean imgProcedencia1, imgProcedencia2;
    private RelativeLayout rltimg1, rltimg2;
    LinearLayout linearCameraPrincipal, linearCuadroFotos, btnCameraPrincipal;
    ImageView imgF1, imgF2, btnAddF1, btnAddF2,img_f1actual,img_f2actual;
    RelativeLayout rlDeleteF1, rlDeleteF2;
    int idmodelo ,idmarca, modserv,marcsrv;
    String pathImg1, pathImg2,nombremodelo,nombremarca,cond_id,pathImg1serv, pathImg2serv, pathImg1ok,pathImg2ok;
    private static final String IMAGE_DIRECTORY = "/santiappconductor";
    private int GALLERY = 1, CAMERA = 2;
    TextView tvimag;
    TextView tool_titulo_atras;
    private String mod,marc;
    private String ft1,ft2;
    LinearLayout cont_tomarfoto,cont_mostrarfoto;
    static final int REQUEST_TAKE_PHOTO = 2;
    private String tmpRuta = "";
    Bitmap imagen;
    Uri photoURI;
    String currentPhotoPath;

    private int idservmarca, idservmodelo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_vehiculo);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        iv_Close = findViewById(R.id.iv_Close);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Edita los datos de tu vehículo");
        spMarca = findViewById(R.id.spMarca);
        spModelo = findViewById(R.id.spModelo);
        etPlaca = findViewById(R.id.etPlaca);
        etMarca = findViewById(R.id.etMarca);
        etModelo = findViewById(R.id.etModelo);
        tvModelo = findViewById(R.id.tvModelo);
        tvMarca = findViewById(R.id.tvMarca);
        etAnio = findViewById(R.id.etAnio);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnEditar = findViewById(R.id.btnEditar);
        context = EditarVehiculoActivity.this;
        linearCameraPrincipal = findViewById(R.id.ll_input_foto);
        linearCuadroFotos = findViewById(R.id.ll_fotos);
        lyMarca = findViewById(R.id.lyMarca);
        etservicio = findViewById(R.id.etservicio);
        lyModelo = findViewById(R.id.lyModelo);
        imgF1 = findViewById(R.id.img_f1);
        imgF2 = findViewById(R.id.img_f2);
        rlDeleteF1 = findViewById(R.id.rl_delete_f1);
        rlDeleteF2 = findViewById(R.id.rl_delete_f2);
        btnAddF1 = findViewById(R.id.btn_add_f1);
        btnAddF2 = findViewById(R.id.btn_add_f2);
        btnCameraPrincipal = findViewById(R.id.ll_btn_camera);
        rltimg1 = findViewById(R.id.relativeimg1);
        rltimg2 = findViewById(R.id.relativeimg2);
        pathImg1="";
        pathImg2="";
        tvimag = findViewById(R.id.tvimag);
        btnGuardar.setOnClickListener(this);
        btnEditar.setEnabled(false);
        btnCameraPrincipal.setOnClickListener(this);
        linearCameraPrincipal.setOnClickListener(this);
        linearCuadroFotos.setOnClickListener(this);
        btnAddF1.setOnClickListener(this);
        btnAddF2.setOnClickListener(this);
        rlDeleteF1.setOnClickListener(this);
        rlDeleteF2.setOnClickListener(this);
        cont_tomarfoto = findViewById(R.id.cont_tomarfoto);
        cont_mostrarfoto = findViewById(R.id.cont_mostrarfoto);
        img_f1actual = findViewById(R.id.img_f1actual);
        img_f2actual = findViewById(R.id.img_f2actual);
        switch (2){
            case 1:
                rltimg1.setVisibility(View.VISIBLE);
                break;
            case 2:
                rltimg1.setVisibility(View.VISIBLE);
                rltimg2.setVisibility(View.VISIBLE);
                tvimag.setText("Subir Imágenes");
                break;
        }
        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cont_tomarfoto.setVisibility(View.VISIBLE);
                cont_mostrarfoto.setVisibility(View.GONE);
                btnGuardar.setVisibility(View.VISIBLE);
                btnEditar.setVisibility(View.GONE);
            }
        });
        System.out.println("Vehiculo enviar");
        ObtenerVehiculo();

        /*etPlaca.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!etPlaca.getText().toString().isEmpty()){
                    cont_tomarfoto.setVisibility(View.GONE);
                    cont_mostrarfoto.setVisibility(View.GONE);
                    btnGuardar.setVisibility(View.VISIBLE);
                    btnEditar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!etPlaca.getText().toString().isEmpty()){
                    cont_tomarfoto.setVisibility(View.GONE);
                    cont_mostrarfoto.setVisibility(View.GONE);
                    btnGuardar.setVisibility(View.VISIBLE);
                    btnEditar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!etPlaca.getText().toString().isEmpty()){
                    cont_tomarfoto.setVisibility(View.GONE);
                    cont_mostrarfoto.setVisibility(View.GONE);
                    btnGuardar.setVisibility(View.VISIBLE);
                    btnEditar.setVisibility(View.VISIBLE);
                }
            }
        });

        etAnio.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!etAnio.getText().toString().isEmpty()){
                    cont_tomarfoto.setVisibility(View.GONE);
                    cont_mostrarfoto.setVisibility(View.GONE);
                    btnGuardar.setVisibility(View.VISIBLE);
                    btnEditar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(!etAnio.getText().toString().isEmpty()){
                    cont_tomarfoto.setVisibility(View.GONE);
                    cont_mostrarfoto.setVisibility(View.GONE);
                    btnGuardar.setVisibility(View.VISIBLE);
                    btnEditar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!etAnio.getText().toString().isEmpty()){
                    cont_tomarfoto.setVisibility(View.GONE);
                    cont_mostrarfoto.setVisibility(View.GONE);
                    btnGuardar.setVisibility(View.VISIBLE);
                    btnEditar.setVisibility(View.VISIBLE);
                }
            }
        });*/
    }

    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.btnGuardar:
                    ValidarCampos();
                    break;
                case R.id.ll_btn_camera:
                    imgProcedencia1 = true;
                    validarPermiso();
                    break;
                //case R.id.ll_input_foto:
                 //   imgProcedencia1 = true;
                 //   validarPermiso();
                 //   break;
               // case R.id.ll_fotos:
                //    imgProcedencia1 = true;
                 //   validarPermiso();
                 //   break;
                case R.id.btn_add_f1:
                    imgProcedencia1 = true;
                    validarPermiso();
                    break;
                case R.id.btn_add_f2:
                    imgProcedencia2 = true;
                    validarPermiso();
                    break;
                case R.id.rl_delete_f1:
                    imgProcedencia1 = true;
                    deleteFoto();
                    break;
                case R.id.rl_delete_f2:
                    imgProcedencia2 = true;
                    deleteFoto();
                    break;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ObtenerVehiculo(){
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        System.out.println("1: "+prefs.getInt(Constants.primary_id,0));
        Call<ResponseVehiculoBody> callVerificarServicioActivo = conductorService.getSeleccionarVehiculo(prefs.getInt(Constants.primary_id,0));
        callVerificarServicioActivo.enqueue(new Callback<ResponseVehiculoBody>() {
            @Override
            public void onResponse(Call<ResponseVehiculoBody> call, Response<ResponseVehiculoBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("2");
                if (response.isSuccessful()) {
                    System.out.println("3");
                    if (response.body() != null) {
                        System.out.println("4");
                        if (response.body().isEstado()) {
                            System.out.println("6");
                            listarMarca();
                            System.out.println("Vehiculo");
                            etservicio.setEnabled(false);
                            btnEditar.setEnabled(true);
                            //btnGuardar.setEnabled(false);
                            idservmarca = response.body().getValor().getMarc_Id();
                            idservmodelo = response.body().getValor().getMode_Id();
                            ft1 = response.body().getValor().getVehi_Foto1();
                            ft2 = response.body().getValor().getVehi_Foto2();
                            pathImg1serv = response.body().getValor().getVehi_Foto1Base64();
                            pathImg2serv = response.body().getValor().getVehi_Foto1Base64();

                            System.out.println("mod" + idservmodelo);
                            etservicio.setText(response.body().getValor().getTser_Nombre());
                            spMarca.setHint(response.body().getValor().getMarc_Nombre());
                            spModelo.setHint(response.body().getValor().getMode_Nombre());
                            etPlaca.setText(response.body().getValor().getVehi_NroPlaca());
                            etAnio.setText(response.body().getValor().getVehi_AnioFab());

                            System.out.println("pathImg1serv: " + pathImg1serv);
                            System.out.println("ft1: " + ft1);
                            System.out.println("ft2: " + ft2);

                            if(!pathImg1serv.isEmpty()){
                                cont_tomarfoto.setVisibility(View.GONE);
                                cont_mostrarfoto.setVisibility(View.VISIBLE);
                                btnGuardar.setVisibility(View.VISIBLE);

                                Glide.with(img_f1actual)
                                        .load(ft1)
                                        .apply(RequestOptions.skipMemoryCacheOf(true))
                                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                                        .fitCenter()
                                        .into(img_f1actual);

                                Glide.with(img_f2actual)
                                        .load(ft2)
                                        .apply(RequestOptions.skipMemoryCacheOf(true))
                                        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                                        .fitCenter()
                                        .into(img_f2actual);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseVehiculoBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
            }
        });
    }
    // ---------------------------------------------------------- inicio camara --------------------------------

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Aqui 20mil");
                // selectLocationOnMap();
            }
        }
        switch (requestCode){
            case CAMARA:{
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if (PermisionChecker.isRaedStorageExternalEnable(context)){
                        //Intent i = new Intent(getApplicationContext(),CameraActivity.class);
                        //startActivity(i);
                        // finish();
                    }else{
                        ActivityCompat.requestPermissions(EditarVehiculoActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                }
                break;
            }
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //Intent i = new Intent(getApplicationContext(),CameraActivity.class);
                    //  startActivity(i);
                }
                break;
        }
    }

    //check for location permission
    public static boolean hasPermissionInManifest(Activity activity, int requestCode, String permissionName) {
        if (ContextCompat.checkSelfPermission(activity,
                permissionName)
                != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                    new String[]{permissionName},
                    requestCode);
        } else {
            return true;
        }
        return false;
    }

    private void deleteFoto() {
        if(imgProcedencia1){
            eliminarFotoDevice(pathImg1);
            pathImg1="";
            btnAddF1.setVisibility(View.VISIBLE);
            rlDeleteF1.setVisibility(View.GONE);
            imgF1.setVisibility(View.GONE);
            imgProcedencia1=false;
        }
        if(imgProcedencia2){
            pathImg2="";
            btnAddF2.setVisibility(View.VISIBLE);
            rlDeleteF2.setVisibility(View.GONE);
            imgF2.setVisibility(View.GONE);
            imgProcedencia2=false;
        }
        if(pathImg1.isEmpty() && pathImg2.isEmpty()){
            linearCameraPrincipal.setVisibility(View.VISIBLE);
            linearCuadroFotos.setVisibility(View.GONE);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("0");
        String path ;

        if (resultCode == InformacionDocumentosActivity.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), contentURI);
                    path= saveImage(bitmap);
                    System.out.println("path: " + path);

                    if(imgProcedencia1){
                        pathImg1=path;
                        cargarFoto(bitmap);
                    }else{
                        pathImg2=path;
                        cargarFoto(bitmap);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == REQUEST_TAKE_PHOTO) {
            /*Bitmap bitmap2 = (Bitmap) data.getExtras().get("data");
            int width = bitmap2.getWidth();
            int heightt = bitmap2.getHeight();
            System.out.println("tamaños: " + width + " - "+heightt);

            *//*widht = thumbnail.getWidth();
            height = thumbnail.getHeight();
            Bitmap resized;
            if(height<widht){
                resized = Bitmap.createScaledBitmap(thumbnail, 480, 320, true);
                resized = rotateImage(resized,0);
            }else {
                resized = Bitmap.createScaledBitmap(thumbnail, 320, 480, true);
            }*//*
            path= saveImage(bitmap2);
            System.out.println("path2: " + path);
            if(imgProcedencia1){
                pathImg1=path;
                cargarFoto(bitmap2);
            }else{
                pathImg2=path;
                cargarFoto(bitmap2);
            }*/

            try {
                imagen = getBitmapFromUri (photoURI);
            } catch (IOException e) {
                e.printStackTrace();
            }
            path = tmpRuta;
            if(imgProcedencia1){
                System.out.println("aqui1: " + path);
                pathImg1=path;
                cargarFoto(imagen);
            }else{
                System.out.println("aqui2: "+path);
                pathImg2=path;
                cargarFoto(imagen);
            }
        }


    }

    private void dispatchTakePictureIntent() {
        System.out.println("11");
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                System.out.println("12");
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                System.out.println("13");
                photoURI = FileProvider.getUriForFile(EditarVehiculoActivity.this, getApplicationContext().getPackageName() + ".provider", photoFile);
                //Uri photoURI = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                tmpRuta = photoFile.getPath();
                System.out.println("outputUri2: " + photoURI);
                System.out.println("tmpRuta2: " + photoFile.getPath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        System.out.println("14");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY);
        storageDir.mkdirs(); // make sure you call mkdirs() and not mkdir()
        File image = File.createTempFile(imageFileName,".jpg",storageDir);

        tmpRuta = image.getPath();
        currentPhotoPath = "file:" + image.getAbsolutePath();
        System.out.println("our file"+ image.toString());
        return image;
    }

    public Bitmap getBitmapFromUri ( Uri uri ) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver (). openFileDescriptor ( uri , "r" );
        FileDescriptor fileDescriptor = parcelFileDescriptor . getFileDescriptor ();
        Bitmap image = BitmapFactory. decodeFileDescriptor ( fileDescriptor );
        parcelFileDescriptor . close ();
        return image ;
    }

    private void eliminarFotoDevice(String path) {
        File file = new File(path);
        boolean deleted = file.delete();
        if(deleted){
            callBroadCast();
        }else{
            //Toast.makeText(this,"No se pudo eliminar",Toast.LENGTH_LONG).show();
        }
    }

    public void callBroadCast() {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(context, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    private void validarPermiso() {
        if (PermisionChecker.isCameraEnable(context)) {
            if (PermisionChecker.isWriteStorageExternalEnable(context)){
                if(PermisionChecker.isRaedStorageExternalEnable(context)){
                    System.out.println("Aqui 1");
                    showOptionAddImage();
                }else{
                    ActivityCompat.requestPermissions(EditarVehiculoActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    System.out.println("Aqui 2");
                }
            }else{
                ActivityCompat.requestPermissions(EditarVehiculoActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                System.out.println("Aqui 3");
            }
        } else {
            ActivityCompat.requestPermissions(EditarVehiculoActivity.this, new String[]{Manifest.permission.CAMERA}, CAMARA);
            System.out.println("Aqui 4");
            showOptionAddImage();
        }

    }

    private void showOptionAddImage() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(context);
        pictureDialog.setTitle( getString(R.string.title_opciones_imagen));
        String[] pictureDialogItems = {
                getString(R.string.texto_from_gallery),
                getString(R.string.texto_from_camera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        dispatchTakePictureIntent();
        //Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        //startActivityForResult(intent, CAMERA);
    }

    public String saveImage(Bitmap myBitmap) {
        String path="";
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 30, bytes);

        File wallpaperDirectory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + IMAGE_DIRECTORY);
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis() + ".jpg");
            Log.i("file:",f.getAbsolutePath());
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(context,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            path =  f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
            return path;
        }
        return path;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    private void cargarFoto(Bitmap bitmap) {
        linearCameraPrincipal.setVisibility(View.GONE);
        linearCuadroFotos.setVisibility(View.VISIBLE);
        if(imgProcedencia1){
            if(pathImg2.isEmpty()){
                btnAddF2.setVisibility(View.VISIBLE);
                rlDeleteF2.setVisibility(View.GONE);
            }
            imgF1.setVisibility(View.VISIBLE);
            btnAddF1.setVisibility(View.GONE);
            rlDeleteF1.setVisibility(View.VISIBLE);
            imgF1.setImageBitmap(bitmap);
            imgProcedencia1=false;
        }
        if(imgProcedencia2){
            if(pathImg1.isEmpty()){
                btnAddF1.setVisibility(View.VISIBLE);
                rlDeleteF1.setVisibility(View.GONE);
            }
            imgF2.setVisibility(View.VISIBLE);
            btnAddF2.setVisibility(View.GONE);
            rlDeleteF2.setVisibility(View.VISIBLE);
            imgF2.setImageBitmap(bitmap);
            imgProcedencia2=false;
        }

        if(!pathImg1.isEmpty() && !pathImg2.isEmpty()){
            btnGuardar.setEnabled(true);
            btnGuardar.setBackgroundResource(R.drawable.button_primary_2);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            //finish(); // close this activity and return to preview activity (if there is any)

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setTitle("¿Deseas cancelar el registro de datos del vehiculo? (se perderán los datos ingresados)")
                    .setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(EditarVehiculoActivity.this, InicioActivity.class);
                            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                        }

                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            builder.create();
            builder.show();

        }
        return super.onOptionsItemSelected(item);
    }

    public String CargarFoto (String path_foto){
        String foto_str="";
        String encodedString="";
        if(path_foto!=null ){
            try {
                File file = new File(path_foto);
                try (FileInputStream fis = new FileInputStream(file)) {
                    byte[] bytes = new byte[(int) file.length()];
                    fis.read(bytes);
                    encodedString = Base64.encodeToString(Constants.resizeImage(context,bytes,500,700), Base64.NO_WRAP);
                    //encodedString = Base64.encodeToString(Constants.resizeImage(context,bytes,2,2), Base64.NO_WRAP);
                } catch (IOException ex) {
                    System.out.println("Error al convertir la imagen. " + ex.getMessage() +  ex);
                }

                //foto_str = Base64.encodeToString(String.valueOf(bitmapFile),Base64.DEFAULT);
            }catch (Exception e){
                e.printStackTrace();
                // return  foto_str;
                return encodedString;
            }
            Log.e("foto",foto_str);
        }
        System.out.println("encodedString: " + encodedString);
        return encodedString;
    }

    public String CargarFotoServicio (String path_foto){
        String foto_str="";
        String encodedString="";
        if(path_foto!=null ){
            try {
                File file = new File(path_foto);
                try (FileInputStream fis = new FileInputStream(file)) {
                    byte[] bytes = new byte[(int) file.length()];
                    fis.read(bytes);
                    encodedString = Base64.encodeToString(Constants.resizeImage(context,bytes,150,150), Base64.DEFAULT);
                } catch (IOException ex) {
                    System.out.println("Error al convertir la imagen. " + ex.getMessage() +  ex);
                }

                //foto_str = Base64.encodeToString(String.valueOf(bitmapFile),Base64.DEFAULT);
            }catch (Exception e){
                e.printStackTrace();
                // return  foto_str;
                return encodedString;
            }
            Log.e("foto",foto_str);
        }
        System.out.println("ff" + encodedString);
        return encodedString;
    }
// ---------------------------------------------------------- fin camara  ----------------------------------

    private void listarMarca() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        Call<ResponseMarcaBody> callValidarCodigoVerificacion = conductorService.getListarMarca();
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseMarcaBody>() {
            @Override
            public void onResponse(Call<ResponseMarcaBody> call, Response<ResponseMarcaBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            responseMarcaBody = response.body();
                            List<String> listCategorias = new ArrayList<String>();

                            if (responseMarcaBody != null) {
                                for (int i = 0; i < responseMarcaBody.getValor().size(); i++) {
                                    Log.d("aca sub", responseMarcaBody.getValor().get(i).getMarc_Nombre() + responseMarcaBody.getValor().get(i).getMarc_Id());
                                    requestMarcaList.add(responseMarcaBody.getValor().get(i));
                                    listCategorias.add(responseMarcaBody.getValor().get(i).getMarc_Nombre());
                                }
                                String[] stringListCat = listCategorias.toArray(new String[0]);
                                ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(EditarVehiculoActivity.this, android.R.layout.simple_dropdown_item_1line, stringListCat);
                                spMarca.setAdapter(adapterCat);
                                spMarca.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        if(requestMarcaList.get(i).getMarc_Id() == 1){
                                            System.out.println("idmarca1" + idmarca);
                                            idmarca = requestMarcaList.get(i).getMarc_Id();
                                            tvMarca.setVisibility(View.VISIBLE);
                                            lyMarca.setVisibility(View.VISIBLE);
                                            tvModelo.setVisibility(View.VISIBLE);
                                            lyModelo.setVisibility(View.GONE);
                                            System.out.println("1" + requestMarcaList.get(i).getMarc_Id());
                                            spModelo.setText("");
                                            requestModeloList.clear();
                                            listarModelos(requestMarcaList.get(i).getMarc_Id());
                                        }else{
                                            System.out.println("idmarca2" + idmarca);
                                            idmarca = requestMarcaList.get(i).getMarc_Id();
                                            nombremarca = requestMarcaList.get(i).getMarc_Nombre();
                                            tvMarca.setVisibility(View.VISIBLE);
                                            lyMarca.setVisibility(View.GONE);
                                            tvModelo.setVisibility(View.VISIBLE);
                                            lyModelo.setVisibility(View.GONE);
                                            spModelo.setText("");
                                            requestModeloList.clear();
                                            listarModelos(requestMarcaList.get(i).getMarc_Id());
                                        }
                                    }
                                });
                            }

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }

            }
            @Override
            public void onFailure(Call<ResponseMarcaBody> call, Throwable e) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    private void listarModelos(int Id) {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        Call<ResponseModeloBody> callValidarCodigoVerificacion = conductorService.getListarModelos(Id);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseModeloBody>() {
            @Override
            public void onResponse(Call<ResponseModeloBody> call, Response<ResponseModeloBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            responseModeloBody = response.body();
                            List<String> listCategorias = new ArrayList<String>();
                            if (responseModeloBody != null) {
                                System.out.println("Crgando modelossssssssss");
                                for (int i = 0; i < responseModeloBody.getValor().size(); i++) {
                                    Log.d("aca sub", responseModeloBody.getValor().get(i).getMode_Nombre() + responseModeloBody.getValor().get(i).getMode_Id());
                                    requestModeloList.add(responseModeloBody.getValor().get(i));
                                    listCategorias.add(responseModeloBody.getValor().get(i).getMode_Nombre());
                                }
                                String[] stringListCat = listCategorias.toArray(new String[0]);
                                ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(EditarVehiculoActivity.this, android.R.layout.simple_dropdown_item_1line, stringListCat);
                                spModelo.setAdapter(adapterCat);
                                spModelo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        idmodelo = requestModeloList.get(i).getMode_Id();
                                        nombremodelo = requestModeloList.get(i).getMode_Nombre();
                                        if(idmodelo == 1){
                                            tvModelo.setVisibility(View.VISIBLE);
                                            lyModelo.setVisibility(View.VISIBLE);
                                            System.out.println("3 " + idmodelo + " "+nombremodelo);
                                        }else{
                                            System.out.println("idmodelo" + idmodelo);
                                            idmodelo = requestModeloList.get(i).getMode_Id();
                                            nombremodelo = requestModeloList.get(i).getMode_Nombre();
                                            tvModelo.setVisibility(View.VISIBLE);
                                            lyModelo.setVisibility(View.GONE);
                                            System.out.println("4 " + idmodelo +" "+nombremodelo);
                                        }
                                    }
                                });
                            }

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }

            }
            @Override
            public void onFailure(Call<ResponseModeloBody> call, Throwable e) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    private void ValidarCampos() {
        //if(idmarca == 0){
            //Toast.makeText(EditarVehiculoActivity.this, "Debeseleccionar la marca del vehiculo poder registrar", Toast.LENGTH_LONG).show();

        //}else if(idmodelo == 0){
           // Toast.makeText(EditarVehiculoActivity.this, "Debeseleccionar el modelo del vehiculo poder registrar", Toast.LENGTH_LONG).show();

        //} else
            if(etPlaca.getText().toString().isEmpty()){
            Toast.makeText(EditarVehiculoActivity.this, "Debe llenar el campo placa para poder actualizar", Toast.LENGTH_LONG).show();

        } else if(etAnio.getText().toString().isEmpty()){
            Toast.makeText(EditarVehiculoActivity.this, "Debe ingresar el año para poder actualizar", Toast.LENGTH_LONG).show();

        }else{
            enviarRegistro();
        }

    }

    public void enviarRegistro(){
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();

        if(idmodelo>0){
            modserv  = idmodelo;
        }else{
            modserv = idservmodelo;
        }

        if(idmarca>0){
            marcsrv  = idmarca;
        }else{
            marcsrv = idservmodelo;
        }

        if(pathImg1!=null){
            if(pathImg1!=null){
                if(pathImg1.trim()!=""){
                    System.out.println("pathImg1:" + pathImg1);
                    pathImg1ok = (compressImage(EditarVehiculoActivity.this,pathImg1));
                    pathImg1ok = (CargarFoto(pathImg1));
                    System.out.println("pathImg1ok1:" + pathImg1ok);
                }
            }

            if(pathImg2!=null) {
                if (pathImg2.trim() != "") {
                    System.out.println("pathImg2:" + pathImg2);
                    pathImg2ok = (compressImage(EditarVehiculoActivity.this,pathImg2));
                    pathImg2ok = (CargarFoto(pathImg2));
                    System.out.println("pathImg2ok1:" + pathImg2ok);
                }
            }
        }else{
            if(pathImg1serv!=null){
                pathImg1ok = pathImg1serv;
                System.out.println("pathImg1ok2:" + pathImg1ok);
            }

            if(pathImg2serv!=null){
                pathImg2ok = pathImg2serv;
                System.out.println("pathImg2ok2:" + pathImg2ok);
            }
        }



        if(etModelo.getText().toString().isEmpty()){
            mod = null;
        }else{
            mod = etModelo.getText().toString();
        }

        if(etMarca.getText().toString().toString().isEmpty()){
            marc = null;
        }else{
            marc = etMarca.getText().toString();
        }

        System.out.println("   ");
        System.out.println("pathImg1ok: " + pathImg1ok);
        System.out.println("   ");
        System.out.println("pathImg2ok: " + pathImg2ok);
        System.out.println("   ");

        RespondeActualizarVehiculo request2 = new RespondeActualizarVehiculo();
        ResponseActualizarCar request = new ResponseActualizarCar();

        request2.setMode_Id(modserv);
        request2.setCond_Id(prefs.getInt(Constants.primary_id, 0));
        request2.setVehi_AnioFab(Integer.parseInt(etAnio.getText().toString()));
        request2.setVehi_NroPlaca(etPlaca.getText().toString());
        request2.setVehi_Foto1(pathImg1ok);
        request2.setVehi_Foto2(pathImg2ok);
        request.setVehiculo(request2);
        request.setMarc_Nombre(mod);
        request.setMode_Nombre(marc);
        System.out.println("md: " + idmodelo +"mc: " + idmarca +"idmodelo: " + modserv + " primary_id: " + prefs.getInt(Constants.primary_id, 0) + " etAnio: "+etAnio.getText().toString() + " etPlaca: " + etPlaca.getText().toString() + " mod: " + mod + " marc: " + marc+ " Path1: "+pathImg1ok) ;
        Call<ResponseBody> callValidarCodigoVerificacion = conductorService.putActualizarDatosVehiculo(request);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    Log.d("aca sub", String.valueOf(response.body().getValor()));
                    //if (response.body() != null) {
                        if (response.body().isEstado()) {
                            int valor = (int)(double)response.body().getValor();
                            System.out.println("Valor:" + valor);
                            AlertDialog.Builder builder = new AlertDialog.Builder(EditarVehiculoActivity.this);
                            builder.setTitle(R.string.doc_titulo);
                            builder.setMessage("El vehículo se actualizó con éxito.");
                            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onBackPressed();
                                    finish();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    //} else {
                       // displayMessageSingle(getString(R.string.code_error));
                  //  }
                } else {
                    displayMessageSingle(response.body().getMensaje());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

}