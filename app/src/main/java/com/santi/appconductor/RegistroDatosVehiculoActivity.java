package com.santi.appconductor;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.google.android.material.textfield.TextInputLayout;
import com.santi.appconductor.R;
import com.santi.appconductor.adapters.DocumentAdapter;
import com.santi.appconductor.models.DriverInfo;
import com.santi.appconductor.models.RequestDocument;
import com.santi.appconductor.models.RequestMarca;
import com.santi.appconductor.models.RequestModelo;
import com.santi.appconductor.models.RespondeVehiculo;
import com.santi.appconductor.models.ResponseAdjuntoBody;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseCar;
import com.santi.appconductor.models.ResponseDocumentBody;
import com.santi.appconductor.models.ResponseMarcaBody;
import com.santi.appconductor.models.ResponseModeloBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;
import com.santi.appconductor.utils.PermisionChecker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.santi.appconductor.utils.Constants.CAMARA;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;

public class RegistroDatosVehiculoActivity extends BaseActivity {

    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Button btnSiguienteRegVeh;
    TextView tool_titulo;
    EditText etPlaca,etAnio,etMarca,etModelo;
    Context context;
    SharedPreferences prefs;
    LinearLayout lyMarca, lyModelo;
    TextInputLayout tvModelo,tvMarca;
    AutoCompleteTextView spMarca,spModelo;
    private ResponseMarcaBody responseMarcaBody;
    private List<RequestMarca> requestMarcaList = new ArrayList();
    private ResponseModeloBody responseModeloBody;
    private List<RequestModelo> requestModeloList = new ArrayList();
    int idmodelo ,idmarca;
    String nombremodelo,nombremarca,cond_id;
    int idd;
    private String mod,marc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_datos_vehiculo);
        tool_titulo = findViewById(R.id.tool_titulo);
        tool_titulo.setText(R.string.paso2);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        tool_titulo.setVisibility(View.VISIBLE);
        spMarca = findViewById(R.id.spMarca);
        spModelo = findViewById(R.id.spModelo);
        etPlaca = findViewById(R.id.etPlaca);
        etMarca = findViewById(R.id.etMarca);
        etModelo = findViewById(R.id.etModelo);
        tvModelo = findViewById(R.id.tvModelo);
        tvMarca = findViewById(R.id.tvMarca);
        etAnio = findViewById(R.id.etAnio);
        btnSiguienteRegVeh = findViewById(R.id.btnSiguienteRegVeh);
        context = RegistroDatosVehiculoActivity.this;
        lyMarca = findViewById(R.id.lyMarca);
        lyModelo = findViewById(R.id.lyModelo);

        if (getIntent().getStringExtra("cond_id") == null) {
            idd = prefs.getInt(Constants.primary_id, 0);
            System.out.println("idd1: "+ idd);
        } else {
            cond_id = getIntent().getStringExtra("cond_id");
            idd = Integer.parseInt(cond_id);
            System.out.println("idd2: "+ idd);
        }
        System.out.println("cond_id: " + idd + prefs.getInt(Constants.primary_id, 0));

        listarMarca();
        btnSiguienteRegVeh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ValidarCampos();
            }
        });
    }

    private void listarMarca() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        Call<ResponseMarcaBody> callValidarCodigoVerificacion = conductorService.getListarMarca();
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseMarcaBody>() {
            @Override
            public void onResponse(Call<ResponseMarcaBody> call, Response<ResponseMarcaBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            responseMarcaBody = response.body();
                            List<String> listCategorias = new ArrayList<String>();

                            if (responseMarcaBody != null) {
                                for (int i = 0; i < responseMarcaBody.getValor().size(); i++) {
                                    Log.d("aca sub", responseMarcaBody.getValor().get(i).getMarc_Nombre() + responseMarcaBody.getValor().get(i).getMarc_Id());
                                    requestMarcaList.add(responseMarcaBody.getValor().get(i));
                                    listCategorias.add(responseMarcaBody.getValor().get(i).getMarc_Nombre());
                                }
                                String[] stringListCat = listCategorias.toArray(new String[0]);
                                ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(RegistroDatosVehiculoActivity.this, android.R.layout.simple_dropdown_item_1line, stringListCat);
                                spMarca.setAdapter(adapterCat);
                                spMarca.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        if(requestMarcaList.get(i).getMarc_Id() == 1){
                                            idmarca = requestMarcaList.get(i).getMarc_Id();
                                            tvMarca.setVisibility(View.VISIBLE);
                                            lyMarca.setVisibility(View.VISIBLE);
                                            tvModelo.setVisibility(View.VISIBLE);
                                            lyModelo.setVisibility(View.GONE);
                                            System.out.println("1" + requestMarcaList.get(i).getMarc_Id());
                                            spModelo.setText("");
                                            requestModeloList.clear();
                                            listarModelos(requestMarcaList.get(i).getMarc_Id());
                                        }else{
                                            idmarca = requestMarcaList.get(i).getMarc_Id();
                                            nombremarca = requestMarcaList.get(i).getMarc_Nombre();
                                            tvMarca.setVisibility(View.VISIBLE);
                                            lyMarca.setVisibility(View.GONE);
                                            tvModelo.setVisibility(View.VISIBLE);
                                            lyModelo.setVisibility(View.GONE);
                                            spModelo.setText("");
                                            requestModeloList.clear();
                                            listarModelos(requestMarcaList.get(i).getMarc_Id());
                                        }
                                    }
                                });
                            }

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }

            }
            @Override
            public void onFailure(Call<ResponseMarcaBody> call, Throwable e) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    private void listarModelos(int Id) {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        Call<ResponseModeloBody> callValidarCodigoVerificacion = conductorService.getListarModelos(Id);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseModeloBody>() {
            @Override
            public void onResponse(Call<ResponseModeloBody> call, Response<ResponseModeloBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            responseModeloBody = response.body();
                            List<String> listCategorias = new ArrayList<String>();
                            if (responseModeloBody != null) {
                                System.out.println("Crgando modelossssssssss");
                                for (int i = 0; i < responseModeloBody.getValor().size(); i++) {
                                    Log.d("aca sub", responseModeloBody.getValor().get(i).getMode_Nombre() + responseModeloBody.getValor().get(i).getMode_Id());
                                    requestModeloList.add(responseModeloBody.getValor().get(i));
                                    listCategorias.add(responseModeloBody.getValor().get(i).getMode_Nombre());
                                }
                                String[] stringListCat = listCategorias.toArray(new String[0]);
                                ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(RegistroDatosVehiculoActivity.this, android.R.layout.simple_dropdown_item_1line, stringListCat);
                                spModelo.setAdapter(adapterCat);
                                spModelo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        idmodelo = requestModeloList.get(i).getMode_Id();
                                        nombremodelo = requestModeloList.get(i).getMode_Nombre();

                                            if(idmodelo == 1){
                                                tvModelo.setVisibility(View.VISIBLE);
                                                lyModelo.setVisibility(View.VISIBLE);
                                                etModelo.setText("");

                                                System.out.println("3 " + idmodelo + " "+nombremodelo);
                                            }else{
                                                idmodelo = requestModeloList.get(i).getMode_Id();
                                                nombremodelo = requestModeloList.get(i).getMode_Nombre();
                                                tvModelo.setVisibility(View.VISIBLE);
                                                lyModelo.setVisibility(View.GONE);
                                                System.out.println("4 " + idmodelo +" "+nombremodelo);
                                            }
                                    }
                                });
                            }

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }

            }
            @Override
            public void onFailure(Call<ResponseModeloBody> call, Throwable e) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    private void ValidarCampos() {
        //if(String.valueOf("null") || etMarca == null){
         //   Toast.makeText(RegistroDatosVehiculoActivity.this, "Debeseleccionar la marca del vehículo poder registrar", Toast.LENGTH_LONG).show();

       // }else if(idmodelo == 0 || etModelo == null){
       //     Toast.makeText(RegistroDatosVehiculoActivity.this, "Debe seleccionar el modelo del vehículo poder registrar", Toast.LENGTH_LONG).show();

       //} else
        if(etPlaca.getText().toString().isEmpty()){
            Toast.makeText(RegistroDatosVehiculoActivity.this, "Debe llenar el campo placa para poder registrar", Toast.LENGTH_LONG).show();

        } else if(etAnio.getText().toString().isEmpty()){
            Toast.makeText(RegistroDatosVehiculoActivity.this, "Debe ingresar el año para poder registrar", Toast.LENGTH_LONG).show();

        }else{
            enviarRegistro();
        }

    }

    public void enviarRegistro(){
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();

        if(etModelo.getText().toString().isEmpty()){
            mod = null;
        }else{
            mod = etModelo.getText().toString();
        }

        if(etMarca.getText().toString().toString().isEmpty()){
            marc = null;
        }else{
            marc = etMarca.getText().toString();
        }

        RespondeVehiculo request1 = new RespondeVehiculo();
        ResponseCar request2 = new ResponseCar();
        request1.setMarc_id(idmarca);
        request1.setMode_Id(idmodelo);
        request1.setCond_Id(idd);
        request1.setVehi_AnioFab(Integer.parseInt(etAnio.getText().toString()));
        request1.setVehi_NroPlaca(etPlaca.getText().toString());

        request2.setVehiculo(request1);
        request2.setMarc_Nombre(marc);
        request2.setMode_Nombre(mod);

        //Toast.makeText(RegistroDatosVehiculoActivity.this, "Mode_Id: " + idmodelo + " Cond_Id: "+idd + " Vehi_AnioFab: "+etAnio.getText().toString() + " Vehi_NroPlaca: " + etPlaca.getText().toString() + " Marc_Nombre: "+ etMarca.getText().toString() + " Mode_Nombre: " + etModelo.getText().toString(), Toast.LENGTH_SHORT).show();
        System.out.println("Marc_Id: " + idmarca + " Mode_Id: " + idmodelo + " Cond_Id: "+idd + " Vehi_AnioFab: "+etAnio.getText().toString() + " Vehi_NroPlaca: " + etPlaca.getText().toString() + " Marc_Nombre: "+ marc + " Mode_Nombre: " + mod);
        Call<ResponseBody> callValidarCodigoVerificacion = conductorService.postRegistrarDatosVehiculo(request2);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.i("mensaje","responseResumen: "+response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putInt(Constants.primary_id, idd);
                            editor.apply();

                            System.out.println("Valor:" + response.body().getValor());
                            Intent intent = new Intent(RegistroDatosVehiculoActivity.this, ListaDocumentosActivity.class);
                            intent.putExtra("cond_id ", prefs.getInt(Constants.primary_id, 0));
                            startActivity(intent);
                        } else {
                            System.out.println("mensaje: "+response.body().getMensaje());
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

}