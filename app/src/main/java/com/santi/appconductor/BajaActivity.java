package com.santi.appconductor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BajaActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Button btnAceptar;
    TextView tv2;
    String numero, prefijo, numeroayuda, correo;
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baja);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        tv2 = findViewById(R.id.tv2);
        btnAceptar = findViewById(R.id.btnAceptar);
        prefijo = prefs.getString(Constants.prefij,"");
        numero = prefs.getString(Constants.num_CEL,"");
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.apply();
                Intent intent = new Intent(BajaActivity.this, SplashScreenActivity.class);
                startActivity(intent);
                finish();
            }
        });
        consultarTelfAyuda();
    }

    private void consultarTelfAyuda() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        Call<ResponseBody> callServicio = conductorService.getSeleccionarTelefonoAyudaConductor();
        callServicio.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            numeroayuda = response.body().getValor().toString();
                            consultarCorAyuda();
                            //tv2.setText("Tu cuenta registrada en Santi con el número "+prefijo+" "+numero+" fue dada de baja. Para mayor información comunicarse con la central al "+numeroayuda+"o escribir a "+correo+".\n\n- Equipo Santi.");
                        } else {
                            Toast.makeText(BajaActivity.this, response.body().getMensaje(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(BajaActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(BajaActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Toast.makeText(BajaActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void consultarCorAyuda() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        Call<ResponseBody> callServicio = conductorService.getSeleccionarCorreoAyudaConductor();
        callServicio.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("MissingPermission")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            correo = response.body().getValor().toString();
                            tv2.setText("Tu cuenta registrada en Santi con el número "+prefijo+" "+numero+" fue dada de baja. Para mayor información comunicarse con la central al "+numeroayuda+" o escribir a "+correo+".\n\n- Equipo Santi.");
                        } else {
                            Toast.makeText(BajaActivity.this, response.body().getMensaje(), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(BajaActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(BajaActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Toast.makeText(BajaActivity.this, getString(R.string.call_help_error), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {}
}