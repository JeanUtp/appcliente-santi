package com.santi.appconductor;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.santi.appconductor.models.FirebaseMessage;
import com.santi.appconductor.models.ResponseDriver;
import com.santi.appconductor.models.ResponseDriverBody;
import com.santi.appconductor.models.ResponsePais;
import com.santi.appconductor.models.ResponsePaisBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;
import com.santi.appconductor.utils.PermisionChecker;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.santi.appconductor.MenuActivity.isLocationEnabled;
import static com.santi.appconductor.utils.Constants.CAMARA;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_CALL_PHONE;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_LOCATION;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;

public class SplashScreenActivity extends BaseActivity{
    private static final String TAG = "";
    private static int SPLASH_SCREEN = 1000;
    public static final int CAMARA = 1;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 132;
    public static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    public static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 98;
    public static final int MY_PERMISSIONS_REQUEST_USE_FULL_SCREEN_INTENT = 99;
    //public static final int MY_PERMISSIONS_REQUEST_ACCESS_BACKGROUND_LOCATION = 2;

    private SharedPreferences prefs;
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    String token_fb = "";
    String deviceId, num_telefono, cond_id, pais_Id, latitude, longitude, bestProvider, contryCode, pais_PrefijoTelefono, pais_code;
    public Criteria criteria;
    LocationManager locationManager;
    Location location;
    boolean pais_Activo;
    boolean askedforGPS = false;
    String lat, lon;
    int idcond;
    //boolean dialogShow = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        deviceId = getDeviceId(this);
        ConnectivityManager cm;
        NetworkInfo ni;
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        ni = cm.getActiveNetworkInfo();
        boolean tipoConexion1 = false;
        boolean tipoConexion2 = false;
        idcond = prefs.getInt(Constants.primary_id, 0);
        System.out.println("id: " + prefs.getInt(Constants.primary_id, 0));
        if (ni != null) {
            ConnectivityManager connManager1 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager1.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connManager2 = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mMobile = connManager2.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (mWifi.isConnected()) {
                tipoConexion1 = true;
            }
            if (mMobile.isConnected()) {
                tipoConexion2 = true;
            }

            if (tipoConexion1 == true || tipoConexion2 == true) {
                //Toast.makeText(SplashScreenActivity.this, "Conecctado a Internet", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            LayoutInflater inflater = getLayoutInflater();
            View dialoglayout = inflater.inflate(R.layout.layout_internet, null);
            AlertDialog.Builder ad = new AlertDialog.Builder(this);

            ad.setView(dialoglayout);
            ad.setCancelable(false);
            final AlertDialog alert = ad.show();

            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

            Button btnHabilitar = dialoglayout.findViewById(R.id.btnHabilitar);
            btnHabilitar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent configGPS = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(configGPS);
                    alert.dismiss();
                    //turnGPSOn();
                }
            });
        }
        validarPermiso();
    }

    public void displayGPS() {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_gps, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnHabilitar = dialoglayout.findViewById(R.id.btnHabilitar);
        btnHabilitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent configGPS = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(configGPS);
                alert.dismiss();
                //turnGPSOn();
            }
        });
    }

    @SuppressLint({"HardwareIds", "NewApi"})
    public static String getDeviceId(Context context) {
        String deviceId;
        deviceId = Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return deviceId;
    }

    public void cargarDatosConductor(final String tokenFb) {
        System.out.println("cargarDatosConductor");
        System.out.println("ID: " +prefs.getInt(Constants.primary_id, 0) + "Pais: "+prefs.getInt(Constants.pais_id, 0));
        Call<ResponseDriverBody> callSeleccionarCliente = conductorService.getSeleccionarConductor(prefs.getInt(Constants.pais_id, 0), prefs.getInt(Constants.primary_id, 0));
        callSeleccionarCliente.enqueue(new Callback<ResponseDriverBody>() {
            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onResponse(Call<ResponseDriverBody> call, Response<ResponseDriverBody> response) {
                System.out.println("3.");
                Log.i("mensaje", "response: " + response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            System.out.println("estado." + response.body().isEstado());
                            ResponseDriver conductorResponse = response.body().getValor();
                            num_telefono = String.valueOf(response.body().getValor().getCond_NroTelefono());
                            cond_id = String.valueOf(response.body().getValor().getCond_Id());
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString(Constants.token, tokenFb);
                            editor.putInt(Constants.primary_id, response.body().getValor().getCond_Id());
                            editor.apply();
                            if (conductorResponse.getCond_IdDispositivo() != null && conductorResponse.getCond_IdDispositivo().equals(deviceId)) {
                                System.out.println("Econ_Id: " + response.body().getValor().getEcon_Id());
                                System.out.println("Etap_Id: " + response.body().getValor().getEtap_Id());
                                switch (response.body().getValor().getEcon_Id()) {
                                    //Postulante
                                    case 1:
                                        SharedPreferences.Editor editor1 = prefs.edit();
                                        editor1.putString(Constants.estado_con, "1");
                                        editor1.apply();
                                        switch (response.body().getValor().getEtap_Id()) {
                                            //Conductor nuevo
                                            case 0:
                                                Intent intent1 = new Intent(SplashScreenActivity.this, RegistroDatosConductorActivity.class);
                                                intent1.putExtra("num_telefono", num_telefono);
                                                intent1.putExtra("pais_Id", pais_Id);
                                                startActivity(intent1);
                                                finish();
                                                break;
                                            //Datos Personales completos
                                            case 1:
                                                Intent intent2 = new Intent(SplashScreenActivity.this, RegistroDatosVehiculoActivity.class);
                                                intent2.putExtra("num_telefono", num_telefono);
                                                intent2.putExtra("pais_Id", pais_Id);
                                                intent2.putExtra("cond_id", cond_id);
                                                System.out.println("cond_id" + cond_id);
                                                startActivity(intent2);
                                                finish();
                                                break;
                                            //Datos Vehiculo completos
                                            case 2:
                                                Intent intent3 = new Intent(SplashScreenActivity.this, ListaDocumentosActivity.class);
                                                intent3.putExtra("num_telefono", num_telefono);
                                                intent3.putExtra("pais_Id", pais_Id);
                                                intent3.putExtra("cond_id", cond_id);
                                                System.out.println("cond_id" + cond_id);
                                                startActivity(intent3);
                                                finish();
                                                break;
                                            //Datos Adjuntos completos
                                            case 3:
                                                Intent intent = new Intent(SplashScreenActivity.this, EsperaActivity.class);
                                                startActivity(intent);
                                                finish();
                                                break;
                                        }
                                        break;
                                    //Observado
                                    case 2:
                                        Intent intent2 = new Intent(SplashScreenActivity.this, ListaDocumentosActivity.class);
                                        intent2.putExtra("token_fb", tokenFb);
                                        intent2.putExtra("cond_id ", response.body().getValor().getCond_Id());
                                        SharedPreferences.Editor editor2 = prefs.edit();
                                        editor2.putString(Constants.estado_con, "2");
                                        editor2.apply();
                                        startActivity(intent2);
                                        finish();
                                        break;
                                    //Activo
                                    case 3:
                                        Intent intent3 = new Intent(SplashScreenActivity.this, MenuActivity.class);
                                        intent3.putExtra("token_fb", tokenFb);
                                        SharedPreferences.Editor editor3 = prefs.edit();
                                        editor3.putString(Constants.estado_con, "3");
                                        editor3.apply();
                                        startActivity(intent3);
                                        finish();
                                        break;
                                    //Rechazado
                                    case 4:
                                        Intent intent4 = new Intent(SplashScreenActivity.this, RechazadoActivity.class);
                                        SharedPreferences.Editor editor4 = prefs.edit();
                                        editor4.putString(Constants.estado_con, "4");
                                        editor4.apply();
                                        startActivity(intent4);
                                        finish();
                                        break;
                                    //Subsanado
                                    case 5:
                                        Intent intent5 = new Intent(SplashScreenActivity.this, EsperaActivity.class);
                                        SharedPreferences.Editor editor5 = prefs.edit();
                                        editor5.putString(Constants.estado_con, "5");
                                        editor5.apply();
                                        startActivity(intent5);
                                        finish();
                                        break;
                                    //Baja
                                    case 6:
                                        Intent intent6 = new Intent(SplashScreenActivity.this, BajaActivity.class);
                                        SharedPreferences.Editor editor6 = prefs.edit();
                                        editor6.putString(Constants.estado_con, "6");
                                        editor6.apply();
                                        startActivity(intent6);
                                        finish();
                                        break;
                                    //Deuda
                                    case 7:
                                        Intent intent7 = new Intent(SplashScreenActivity.this, MenuActivity.class);
                                        SharedPreferences.Editor editor7 = prefs.edit();
                                        editor7.putString(Constants.estado_con, "7");
                                        editor7.apply();
                                        startActivity(intent7);
                                        finish();
                                        break;
                                }
                            } else { //clean up
                                System.out.println("error 1");
                                cleanUp(tokenFb);
                            }
                        } else {
                            System.out.println("error 2");
                            cleanUp(tokenFb);
                        }

                    } else {
                        System.out.println("error 3");
                        cleanUp(tokenFb);
                    }
                } else {
                    System.out.println("error 4");
                    cleanUp(tokenFb);
                }
            }

            @Override
            public void onFailure(Call<ResponseDriverBody> call, Throwable t) {
                Log.i("mensaje", "onFailure: " + t);
                Log.d("device ID check", "response failure");
                System.out.println("error 5");
                //cleanUp(tokenFb);
            }
        });
    }

    @SuppressLint("NewApi")
    private void cleanUp(String tokenFb) {
        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
        Intent intent = new Intent(SplashScreenActivity.this, RegistroNumeroActivity.class);
        intent.putExtra("token_fb", tokenFb);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //savedInstanceState.putBoolean("dialogShow", dialogShow);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //dialogShow = savedInstanceState.getBoolean("dialogShow");
    }

    private void validarPermiso() {
        if(PermisionChecker.isFullScreenIntentEnable(getApplicationContext()))
        {
            if (PermisionChecker.isCameraEnable(getApplicationContext())) {
                if (PermisionChecker.isWriteStorageExternalEnable(getApplicationContext())) {
                    if (PermisionChecker.isRaedStorageExternalEnable(getApplicationContext())) {

                        /*if(PermisionChecker.isFineAccessBackgroundLocationAvaliable(getApplicationContext()))
                        {
                            dialogShow = false;
                        }
                        else
                        {
                            if(!dialogShow)
                            {
                                Log.d("validarPermiso", "displayMessagePermissionAccessBackground");
                                displayMessagePermissionAccessBackground(getString(R.string.msgPermissionBackground), MY_PERMISSIONS_REQUEST_ACCESS_BACKGROUND_LOCATION);
                                dialogShow = true;
                            }
                        }*/
                    } else {
                        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        System.out.println("Aqui 2");
                    }
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                    System.out.println("Aqui 3");
                }
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMARA);
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                System.out.println("Aqui 4");
            }
        }
        else
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.USE_FULL_SCREEN_INTENT}, MY_PERMISSIONS_REQUEST_USE_FULL_SCREEN_INTENT);
            System.out.println("Aqui 5");
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if(PermisionChecker.isFullScreenIntentEnable(getApplicationContext()))
        {

        }
        else
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.USE_FULL_SCREEN_INTENT}, MY_PERMISSIONS_REQUEST_USE_FULL_SCREEN_INTENT);
        }
        if (PermisionChecker.isFineLocationAvaliable(getApplicationContext())) {
            if (PermisionChecker.isWriteStorageExternalEnable(SplashScreenActivity.this))
            {
                if (PermisionChecker.isRaedStorageExternalEnable(SplashScreenActivity.this)) {

                    //if(PermisionChecker.isFineAccessBackgroundLocationAvaliable(SplashScreenActivity.this))
                    //{
                        //dialogShow = false;
                        new Handler().postDelayed(new Runnable() {
                            @SuppressLint("MissingPermission")
                            @Override
                            public void run() {
                                if (!isLocationEnabled(getApplicationContext())) {
                                    if (!askedforGPS) {
                                        askedforGPS = true;
                                        displayGPS();
                                    }
                                } else {
                                    FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(SplashScreenActivity.this);
                                    mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                                        @Override
                                        public void onSuccess(Location location) {
                                            if (location != null) {
                                                try {
                                                    latitude = String.valueOf(location.getLatitude());
                                                    longitude = String.valueOf(location.getLongitude());
                                                    System.out.println("Longitud:" + location.getLongitude() + "Latitud:" + location.getLatitude());
                                                    //Toast.makeText(SplashScreenActivity.this, "GPS" + "latitude1:" + latitude + " longitude1:" + longitude, Toast.LENGTH_SHORT).show();

                                                    Geocoder geocoder;
                                                    List<Address> addresses;
                                                    geocoder = new Geocoder(SplashScreenActivity.this, Locale.getDefault());
                                                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                                                    contryCode = addresses.get(0).getCountryCode();
                                                    System.out.println("CountryCode: " + contryCode);
                                                    idcond = prefs.getInt(Constants.primary_id, 0);
                                                    System.out.println("id1: " + prefs.getInt(Constants.primary_id, 0));
                                                    //Validar pais
                                                    Call<ResponsePaisBody> callValidarNumeroCelular = conductorService.getValidaPais(contryCode);
                                                    callValidarNumeroCelular.enqueue(new Callback<ResponsePaisBody>() {
                                                        @Override
                                                        public void onResponse(Call<ResponsePaisBody> call, Response<ResponsePaisBody> response) {
                                                            if (response.isSuccessful()) {
                                                                if (response.body() != null) {
                                                                    if (response.body().isEstado() == true) {
                                                                        ResponsePais paisResponse = response.body().getValor();
                                                                        pais_Id = String.valueOf(paisResponse.getPais_Id());
                                                                        SharedPreferences.Editor editor = prefs.edit();
                                                                        editor.putInt(Constants.pais_id, paisResponse.getPais_Id());
                                                                        editor.putString(Constants.prefij, paisResponse.getPais_PrefijoTelefono());
                                                                        editor.putString(Constants.icpais, paisResponse.getPais_Icono());
                                                                        editor.putInt(Constants.caracteres, paisResponse.getPais_CaracterTelefono());
                                                                        editor.putString(Constants.monesym, paisResponse.getMone_Simbolo());
                                                                        editor.apply();

                                                                        pais_code = paisResponse.getPais_CountryCode();
                                                                        pais_Activo = paisResponse.isPais_Activo();
                                                                        pais_PrefijoTelefono = paisResponse.getPais_PrefijoTelefono();
                                                                        deviceId = getDeviceId(SplashScreenActivity.this);
                                                                        //guardar variables de la respuesta de la validacion de pais
                                                                        System.out.println("pais_Id: " + pais_Id + " - " + "pais_Activo: " + pais_Activo + " - " + "pais_PrefijoTelefono: " + pais_PrefijoTelefono + " " + prefs.getString(Constants.prefij, "") + " - " + deviceId+ " "+response.body().getValor().getMone_Simbolo());
                                                                        if (pais_Activo == true) {
                                                                            FirebaseMessaging.getInstance().setAutoInitEnabled(true);
                                                                            FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<String> task) {
                                                                                    if (!task.isSuccessful()) {
                                                                                        Log.w("Firebase", "Fetching FCM registration token failed", task.getException());
                                                                                        return;
                                                                                    }
                                                                                    // Get new FCM registration token
                                                                                    token_fb = task.getResult();
                                                                                    Log.d("Firebase", token_fb);
                                                                                    new Handler().postDelayed(new Runnable() {
                                                                                        @Override
                                                                                        public void run() {
                                                                                            //*getApplicationContext().sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
                                                                                            getApplicationContext().sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));//*
                                                                                            Log.d("firebase", "heartbeat llamado");
                                                                                            System.out.println("idconductor: " + prefs.getInt(Constants.primary_id, 0));
                                                                                            idcond = prefs.getInt(Constants.primary_id, 0);
                                                                                            System.out.println("id: " + prefs.getInt(Constants.primary_id, 0));
                                                                                            if (idcond > 0) {
                                                                                                cargarDatosConductor(token_fb);
                                                                                                System.out.println("1");
                                                                                            } else {
                                                                                                System.out.println("2");
                                                                                                Intent intent = new Intent(SplashScreenActivity.this, InicioActivity.class);
                                                                                                intent.putExtra("token_fb", token_fb);
                                                                                                intent.putExtra("pais_Id", pais_Id);
                                                                                                intent.putExtra("pais_Prefijo", pais_PrefijoTelefono);
                                                                                                intent.putExtra("pais_code", pais_code);
                                                                                                System.out.println("token_fb:" + token_fb + " ID_CEL: "+ deviceId);
                                                                                                SharedPreferences.Editor editor = prefs.edit();
                                                                                                editor.putString(Constants.token, token_fb);
                                                                                                editor.putString(Constants.ID_CEL, deviceId);
                                                                                                editor.apply();
                                                                                                startActivity(intent);
                                                                                                finish();
                                                                                            }

                                                                                        }
                                                                                    }, Constants.splash_timeout);
                                                                                }
                                                                            });

                                                                        } else {
                                                                            Intent intent = new Intent(SplashScreenActivity.this, PaisNoDisponibleActivity.class);
                                                                            startActivity(intent);
                                                                            finish();
                                                                        }
                                                                    } else {
                                                                        displayMessageSingle(response.body().getMensaje());
                                                                        System.out.println("Pais no Disponible");
                                                                        Intent intent = new Intent(SplashScreenActivity.this, PaisNoDisponibleActivity.class);
                                                                        startActivity(intent);
                                                                        finish();
                                                                    }
                                                                } else {
                                                                    displayMessageSingle(getString(R.string.error));
                                                                }
                                                            } else {
                                                                displayMessageSingle(getString(R.string.error));
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<ResponsePaisBody> call, Throwable t) {
                                                            if (pDialog != null && pDialog.isShowing()) {
                                                                pDialog.dismiss();
                                                            }
                                                            displayMessageSingle(getString(R.string.error));
                                                        }
                                                    });
                                                } catch (Exception e) {
                                                    Toast.makeText(SplashScreenActivity.this, "No se puede encontrar la ubicación", Toast.LENGTH_SHORT).show();
                                                    e.printStackTrace();
                                                }
                                            } else {
                                                try {
                                                    location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                                                    latitude = String.valueOf(location.getLatitude());
                                                    longitude = String.valueOf(location.getLongitude());
                                                    System.out.println("latitude1:" + latitude + " longitude1:" + longitude);
                                                    //Toast.makeText(SplashScreenActivity.this, "Network" + "latitude1:" + latitude + " longitude1:" + longitude, Toast.LENGTH_SHORT).show();
                                                    Geocoder geocoder;
                                                    List<Address> addresses;
                                                    geocoder = new Geocoder(SplashScreenActivity.this, Locale.getDefault());
                                                    addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                                                    contryCode = addresses.get(0).getCountryCode();
                                                    System.out.println("CountryCode: " + contryCode);
                                                    idcond = prefs.getInt(Constants.primary_id, 0);
                                                    deviceId = getDeviceId(SplashScreenActivity.this);
                                                    //Validar pais
                                                    Call<ResponsePaisBody> callValidarNumeroCelular = conductorService.getValidaPais(contryCode);
                                                    callValidarNumeroCelular.enqueue(new Callback<ResponsePaisBody>() {
                                                        @Override
                                                        public void onResponse(Call<ResponsePaisBody> call, Response<ResponsePaisBody> response) {
                                                            if (response.isSuccessful()) {
                                                                if (response.body() != null) {
                                                                    if (response.body().isEstado() == true) {
                                                                        ResponsePais paisResponse = response.body().getValor();
                                                                        pais_Id = String.valueOf(paisResponse.getPais_Id());
                                                                        SharedPreferences.Editor editor = prefs.edit();
                                                                        editor.putInt(Constants.pais_id, paisResponse.getPais_Id());
                                                                        editor.putString(Constants.prefij, paisResponse.getPais_PrefijoTelefono());
                                                                        editor.apply();
                                                                        pais_code = paisResponse.getPais_CountryCode();
                                                                        pais_Activo = paisResponse.isPais_Activo();
                                                                        pais_PrefijoTelefono = paisResponse.getPais_PrefijoTelefono();
                                                                        //guardar variables de la respuesta de la validacion de pais
                                                                        System.out.println("pais_Id: " + pais_Id + " - " + "pais_Activo: " + pais_Activo + " - " + "pais_PrefijoTelefono: " + pais_PrefijoTelefono + " - " + deviceId);
                                                                        if (pais_Activo == true) {
                                                                            FirebaseMessaging.getInstance().setAutoInitEnabled(true);
                                                                            FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                                                                                @Override
                                                                                public void onComplete(@NonNull Task<String> task) {
                                                                                    if (!task.isSuccessful()) {
                                                                                        Log.w("Firebase", "Fetching FCM registration token failed", task.getException());
                                                                                        return;
                                                                                    }
                                                                                    // Get new FCM registration token
                                                                                    token_fb = task.getResult();
                                                                                    Log.d("Firebase", token_fb);
                                                                                    new Handler().postDelayed(new Runnable() {
                                                                                        @Override
                                                                                        public void run() {
                                                                                            //*getApplicationContext().sendBroadcast(new Intent("com.google.android.intent.action.GTALK_HEARTBEAT"));
                                                                                            getApplicationContext().sendBroadcast(new Intent("com.google.android.intent.action.MCS_HEARTBEAT"));//*
                                                                                            Log.d("firebase", "heartbeat llamado");
                                                                                            System.out.println("idconductor: " + prefs.getInt(Constants.primary_id, 0));
                                                                                            idcond = prefs.getInt(Constants.primary_id, 0);
                                                                                            if (idcond > 0) {
                                                                                                System.out.println("1.1");
                                                                                                cargarDatosConductor(token_fb);
                                                                                            } else {
                                                                                                System.out.println("token_fb:" + token_fb + " ID_CEL: "+ deviceId);
                                                                                                System.out.println("2.2");
                                                                                                Intent intent = new Intent(SplashScreenActivity.this, InicioActivity.class);
                                                                                                intent.putExtra("token_fb", token_fb);
                                                                                                intent.putExtra("pais_Id", pais_Id);
                                                                                                intent.putExtra("pais_Prefijo", pais_PrefijoTelefono);
                                                                                                intent.putExtra("pais_code", pais_code);
                                                                                                SharedPreferences.Editor editor = prefs.edit();
                                                                                                editor.putString(Constants.token, token_fb);
                                                                                                editor.putString(Constants.ID_CEL, deviceId);
                                                                                                editor.apply();
                                                                                                startActivity(intent);
                                                                                                finish();
                                                                                            }

                                                                                        }
                                                                                    }, Constants.splash_timeout);
                                                                                }
                                                                            });


                                                                        } else {
                                                                            Intent intent = new Intent(SplashScreenActivity.this, PaisNoDisponibleActivity.class);
                                                                            startActivity(intent);
                                                                            finish();
                                                                        }
                                                                    } else {
                                                                        displayMessageSingle(response.body().getMensaje());
                                                                        System.out.println("Pais no Disponible");
                                                                        Intent intent = new Intent(SplashScreenActivity.this, PaisNoDisponibleActivity.class);
                                                                        startActivity(intent);
                                                                        finish();
                                                                    }
                                                                } else {
                                                                    displayMessageSingle(getString(R.string.error));
                                                                }
                                                            } else {
                                                                displayMessageSingle(getString(R.string.error));
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<ResponsePaisBody> call, Throwable t) {
                                                            if (pDialog != null && pDialog.isShowing()) {
                                                                pDialog.dismiss();
                                                            }
                                                            displayMessageSingle(getString(R.string.error));
                                                        }
                                                    });
                                                } catch (Exception e) {
                                                    Toast.makeText(SplashScreenActivity.this, "No se puede encontrar la ubicación", Toast.LENGTH_SHORT).show();
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Toast.makeText(SplashScreenActivity.this, "No se puede encontrar la ubicación", Toast.LENGTH_SHORT).show();
                                                    e.printStackTrace();
                                                }
                                            });
                                    //locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                                    //criteria = new Criteria();
                                    // bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true)).toString();
                                    //if (ActivityCompat.checkSelfPermission(SplashScreenActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SplashScreenActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    //    return;
                                    // }
                                    //location = locationManager.getLastKnownLocation(bestProvider);
                                    //location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                    //location = locationManager.getCurrentLocation();
                                    //Toast.makeText(SplashScreenActivity.this, "Location:" + location, Toast.LENGTH_SHORT).show();
                                    //Toast.makeText(SplashScreenActivity.this, "Longitud:" + location.getLongitude() + "Latitud: " + location.getLatitude(), Toast.LENGTH_SHORT).show();
                                    //System.out.println("Longitud:" + location.getLongitude() + "Latitud:" + location.getLatitude());
                                    //validacion de pais
                                }

                            }
                        }, SPLASH_SCREEN);
                        //startActivity(new Intent(SplashActivity.this,MainActivity.class));
                    //}

                    /*else
                    {
                        if(!dialogShow)
                        {
                            Log.d("onPostResume", "displayMessagePermissionAccessBackground");
                            displayMessagePermissionAccessBackground(getString(R.string.msgPermissionBackground), MY_PERMISSIONS_REQUEST_ACCESS_BACKGROUND_LOCATION);
                            dialogShow = true;

                        }
                    }*/

                } else {
                    ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
            }
            else
            {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

            }

        } else {
            ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //    getTokenGCM();
        if (PermisionChecker.isFineLocationAvaliable(getApplicationContext())) {

            if (PermisionChecker.isWriteStorageExternalEnable(SplashScreenActivity.this))
            {
                if (PermisionChecker.isRaedStorageExternalEnable(SplashScreenActivity.this)) {

                } else {
                    ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                }
            }
            else
            {
                ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

            }


        } else {
            ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }



        if(PermisionChecker.isFullScreenIntentEnable(getApplicationContext()))
        {

        }
        else
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.USE_FULL_SCREEN_INTENT}, MY_PERMISSIONS_REQUEST_USE_FULL_SCREEN_INTENT);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            /*case MY_PERMISSIONS_REQUEST_ACCESS_BACKGROUND_LOCATION:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    dialogShow = false;
                }
                else
                {
                    if(!dialogShow)
                    {
                        Log.d("onRequestPermissionsResult", "displayMessagePermissionAccessBackground");
                        displayMessagePermissionAccessBackground(getString(R.string.msgPermissionBackground), MY_PERMISSIONS_REQUEST_ACCESS_BACKGROUND_LOCATION);
                        dialogShow = true;
                    }
                }
            }*/

            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (PermisionChecker.isCallPhoneAvaliable(getApplicationContext())) {
                        //startActivity(new Intent(InicialActivity.this,opcionesDenunciaslActivity.class));
                    } else {
                        ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.CALL_PHONE}, MY_PERMISSIONS_REQUEST_CALL_PHONE);
                    }
                    if (PermisionChecker.isRaedStorageExternalEnable(getApplicationContext())) {
                        //startActivity(new Intent(InicialActivity.this,opcionesDenunciaslActivity.class));
                    } else {
                        ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                }
                break;
            }

            case MY_PERMISSIONS_REQUEST_USE_FULL_SCREEN_INTENT:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    if (PermisionChecker.isFullScreenIntentEnable(getApplicationContext()))
                    {

                    }
                    else {
                        ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.USE_FULL_SCREEN_INTENT}, MY_PERMISSIONS_REQUEST_USE_FULL_SCREEN_INTENT);
                    }
                }
                break;
            }

            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    if (PermisionChecker.isFullScreenIntentEnable(getApplicationContext()))
                    {

                    }
                    else {
                        ActivityCompat.requestPermissions(SplashScreenActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                    }
                }
                break;
            }

            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //startActivity(new Intent(InicialActivity.this,opcionesDenunciaslActivity.class));
                }
                break;
        }
    }

}