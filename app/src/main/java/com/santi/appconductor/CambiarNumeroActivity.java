package com.santi.appconductor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseDriver;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CambiarNumeroActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Bundle bundle;
    SharedPreferences prefs;
    ResponseDriver conductorResponse;
    EditText etCelular;
    Button btnSiguiente;
    TextView tvTelefono, tvPrefij;
    String pais_Id,tel;
    ImageView iv_Close;
    int caracteres;
    TextView tool_titulo_atras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_numero);

        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Edita tu número de celular");
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        caracteres = prefs.getInt(Constants.caracteres,0);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            conductorResponse = (ResponseDriver) bundle.getSerializable("conductor");
        }
        iv_Close = findViewById(R.id.iv_Close);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        tvPrefij = findViewById(R.id.tvPrefij);
        tvPrefij.setText(prefs.getString(Constants.prefij,""));
        etCelular = findViewById(R.id.etCelular);
        etCelular.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        tvTelefono = findViewById(R.id.tvTelefono);
        tel = getIntent().getStringExtra("tel");
        tvTelefono.setText(tel);
        btnSiguiente = findViewById(R.id.btnSiguiente);
        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyb();
                if (etCelular.getText().toString().length() == caracteres) {
                    validarNumero();
                    //Intent intent = new Intent(RegistroNumeroActivity.this, RegistroCodigoVerificacionActivity.class);
                    //startActivity(intent);
                } else {
                    displayMessageSingle(getString(R.string.phone_required));
                }
            }
        });
    }

    public void validarNumero() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        pais_Id = String.valueOf(prefs.getInt(Constants.pais_id,0));
        Log.i("validarNumero pais_Id", pais_Id);
        Call<ResponseBody> callValidarNumeroCelular = conductorService.getValidaTelefonoConductor(etCelular.getText().toString(), pais_Id);
        callValidarNumeroCelular.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();

                }
                Log.i("validarNumero http", new Gson().toJson(response.body()));
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i("validarNumero", response.body().isEstado()+"");
                        if (response.body().isEstado()) {
                            System.out.println("phone_number: " + etCelular.getText().toString() + " - " + "code: " + response.body().getValor().toString() + "token_fb: " );
                            Intent intent = new Intent(CambiarNumeroActivity.this, CambiarNumeroCodigoActivity.class);
                            intent.putExtra("code", response.body().getValor().toString());
                            intent.putExtra("pais_Id", pais_Id);

                            intent.putExtra("num_telefono", etCelular.getText().toString());
                            intent.putExtra("phone_number", etCelular.getText().toString());
                            startActivity(intent);
                            finish();
                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.phone_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.phone_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.phone_error));
            }
        });
    }

}