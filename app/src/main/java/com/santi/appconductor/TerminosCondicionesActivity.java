package com.santi.appconductor;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.santi.appconductor.models.ResponseBodyTerminosCondicionesGenerales;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TerminosCondicionesActivity extends AppCompatActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Button btnAccept;
    TextView tvTerms;
    WebView wvTerms;
    ImageView ivBack;
    TextView tool_titulo_atras;
    SharedPreferences prefs;
    int tipo = 0;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminos_condiciones);
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Términos y condiciones");
        bundle = getIntent().getExtras();
        tipo = bundle.getInt("tipo");
        prefs = getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        ivBack = findViewById(R.id.iv_Atras);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        tvTerms = findViewById(R.id.tvTerms);
        wvTerms = findViewById(R.id.wvTerms);
        btnAccept = findViewById(R.id.btnAccept);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (tipo){
                    case 1:
                        SharedPreferences.Editor editor0 = prefs.edit();
                        editor0.putBoolean(Constants.ACEPTA_TERMINOS_CONDICIONES_GENERALES, true);
                        editor0.apply();
                        break;
                    case 2:
                        SharedPreferences.Editor editor2 = prefs.edit();
                        editor2.putBoolean(Constants.ACEPTA_TERMINOS_CONDICIONES, true);
                        editor2.apply();
                        break;
                    case 3:
                        break;
                }
                onBackPressed();
            }
        });

    }

    public void cargarTerminosCondiciones() {
                Call<ResponseBodyTerminosCondicionesGenerales> callTerminosCondicionesGenerales = conductorService.SeleccionarTerminosYCondicionesGenerales();
                callTerminosCondicionesGenerales.enqueue(new Callback<ResponseBodyTerminosCondicionesGenerales>() {
                    @Override
                    public void onResponse(Call<ResponseBodyTerminosCondicionesGenerales> call, Response<ResponseBodyTerminosCondicionesGenerales> response) {
                        if (response.isSuccessful()&&response.body() != null) {
                            if (response.body().isEstado()) {
                                tvTerms.setText(Html.fromHtml(response.body().getValor()));
                                tvTerms.setVisibility(View.GONE);
                                wvTerms.loadData(response.body().getValor(), "text/html", "UTF-8");
                                wvTerms.setVisibility(View.VISIBLE);
                            } else {
                                Toast.makeText(TerminosCondicionesActivity.this,response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(TerminosCondicionesActivity.this,getString(R.string.terminos_error), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBodyTerminosCondicionesGenerales> call, Throwable t) {
                        Toast.makeText(TerminosCondicionesActivity.this,getString(R.string.terminos_error), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        cargarTerminosCondiciones();
    }
}