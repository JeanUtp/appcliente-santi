package com.santi.appconductor;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.santi.appconductor.adapters.ChatMessageAdapter;
import com.santi.appconductor.models.ActiveService;
import com.santi.appconductor.models.ChatMessage;
import com.santi.appconductor.models.FirebaseMessage;
import com.santi.appconductor.models.RequestAddChat;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;
import com.santi.appconductor.utils.Utilities;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    ImageView iv_atras;
    TextView tvTitulo;
    TextView tool_titulo_atras;
    CircleImageView ivDriver;
    EditText etMensaje;
    ImageButton ibEnviar;
    String cliente;
    boolean busy;

    int serv_id;
    List<ChatMessage> mensajes_chat = new ArrayList<>();
    List<ActiveService> serviciosActivos = new ArrayList<>();

    AlertDialog adDisplayService;

    RecyclerView rvMensajes;
    ChatMessageAdapter adapter;

    Utilities util = new Utilities();

    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Chat");
        iv_atras = findViewById(R.id.iv_Atras);
        iv_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tvTitulo = findViewById(R.id.tvTitulo);
        /*ivDriver = findViewById(R.id.ivDriver);
        if (serviciosActivos.get(0).getFotoCliente()!=null){
            Glide.with(this)
                    .load(serviciosActivos.get(0).getFotoCliente())
                    .apply(RequestOptions.skipMemoryCacheOf(true))
                    .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                    .into(ivDriver);
        }*/

        Type listType1 = new TypeToken<ArrayList<ActiveService>>(){}.getType();

        Gson gson = new Gson();
        String serviciosActivosString = getIntent().getStringExtra("serviciosActivos");
        serviciosActivos = gson.fromJson(serviciosActivosString, listType1);

        serv_id = getIntent().getIntExtra("serv_id", -1);
        cliente = getIntent().getStringExtra("cliente");
        busy = getIntent().getBooleanExtra("busy", false);
        tvTitulo.setText(String.format(getString(R.string.chat_title_expression), serviciosActivos.get(0).getCliente()));

        etMensaje = findViewById(R.id.etMensaje);
        ibEnviar = findViewById(R.id.ibEnviar);

        ibEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyb();
                if (!etMensaje.getText().toString().isEmpty()) {
                    sendMesage(etMensaje.getText().toString());
                }
            }
        });


        String mensajesChatString = getIntent().getStringExtra("mensajesChat");
        //String mensajesChatString = prefs.getString(Constants.CHAT_MENSAJES, "");
        //if (!mensajesChatString.isEmpty()) {
            Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
            mensajes_chat = gson.fromJson(mensajesChatString, listType);
            System.out.println("1 " + mensajesChatString);
        //}

        rvMensajes =findViewById(R.id.rvMensajes);
        adapter = new ChatMessageAdapter(mensajes_chat);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        rvMensajes.setLayoutManager(mLayoutManager);
        rvMensajes.addItemDecoration(new GridSpacingItemDecoration(1, util.dpToPx(this, 0), true));
        rvMensajes.setItemAnimator(new DefaultItemAnimator());
        rvMensajes.setAdapter(adapter);

        DisplayProgressDialog(getString(R.string.updating));
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkMessages();
    }

    public static class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private final int spanCount;
        private final int spacing;
        private final boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private void sendMesage(String mensaje) {
        insertarChat(mensaje);
    }

    public void addMessage(ChatMessage mc) {
        mensajes_chat.add(mc);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        Gson gson = new Gson();
        String mensajesChatString = gson.toJson(mensajes_chat);
        intent.putExtra("mensajesChat", mensajesChatString);
        System.out.println("2 " + mensajesChatString);
        setResult(RESULT_OK, intent);
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver2), new IntentFilter("MyData"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver2);
    }

    private final BroadcastReceiver mMessageReceiver2 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkMessages();
        }
    };

    private void checkMessages() {
        if (adDisplayService == null || !adDisplayService.isShowing()) {
            List<FirebaseMessage> pending_messages;
            String pendingmessages = prefs.getString("pending_messages", "");
            if (!pendingmessages.isEmpty() && pendingmessages.length() > 2) {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<FirebaseMessage>>(){}.getType();
                pending_messages = gson.fromJson(pendingmessages, listType);
                FirebaseMessage fbMessage = pending_messages.get(0);
                pending_messages.remove(0);

                pendingmessages = gson.toJson(pending_messages);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("pending_messages", pendingmessages);
                editor.apply();

                processMessage(fbMessage);
            }
        }
    }

    private void processMessage(FirebaseMessage mensaje) {
        try {
            final JSONObject obj = new JSONObject(mensaje.getMessage());
            if (obj.has("Tipo")) {
                switch (obj.getInt("Tipo")) {
                    case 1: {
                        /*if (!busy) {*/
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"),obj.getString("Moneda"),  new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(RESULT_FIRST_USER, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        /*}*/
                        break;
                    }
                    case 3: {
                        if (obj.getInt("Eser_Id") == 6 && !serviciosActivos.isEmpty() && obj.getInt("Serv_Id") == serviciosActivos.get(0).getServ_id()) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                        break;
                    }
                    case 4: {
                        ChatMessage msg = new ChatMessage(obj.getString("Chat_Emisor"), obj.getString("Chat_Mensaje"), obj.getString("Chat_Fecha"));
                        mensajes_chat.add(msg);
                        adapter.notifyDataSetChanged();
                        rvMensajes.smoothScrollToPosition(mensajes_chat.size() - 1);
                        checkMessages();
                        break;
                    }
                    case 5: {
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayScheduledService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(2, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        break;
                    }
                    case 6: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_canceled_by_customer), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("NombreCompleto")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        break;
                    }
                    case 7: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_reminder), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("Cliente")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        break;
                    }
                    case 9: {
                        /*if (!busy) {*/
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayServiceExtended(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Moneda"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(RESULT_FIRST_USER, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        /*}*/
                        break;
                    }
                    default: {
                        checkMessages();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            checkMessages();
            e.printStackTrace();
        }
    }

    private void insertarChat(final String mensaje) {
        DisplayProgressDialog(getString(R.string.sending));
        pDialog.show();

        RequestAddChat request = new RequestAddChat();
        request.setServ_Id(serv_id);
        request.setChat_Mensaje(mensaje);

        Call<ResponseBody> callInsertarChat = conductorService.postInsertarChat(request);
        callInsertarChat.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            ChatMessage mc = new ChatMessage();
                            mc.setChat_Emisor("C");
                            mc.setChat_Mensaje(mensaje);
                            addMessage(mc);
                            adapter.notifyDataSetChanged();
                            rvMensajes.smoothScrollToPosition(mensajes_chat.size() - 1);
                            etMensaje.setText("");

                            Gson gson = new Gson();

                            String mensajes_recibidos = gson.toJson(mensajes_chat);

                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString(Constants.CHAT_MENSAJES, mensajes_recibidos);
                            editor.apply();

                        } else {
                            adDisplayService = displayMessage(response.body().getMensaje(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            //wired
                            /*MensajeChat mc = new MensajeChat();
                            mc.setChat_Emisor("C");
                            mc.setChat_Mensaje(mensaje);
                            addMessage(mc);
                            adapter.notifyDataSetChanged();
                            etMensaje.setText("");*/
                        }
                    } else {
                        adDisplayService = displayMessage(getString(R.string.send_chat_error), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                    }
                } else {
                    adDisplayService = displayMessage(getString(R.string.send_chat_error), new Runnable() {
                        @Override
                        public void run() {
                            checkMessages();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                adDisplayService = displayMessage(getString(R.string.send_chat_error), new Runnable() {
                    @Override
                    public void run() {
                        checkMessages();
                    }
                });
            }
        });
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            ChatMessage msj = (ChatMessage) b.getSerializable("mensaje");
            mensajes_chat.add(msj);
            adapter.notifyDataSetChanged();
        }
    }
}

