package com.santi.appconductor;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.santi.appconductor.models.ActiveService;
import com.santi.appconductor.models.Bancos;
import com.santi.appconductor.models.ChatMessage;
import com.santi.appconductor.models.CuentaBancaria;
import com.santi.appconductor.models.FirebaseMessage;
import com.santi.appconductor.models.RequestAbono;
import com.santi.appconductor.models.RequestCuentaBancaria;
import com.santi.appconductor.models.RequestMarca;
import com.santi.appconductor.models.ResponseBancosBody;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseCuentaBancariaBody;
import com.santi.appconductor.models.ResponseDriver;
import com.santi.appconductor.models.ResponseMarcaBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CuentaBancariaActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Bundle bundle;
    SharedPreferences prefs;
    String cliente,nombrebanco;
    ResponseDriver conductorResponse;
    ImageView iv_Atras;
    List<ChatMessage> mensajes_chat = new ArrayList<>();
    List<ActiveService> serviciosActivos = new ArrayList<>();
    boolean chatAvailable, available, busy;
    int serv_id = 1,idbanco=0;
    int idact, idact1;
    AlertDialog adDisplayService;
    private ResponseBancosBody responseBancosBody;
    private List<Bancos> requestBancosLista = new ArrayList();
    AutoCompleteTextView spBanco;
    EditText etCuenta, etNombreCompleto, etDocumentoIdentidad;
    Button btnGuardar,btnActualizar;
    TextView tool_titulo_atras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta_bancaria);

        iv_Atras = findViewById(R.id.iv_Atras);
        iv_Atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Cuenta bancaria");
        spBanco = findViewById(R.id.spBanco);
        etCuenta = findViewById(R.id.etCuenta);
        //etCuentaInterbancaria = findViewById(R.id.etCuentaInterbancaria);
        etNombreCompleto = findViewById(R.id.etNombreCompleto);
        etDocumentoIdentidad = findViewById(R.id.etDocumentoIdentidad);
        btnGuardar = findViewById(R.id.btnGuardar);
        btnActualizar = findViewById(R.id.btnActualizar);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        bundle = getIntent().getExtras();
        conductorResponse = (ResponseDriver)getIntent().getSerializableExtra("conductor");
        Type listType1 = new TypeToken<ArrayList<ActiveService>>(){}.getType();
        Gson gson = new Gson();
        String serviciosActivosString = getIntent().getStringExtra("serviciosActivos");
        serviciosActivos = gson.fromJson(serviciosActivosString, listType1);
        cliente = getIntent().getStringExtra("cliente");
        chatAvailable = getIntent().getBooleanExtra("chatAvailable", false);
        available = getIntent().getBooleanExtra("available", false);
        busy = getIntent().getBooleanExtra("busy", false);
        serv_id = getIntent().getIntExtra("serv_id", -1);
        //System.out.println("Nombere: " + conductorResponse.getCond_Nombres());
        etNombreCompleto.setText(conductorResponse.getCond_Nombres() + " " + conductorResponse.getCond_ApellidoPaterno() + " " + conductorResponse.getCond_ApellidoMaterno());
        etDocumentoIdentidad.setText(conductorResponse.getCond_NroDocumento());
        ObtenerCuentaBancaria();
        listarBancos();
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                eviarCuenta();
            }
        });
        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CuentaBancariaActivity.this);
                builder.setTitle("Actualizar Cuenta Bancaria");
                builder.setMessage("Revisa cuidadosamente que tus datos sean correctos.");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        actualizarCuenta();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        if (chatAvailable) {
            Type listType = new TypeToken<ArrayList<ChatMessage>>(){}.getType();
            String mensajesChatString = getIntent().getStringExtra("mensajesChat");
            mensajes_chat = gson.fromJson(mensajesChatString, listType);
        }

    }

    private void ObtenerCuentaBancaria() {
        //DisplayProgressDialog(getString(R.string.loading));
        //pDialog.show();
        Call<ResponseCuentaBancariaBody> callValidarCodigoVerificacion = conductorService.getCuentaBancaria(prefs.getInt(Constants.primary_id, 0));
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseCuentaBancariaBody>() {
            @Override
            public void onResponse(Call<ResponseCuentaBancariaBody> call, Response<ResponseCuentaBancariaBody> response) {
                //if (pDialog != null && pDialog.isShowing()) {
                    //pDialog.dismiss();
                //}
                if (response.isSuccessful()) {
                    if(response.body().getValor() == null){
                        spBanco.setHint("Escoja su banco");
                        btnActualizar.setVisibility(View.GONE);
                        btnGuardar.setVisibility(View.VISIBLE);
                    }else {
                        btnActualizar.setVisibility(View.VISIBLE);
                        btnGuardar.setVisibility(View.GONE);
                        spBanco.setHint(response.body().getValor().getBanc_Nombre());
                        //spBanco.setText(response.body().getValor().getBanc_Nombre());
                        etCuenta.setText(response.body().getValor().getCban_Numero());
                        //etCuentaInterbancaria.setText(response.body().getValor().getCban_CCI());
                        idact1 = response.body().getValor().getBanc_Id();
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }

            }
            @Override
            public void onFailure(Call<ResponseCuentaBancariaBody> call, Throwable e) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    private void listarBancos() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        Call<ResponseBancosBody> callValidarCodigoVerificacion = conductorService.getListarBancos(prefs.getInt(Constants.pais_id,0));
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBancosBody>() {
            @Override
            public void onResponse(Call<ResponseBancosBody> call, Response<ResponseBancosBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }

                if (response.isSuccessful()) {
                    Log.i("listarBancos", response.body().getMensaje());
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            responseBancosBody = response.body();
                            List<String> listCategorias = new ArrayList<String>();
                            if (responseBancosBody != null) {
                                for (int i = 0; i < responseBancosBody.getValor().size(); i++) {
                                    Log.d("aca sub", responseBancosBody.getValor().get(i).getBanc_Nombre() + responseBancosBody.getValor().get(i).getBanc_Id());
                                    requestBancosLista.add(responseBancosBody.getValor().get(i));
                                    listCategorias.add(responseBancosBody.getValor().get(i).getBanc_Nombre());
                                }


                                String[] stringListCat = listCategorias.toArray(new String[0]);
                                ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(CuentaBancariaActivity.this, android.R.layout.simple_dropdown_item_1line, stringListCat);
                                spBanco.setAdapter(adapterCat);
                                spBanco.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        if (requestBancosLista.get(i).getBanc_Id() != 0 || requestBancosLista.get(i).getBanc_Nombre() != null) {
                                            idbanco = requestBancosLista.get(i).getBanc_Id();
                                            nombrebanco = requestBancosLista.get(i).getBanc_Nombre();
                                        } else {
                                            idbanco = 0;
                                            nombrebanco = null;
                                        }

                                    }
                                });
                            }

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    Log.i("listarBancos", new Gson().toJson(response));
                    displayMessageSingle(getString(R.string.code_error));
                }

            }
            @Override
            public void onFailure(Call<ResponseBancosBody> call, Throwable e) {

                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    public void eviarCuenta() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        RequestCuentaBancaria requestCuentaBancaria = new RequestCuentaBancaria();
        requestCuentaBancaria.setCond_Id(prefs.getInt(Constants.primary_id, 0));
        requestCuentaBancaria.setBanc_Id(idbanco);
        requestCuentaBancaria.setCban_Numero(etCuenta.getText().toString());
        requestCuentaBancaria.setCban_CCI("");
        requestCuentaBancaria.setCban_Nombre(etNombreCompleto.getText().toString());
        requestCuentaBancaria.setCban_NroDocumento(etDocumentoIdentidad.getText().toString());
        System.out.println("idbanco: " + idbanco + " etCuenta: " + etCuenta.getText().toString());
        Call<ResponseBody> callValidarCodigoVerificacion = conductorService.postRegistrCuenta(requestCuentaBancaria);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            //retorna atras
                            AlertDialog.Builder builder = new AlertDialog.Builder(CuentaBancariaActivity.this);
                            builder.setTitle("Registro Exitoso");
                            builder.setMessage("La cuenta bancaria se registró con éxito.");
                            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //onBackPressed();
                                    Intent intent = new Intent(CuentaBancariaActivity.this, MenuActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    public void actualizarCuenta() {
        if(idbanco == 0){
            idact =  idact1;
        }else{
            idact = idbanco;
        }
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        RequestCuentaBancaria requestCuentaBancaria = new RequestCuentaBancaria();
        requestCuentaBancaria.setCond_Id(prefs.getInt(Constants.primary_id, 0));
        requestCuentaBancaria.setBanc_Id(idact);
        requestCuentaBancaria.setCban_Numero(etCuenta.getText().toString());
        requestCuentaBancaria.setCban_CCI(" ");
        requestCuentaBancaria.setCban_Nombre(etNombreCompleto.getText().toString());
        requestCuentaBancaria.setCban_NroDocumento(etDocumentoIdentidad.getText().toString());
        System.out.println("primary_id: " + prefs.getInt(Constants.primary_id, 0) + " idbanco: " + idbanco + " etCuenta: " + etCuenta.getText().toString() + " etNombreCompleto: " + etNombreCompleto.getText().toString() + " etDocumentoIdentidad: " + etDocumentoIdentidad.getText().toString());
        Call<ResponseBody> callValidarCodigoVerificacion = conductorService.postRegistrCuenta(requestCuentaBancaria);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            //retorna atras
                            AlertDialog.Builder builder = new AlertDialog.Builder(CuentaBancariaActivity.this);
                            builder.setTitle("Registro Exitoso");
                            builder.setMessage("La cuenta bancaria se actualizó con éxito.");
                            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //onBackPressed();
                                    Intent intent = new Intent(CuentaBancariaActivity.this, MenuActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkMessages();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        if (!mensajes_chat.isEmpty()) {
            Gson gson = new Gson();
            String mensajesChatString = gson.toJson(mensajes_chat);
            intent.putExtra("mensajesChat", mensajesChatString);
            setResult(RESULT_OK, intent);
        } else {
            setResult(RESULT_OK);
        }
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (available) {
            LocalBroadcastManager.getInstance(this).registerReceiver((mMessageReceiver3), new IntentFilter("MyData"));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (available) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver3);
        }
    }

    private final BroadcastReceiver mMessageReceiver3 = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkMessages();
        }
    };

    private void checkMessages() {
        if (adDisplayService == null || !adDisplayService.isShowing()) {
            List<FirebaseMessage> pending_messages;
            String pendingmessages = prefs.getString("pending_messages", "");
            if (!pendingmessages.isEmpty() && pendingmessages.length() > 2) {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<FirebaseMessage>>(){}.getType();
                pending_messages = gson.fromJson(pendingmessages, listType);
                FirebaseMessage fbMessage = pending_messages.get(0);
                pending_messages.remove(0);

                pendingmessages = gson.toJson(pending_messages);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("pending_messages", pendingmessages);
                editor.apply();

                processMessage(fbMessage);
            }
        }
    }

    private void processMessage(FirebaseMessage mensaje) {
        try {
            final JSONObject obj = new JSONObject(mensaje.getMessage());
            if (obj.has("Tipo")) {
                switch (obj.getInt("Tipo")) {
                    case 1: {
                        /*if (!busy) {*/
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Moneda"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(RESULT_FIRST_USER, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        /*}*/
                        break;
                    }
                    case 3: {
                        if (obj.getInt("Eser_Id") == 6 && !serviciosActivos.isEmpty() && obj.getInt("Serv_Id") == serviciosActivos.get(0).getServ_id()) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                        break;
                    }
                    case 4: {
                        adDisplayService = displayNotificationChat(String.format(getString(R.string.new_message_expression), serviciosActivos.get(0).getCliente().split(" ")[0], obj.getString("Chat_Mensaje")), new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        ChatMessage msg = new ChatMessage(obj.getString("Chat_Emisor"), obj.getString("Chat_Mensaje"), obj.getString("Chat_Fecha"));
                        mensajes_chat.add(msg);
                        break;
                    }
                    case 5: {
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayScheduledService(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(2, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        break;
                    }
                    case 6: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_canceled_by_customer), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("NombreCompleto")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        break;
                    }
                    case 7: {
                        adDisplayService = displayNotification(String.format(getString(R.string.scheduled_service_reminder), obj.getString("Soli_ServicioFecha"), obj.getString("Soli_ServicioHora"), obj.getString("Cliente")) , new Runnable() {
                            @Override
                            public void run() {
                                checkMessages();
                            }
                        });
                        break;
                    }
                    case 9: {
                        /*if (!busy) {*/
                        if (compareDates(Calendar.getInstance().getTime(), mensaje.getReceivedOn()) >= -20) {
                            adDisplayService = displayServiceExtended(String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeRecojo"))), String.format(getString(R.string.time_expression), String.valueOf(obj.getInt("TiempoDeViaje"))), obj.getString("Cliente"), obj.getString("Origen"), obj.getString("Destino"), obj.getDouble("Precio"),obj.getString("Moneda"), new Runnable() {
                                @Override
                                public void run() {
                                    Intent i = new Intent();
                                    try {
                                        i.putExtra("Soli_Id", obj.getInt("Soli_Id"));
                                        i.putExtra("Cliente", obj.getString("Cliente"));
                                        i.putExtra("Origen", obj.getString("Origen"));
                                        i.putExtra("Destino", obj.getString("Destino"));
                                        setResult(RESULT_FIRST_USER, i);
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }, new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                            expireService(adDisplayService, (mensaje.getReceivedOn().getTime()) - Calendar.getInstance().getTimeInMillis(), new Runnable() {
                                @Override
                                public void run() {
                                    checkMessages();
                                }
                            });
                        } else {
                            checkMessages();
                        }
                        /*}*/
                        break;
                    }
                    default: {
                        checkMessages();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            checkMessages();
            e.printStackTrace();
        }
    }
}