package com.santi.appconductor;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.santi.appconductor.models.RequestUpdateCorreo;
import com.santi.appconductor.models.RequestUpdateFotPerfil;
import com.santi.appconductor.models.RequestUpdateNumber;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseDriver;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CambiarEmailActivity extends BaseActivity {

    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    EditText edtcorreo;
    TextView tvCorreo;
    Button btnSiguiente;
    Bundle bundle;
    ImageView iv_Close;
    SharedPreferences prefs;
    ResponseDriver conductorResponse;
    String cor;
    TextView tool_titulo_atras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_email);

        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Edita tu correo electrónico");
        edtcorreo = findViewById(R.id.edtcorreo);
        tvCorreo = findViewById(R.id.tvCorreo);
        btnSiguiente = findViewById(R.id.btnSiguiente);
        cor = getIntent().getStringExtra("cor");
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            conductorResponse = (ResponseDriver) bundle.getSerializable("conductor");
        }
        iv_Close = findViewById(R.id.iv_Close);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        tvCorreo.setText(cor);
        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ValidarCampos();
            }
        });

    }


    private void ValidarCampos() {
        if(edtcorreo.getText().toString().isEmpty()){
            Toast.makeText(CambiarEmailActivity.this, "Debe llenar su correo para poder continuar", Toast.LENGTH_LONG).show();

        }else{
            final String compruebaemail = edtcorreo.getEditableText().toString().trim();
            final String regex = "(?:[^<>()\\[\\].,;:\\s@\"]+(?:\\.[^<>()\\[\\].,;:\\s@\"]+)*|\"[^\\n\"]+\")@(?:[^<>()\\[\\].,;:\\s@\"]+\\.)+[^<>()\\[\\]\\.,;:\\s@\"]{2,63}";
            if (!compruebaemail.matches(regex)) {
                Toast.makeText(CambiarEmailActivity.this, "Por favor, introduce un correo valido", Toast.LENGTH_LONG).show();
            }else{
                actualizarCorreo();
            }
        }
    }

    private void actualizarCorreo() {
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        RequestUpdateCorreo requestUpdateCorreo = new RequestUpdateCorreo();
        requestUpdateCorreo.setCond_Id(prefs.getInt(Constants.primary_id,0));
        requestUpdateCorreo.setPais_Id(prefs.getInt(Constants.pais_id,0));
        requestUpdateCorreo.setCond_Correo(edtcorreo.getText().toString());

        Call<ResponseBody> callValidarCodigoVerificacion = conductorService.actualizarcorrreo(requestUpdateCorreo);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            //retorna atras
                            AlertDialog.Builder builder = new AlertDialog.Builder(CambiarEmailActivity.this);
                            builder.setTitle(R.string.doc_titulo);
                            builder.setMessage("El correo se actualizó con éxito.");
                            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onBackPressed();
                                    finish();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }


}