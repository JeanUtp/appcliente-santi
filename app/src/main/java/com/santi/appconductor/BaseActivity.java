package com.santi.appconductor;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.santi.appconductor.utils.Constants;
import com.santi.appconductor.utils.Utilities;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class BaseActivity extends AppCompatActivity {

    ProgressDialog pDialog;
    private SharedPreferences prefs;
    Utilities util = new Utilities();
    String clie, ori, desti;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        setupUI(getWindow().getDecorView().getRootView());
    }

    public void DisplayProgressDialog(String message) {

        pDialog = new ProgressDialog(this);
        pDialog.setMessage(message);
        pDialog.setCancelable(false);
        pDialog.setIndeterminate(false);
        //pDialog.show();
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @SuppressLint("ClickableViewAccessibility")
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyb();
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }

    public void hideKeyb() {
        try {
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null && getCurrentFocus() != null) {
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        } catch (Exception e) { //No debe pasar nunca
            e.printStackTrace();
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public AlertDialog displayService(String tiempoRecojo, String tiempoViaje, String cliente, String origen, String destino, double precio, String moneda, final Runnable runAccept, final Runnable runReject) {
        Log.d("prueba2", "holamundo");

        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_servicio, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        TextView tvTiempoRecojo = dialoglayout.findViewById(R.id.tvTiempoRecojo);
        TextView tvTiempoViaje = dialoglayout.findViewById(R.id.tvTiempoViaje);
        TextView tvCliente = dialoglayout.findViewById(R.id.tvCliente);
        TextView tvOrigen = dialoglayout.findViewById(R.id.tvOrigen);
        TextView tvDestino = dialoglayout.findViewById(R.id.tvDestino);
        TextView tvPrecio = dialoglayout.findViewById(R.id.tvPrecio);

        clie = cliente;
        ori = origen;
        desti = destino;

        tvTiempoRecojo.setText(tiempoRecojo);
        tvTiempoViaje.setText(tiempoViaje);
        tvCliente.setText(cliente);
        tvOrigen.setText(origen);
        tvDestino.setText(destino);
        tvPrecio.setText(moneda+" "+String.format(getString(R.string.amount_expression2), util.formatearNumero(precio)));

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnRechazar = dialoglayout.findViewById(R.id.btnRechazar);
        btnRechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                runReject.run();
            }
        });

        Button btnAceptar = dialoglayout.findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(Constants.client, clie);
                editor.putString(Constants.orig, ori);
                editor.putString(Constants.desti, desti);
                editor.apply();
                alert.dismiss();
                runAccept.run();
            }
        });

        playSound();

        return alert;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public AlertDialog displayServiceExtended(String tiempoRecojo, String tiempoViaje, String cliente, String origen, String destino, double precio, String moneda, final Runnable runAccept, final Runnable runReject) {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_servicio_extendido, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        TextView tvTiempoRecojo = dialoglayout.findViewById(R.id.tvTiempoRecojo);
        TextView tvTiempoViaje = dialoglayout.findViewById(R.id.tvTiempoViaje);
        TextView tvCliente = dialoglayout.findViewById(R.id.tvCliente);
        TextView tvOrigen = dialoglayout.findViewById(R.id.tvOrigen);
        TextView tvDestino = dialoglayout.findViewById(R.id.tvDestino);
        TextView tvPrecio = dialoglayout.findViewById(R.id.tvPrecio);

        tvTiempoRecojo.setText(tiempoRecojo);
        tvTiempoViaje.setText(tiempoViaje);
        tvCliente.setText(cliente);
        tvOrigen.setText(origen);
        tvDestino.setText(destino);
        tvPrecio.setText(moneda+" "+String.format(getString(R.string.amount_expression2), util.formatearNumero(precio)));

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnRechazar = dialoglayout.findViewById(R.id.btnRechazar);
        btnRechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                runReject.run();
            }
        });

        Button btnAceptar = dialoglayout.findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                runAccept.run();
            }
        });

        playSound();

        return alert;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public AlertDialog displayScheduledService(String tiempoViaje, String cliente, String origen, String destino, double precio, String fecha, String hora, final Runnable runAccept, final Runnable runReject) {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_scheduled_service, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        TextView tvFecha = dialoglayout.findViewById(R.id.tvFecha);
        TextView tvHora = dialoglayout.findViewById(R.id.tvHora);
        TextView tvTiempoViaje = dialoglayout.findViewById(R.id.tvTiempoViaje);
        TextView tvCliente = dialoglayout.findViewById(R.id.tvCliente);
        TextView tvOrigen = dialoglayout.findViewById(R.id.tvOrigen);
        TextView tvDestino = dialoglayout.findViewById(R.id.tvDestino);
        TextView tvPrecio = dialoglayout.findViewById(R.id.tvPrecio);

        tvFecha.setText(fecha);
        tvHora.setText(hora);
        tvTiempoViaje.setText(tiempoViaje);
        tvCliente.setText(cliente);
        tvOrigen.setText(origen);
        tvDestino.setText(destino);
        tvPrecio.setText(String.format(getString(R.string.amount_expression), util.formatearNumero(precio)));

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnRechazar = dialoglayout.findViewById(R.id.btnRechazar);
        btnRechazar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                runReject.run();
            }
        });

        Button btnAceptar = dialoglayout.findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                runAccept.run();
            }
        });

        playSound();

        return alert;
    }

    public void expireService(final AlertDialog dialog, long exp, final Runnable r) {
        SharedPreferences prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        long expDef = prefs.getLong("ExpiracionSolicitud",20000);
        new CountDownTimer(exp + expDef, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                    r.run();
                }
            }
        }.start();
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public AlertDialog displayMessage(String mensaje, final Runnable r) {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_display_message, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        TextView tvMensaje = dialoglayout.findViewById(R.id.tvMensaje);
        tvMensaje.setText(mensaje);

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnAceptar = dialoglayout.findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                r.run();
            }
        });

        return alert;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void displayMessagePermissionAccessBackground(String mensaje, int codPermission) {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_display_message, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        TextView tvMensaje = dialoglayout.findViewById(R.id.tvMensaje);
        tvMensaje.setText(mensaje);

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnAceptar = dialoglayout.findViewById(R.id.btnAceptar);
        btnAceptar.setText("Dar Permiso");
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(BaseActivity.this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, codPermission);
                alert.dismiss();
            }
        });
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void displayMessageSingle(String mensaje) {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_display_message, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        TextView tvMensaje = dialoglayout.findViewById(R.id.tvMensaje);
        tvMensaje.setText(mensaje);

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnAceptar = dialoglayout.findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public AlertDialog displayNotification(String mensaje, final Runnable r) {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_notificacion, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        TextView tvMensaje = dialoglayout.findViewById(R.id.tvMensaje);
        tvMensaje.setText(mensaje);

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnAceptar = dialoglayout.findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                r.run();
            }
        });

        return alert;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public AlertDialog displayNotificationChat(String mensaje, final Runnable r) {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_notificacion_mensaje, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        TextView tvMensaje = dialoglayout.findViewById(R.id.tvMensaje);
        tvMensaje.setText(mensaje);

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_servicio));

        Button btnAceptar = dialoglayout.findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                r.run();
            }
        });

        playSound();

        return alert;
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public AlertDialog displayQualification(String cliente, final Runnable cal, final Runnable sig, final Runnable omitir) {
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.layout_calificacion, null);
        AlertDialog.Builder ad = new AlertDialog.Builder(this);

        TextView tvTitulo = dialoglayout.findViewById(R.id.tvTitulo);
        tvTitulo.setText(String.format(getString(R.string.qualification_title), cliente));

        ad.setView(dialoglayout);
        ad.setCancelable(false);
        final AlertDialog alert = ad.show();

        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(getResources().getDrawable(R.drawable.background_calificacion));

        RatingBar ratingBar = dialoglayout.findViewById(R.id.ratingBar);
        ratingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                cal.run();
            }
        });

        Button button = dialoglayout.findViewById(R.id.btnAceptar);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                sig.run();
            }
        });

        TextView tvOmitir = dialoglayout.findViewById(R.id.tvOmitir);
        tvOmitir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
                omitir.run();
            }
        });

        return alert;
    }

    public long compareDates(Date d1, Date d2) {
        long diffInMs = d2.getTime() - d1.getTime();
        long diffInSecs = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
        Log.d("COMPARADO", String.valueOf(diffInSecs));
        return diffInSecs;
    }

    public void playSound() {
        try {
            Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.misc237);
            //Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), sound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}