package com.santi.appconductor;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.santi.appconductor.R;
import com.santi.appconductor.models.DriverInfo;
import com.santi.appconductor.models.RequestDocument;
import com.santi.appconductor.models.RequestDriverValidateCode;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseDocumentBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;
import com.santi.appconductor.utils.PermisionChecker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.santi.appconductor.utils.Constants.CAMARA;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;

public class RegistroDatosConductorActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    Button btnSiguienteRegCond;
    TextView tool_titulo,tvTerms;
    CheckBox cbTerms;
    EditText etNombre, etApellidoPaterno, etApellidoMaterno, etNumeroDocumento, etCorreo;
    private AutoCompleteTextView spTipoDocumento;
    private ResponseDocumentBody responseDocumentBody;
    private List<RequestDocument> requestDocumentList = new ArrayList();
    Context context;
    int iddocu;
    //variables imagenes
    String num_telefono, pais_Id, nombredocu,dv_id;
    SharedPreferences prefs;
    int id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_datos_conductor);
        tool_titulo = findViewById(R.id.tool_titulo);
        tool_titulo.setText(R.string.paso1);
        tool_titulo.setVisibility(View.VISIBLE);
        etNombre = findViewById(R.id.etNombre);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        dv_id = getDeviceId(this);
        num_telefono = getIntent().getStringExtra("num_telefono");
        pais_Id = getIntent().getStringExtra("pais_Id");
        etApellidoPaterno = findViewById(R.id.etApellidoPaterno);
        etApellidoMaterno = findViewById(R.id.etApellidoMaterno);
        etNumeroDocumento = findViewById(R.id.etNumeroDocumento);
        etCorreo = findViewById(R.id.etCorreo);
        cbTerms = findViewById(R.id.cbTerms);
        tvTerms = findViewById(R.id.tvTerms);
        spTipoDocumento = findViewById(R.id.spTipoDocumento);
        btnSiguienteRegCond = findViewById(R.id.btnSiguienteRegCond);
        context = RegistroDatosConductorActivity.this;
        obtenerTipoDocumento();
        cbTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean(Constants.ACEPTA_TERMINOS_CONDICIONES_GENERALES, isChecked);
                editor.apply();
            }
        });

        tvTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegistroDatosConductorActivity.this, TerminosCondicionesActivity.class);
                intent.putExtra("tipo",1);
                startActivity(intent);
                tvTerms.setEnabled(false);
            }
        });

        if (prefs.getInt(Constants.primary_id, 0) != 0) {
            id = prefs.getInt(Constants.primary_id, 0);
            System.out.println("id: " + id);
        } else {
            id = 0;
            System.out.println("id: " + id);
        }
        btnSiguienteRegCond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ValidarCampos();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (prefs.getInt(Constants.primary_id, 0) != 0) {
            id = prefs.getInt(Constants.primary_id, 0);
            System.out.println("idres: "+ id);
        } else {
            id = 0;
            System.out.println("idres: "+ id);
        }
    }

    private void obtenerTipoDocumento() {
        Call<ResponseDocumentBody> callValidarCodigoVerificacion = conductorService.getListarDocumento();
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseDocumentBody>() {
            @Override
            public void onResponse(Call<ResponseDocumentBody> call, Response<ResponseDocumentBody> response) {

                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            responseDocumentBody = response.body();
                            List<String> listCategorias = new ArrayList<String>();
                            if (responseDocumentBody != null) {
                                for (int i = 0; i < responseDocumentBody.getValor().size(); i++) {
                                    Log.d("aca sub", responseDocumentBody.getValor().get(i).getTdoc_Nombre() + responseDocumentBody.getValor().get(i).getTdoc_Id());
                                    requestDocumentList.add(responseDocumentBody.getValor().get(i));
                                    listCategorias.add(responseDocumentBody.getValor().get(i).getTdoc_Nombre());
                                }
                                String[] stringListCat = listCategorias.toArray(new String[0]);
                                ArrayAdapter<String> adapterCat = new ArrayAdapter<String>(RegistroDatosConductorActivity.this, android.R.layout.simple_dropdown_item_1line, stringListCat);
                                spTipoDocumento.setAdapter(adapterCat);
                                spTipoDocumento.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                        //if (requestDocumentList.get(i).getTdoc_Id() != 0 || requestDocumentList.get(i).getTdoc_Nombre() != null) {
                                            iddocu = requestDocumentList.get(i).getTdoc_Id();
                                            nombredocu = requestDocumentList.get(i).getTdoc_Nombre();
                                        //} else {
                                         //   iddocu = 0;
                                         //   nombredocu = null;
                                       // }

                                    }
                                });
                            }

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }
            @Override
            public void onFailure(Call<ResponseDocumentBody> call, Throwable e) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    private void ValidarCampos() {

        /*else if(etApellidoMaterno.getText().toString().isEmpty()){
            Toast.makeText(RegistroDatosConductorActivity.this, "Ingrese su apellido materno para poder registrarse", Toast.LENGTH_LONG).show();

        }*/

        if(etNombre.getText().toString().isEmpty()){
            Toast.makeText(RegistroDatosConductorActivity.this, "Ingrese sus nombres para poder registrarse", Toast.LENGTH_LONG).show();

        }else if(etApellidoPaterno.getText().toString().isEmpty()){
            Toast.makeText(RegistroDatosConductorActivity.this, "Ingrese su apellido paterno para poder registrarse", Toast.LENGTH_LONG).show();

        } else if(iddocu == 0){
            Toast.makeText(RegistroDatosConductorActivity.this, "Debe seleccionar el tipo de documento poder registrarse", Toast.LENGTH_LONG).show();

        } else if(etNumeroDocumento.getText().toString().isEmpty()){
            Toast.makeText(RegistroDatosConductorActivity.this, "Ingrese su documento para poder registrarse", Toast.LENGTH_LONG).show();

        } else if(etNumeroDocumento.getText().toString().isEmpty()){
            Toast.makeText(RegistroDatosConductorActivity.this, "Ingrese su correo para poder registrarse", Toast.LENGTH_LONG).show();
        }else{
            final String compruebaemail = etCorreo.getEditableText().toString().trim();
            final String regex = "(?:[^<>()\\[\\].,;:\\s@\"]+(?:\\.[^<>()\\[\\].,;:\\s@\"]+)*|\"[^\\n\"]+\")@(?:[^<>()\\[\\].,;:\\s@\"]+\\.)+[^<>()\\[\\]\\.,;:\\s@\"]{2,63}";
            if (!compruebaemail.matches(regex)) {
                Toast.makeText(RegistroDatosConductorActivity.this, "Por favor, introduce un correo válido", Toast.LENGTH_LONG).show();
            }else{
                if (prefs.getBoolean(Constants.ACEPTA_TERMINOS_CONDICIONES_GENERALES,false)) {
                    enviarRegistro();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.terms_requeridos), Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {
        String deviceId;
        deviceId = Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return deviceId;
    }

    public void enviarRegistro(){
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();

        DriverInfo request = new DriverInfo();
        request.setCond_Id(id);
        request.setCond_Nombres(etNombre.getText().toString());
        request.setCond_ApellidoPaterno(etApellidoPaterno.getText().toString());
        request.setCond_ApellidoMaterno(etApellidoMaterno.getText().toString());
        request.setCond_NroTelefono(num_telefono);
        request.setTdoc_Id(iddocu);
        request.setPais_Id(Integer.parseInt(pais_Id));
        request.setCond_NroDocumento(etNumeroDocumento.getText().toString());
        request.setCond_Correo(etCorreo.getText().toString());
        request.setCond_TokenFirebase(prefs.getString(Constants.token,""));
        System.out.println("token_fb: " + prefs.getString(Constants.token,""));
        request.setCond_IdDispositivo(dv_id);
        System.out.println("getDeviceId: " + dv_id);

        System.out.println("id: " + id + " etNombre: " + etNombre.getText().toString() + " etApellidoPaterno: " + etApellidoPaterno.getText().toString() + " etApellidoMaterno: " + etApellidoMaterno.getText().toString() + " num_telefono: " + num_telefono + " iddocu: " + iddocu + " etNumeroDocumento: " + etNumeroDocumento.getText().toString() +" pais_Id: " + pais_Id + " etCorreo: " + etCorreo.getText().toString());
        Call<ResponseBody> callValidarCodigoVerificacion = conductorService.postRegistrarDatosConductor(request);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            int valor = (int)(double)response.body().getValor();
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putInt(Constants.primary_id, valor);
                            editor.apply();
                            System.out.println("Valor:" + valor);
                            Intent intent = new Intent(RegistroDatosConductorActivity.this, RegistroDatosVehiculoActivity.class);
                            intent.putExtra("cond_id ", valor);
                            startActivity(intent);

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

}