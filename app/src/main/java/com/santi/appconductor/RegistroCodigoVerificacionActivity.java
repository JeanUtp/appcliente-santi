package com.santi.appconductor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.santi.appconductor.R;
import com.santi.appconductor.models.RequestDriverValidateCode;
import com.santi.appconductor.models.RequestUpdateFotPerfil;
import com.santi.appconductor.models.RequestUpdateNumber;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseCodigoBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroCodigoVerificacionActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    EditText etCodigo1,etCodigo2,etCodigo3,etCodigo4;
    TextView tvIndicacion, tvContador, tvReenviar;
    Button btnValidar;
    Bundle bundle;
    SharedPreferences prefs;
    String token_fb,pais_Id,num_telefono, cond_id,dv_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_codigo_verificacion);
        setupUI(getWindow().getDecorView().getRootView());
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        bundle = getIntent().getExtras();
        dv_id = getDeviceId(this);
        token_fb = getIntent().getStringExtra("token_fb");
        pais_Id = getIntent().getStringExtra("pais_Id");
        num_telefono = getIntent().getStringExtra("num_telefono");
        System.out.println("pais_Id: " + pais_Id);
        etCodigo1 = findViewById(R.id.etCodigo1);
        etCodigo2 = findViewById(R.id.etCodigo2);
        etCodigo3 = findViewById(R.id.etCodigo3);
        etCodigo4 = findViewById(R.id.etCodigo4);
        tvIndicacion = findViewById(R.id.tvIndicacion);
        tvContador = findViewById(R.id.tvContador);
        tvReenviar = findViewById(R.id.tvReenviar);

        btnValidar = findViewById(R.id.btnValidar);
        btnValidar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyb();
                if (etCodigo1.getText().toString().equals("") && etCodigo2.getText().toString().equals("") && etCodigo3.getText().toString().equals("") & etCodigo4.getText().toString().equals("")) {
                    displayMessageSingle(getString(R.string.code_required));
                } else {
                    validarCodigoVerificacion();
                }
            }
        });

        tvIndicacion.setText(String.format(getString(R.string.enter_code), bundle.getString("phone_number")));
        DisplayProgressDialog(getString(R.string.validating));
        tvReenviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarNumero();
            }
        });
        runCounter();

        etCodigo1.requestFocus();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        etCodigo1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etCodigo2.requestFocus();
                if(etCodigo1.getText().toString().isEmpty()){
                    etCodigo1.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
        etCodigo2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etCodigo3.requestFocus();
                if(etCodigo2.getText().toString().isEmpty()){
                    etCodigo1.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etCodigo3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etCodigo4.requestFocus();
                if(etCodigo3.getText().toString().isEmpty()) {
                    etCodigo2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        etCodigo4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(etCodigo4.getText().toString().isEmpty()){
                    etCodigo3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (etCodigo1.getText().toString().equals("") && etCodigo2.getText().toString().equals("") && etCodigo3.getText().toString().equals("") & etCodigo4.getText().toString().equals("")) {
            //Toast.makeText(RegistroCodigoVerificacionActivity.this, getString(R.string.code_required), Toast.LENGTH_SHORT).show();
        } else {
            validarCodigoVerificacion();
        }
    }

    public void validarCodigoVerificacion() {
        pDialog.show();
        final String celular = bundle.getString("phone_number");
        String codigo = etCodigo1.getText().toString()+etCodigo2.getText().toString()+etCodigo3.getText().toString()+etCodigo4.getText().toString();

        RequestDriverValidateCode request = new RequestDriverValidateCode();
        request.setNroTelefono(celular);
        System.out.println("celular: " + celular);
        request.setCodigoVerificacion(codigo);
        System.out.println("codigo: " + codigo);
        request.setTokenFirebase(prefs.getString(Constants.token,""));
        System.out.println("token_fb: " + prefs.getString(Constants.token,""));
        request.setIdDispositivo(dv_id);
        System.out.println("getDeviceId: " + dv_id);
        request.setPais_Id(prefs.getInt(Constants.pais_id,0));
        System.out.println("pais_id: " + prefs.getInt(Constants.pais_id,0));

        Call<ResponseCodigoBody> callValidarCodigoVerificacion = conductorService.postValidarCodigoVerificacion(request);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseCodigoBody>() {
            @Override
            public void onResponse(Call<ResponseCodigoBody> call, Response<ResponseCodigoBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                System.out.println("Response: "+response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString(Constants.num_CEL, celular);
                                editor.apply();
                                cond_id = String.valueOf(response.body().getValor().getCond_Id());
                                System.out.println("valor:" + cond_id);
                                int etapa = response.body().getValor().getEtapa().getEtap_Id();
                                System.out.println("etapa:" + etapa);
                                System.out.println("estado:" + response.body().getValor().getEcon_Id());
                                switch(response.body().getValor().getEcon_Id()){
                                //Nuevo
                                case 0:
                                    switch(etapa){
                                            //Conductor nuevo
                                            case 0:
                                                Intent intent1 = new Intent(RegistroCodigoVerificacionActivity.this, RegistroDatosConductorActivity.class);
                                                intent1.putExtra("num_telefono", num_telefono);
                                                intent1.putExtra("pais_Id", pais_Id);
                                                startActivity(intent1);
                                                finish();
                                                break;
                                            //Datos Personales completos
                                            case 1:
                                                Intent intent2 = new Intent(RegistroCodigoVerificacionActivity.this, RegistroDatosVehiculoActivity.class);
                                                intent2.putExtra("num_telefono", num_telefono);
                                                intent2.putExtra("pais_Id", pais_Id);
                                                intent2.putExtra("cond_id", cond_id);
                                                System.out.println("cond_id" + cond_id);
                                                startActivity(intent2);
                                                finish();
                                                break;
                                            //Datos Vehiculo completos
                                            case 2:
                                                SharedPreferences.Editor editor1 = prefs.edit();
                                                editor1.putInt(Constants.primary_id, response.body().getValor().getCond_Id());
                                                editor1.apply();
                                                Intent intent3 = new Intent(RegistroCodigoVerificacionActivity.this, ListaDocumentosActivity.class);
                                                intent3.putExtra("num_telefono", num_telefono);
                                                intent3.putExtra("pais_Id", pais_Id);
                                                intent3.putExtra("cond_id", cond_id);
                                                System.out.println("cond_id" + cond_id);
                                                startActivity(intent3);
                                                finish();
                                                break;
                                            //Datos Adjuntos completos
                                            case 3:
                                                Intent intent5 = new Intent(RegistroCodigoVerificacionActivity.this, EsperaActivity.class);
                                                startActivity(intent5);
                                                finish();
                                                break;
                                        }
                                    break;

                                //Postulante
                                case 1:
                                    switch(etapa){
                                        //Conductor nuevo
                                        case 0:
                                            Intent intent1 = new Intent(RegistroCodigoVerificacionActivity.this, RegistroDatosConductorActivity.class);
                                            intent1.putExtra("num_telefono", num_telefono);
                                            intent1.putExtra("pais_Id", pais_Id);
                                            startActivity(intent1);
                                            finish();
                                            break;
                                        //Datos Personales completos
                                        case 1:
                                            Intent intent2 = new Intent(RegistroCodigoVerificacionActivity.this, RegistroDatosVehiculoActivity.class);
                                            intent2.putExtra("num_telefono", num_telefono);
                                            intent2.putExtra("pais_Id", pais_Id);
                                            intent2.putExtra("cond_id", cond_id);
                                            System.out.println("cond_id" + cond_id);
                                            startActivity(intent2);
                                            finish();
                                            break;
                                        //Datos Vehiculo completos
                                        case 2:
                                            SharedPreferences.Editor editor1 = prefs.edit();
                                            editor1.putInt(Constants.primary_id, response.body().getValor().getCond_Id());
                                            editor1.apply();
                                            Intent intent3 = new Intent(RegistroCodigoVerificacionActivity.this, ListaDocumentosActivity.class);
                                            intent3.putExtra("num_telefono", num_telefono);
                                            intent3.putExtra("pais_Id", pais_Id);
                                            intent3.putExtra("cond_id", cond_id);
                                            System.out.println("cond_id" + cond_id);
                                            startActivity(intent3);
                                            finish();
                                            break;
                                        //Datos Adjuntos completos
                                        case 3:
                                            Intent intent5 = new Intent(RegistroCodigoVerificacionActivity.this, EsperaActivity.class);
                                            startActivity(intent5);
                                            finish();
                                            break;
                                    }
                                    break;
                                //Observado
                                case 2:
                                    SharedPreferences.Editor editor2 = prefs.edit();
                                    editor2.putInt(Constants.primary_id, response.body().getValor().getCond_Id());
                                    editor2.apply();
                                    Intent intent2 = new Intent(RegistroCodigoVerificacionActivity.this, ListaDocumentosActivity.class);
                                    intent2.putExtra("token_fb", prefs.getString(Constants.token,""));
                                    intent2.putExtra("cond_id ", response.body().getValor().getCond_Id());
                                    startActivity(intent2);
                                    finish();
                                    break;
                                //Activo
                                case 3:
                                    SharedPreferences.Editor editor3 = prefs.edit();
                                    editor3.putInt(Constants.primary_id, response.body().getValor().getCond_Id());
                                    editor3.apply();
                                    Intent intent3 = new Intent(RegistroCodigoVerificacionActivity.this, MenuActivity.class);
                                    intent3.putExtra("token_fb", prefs.getString(Constants.token,""));
                                    startActivity(intent3);
                                    finish();
                                    break;
                                //Rechazado
                                case 4:
                                    SharedPreferences.Editor editor4 = prefs.edit();
                                    editor4.putInt(Constants.primary_id, response.body().getValor().getCond_Id());
                                    Intent intent4 = new Intent(RegistroCodigoVerificacionActivity.this, RechazadoActivity.class);
                                    startActivity(intent4);
                                    finish();
                                    break;
                                //Subsanado
                                case 5:
                                    SharedPreferences.Editor editor5 = prefs.edit();
                                    editor5.putInt(Constants.primary_id, response.body().getValor().getCond_Id());
                                    Intent intent5 = new Intent(RegistroCodigoVerificacionActivity.this, EsperaActivity.class);
                                    startActivity(intent5);
                                    finish();
                                    break;
                                //Baja
                                case 6:
                                    SharedPreferences.Editor editor6 = prefs.edit();
                                    editor6.putInt(Constants.primary_id, response.body().getValor().getCond_Id());
                                    Intent intent6 = new Intent(RegistroCodigoVerificacionActivity.this, BajaActivity.class);
                                    startActivity(intent6);
                                    finish();
                                    break;
                                    //Deuda
                                case 7:
                                    SharedPreferences.Editor editor7 = prefs.edit();
                                    editor7.putInt(Constants.primary_id, response.body().getValor().getCond_Id());
                                    editor7.putString(Constants.estado_con, "7");
                                    editor7.apply();
                                    Intent intent7 = new Intent(RegistroCodigoVerificacionActivity.this, MenuActivity.class);
                                    intent7.putExtra("token_fb", prefs.getString(Constants.token,""));
                                    startActivity(intent7);
                                    finish();
                                    break;
                            }


                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseCodigoBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    public void runCounter() {
        tvContador.setVisibility(View.VISIBLE);
        CountDownTimer countDownTimer = new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                String cifra = String.format(Locale.getDefault(), "00:%d", millisUntilFinished / 1000L);
                if (cifra.length() < 5) {
                    cifra = "00:0" + cifra.substring(3);
                }
                tvContador.setText(String.format(getString(R.string.resend_countdown), cifra));
            }

            public void onFinish() {
                tvReenviar.setVisibility(View.VISIBLE);
                tvContador.setVisibility(View.GONE);
            }
        }.start();
    }

    public void validarNumero() {
        pDialog.show();
        Call<ResponseBody> callValidarNumeroCelular = conductorService.getValidaTelefonoConductor(bundle.getString("phone_number"),bundle.getString("pais_Id"));
        callValidarNumeroCelular.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            Toast.makeText(RegistroCodigoVerificacionActivity.this, "Código reenviado por SMS.", Toast.LENGTH_SHORT).show();
                            //displayMessageSingle(getString(R.string.code_sent));
                            tvReenviar.setVisibility(View.GONE);
                            runCounter();
                        } else {
                            displayMessageSingle(getString(R.string.resend_error));
                        }
                    } else {
                        displayMessageSingle(getString(R.string.resend_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.resend_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.resend_error));
            }
        });
    }

    @SuppressLint("HardwareIds")
    public static String getDeviceId(Context context) {
        String deviceId;
        deviceId = Settings.Secure.getString(
                context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        return deviceId;
    }
}