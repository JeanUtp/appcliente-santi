package com.santi.appconductor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.health.SystemHealthManager;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.santi.appconductor.models.RequestAbono;
import com.santi.appconductor.models.RequestAddAdjunto;
import com.santi.appconductor.models.RequestAdjunto;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.ResponseResumenTotalBody;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;
import com.santi.appconductor.utils.Constants;
import com.santi.appconductor.utils.PermisionChecker;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.santi.appconductor.utils.Constants.CAMARA;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;
import static com.santi.appconductor.utils.Constants.MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE;
import static com.santi.appconductor.utils.Constants.compressImage;

public class AbonoActivity extends BaseActivity implements View.OnClickListener{
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    TextView tvTituloAbono, tvDescripcionAbono;
    EditText etAbono;
    Button btnEnviarFoto;
    LinearLayout mensajeerror;
    TextView tool_titulo_atras;
    //variables imagenes;
    private int num_img;
    private RelativeLayout rltimg1, rltimg2;
    boolean imgProcedencia1, imgProcedencia2;
    LinearLayout linearCameraPrincipal, linearCuadroFotos, btnCameraPrincipal;
    ImageView imgF1, imgF2, btnAddF1, btnAddF2,iv_Close;
    RelativeLayout rlDeleteF1, rlDeleteF2;
    String pathImg1, pathImg2,cond_id;
    private static final String IMAGE_DIRECTORY = "/santiappconductor";
    private int GALLERY = 1, CAMERA = 2;
    Context context;
    SharedPreferences prefs;
    double monto;

    static final int REQUEST_TAKE_PHOTO = 2;
    private String tmpRuta = "";
    Bitmap imagen;
    Uri photoURI;
    String currentPhotoPath;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abono);

        iv_Close = findViewById(R.id.iv_Atras);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });
        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Abono");
        tvTituloAbono = findViewById(R.id.tvTituloAbono);
        tvDescripcionAbono = findViewById(R.id.tvDescripcionAbono);
        etAbono = findViewById(R.id.etAbono);
        mensajeerror = findViewById(R.id.mensajeerror);
        btnEnviarFoto = findViewById(R.id.btnEnviarFoto);
        prefs = getApplication().getSharedPreferences(Constants.share_tag, Context.MODE_PRIVATE);
        context = AbonoActivity.this;
        linearCameraPrincipal = findViewById(R.id.ll_input_foto);
        linearCuadroFotos = findViewById(R.id.ll_fotos);
        imgF1 = findViewById(R.id.img_f1);
        imgF2 = findViewById(R.id.img_f2);
        rlDeleteF1 = findViewById(R.id.rl_delete_f1);
        rlDeleteF2 = findViewById(R.id.rl_delete_f2);
        btnAddF1 = findViewById(R.id.btn_add_f1);
        btnAddF2 = findViewById(R.id.btn_add_f2);
        btnCameraPrincipal = findViewById(R.id.ll_btn_camera);
        rltimg1 = findViewById(R.id.relativeimg1);
        rltimg2 = findViewById(R.id.relativeimg2);
        pathImg1="";
        pathImg2="";
        btnEnviarFoto.setOnClickListener(this);
        btnCameraPrincipal.setOnClickListener(this);
        linearCameraPrincipal.setOnClickListener(this);
        linearCuadroFotos.setOnClickListener(this);
        btnAddF1.setOnClickListener(this);
        btnAddF2.setOnClickListener(this);
        rlDeleteF1.setOnClickListener(this);
        rlDeleteF2.setOnClickListener(this);
        switch (1){
            case 1:
                rltimg1.setVisibility(View.VISIBLE);
                break;
            case 2:
                rltimg1.setVisibility(View.VISIBLE);
                rltimg2.setVisibility(View.VISIBLE);
                break;
        }
        obtenerTotal();
    }

    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.btnEnviarFoto:
                    validarMonto();
                    break;
                case R.id.ll_btn_camera:
                    imgProcedencia1 = true;
                    validarPermiso();
                    break;
                case R.id.ll_input_foto:
                    imgProcedencia1 = true;
                    validarPermiso();
                    break;
                case R.id.ll_fotos:
                    imgProcedencia1 = true;
                    validarPermiso();
                    break;
                case R.id.btn_add_f1:
                    imgProcedencia1 = true;
                    validarPermiso();
                    break;
                case R.id.btn_add_f2:
                    imgProcedencia2 = true;
                    validarPermiso();
                    break;
                case R.id.rl_delete_f1:
                    imgProcedencia1 = true;
                    deleteFoto();
                    break;
                case R.id.rl_delete_f2:
                    imgProcedencia2 = true;
                    deleteFoto();
                    break;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void obtenerTotal(){
        System.out.println("Aqui ire" + prefs.getInt(Constants.primary_id,0));
        Call<ResponseResumenTotalBody> callServiciosResumen = conductorService.getObtenerTotal(prefs.getInt(Constants.primary_id,0));
        callServiciosResumen.enqueue(new Callback<ResponseResumenTotalBody>() {
            @Override
            public void onResponse(Call<ResponseResumenTotalBody> call, Response<ResponseResumenTotalBody> response) {
                Log.i("mensajess","responseResumen: "+response);
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            monto = response.body().getValor().getTotal();
                            System.out.println("Monto: " + monto);
                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseResumenTotalBody> call, Throwable t) {
                System.out.println("error");
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }
    // ---------------------------------------------------------- inicio camara --------------------------------

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Aqui 20mil");
                // selectLocationOnMap();
            }
        }
        switch (requestCode){
            case CAMARA:{
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    if (PermisionChecker.isRaedStorageExternalEnable(context)){
                        //Intent i = new Intent(getApplicationContext(),CameraActivity.class);
                        //startActivity(i);
                        // finish();
                    }else{
                        ActivityCompat.requestPermissions(AbonoActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                }
                break;
            }
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //Intent i = new Intent(getApplicationContext(),CameraActivity.class);
                    //  startActivity(i);
                }
                break;
        }
    }

    //check for location permission
    public static boolean hasPermissionInManifest(Activity activity, int requestCode, String permissionName) {
        if (ContextCompat.checkSelfPermission(activity,
                permissionName)
                != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(activity,
                    new String[]{permissionName},
                    requestCode);
        } else {
            return true;
        }
        return false;
    }

    private void deleteFoto() {
        if(imgProcedencia1){
            eliminarFotoDevice(pathImg1);
            pathImg1="";
            btnAddF1.setVisibility(View.VISIBLE);
            rlDeleteF1.setVisibility(View.GONE);
            imgF1.setVisibility(View.GONE);
            imgProcedencia1=false;
        }
        if(imgProcedencia2){
            pathImg2="";
            btnAddF2.setVisibility(View.VISIBLE);
            rlDeleteF2.setVisibility(View.GONE);
            imgF2.setVisibility(View.GONE);
            imgProcedencia2=false;
        }
        if(pathImg1.isEmpty() && pathImg2.isEmpty()){
            linearCameraPrincipal.setVisibility(View.VISIBLE);
            linearCuadroFotos.setVisibility(View.GONE);
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String path ;

        if (resultCode == InformacionDocumentosActivity.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {

                Uri contentURI = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), contentURI);
                    System.out.println("onActivityResult" + bitmap.getByteCount());
                    path= saveImage(bitmap);
                    System.out.println("path: " + path);

                    if(imgProcedencia1){
                        pathImg1=path;
                        cargarFoto(bitmap);
                    }else{
                        pathImg2=path;
                        cargarFoto(bitmap);
                    }

                } catch (IOException e) {
                    System.out.println("Error en onActivityResult: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        } else if (requestCode == REQUEST_TAKE_PHOTO) {
            /*Bitmap bitmap2 = (Bitmap) data.getExtras().get("data");
            int width = bitmap2.getWidth();
            int heightt = bitmap2.getHeight();
            System.out.println("tamaños: " + width + " - "+heightt);

            *//*widht = thumbnail.getWidth();
            height = thumbnail.getHeight();
            Bitmap resized;
            if(height<widht){
                resized = Bitmap.createScaledBitmap(thumbnail, 480, 320, true);
                resized = rotateImage(resized,0);
            }else {
                resized = Bitmap.createScaledBitmap(thumbnail, 320, 480, true);
            }*//*
            path= saveImage(bitmap2);
            System.out.println("path2: " + path);
            if(imgProcedencia1){
                pathImg1=path;
                cargarFoto(bitmap2);
            }else{
                pathImg2=path;
                cargarFoto(bitmap2);
            }*/

            try {
                imagen = getBitmapFromUri (photoURI);
            } catch (IOException e) {
                e.printStackTrace();
            }
            path = tmpRuta;
            if(imgProcedencia1){
                System.out.println("aqui1: " + path);
                pathImg1=path;
                cargarFoto(imagen);
            }else{
                System.out.println("aqui2: "+path);
                pathImg2=path;
                cargarFoto(imagen);
            }
        }


    }

    private void dispatchTakePictureIntent() {
        System.out.println("11");
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                System.out.println("12");
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                System.out.println("13");
                photoURI = FileProvider.getUriForFile(AbonoActivity.this, getApplicationContext().getPackageName() + ".provider", photoFile);
                //Uri photoURI = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                tmpRuta = photoFile.getPath();
                System.out.println("outputUri2: " + photoURI);
                System.out.println("tmpRuta2: " + photoFile.getPath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        System.out.println("14");
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY);
        storageDir.mkdirs(); // make sure you call mkdirs() and not mkdir()
        File image = File.createTempFile(imageFileName,".jpg",storageDir);

        tmpRuta = image.getPath();
        currentPhotoPath = "file:" + image.getAbsolutePath();
        System.out.println("our file"+ image.toString());
        return image;
    }

    public Bitmap getBitmapFromUri ( Uri uri ) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver (). openFileDescriptor ( uri , "r" );
        FileDescriptor fileDescriptor = parcelFileDescriptor . getFileDescriptor ();
        Bitmap image = BitmapFactory. decodeFileDescriptor ( fileDescriptor );
        parcelFileDescriptor . close ();
        return image ;
    }

    private void eliminarFotoDevice(String path) {
        File file = new File(path);
        boolean deleted = file.delete();
        if(deleted){
            callBroadCast();
            //Toast.makeText(this,"Imagen eliminado",Toast.LENGTH_LONG).show();
        }else{
            //Toast.makeText(this,"No se pudo eliminar",Toast.LENGTH_LONG).show();
        }
    }

    public void callBroadCast() {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(context, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        } else {
            Log.e("-->", " < 14");
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    private void validarPermiso() {
        if (PermisionChecker.isCameraEnable(context)) {
            if (PermisionChecker.isWriteStorageExternalEnable(context)){
                if(PermisionChecker.isRaedStorageExternalEnable(context)){
                    System.out.println("Aqui 1");
                    showOptionAddImage();
                }else{
                    ActivityCompat.requestPermissions(AbonoActivity.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    System.out.println("Aqui 2");
                }
            }else{
                ActivityCompat.requestPermissions(AbonoActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
                System.out.println("Aqui 3");
            }
        } else {
            ActivityCompat.requestPermissions(AbonoActivity.this, new String[]{Manifest.permission.CAMERA}, CAMARA);
            System.out.println("Aqui 4");
            showOptionAddImage();
        }

    }

    private void showOptionAddImage() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(context);
        pictureDialog.setTitle( getString(R.string.title_opciones_imagen));
        String[] pictureDialogItems = {
                getString(R.string.texto_from_gallery),
                getString(R.string.texto_from_camera)};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();

    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        dispatchTakePictureIntent();
        //Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        //startActivityForResult(intent, CAMERA);
    }

    public String saveImage(Bitmap myBitmap) {
        String path="";
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File wallpaperDirectory = new File(
                getExternalFilesDir(Environment.getStorageDirectory().getAbsolutePath()).getAbsolutePath()+IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
       if (!wallpaperDirectory.exists()) {
            Log.i("wallpaperDirectory: ",wallpaperDirectory.getAbsolutePath());
            wallpaperDirectory.mkdir();
            if(wallpaperDirectory.exists())
            {
                Log.i("wallpaperDirectory: ",wallpaperDirectory.getAbsolutePath());
            }
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");

            Log.i("saveImage:",f.getAbsolutePath());
            f.createNewFile();
            Log.i("createNewFile:",f.getAbsolutePath());
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(context,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            path =  f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
            return path;
        }
        return path;
    }

    private void cargarFoto(Bitmap bitmap) {
        linearCameraPrincipal.setVisibility(View.GONE);
        linearCuadroFotos.setVisibility(View.VISIBLE);
        if(imgProcedencia1){
            if(pathImg2.isEmpty()){
                btnAddF2.setVisibility(View.VISIBLE);
                rlDeleteF2.setVisibility(View.GONE);
            }
            imgF1.setVisibility(View.VISIBLE);
            btnAddF1.setVisibility(View.GONE);
            rlDeleteF1.setVisibility(View.VISIBLE);
            imgF1.setImageBitmap(bitmap);
            imgProcedencia1=false;
        }
        if(imgProcedencia2){
            if(pathImg1.isEmpty()){
                btnAddF1.setVisibility(View.VISIBLE);
                rlDeleteF1.setVisibility(View.GONE);
            }
            imgF2.setVisibility(View.VISIBLE);
            btnAddF2.setVisibility(View.GONE);
            rlDeleteF2.setVisibility(View.VISIBLE);
            imgF2.setImageBitmap(bitmap);
            imgProcedencia2=false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            //finish(); // close this activity and return to preview activity (if there is any)

            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setCancelable(false);
            builder.setTitle("¿Deseas cancelar el registro de documento? (se perderán los datos ingresados)")
                    .setPositiveButton("Salir", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(AbonoActivity.this, MenuActivity.class);
                            intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            startActivity(intent);
                        }

                    })
                    .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    });
            builder.create();
            builder.show();

        }
        return super.onOptionsItemSelected(item);
    }

    public String CargarFoto (String path_foto){
        String foto_str="";
        String encodedString="";
        if(path_foto!=null ){
            try {
                File file = new File(path_foto);
                try (FileInputStream fis = new FileInputStream(file)) {
                    byte[] bytes = new byte[(int) file.length()];
                    fis.read(bytes);
                    encodedString = Base64.encodeToString(Constants.resizeImage(context,bytes,500,700), Base64.NO_WRAP);
                    //encodedString = Base64.encodeToString(Constants.resizeImage(context,bytes,2,2), Base64.NO_WRAP);
                } catch (IOException ex) {
                    System.out.println("Error al convertir la imagen. " + ex.getMessage() +  ex);
                }

                //foto_str = Base64.encodeToString(String.valueOf(bitmapFile),Base64.DEFAULT);
            }catch (Exception e){
                e.printStackTrace();
                // return  foto_str;
                return encodedString;
            }
            Log.e("foto",foto_str);
        }
        System.out.println("encodedString: " + encodedString);
        return encodedString;
    }
// ---------------------------------------------------------- fin camara  ----------------------------------

    public void validarMonto(){
        //System.out.println("edt: " + etAbono.getText().toString());
        double et = Double.parseDouble(etAbono.getText().toString().toString());
        //double mont = Integer.parseInt(monto);
         System.out.println("Monto:" + monto);
        if(et <= monto){
            System.out.println("Menor");
            mensajeerror.setVisibility(View.GONE);
            eviarDocumento();
        }else{
            mensajeerror.setVisibility(View.VISIBLE);
        }

    }

    public void eviarDocumento(){
        DisplayProgressDialog(getString(R.string.loading));
        pDialog.show();
        System.out.println("Aqui");
        if(pathImg1!=null){
            if(pathImg1.trim()!=""){
                System.out.println("pathImg1:" + pathImg1);
                pathImg1 = (compressImage(AbonoActivity.this,pathImg1));
                pathImg1 = (CargarFoto(pathImg1));
                System.out.println("pathImg1:" + pathImg1);
            }
        }

        if(pathImg2!=null) {
            if (pathImg2.trim() != "") {
                System.out.println("pathImg2:" + pathImg2);
                pathImg2 = (compressImage(AbonoActivity.this,pathImg2));
                pathImg2 = (CargarFoto(pathImg2));
                System.out.println("pathImg2:" + pathImg2);
            }
        }
        System.out.println("PaisId: " + prefs.getInt(Constants.pais_id,0) + " " + "CondId: " + prefs.getInt(Constants.primary_id,0));
        RequestAbono requestAbono = new RequestAbono();
        requestAbono.setPais_Id(prefs.getInt(Constants.pais_id,0));
        requestAbono.setCond_Id(prefs.getInt(Constants.primary_id,0));
        requestAbono.setAbon_Monto(Double.parseDouble(etAbono.getText().toString()));
        System.out.println("setAbon_NombreArchivo: " + pathImg1);
        requestAbono.setAbon_NombreArchivo(pathImg1);
        Call<ResponseBody> callValidarCodigoVerificacion = conductorService.postRegistrarAbono(requestAbono);
        callValidarCodigoVerificacion.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().isEstado()) {
                            //retorna atras
                            AlertDialog.Builder builder = new AlertDialog.Builder(AbonoActivity.this);
                            builder.setTitle("Registro Exitoso");
                            builder.setMessage("El registro del abono se envió con éxito");
                            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    onBackPressed();
                                    finish();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();

                        } else {
                            displayMessageSingle(response.body().getMensaje());
                        }
                    } else {
                        displayMessageSingle(getString(R.string.code_error));
                    }
                } else {
                    displayMessageSingle(getString(R.string.code_error));
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                displayMessageSingle(getString(R.string.code_error));
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}