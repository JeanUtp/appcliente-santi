package com.santi.appconductor;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.santi.appconductor.models.CalificacionClientePost;
import com.santi.appconductor.models.RequestAdjunto;
import com.santi.appconductor.models.ResponseBody;
import com.santi.appconductor.models.Travel;
import com.santi.appconductor.services.Apis;
import com.santi.appconductor.services.DriverService;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetalleViajeActivity extends BaseActivity {
    DriverService conductorService = Apis.getConfigurationApi().create(DriverService.class);
    ImageView iv_Atras;
    TextView tv_forma_pago, tool_titulo_atras, tv_fecha_hora, tv_lugar_recojo, tv_nombre_conductor, tv_destino, tv_tipo_moneda, tv_precio, tv_vehiculo, tv_placa, tv_tipo_servicio, tv_cancelado_conductor, tv_cancelado_cliente;
    CircleImageView iv_foto;
    LinearLayout ly_precio;
    RatingBar ratingBar;
    AlertDialog adDisplayService;
    private Travel travel = new Travel();
    boolean valor = false;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_viaje);
        travel = (Travel) getIntent().getSerializableExtra("travel");
        iv_Atras = findViewById(R.id.iv_Atras);
        iv_Atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //onBackPressed();
                //Intent intent = new Intent(DetalleViajeActivity.this, ViajesActivity.class);
                //startActivity(intent);
                finish();
            }
        });

        tool_titulo_atras = findViewById(R.id.tool_titulo_atras);
        tool_titulo_atras.setText("Detalle de viaje");

        tv_fecha_hora = findViewById(R.id.tv_fecha_hora);
        tv_lugar_recojo = findViewById(R.id.tv_lugar_recojo);
        tv_forma_pago = findViewById(R.id.tv_forma_pago);
        tv_destino = findViewById(R.id.tv_destino);
        tv_tipo_moneda = findViewById(R.id.tv_tipo_moneda);
        tv_precio = findViewById(R.id.tv_precio);
        tv_nombre_conductor = findViewById(R.id.tv_nombre_conductor);
        tv_vehiculo = findViewById(R.id.tv_vehiculo);
        tv_placa = findViewById(R.id.tv_placa);
        tv_tipo_servicio = findViewById(R.id.tv_tipo_servicio);
        iv_foto = findViewById(R.id.iv_foto);
        tv_cancelado_conductor = findViewById(R.id.tv_cancelado_conductor);
        tv_cancelado_cliente = findViewById(R.id.tv_cancelado_cliente);
        ly_precio = findViewById(R.id.ly_precio);
        ratingBar = findViewById(R.id.ratingBar);
        String cal = String.valueOf(travel.getCalificacion());
        System.out.println("calificacion: " + cal);
        if(cal.equals("0.0")){
            System.out.println("00");
            ratingBar.setEnabled(true);
            valor = true;
            ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                    if(valor==true){
                        calificarCliente(ratingBar.getRating());
                        System.out.println("ff" + ratingBar.getRating());
                    }else{
                        Toast.makeText(DetalleViajeActivity.this, "El cliente ya tiene valoracion", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }else{
            System.out.println("else");
            valor = false;
            ratingBar.setEnabled(false);
        }

        switch (travel.getAnuladoPor()){
            //Concluido
            case 0:
                tv_cancelado_conductor.setVisibility(View.GONE);
                tv_cancelado_cliente.setVisibility(View.GONE);
                ly_precio.setVisibility(View.VISIBLE);
                break;
            //Cancelado Conductor
            case 1:
                tv_cancelado_conductor.setVisibility(View.VISIBLE);
                tv_cancelado_cliente.setVisibility(View.GONE);
                ly_precio.setVisibility(View.GONE);
                break;
            //Cancelado Cliente
            case 2:
                tv_cancelado_conductor.setVisibility(View.GONE);
                tv_cancelado_cliente.setVisibility(View.VISIBLE);
                ly_precio.setVisibility(View.GONE);
                break;
        }
        tv_fecha_hora.setText(travel.getSoli_FechaRegistro() + " " + travel.getSoli_HoraRegistro());
        tv_lugar_recojo.setText(travel.getSoli_OrigenDireccion());
        tv_destino.setText(travel.getSoli_DestinoDireccion());
        tv_tipo_moneda.setText(travel.getMone_Simbolo());
        tv_forma_pago.setText(travel.getFpag_Nombre());
        //mantener decimales de double al pasarlo a String
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.US);
        otherSymbols.setDecimalSeparator('.');
        DecimalFormat mf = new DecimalFormat("0.00",otherSymbols);
        Double d = travel.getServ_Precio();   // 0.85
        mf.setMinimumFractionDigits(2);
        String s = mf.format(d);
        tv_precio.setText(s);
        System.out.println("Viaje" + travel.getServ_Precio());
        tv_nombre_conductor.setText(travel.getNombreCompleto());
        Glide.with(iv_foto)
                .load(travel.getFoto())
                .apply(RequestOptions.skipMemoryCacheOf(true))
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE))
                .fitCenter()
                .into(iv_foto);
        tv_tipo_servicio.setText(travel.getTser_Nombre());
        tv_vehiculo.setText(travel.getMode_Nombre());
        tv_placa.setText(travel.getVehi_NroPlaca());
        System.out.println("calificacion: " + travel.getCalificacion());
        ratingBar.setRating(travel.getCalificacion());

    }

    private void calificarCliente(float calificacion) {
        System.out.println("aqui: " + "Clie_Id: " +travel.getClie_Id() + " Serv_Id: " + travel.getServ_Id() + " calificacion: " + calificacion);
        System.out.println("Calificando");
        DisplayProgressDialog(getString(R.string.qualifying));
        pDialog.show();
        CalificacionClientePost calificacionClientePost = new CalificacionClientePost();
        calificacionClientePost.setClie_Id(travel.getClie_Id());
        calificacionClientePost.setServ_Id(travel.getServ_Id());
        calificacionClientePost.setServ_CalificacionCliente(calificacion);
        calificacionClientePost.setServ_CalificacionComentario("");
        Call<ResponseBody> callService = conductorService.putCalificarCliente(calificacionClientePost);
        callService.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.i("mensaje","responseResumen: "+response);
                if (response.isSuccessful()) {
                            System.out.println("mensaje: " + response.body().getMensaje());
                            System.out.println("Estado: " + response.body().isEstado());
                            System.out.println("Valor: " + response.body().getValor());

                            AlertDialog.Builder builder = new AlertDialog.Builder(DetalleViajeActivity.this);
                            builder.setTitle("Registro Exitoso");
                            builder.setMessage("La valoración se envió con éxito");
                            builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                @SuppressLint("ResourceAsColor")
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ratingBar.setEnabled(false);
                                    valor = false;
                                    //onBackPressed();
                                    //finish();
                                }
                            });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                } else {
                    Log.d("Calificar usuario", "No se pudo enviar la calificación");
                    System.out.println("error");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                }
                Log.d("Calificar usuario", "No se pudo enviar la calificación");
            }
        });
    }
}